<?php
/* 
Title: WP Framework
Description: Wordpress & PHP framework designed for aiding development
Author Name: Razvan Rotaru
Company Name: Digital Space SRL
Author Email: razvan@digital-space.eu
Author Website: https://www.digital-space.eu/
*/
define("FRAMEWORK_DIR",dirname(__FILE__));

// For WP only
if( defined( 'ABSPATH' ) ){
	define( 'DATE_FORMAT', get_option( 'date_format' ) );
	define( 'DATE_TIME_FORMAT', DATE_FORMAT . ' ' . get_option( 'time_format' ) );
	define( 'DATE', date( DATE_FORMAT, current_time( 'timestamp' ) ) );
	define( 'DATE_TIME', date( DATE_TIME_FORMAT, current_time( 'timestamp' ) ) );
	define( 'NOW', current_time( 'timestamp' ) );
	define( 'NOW_GMT', current_time( 'timestamp', true ) );
	define( 'SITE_NAME', get_bloginfo( 'name' ) );

	if( !defined( 'SITE_URL' ) ){
		define( 'SITE_URL', get_bloginfo( 'url' ) );
	}
} else {
	define( 'NOW', strtotime( 'now' ) );
	define( 'NOW_GMT', strtotime( 'now' ) );
	define( 'DATE_FORMAT', 'd/m/Y' );
	define( 'DATE_TIME_FORMAT', DATE_FORMAT . ' H:i:s' );
	define( 'DATE', date( DATE_FORMAT ) );
	define( 'DATE_TIME', date( DATE_TIME_FORMAT ) );
	define( 'ARRAY_A', 'array_associative' );
}

define( 'DB_DATE_FORMAT', 'Y-m-d' );
define( 'DB_DATE_TIME_FORMAT', DB_DATE_FORMAT . ' H:i:s' );
define( 'DB_DATE', date( DB_DATE_FORMAT, NOW_GMT ) );
define( 'DB_DATE_TIME', date( DB_DATE_TIME_FORMAT ) );
define( 'TIME_MIDNIGHT', '00:00:00' );
define( 'IS_WP', defined( 'ABSPATH' ) );

include_once(FRAMEWORK_DIR."/functions/php/helpers.php");

class FRAMEWORK{
	var $settings;
	var $static_resources;
	var $users;
	var $smtp;

	function __construct(){
	}

	function settings($settings=array()){
		//SET VARIABLES
		$this->settings = [];
		$this->settings["platform"] = defined( 'ABSPATH' ) ? 'wp' : 'php';
		$this->settings["is_clean_installation"] = false;
		$this->settings["enable_js"]=array();
		$this->settings["enable_resources"]=array();

		// Don't start sessions on cronjobs
		// This is because in wp-cron.php it sets the headers at line 24
		$this->settings["session"] = !defined( 'DOING_CRON' ) || !DOING_CRON;

		if( isset( $settings['debug'] ) ){
			fr_enable_debug( fr_make_array( $settings['debug'] ) );
		}

		if($this->settings["platform"]=="wp"){
			
			define("THEME_DIR",get_stylesheet_directory());
			define("THEME_URL",get_stylesheet_directory_uri());
			define("MAIN_THEME_DIR",get_template_directory());
			if( !defined( 'FRAMEWORK_URL' ) ){
				define("FRAMEWORK_URL",THEME_URL."/framework");
			}

			$theme_data = get_file_data(THEME_DIR."/style.css",array("parent_theme"=>"Template"),1);
			$this->settings["parent_theme"] = $theme_data["parent_theme"];

			$this->static_resources = array(
				"scripts" => array(
					"register" => array(),
					"enqueue" => array(),
					),
				"styles" => array(
					"register" => array(),
					"enqueue" => array(),
					)
				);
			$this->settings["include_theme_includes"] = true;
			$this->settings["enable_js"]=array("script.js");
			$this->settings["enable_resources"]=array();

			if(empty($settings["is_clean_installation"])){
				$this->settings["enable_js"]=array();
				$this->settings["include_theme_includes"] = false;
			}

		}
		
		$this->settings = array_merge($this->settings, $settings);


		if( $this->settings["session"] && session_status() == PHP_SESSION_NONE ) {
			if( headers_sent( $file, $line ) ){
				error_log( json_encode( [
					'Can\'t start session because headers already sent',
					'file' => $file,
					'line' => $line,
					'url' => "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
				], JSON_PRETTY_PRINT ) );
			}
			session_start();
		}


		if($this->settings["platform"]=="php"){
			
			ini_set('display_errors', 1);
			ini_set('display_startup_errors', 1);
			error_reporting(E_ALL);

			if( !defined( 'FRAMEWORK_URL' ) ){
				if( !empty( $this->settings["framework_url"] ) ){
					define("FRAMEWORK_URL",$this->settings["framework_url"]);
				}else{
					die( 'FRAMEWORK URL is required.' );
				}
			}
			
			fr_include_files(FRAMEWORK_DIR."/functions/php");
		
		}else if($this->settings["platform"]=="wp"){
			
			
			fr_include_files(FRAMEWORK_DIR."/functions");
			fr_register_shortcodes();

			//THEME SUPPORT
			add_theme_support( 'title-tag' );
			add_theme_support( 'post-thumbnails' );
			add_theme_support( "menus" );

			//ACTIONS
			add_action('wp_enqueue_scripts', array($this,'load_static_resources'));

			//FILTERS
			add_filter('clean_url',array($this,'add_async_forscript'), 999, 1);
			add_filter('wp_nav_menu_items', 'do_shortcode');//Display shortcodes in menu items

			if($this->settings["is_clean_installation"]){
				$this->init_clean_installation();
			}

			if(!empty($settings["smtp"])){
				$this->smtp=new FR_SMTP($settings["smtp"]);
			}

			$this->enqueue_framework_static_resources();

		}

	}



	function init_clean_installation(){
		$dirs=array();
		$dirs[] = THEME_DIR."/shortcodes";
		$dirs[] = THEME_DIR."/includes";
		$dirs[] = THEME_DIR."/css";
		$dirs[] = THEME_DIR."/js";
		$dirs[] = THEME_DIR."/resources";
		$dirs[] = THEME_DIR."/images";

		if($this->settings["enable_ajax"]){
			$dirs[] = THEME_DIR."/ajax";
		}
		fr_required_dirs($dirs);

		if($this->settings["parent_theme"]){
			if(!file_exists(THEME_DIR."/screenshot.png") && file_exists(MAIN_THEME_DIR."/screenshot.png")){
				copy(MAIN_THEME_DIR."/screenshot.png",THEME_DIR."/screenshot.png");
			}
		}
	}




	//STATIC RESOURCES


	function register_script($name,$script,$dependencies=array("jquery")){
		$this->static_resources["scripts"]["register"][] = $this->static_resource_vars($name,$script,$dependencies);
	}

	function register_style($name,$style,$dependencies=array()){
		$this->static_resources["styles"]["register"][] =  $this->static_resource_vars($name,$style,$dependencies);
	}

	function enqueue_script($name,$script,$dependencies=array("jquery")){
		$this->static_resources["scripts"]["enqueue"][] = $this->static_resource_vars($name,$script,$dependencies);
	}

	function enqueue_style($name,$style,$dependencies=array()){
		$this->static_resources["styles"]["enqueue"][] = $this->static_resource_vars($name,$style,$dependencies);
	}

	function static_resource_vars($name,$file,$dependencies){
		if(substr($file,0,4)!="http" && !strstr($file,"/wp-content/")){
			$file = THEME_URL ."/". $file;
		}

		if(!$name){
			$pi=pathinfo($file);
			$name=$pi["filename"];
			if(!$name){
				$name=md5($file);
			}
		}
		return array($name,$file,$dependencies);
	}

	function load_static_resources(){
		foreach($this->static_resources["scripts"]["register"] as $file){
			wp_register_script($file[0], $file[1], $file[2],'', true);
		}
		foreach($this->static_resources["scripts"]["enqueue"] as $file){
			wp_enqueue_script($file[0], $file[1], $file[2],'', true);
		}
		foreach($this->static_resources["styles"]["register"] as $file){
			wp_register_style($file[0], $file[1], $file[2]);
		}
		foreach($this->static_resources["styles"]["enqueue"] as $file){
			wp_enqueue_style($file[0], $file[1], $file[2]);
		}
		foreach($this->settings["enable_resources"] as $resource){
			if($resource=="slick"){
				$this->enqueue_script("slick",FRAMEWORK_URL."/resources/slick/slick.min.js");
				$this->enqueue_style("slick",FRAMEWORK_URL."/resources/slick/slick.css");
			}
		}
	}

	function enqueue_framework_static_resources(){
		foreach($this->settings["enable_js"] as $file){
			$pi=pathinfo($file);
			$name="fr_".$pi["filename"];
			$this->enqueue_script($name,FRAMEWORK_URL."/js/".$file);
		}

		foreach($this->settings["enable_resources"] as $resource){
			if($resource=="slick"){
				$this->enqueue_script("slick",FRAMEWORK_URL."/resources/slick/slick.min.js");
				$this->enqueue_style("slick",FRAMEWORK_URL."/resources/slick/slick.css");
			}
		}
	}


	//LOAD FILES ASYNC TO NOT BLOCK BROWSER RENDERING

	function add_async_forscript($url){
		if (strpos($url, '#async')===false)
			return $url;
		else if (is_admin())
			return str_replace('#async', '', $url);
		else
			return str_replace('#async', '', $url)."' async='async"; 
	}

}


global $FRAMEWORK;
$FRAMEWORK = new FRAMEWORK();


