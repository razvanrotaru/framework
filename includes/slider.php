<div class="fr-slider color_variations_slider" data-fr='{"slidesToScroll":7,"slidesToShow":7,"arrows":false,"infinite": false}'>
    <a href="#" class="arrow prev"><i class="fa fa-angle-left"></i></a>
    <a href="#" class="arrow next"><i class="fa fa-angle-right"></i></a>
    <div class="slides">
        <?php  while ( $the_query->have_posts() ) {
        $the_query->the_post(); ?>
            <div class="slide col-md-2 col-sm-3 col-xs-6 color-box">
                <a  href="<?php the_permalink(); ?>"><img src="<?php echo get_field('swatch_image_link'); ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" /></a>
                <br />
                <small><?php echo get_field('color'); ?></small>
            </div>
        <?php } ?>
    </div>
</div>