var $=jQuery;


$(window).ready(function(){

    fr_initate_custom_select();
    fr_fix_form_submission_for_ie();
    fr_fix_for_jquery_block_execution_events();



    //Case insensitive search
    // NEW selector

    jQuery.expr.pseudos.Contains = function(a, i, m) {
        return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
    };


    // OVERWRITES old selector

    jQuery.expr.pseudos.contains = function(a, i, m) {
        return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
    };

    //FIX FOR IPHONE
    $('input[readonly]').on('focus', function(ev) {
        $(this).trigger('blur');
    });

    Number.prototype.pad = function(size) {
            var s = String(this);
            while (s.length < (size || 2)) {s = "0" + s;}
            return s;
    }
});





//DOM HELPERS

$(window).ready(function(){
    // Disable links
    $( 'body' ).on( 'click', 'a[disabled]', function( e ){
        e.stopImmediatePropagation();
        e.preventDefault();
    });

    // Google charts

    fr_init_google_charts();

    fr_init_width_as();
    $( window ).on( 'resize', fr_init_width_as );
    $( document ).ajaxComplete( fr_init_width_as );


    $('input.fr_js_radio[type=checkbox]').on( 'change', function(){
        if( $( this ).is( ':checked' ) ){
            var name = $( this ).attr( 'name' );
            $( 'input.fr_js_radio[type=checkbox][name="' + name + '"]' ).not( this ).prop( 'checked', false );
        }else{
            $( this ).prop( 'checked', 'checked' );
        }
    } );
    


    //REPLACERS
    $("body").on("click","[data-fr-replace-src]",function(e){
            e.preventDefault();
            $($(this).attr("data-fr-replace-src")).attr("src",$(this).attr("data-src"));
    });

    $("body").on("click","[data-fr-replace-bg]",function(e){
            e.preventDefault();
            $($(this).attr("data-fr-replace-bg")).css("background-image","url("+$(this).attr("data-background")+")");
    });


 
    /**
     * Load more
     * usage: 
     * <div class="load_more">
     *     <div class="append"></div>
     *     <a href="/page/2/" data-load-more=".load_more,.append">Load</a>
     * </div>
     */
    $( 'body' ).on( 'click', 'a[data-load-more]', function( e ){
        e.preventDefault();
        $( this ).addClass( 'loading' );
        var target = $( this ).data( 'load-more' );

        $.get( $( this ).attr( 'href' ), '', function( data ){
            var link = $( data ).find( target ).data( 'href' );
            var html = $( data ).find( target ).html();
            $( target ).find( '[data-remove]' ).before( html );
            $( target ).find( '[data-remove]' ).last().remove();
            if( !link ){
                $( target ).find( 'a[data-load-more]' ).remove();
            }
        } );
    } );


    //TOGGLE BOX
    
    $(".fr_toggle_box .box_content").each(function(){
            $(this).show();
            var dir=$(this).parents(".fr_toggle_box").first().data("dir");
            var size=$(this).outerHeight();
            if(dir=="left" || dir=="right"){
                    size=$(this).outerWidth();
            }
            $(this).css(dir,-size);
    });

    $(".fr_toggle_box .handle").on( 'click',function(){
            var parent=$(this).parents(".fr_toggle_box").first();
            var content=parent.find(".box_content").first();
            var dir=parent.data("dir");
            if(!dir){
                    dir="bottom";
            }
            data={};
            if(parent.is(".active")){//IS OPEN
                    var size=content.outerHeight();
                    if(dir=="left" || dir=="right"){
                            size=content.outerWidth();
                    }
                    data[dir]=-size;
                    content.stop().animate(data);
                    parent.removeClass("active");
                    parent.find(".bg").fadeOut();
            }else{
                    data[dir]=0;
                    content.stop().animate(data);
                    parent.addClass("active");
                    parent.find(".bg").fadeIn();
        }
    });



    //LINK ON ANY ELEMENT
    var fr_link_dont_redirect=0;
    $("body").on("click","[data-fr-link] a",function(e){
            fr_link_dont_redirect++;
    });

    $("body").on("click","[data-fr-link]",function(e){
        if(fr_link_dont_redirect<=0){
            window.location=$(this).attr("data-fr-link");
        }else{
            fr_link_dont_redirect--;
        }
    });



    //SLIDER
    //Example: {"slidesToScroll":7,"slidesToShow":7,"arrows":false,"infinite": false}
    //More options can be found here: http://kenwheeler.github.io/slick/
    if ($().slick) {
            fr_slider_init(".fr-slider");

            $("body").on("click",".fr-slider .prev",function(e){
                e.preventDefault();
                $(this).parents(".fr-slider").first().find(".slides").slick('slickPrev');
            });
            $("body").on("click",".fr-slider .next",function(e){
                e.preventDefault();
                $(this).parents(".fr-slider").first().find(".slides").slick('slickNext');
            });
    }
});


function fr_fix_form_submission_for_ie(){
    /* Fix submission buttons outside of forms for IE */
    $( 'body' ).on( 'click', '[type="submit"][form]', function( e ){
        e.preventDefault();
        $( 'form#' + $( this ).attr( 'form' ) ).trigger( 'submit' );
    });

}

function fr_slider_init(ob){
        $(ob).each(function(){
                var default_settings={slidesToScroll:1,slidesToShow:1}
                var settings=fr_parse_attr_data($(this).attr("data-fr"));
                settings=$.extend(default_settings,settings);
                settings=fr_apply_filter("fr_slider_init",settings,[$(this)]);

                $(this).find(".slides").slick(settings);


                $(this).find(".slides").on('beforeChange', function(event, slick, currentSlide, nextSlide){
                        next_slide(this,nextSlide);
                });

                function next_slide(ob,nextSlide){
                        if(nextSlide==0){
                                $(ob).parents(".fr-slider").first().find(".arrow.prev").animate({opacity:0});
                        }else{
                                $(ob).parents(".fr-slider").first().find(".arrow.prev").animate({opacity:1});
                        }

                        if(nextSlide>=$(ob).find(".slide").length-settings.slidesToScroll){
                                $(ob).parents(".fr-slider").first().find(".arrow.next").animate({opacity:0});
                        }else{
                                $(ob).parents(".fr-slider").first().find(".arrow.next").animate({opacity:1});
                        }
                }

                next_slide($(this).find(".slides"),0);

                $(this).addClass("init");
        });
}

function fr_slider_delete(ob){
    $(ob).each(function(){
        $(this).find(".slides").slick('unslick');
    });
}


$(window).ready(function(){

    // Stop window scrolling up if link is #
    $( 'body' ).on( 'click', 'a', function( e ){
        if( $( this ).attr( 'href' ) == '#' ){
            e.preventDefault();
        }
    });

    //TABS
    
    $("body").on("click",".fr-tabs .tab",function(e){
        $(this).addClass("active");
        $(this).parents(".fr-tabs").first().find(".tab").not(this).removeClass("active");

        var target=$(this).attr("href");
        if(target.substr(0,4)=="http"){
            return true;
        }

        e.preventDefault();

        var closing_tab=0;
        $(this).parents(".fr-tabs").first().find(".tab").not(this).each(function(){
            var target=$(this).attr("href");
            if(target.substr(0,4)=="http"){
                return;
            }
            if($(target).is(":visible")){
                $(target).animate({opacity:0},function(){
                    $(this).hide();
                });
                closing_tab=1;
            }
        });

        if(!$(target).is(":visible")){
            if(closing_tab){
                setTimeout(function(){
                    $(target).show();
                    $(target).css("opacity",0);
                    $(target).animate({opacity:1});
                },400);
            }else{
                $(target).show();
                $(target).css("opacity",0);
                $(target).animate({opacity:1});
            }
        }
    });

    $(".fr-tabs .tab.active").trigger( 'click' );



    //Copy the height from the neighbour
    if($("[data-fr-height-as]").length){
        height_as();

        function height_as(){
            $("[data-fr-height-as]").each(function(){
                var h=($($(this).attr("data-fr-height-as")).outerHeight());
                $(this).css("min-height",h+"px");
            });
            setTimeout(height_as,100);
        }
    }



    //HASH TAGS FUNCTIONS
    setTimeout( function(){ 
        if(fr_apply_filter("enable_hash", true)){

            //Auto hash link click

            if( fr_window_hash() ){
                setTimeout(function(){
                    scroll( fr_window_hash() );
                },1);
            }


            //Autoscroll to hash

            $( 'body' ).on( 'click', 'a[href*="#"]:not([href="#"])', function( e ){
                if($(this).is(".tab")){
                    return;
                }

                if( e.isDefaultPrevented() ){
                    return;
                }

                var link_url = $( this ).attr( 'href' ).split( '#' )[0];
                var page_url = window.location.href.split( '#' )[0];

                if( link_url.substr( 0, 1 ) == '/' ){
                    page_url = '/' + page_url.split( '/' ).slice( 3 ).join( '/' );
                }

                // Abort if the link is not the same with the current url
                if( link_url && link_url != page_url ){
                    return;
                }

                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                    if( scroll( this.hash ) ){
                        e.preventDefault();
                    }
                }
            });


            function scroll( hash ){

                if( hash.split( '/' ).length > 1 ){
                    return false;
                }

                var target = $( hash );
                target = target.length ? target : $( '[name="' + $.escapeSelector( hash.slice( 1 ) ) + '"]' );

                if ( target.length ) {
                    var duration = fr_apply_filter("hash_scroll_speed", 250 );
                    $( document.scrollingElement ).animate({
                        scrollTop: target.offset().top - fr_apply_filter("hash_offset", 0, target )
                    }, duration );
                    return true;
                }
            }
        }
    }, 1 );


    //If the select options have links, navigate on change
    $( 'body' ).on( 'change', 'select', function(){
        if($(this).find("option:selected").attr("data-href")){
            window.location.href=$(this).find("option:selected").attr("data-href");
            return false;
        }
    });


    //Change the width of select tag based on the selected option
    $(".option_width").on( 'change',function(){
        $("html").append("<select id='tmp_select' class='browser-default' style='width:auto!important'><option>"+$(this).find("option:selected").text()+"</option></select>");
        var w=$("#tmp_select").width()+15;
        $(this).attr("style","width: "+w+"px!important");
        $("#tmp_select").remove();
    });
    $(".option_width").trigger( 'change' );


    //Submit form on change
    $( 'body' ).on( 'change', '.submit_on_change', function( e ){
        $(this).parents("form").trigger( 'submit' );
    });

 
    //Alert user to confirm his action
    $( 'body' ).on( 'click', '[data-confirm]', function( e ){
        if( !confirm($(this).attr("data-confirm")) ){
            e.preventDefault();
            e.stop = true;
        }
    });



    //Javascript search

    if($(".js_search_form").length){

        $(".js_search_form input[type=text]").on( 'keyup', filter_js_search);
        $(".js_search_form").on( 'submit',function(){
            return false;
        });
        $(".js_search_form input[type=text]").on( 'click',function(){
            setTimeout(filter_js_search,1);
        });

        function filter_js_search(){
            var t=$(".js_search_form input[type=text]").val();
            $(".js_search_article").hide();
            $(".js_search_article:contains('"+t+"')").show();
        }
    }
    //If the footer is higher than the bottom of the page than fix it to the bottom
    if($(".fr_footer_fixed_bottom").length){
            fr_scroll_resize_bind(fr_helper_footer_fixed_bottom);
    }
});

function fr_helper_footer_fixed_bottom(){
    $(".footer_fixed_bottom").removeAttr("style");
    if($(".footer_fixed_bottom").offset().top+$(".footer_fixed_bottom").outerHeight()<$(window).outerHeight()){
        $(".footer_fixed_bottom").attr("style","position:fixed;bottom:-5px;width:100%");
    }
}





function fr_scroll_resize_bind(func,delay_time){
    if(!delay_time){
        delay_time=100;
    }
        $(window).on( 'scroll', function(){fr_delay(delay_time,func);});
        $(window).on( 'resize', function(){fr_delay(delay_time,func);});
        func();
}

function fr_browser_history(html, urlPath){
    window.history.replaceState({"html":html,"pageTitle":document.title},"", urlPath);
}



var delay_tmp=[];
function fr_delay(time,name,func){
    if(typeof delay_tmp[name]!="undefined"){
        return;
    }
    delay_tmp[name]=setTimeout(function(){
        delay_tmp[name]=undefined;
        if(!func){
            eval(name+"()");
        }else{
            func();
        }
    },time);
}


/**
 * Helper function that hides an element if the click is outside it
 * @param    mixed excluded_element Element to exclude hiding if clicked on it.
 * @param    mixed element_to_hide    Element to hide
 * @param    function cond_func         Optional function to add an extra condition before the element is hidden
 * @param    {[type]} hide_func         Optional function to hide the elemnt instead of using the default .hide() function
 */
function fr_click_outside( excluded_element, element_to_hide, cond_func, hide_func ){

    $( 'body' ).on( 'click', function( event ) {
        var excluded_element_ob = $( excluded_element );
        var element_to_hide_ob = $( element_to_hide );
        if (
            !excluded_element_ob.is( event.target ) && 
            !element_to_hide_ob.is( event.target ) && 
            excluded_element_ob.has( event.target ).length === 0 && 
            element_to_hide_ob.has( event.target ).length === 0 && 
            !$( this ).parents().filter( element_to_hide_ob ).length && 
            !$( this ).parents().filter( excluded_element_ob ).length 
            ){
            if( ( cond_func && cond_func( element_to_hide_ob ) ) || ( !cond_func && element_to_hide_ob.is( ':visible' ) ) ) {
                if( hide_func ){
                        hide_func( element_to_hide_ob );
                }else{
                        element_to_hide_ob.hide();
                }
            }
        }                
    });

    //FIX FOR IPHONE
    if( fr_is_ios() ){
        $( 'html' ).on( 'touchstart', function( e ) {
            var element_to_hide_ob = $( element_to_hide );
            if( hide_func ){
                    hide_func( element_to_hide_ob );
            }else{
                    element_to_hide_ob.hide();
            }
        });
    }
     $( 'body' ).on( 'touchstart', element_to_hide, function( e ) {
            e.stopPropagation();
    });
}






//FILTER SYSTEM LIKE IN WORDPRESS

function fr_add_filter(filter,func){
    if( typeof fr_filters == 'undefined' ){
        window.fr_filters=[];
    }
    if( typeof fr_filters[filter]=="undefined"){
        fr_filters[filter]=[];
    }
    fr_filters[filter].push( func );
}

function fr_add_action(filter,func){
    filter = 'action|' + filter;
    fr_add_filter( filter, func );
}




function fr_apply_filter(filter,res,args,type){

    var orig_res = res;

    if( typeof fr_filters == 'undefined' ){
        window.fr_filters=[];
    }

    if( !type ){
        type = 'filter';
    }

    if( !args ){
        args = {};
    }

    var args2=[res].concat(args);
    if(typeof fr_filters[filter]!="undefined"){
        var k;
        for(k in fr_filters[filter]){

            if( type == 'filter' ){
                if( fr_is_function( fr_filters[filter][k] ) ){
                    res=fr_filters[filter][k].apply(null,args2);
                }else{
                    res=fr_filters[filter][k];
                }
            } else {
                if( fr_is_function( fr_filters[filter][k] ) ){
                    fr_filters[filter][k].apply( null, args2 );
                }
            }
        }
    }


    if( type == 'filter' ){

        // Trigger events registered without the helper functions
        var event;
        if(typeof(Event) === 'function') {
                event = new Event( 'fr_' + filter );
        }else{
                event = document.createEvent('Event');
                event.initEvent( 'fr_' + filter, true, true);
        }

        window.dispatchEvent( event );

        fr_apply_filter( 'action|' + filter,orig_res,args,'action' );
    }

    
    return res;
}


function fr_remove_filter(filter,func){
    if( typeof fr_filters[filter] != 'undefined' ){
        var new_filter = [];
        for( k in fr_filters[filter] ){
            if( fr_filters[filter][k] !== func ){
                new_filter[k] = func;
            }
        }
        fr_filters[filter] = new_filter;
    }

}














    
function fr_init_width_as(){
    $( '[data-fr-width-as]' ).each( function(){
        $( this ).css( 'width', '' );
        var selector = $( this ).data( 'fr-width-as' );
        var width = $( this ).closest( selector ).width();
        $( this ).width( width ); 
    });
}



//AJAX
//Function for sending multiple ajax calls at the same time

var ajax_tmp=[];
var ajax_tmp_timeout;
var making_ajax;
var fr_ajax_making_request;
function fr_add_ajax(action,send_data,return_func,wait_time){

    if( !send_data ){
        send_data = {};
    }

    fr_apply_filter( 'fr_ajax_added', { 
        action: action,
        send_data: send_data,
        return_func: return_func,
        wait_time: wait_time 
    } );

    if( !wait_time ){
        wait_time = 2;
    }
    if(making_ajax){
        setTimeout(function(){
            fr_add_ajax(action,send_data,return_func);
        },100);
        return;
    }

    if(typeof send_data=="function"){
        return_func=send_data;
        send_data="";
    }

    ajax_tmp.push( [ action, send_data, return_func ] );
    if(ajax_tmp_timeout){
        clearTimeout(ajax_tmp_timeout);
    }

    ajax_tmp_timeout = setTimeout(function(){
        var ajax_callbacks = ajax_tmp;
        var ajax_vars = fr_array_to_object( ajax_tmp );
        ajax_tmp = [];
        for( k in ajax_vars ){
            ajax_vars[k] = fr_object_remove_key( ajax_vars[k], 2 );
        }

        if( fr_is_dev() ){
            fr_time_log( ajax_vars );
        }

        fr_ajax_making_request = fr_wp_ajax("multi_ajax","data="+fr_base64_encode( JSON.stringify( ajax_vars ) ),function(data, status, xhr ){

            fr_ajax_making_request = '';
            making_ajax=1;

            if(data){

                for(key in data){
                    
                    fr_parse_rules( data[key] );
                    
                    for(k2 in data[key]){

                        log( data[key][k2] );

                        // To do: Allow option to send multiple ajax requests at the same time for the same action 
                        // and receive callbacks individually
                        if(data[key][k2]["type"]=="data"){
                            for( k3 in ajax_callbacks ){
                                if( ajax_callbacks[k3][0] == key ){
                                    ajax_callbacks[k3][3] = data[key][k2]['val'];
                                }
                            }
                        }
                    }
                }
            }
            
            for( let k in ajax_callbacks ){

                let response_data = false;

                if(typeof ajax_callbacks[k][2] == "function" ){
                    if( typeof ajax_callbacks[k][3] != 'undefined' ){
                        ajax_callbacks[k][2]( ajax_callbacks[k][3], xhr );
                        response_data = ajax_callbacks[k][3];
                    }else{
                        ajax_callbacks[k][2]( false, xhr );
                    }
                }

                fr_apply_filter( 'fr_ajax_finished', { 
                    action: action,
                    send_data: send_data,
                    data: response_data,
                    xhr
                } );
            }
            making_ajax="";
        });

    },wait_time);


    function log( data ){
        //console.log( 'ajax', data );
    }
}


function fr_wp_ajax( action, data, return_func, type, custom_options ){
    if(!type){
        type = "post";
    }
    if( typeof data == 'string' ){
        data=data+"&action="+action;
    }else{
        data.action = action;
    }

    var default_options = {
        type : type,
        dataType : "json",
        url : fr_setting( 'ajax_url' ),
        data : data,
        success: function( response_data, status, xhr ){

            return_func( response_data, status, xhr );

            fr_apply_filter( 'fr_ajax_finished', { 
                action: action,
                send_data: data,
                data: response_data,
                xhr
            } );
        },
        error : function() {
            if( return_func ){
                return_func( false, xhr.statusText, xhr );
            }
        }
    }


    if( custom_options ){
        var options = $.extend( default_options, custom_options );
    } else {
        var options = default_options;
    }

    var xhr = $.ajax( options );
    return xhr;
}


function fr_direct_ajax( action, data, return_func, type, custom_options ){
    if( !custom_options ){
        custom_options = {};
    }
    custom_options.url = fr_setting( 'direct_ajax_url' );
    fr_wp_ajax( action, data, return_func, type, custom_options );
}


function fr_cancel_ajax( key ){
    ajax_tmp = [];
    if( fr_ajax_making_request ){
        fr_ajax_making_request.abort();
    }
    if( ajax_tmp_timeout ){
        clearTimeout( ajax_tmp_timeout );
    }
}


var fr_submit_form_via_ajax_xhr;
function fr_submit_form_via_ajax(form,data,return_func){
    if(fr_submit_form_via_ajax_xhr){
        fr_submit_form_via_ajax_xhr.abort();
    }
    if(data){
        data=data+"&"+$(form).serialize();
    }else{
        data=$(form).serialize();
    }
    fr_submit_form_via_ajax_xhr=$.ajax({
        url         : $(form).attr('action'),
        type        : $(form).attr('method'),
        dataType: 'html',
        data        : data,
        success: return_func,
        error : function() {
            if( return_func ){
                return_func();
            }
        }
    });        
    return fr_submit_form_via_ajax_xhr;
}










function fr_parse_rules( data ){

    for( k in data ){

        var target_ob =    data[k]["selector"];

        if( typeof target_ob == 'undefined' ){
            continue;
        }
 
        if( target_ob.split( 'parent|' ).length > 1 ){
            target_ob = target_ob.split( 'parent|' )[1];
            target_ob = $( target_ob ).parent();
        } else {
            target_ob = $( target_ob );
        }

        if(data[k]["type"]=="css"){
            var attr = data[k]["val"].split( ':' );
            var a = attr[0];
            attr = attr.slice( 1 );
            var b = attr.join( ':' ); 
            target_ob.css( a, b );
        }
        if(data[k]["type"]=="html"){
            target_ob.html(data[k]["val"]);
        }
        if(data[k]["type"]=="addClass"){
            target_ob.addClass(data[k]["val"]);
        }
        if(data[k]["type"]=="removeClass"){
            target_ob.removeClass(data[k]["val"]);
        }
        if(data[k]["type"]=="val"){
            target_ob.val(data[k]["val"]);
        }
        if(data[k]["type"]=="addAttr"){
            var attr = data[k]["val"].split( '=' );
            target_ob.attr( attr[0], attr[1] );
        }
        if(data[k]["type"]=="remove"){
            $(data[k]["val"]).remove();
        }
        if(data[k]["type"]=="append"){
            target_ob.append( data[k]["val"] );
        }
        if(data[k]["type"]=="trigger"){
            target_ob.trigger( data[k]["val"] );
        }
        if(data[k]["type"]=="apply_filter"){
            fr_apply_filter( data[k]["selector"], data[k]["val"] );
        }

        if(data[k]["type"]=="function"){

            if( typeof data[k]["val"] != 'object' ){
                data[k]["val"] = [ data[k]["val"] ];
            }

            window[data[k]["selector"]]( ...data[k]["val"] );
        }
        if(data[k]["type"]=="redirect"){
            var link = data[k]["val"];
            setTimeout( function(){
                window.location.href = link;
            }, 1 );
        }
    }
}



//INFO HELPERS

function fr_get_url_args( url ){
    
    if( url ){
        if( url.split( '?' ).length == 2 ){
            var query = url.split( '?' )[1];
        } else {
            query = url;
        }
    } else {
        var query = window.location.search.substring(1);
    }

    if( !query ){
        return [];
    }

    var query_string = {};
    var vars = query.split("&");

    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decode_uri( pair[1] );
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]],decode_uri(pair[1]) ];
            query_string[pair[0]] = arr;
        } else {
            query_string[pair[0]].push(decode_uri(pair[1]));
        }
    }

    function decode_uri( s ){
        if( !s ){
            return s;
        }
        s = s.replace(/\+/g, '%20');
        s = decodeURIComponent( s );
        return s;
    }

    return query_string;
};





/*
Remove a parameter from a url
*/
function fr_url_remove_arg( url, param ){
    if( url.split( '?' ).length > 1 ){
        url = url.split( '?' );
        search = url[1];
        if( search.split( param + '=' ).length > 1 ){
            search = search.split( '&' );
            var new_search = [];
            for( k in search ){
                if( search[k].split( param + '=' ).length == 1 ){
                    new_search.push( search[k] );
                }
            }
            search = new_search.join( '&' );
        }
        if( search ){
            url = url[0] + '?' + search;
        } else {
            url = url[0];
        }
    }

    return url
}



function fr_parse_attr_data(data){
    if(!data){
        data="";
    }
    if(data.substr(0,1)!="{"){
        data="{"+data+"}";
    }
    return $.parseJSON(data);
}



/**
 * Load script files async
 *
 * @param string url URL path to the script file
 * @return object
 */

function fr_load_script( url, callback_after_load ) {
    // Abort if script is already loaded
    if( $( 'script[src*="' + url + '"]' ).length ){
        if( callback_after_load ){
            callback_after_load( false );
        }
        return false ;
    }
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
    if( callback_after_load ){
        script.onload = function(){
            callback_after_load( true );
        };
        script.onreadystatechange = function(){
            callback_after_load( true );
        };
    }
    return document.body.appendChild( script );
}


/**
 * Load style files async
 *
 * @param string url URL path to the style file
 * @return object
 */

function fr_load_style( url, callback_after_load ) {
    // Abort if script is already loaded
    if( $( 'link[href*="' + url + '"]' ).length ){
        if( callback_after_load ){
            callback_after_load();
        }
        return false;
    }
    var style = document.createElement( 'link' );
    style.rel = 'stylesheet';
    style.type = 'text/css';
    style.media = 'all';
    style.href = url;

    if( callback_after_load ){
        style.onload = callback_after_load;
        style.onreadystatechange = callback_after_load;
    }
    return document.head.prepend( style );
}


/**
 * Remove empty items from an object
 *
 * @param object object Array with empty items.
 * @param boolean return_array Returns the result as an array
 * @return object
 */

function fr_object_remove_empty(object, return_array) {
    var new_object = {};
    for (k in object) {
        if (object[k]) {
            new_object[k]=object[k];
        }
    }

    if(!return_array){
        return new_object;
    }else{
        return fr_object_to_array(new_object);
    }
}



/**
 * Temporary cache object to keep data into memory
 *
 * @param string key Name of the cache group.
 * @param any options Used to identify the cached object in the group.
 * @param any value Value to store in cache. If missing it will return the value from the cache.
 * @return any Returns false if cache miss
 */

var fr_cache_data = {};
function fr_cache( key, options, value ){
    options = JSON.stringify(options);

    if(typeof fr_cache_data[key]=="undefined"){
        fr_cache_data[key] = {};
    }

    if(value===undefined){
        if(typeof fr_cache_data[key][options]=="undefined"){
            return false;
        }else{
            return fr_cache_data[key][options];
        }
    }else{
        fr_cache_data[key][options] = value;
    }
}



/**
 * Convert object to array
 *
 * @param object object 
 * @return array
 */

function fr_object_to_array(object){
    var array = [];
    for( k in object ){
        array.push( object[k] );
    }
    return array;
}

function fr_object_length( ob ){
    var x = 0;
    for( k in ob ){
        x++;
    }
    return x;
}

function fr_array_intersect( a, b ){
    return $.grep(a, function(i){
        return $.inArray(i, b) > -1;
    }).length > 0;
}

function fr_in_array( value, values ){
    return $.inArray( value, values ) != -1;
}


function fr_format_number( number, decimals, dec_point, thousands_sep ){
        var n = !isFinite(+number) ? 0 : +number, 
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                toFixedFix = function (n, prec) {
                        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                        var k = Math.pow(10, prec);
                        return Math.round(n * k) / k;
                },
                s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
        if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
}    


function fr_init_dropdown_filter(){
    //DROPDOWN FILTER
    if( typeof $().selectivity == 'function' ){
        $('select.filter').each(function(){
            $(this).selectivity({
                    multiple: true,
                    placeholder: $(this).data("placeholder")
            });
        });
    };
}

function fr_init_editor( ob ){

    if( !ob ){
        ob = 'body';
    }

    ob = $( ob );

    //EDITOR
    if( typeof $().jqte == 'function' ){
        if( ob.is( 'textarea' ) ){
            ob.jqte({"source":false});
        } else {
            ob.find("textarea.editor").jqte({"source":false});
        }
    }
}


//ANIMATION


//It will slide the content of the box relative to the other boxes
//Usage: $(".box").click(fr_box_slide);

function fr_box_slide(ob,speed){
    if(!ob){
        ob=this;
    }
    ob=$(ob).find(".fr-box-content");
    var new_active=$(ob).parent().find(".fr-box-content").addClass("active");
    var active=$(ob).parent().parent().find(".fr-box-content.active").not(new_active);
    
    if(!active.length || active.parent().offset().left!=new_active.parent().offset().left){
        new_active.show().css("left","100%").css("right","-100%").css("top","0%").css("bottom","0%");
        if(active.length){
                var ml="-100%";
                var rml="100%";
                if(active.parent().offset().left<new_active.parent().offset().left){
                    var ml="100%";
                    var rml="-100%";
                    new_active.css("left","-100%").css("right","100%");
                }
                active.removeClass("active").stop().animate({"left":ml,"right":rml},speed);
        }
        new_active.stop().animate({"left":"0%","right":"0%"},speed);
    }else{
        new_active.show().css("top","100%").css("bottom","-100%").css("left","0%").css("right","0%");
        var ml="-100%";
        var rml="100%";
        if(active.parent().offset().top<new_active.parent().offset().top){
            var ml="100%";
            var rml="-100%";
            new_active.css("top","-100%").css("bottom","100%");
        }
        active.removeClass("active").stop().animate({"top":ml,"bottom":rml},speed);
        new_active.stop().animate({"top":"0%","bottom":"0%"},speed);
    }
}


//It will slide the content from left to right
//Usage: $(".box").click(fr_box_slide_left);

function fr_box_slide_left(ob,speed){
    if(!ob){
        ob=this;
    }
    ob=$(ob).find(".fr-box-content");
    var new_active=$(ob).parent().find(".fr-box-content").addClass("active");
    var active=$(ob).parent().parent().find(".fr-box-content.active").not(new_active);

    if( !new_active.is( ':visible' ) ){
        new_active.show().css("left","-100%").css("top","0%").css("bottom","0%");
    }

    if(active.length){
        active.removeClass("active").stop().animate({"left":"-100%"},speed);
    }
    new_active.stop().animate({"left":"0%","right":"0%"},speed);

}



/**
 * Audio Player
 * HTML FORMAT:
 * <a href="file.mp3" data-id="1" class="fr_player">
 *     <span class="play"></span>
 *     <span class="pause"></span>
 *     <span class="mute"></span>
 * </a>
 * <div class="fr_spectrogram"></div>
 * <div class="fr_player_progress_bar"></div>
 * <div class="fr_player_time"></div>
 */
var fr_audio_track = [];
function fr_audio_player( callback ){



    $( 'body' ).on( 'click', '.fr_player[href] .play,.fr_player[data-href] .play', function( e ){
        e.preventDefault();
        $( '.fr_player' ).removeClass( 'playing' );

        var file = $( this ).parents( '.fr_player' ).attr( 'href' );
        if( $( this ).parents( '.fr_player' ).attr( 'data-href' ) ){
            file = $( this ).parents( '.fr_player' ).attr( 'data-href' );
        }

        if( typeof fr_audio_track.file == 'undefined' || fr_audio_track.file != file ){
            fr_player_add( file );

            $( fr_audio_track['audio'] ).off( 'ended' ).on("ended",function(){
                if( callback ){
                    callback( 'ended', fr_audio_track );
                }
                $( '.fr_player[href="' + fr_audio_track.file + '"],.fr_player[data-href="' + fr_audio_track.file + '"]' ).removeClass( 'playing' );
            });
        }
        fr_player_start();
        if( callback ){
            callback( 'play', fr_audio_track );
        }
        $( '.fr_player[href="' + fr_audio_track.file + '"],.fr_player[data-href="' + fr_audio_track.file + '"]' ).addClass( 'playing' );
    } );



    $( 'body' ).on( 'click', '.fr_player[href] .pause,.fr_player[data-href] .pause', function( e ){
        e.preventDefault();

        if( typeof fr_audio_track.file == 'undefined' ){
            return;
        }
        
        fr_player_pause();
        if( callback ){
            callback( 'pause', fr_audio_track );
        }
        $( '.fr_player[href="' + fr_audio_track.file + '"],.fr_player[data-href="' + fr_audio_track.file + '"]' ).removeClass( 'playing' );

    } );



    if( callback ){
        check_audio_status();
    }

    // Continusly update progress bar for started tracks
    function check_audio_status(){
        if( typeof fr_audio_track != 'undefined' && typeof fr_audio_track.file != 'undefined' ){
            var audio = fr_audio_track["audio"];

            if( !audio.paused ){
                var at=100/audio.duration*audio.currentTime;

                if(at<0){
                    at=0;
                }
                var m=Math.floor(audio.currentTime/60).toString();
                if( m.length == 1 ){
                    m = '0' + m;
                } else if( m.length == 0 ){
                    m = '00';
                }
                var s=Math.round(audio.currentTime-m*60)+"";
                if(s.length==1){
                    s="0"+s;
                }
                callback( 'status', fr_audio_track, at, m+":"+s );
            }
        }
        setTimeout( check_audio_status ,42);
    }
}

function fr_player_add( track ){
    if( typeof fr_audio_track.file != 'undefined' ){
        fr_player_pause();
    }
    fr_audio_track['audio'] = new Audio();
    fr_audio_track["audio"].src = track;
    fr_audio_track["audio"].preload = "auto";
    fr_audio_track['file'] = track;
}

function fr_player_start(){
    fr_audio_track["audio"].play();
}

function fr_player_pause(){
    fr_audio_track["audio"].pause();
}

function fr_player_mute(){
    fr_audio_track["audio"].muted = true;
}

function fr_player_unmute(){
    fr_audio_track["audio"].muted = false;
}

function fr_player_is_playing(){
    return typeof fr_audio_track.file != 'undefined' && !fr_audio_track.audio.paused;
}
//*** End Audio Player ***//)

function fr_on_click_position_percentage( ob, callback ){
    $( "body" ).on("click",ob,function(e){
            var relX = e.pageX - $(this).offset().left;
            var per=100/$(this).width()*relX;
            callback( per, $( this ) );
    });
}

function fr_on_load( callback, delay ){
    if( document.readyState == 'complete' ){
        if( delay ){
            setTimeout( callback, delay );
        } else {
            callback();
        }
    }else{
        $( window ).on( 'load', function(){
            if( delay ){
                setTimeout( callback, delay );
            } else {
                callback();
            }
        } );
    }
}

function fr_array_remove_item( array, item ){
    var new_array = [];
    for( k in array ){
        if( array[k] != item ){
            new_array.push( array[k] );
        }
    }
    return new_array;
}

function fr_array_remove_key( array, index ){
    if( typeof array[index] != 'undefined' ){
        return    array.splice( index, 1 );
    }
    return array;
}

function fr_object_remove_key( ob, index ){
    var new_ob = {};
    for( k in ob ){
        if( k == index ){
            continue;
        }
        new_ob[k] = ob[k];
    }
    return new_ob;
}

function fr_array_to_object( a ){
    var o = {};
    for( k in a ){
        o[k] = a[k];
    }
    return o;
}

function fr_in_object( key, o ){

        for( k in o ){
            if( o[k] === key ){
                return k;
            }
        }
        return false;
}


function fr_setting( k, v ){

    if( typeof fr_settings == 'undefined' ){
        window.fr_settings = {};
    }

    if( typeof v == 'undefined' ){
        if( typeof fr_settings[ k ] != 'undefined' ){
            return fr_settings[ k ];
        }
    } else {
        fr_settings[ k ] = v;
    }
}

/**
 * $.parseParams - parse query string paramaters into an object.
 */
(function($) {
    var re = /([^&=]+)=?([^&]*)/g;
    var decodeRE = /\+/g;    // Regex for replacing addition symbol with a space
    var decode = function (str) {return decodeURIComponent( str.replace(decodeRE, " ") );};
    $.parseParams = function(query) {
            var params = {}, e;
            while ( e = re.exec(query) ) { 
                    var k = decode( e[1] ), v = decode( e[2] );
                    if (k.substring(k.length - 2) === '[]') {
                            k = k.substring(0, k.length - 2);
                            (params[k] || (params[k] = [])).push(v);
                    }
                    else params[k] = v;
            }
            return params;
    };
})(jQuery);













function fr_initate_custom_select(){
    $("body").on("click",".fr_custom_select .view",function(){
        if( $( this ).parents( '.fr_custom_select' ).is( '.open' ) ){
            $( this ).parents( '.fr_custom_select' ).removeClass( 'open' );
        }else{
            $( '.fr_custom_select.open' ).removeClass( 'open' );
            $( this ).parents( '.fr_custom_select' ).addClass( 'open' );
        }
    });

    var clicked_at = 0;
    var key_at = 0;
    $( 'body' ).on( 'click', function(){
        clicked_at = new Date().getTime();
    } );
        $( 'body' ).on( 'keydown', function(){
        key_at = new Date().getTime();
    } );

    $("body").on("change",".fr_custom_select input[type=checkbox],.fr_custom_select input[type=radio]", function(){ 
        on_change( $( this ) );
    });

    function on_change( ob ){
        var values=[];
        $(ob).parents(".list").first().find("input").each(function(){
            if($(this).is(":checked")){
                $(this).parents(".item").addClass("active");
                values.push( $(this).parents(".item").find( 'span' ).html() );
            }else{
                $(this).parents(".item").removeClass("active");
            }
        });
        if( !values.length ){
            values.push( $( ob ).parents( '.fr_custom_select' ).find( '.inner' ).data( 'default-text' ) );
        }
        $(ob).parents( ".fr_custom_select" ).find(".view .inner").html(values.join(", "));

        if( $( ob ).is( '[type="radio"]' ) && clicked_at - key_at > 200 ){
            $( ob ).parents( '.fr_custom_select' ).removeClass( 'open' );
        }
    }


    fr_click_outside('.fr_custom_select','.fr_custom_select .list',function( ob ){
        $( '.fr_custom_select' ).removeClass( 'open' );
    });

    $("body").on("click",".fr_custom_select.multiple-0 .item",function(){
        $( this ).parents( '.fr_custom_select' ).removeClass( 'open' );
    });

    initiate();

    $(document).on('gform_post_render', function(){
        initiate( '.gform_wrapper' );
    });

    function initiate( parent ){
        if( !parent ){
            parent = 'body';
        }
        $(".fr_custom_select", parent ).each(function(){
            on_change( $("input[type=checkbox]",this).first() );
            on_change( $("input[type=radio]",this).first() );
        });
    }

    fr_add_filter( 'ready', initiate );

    setInterval( function(){
        var ob = $(".fr_custom_scroll:not(.mCustomScrollbar):visible");
        if( ob.length && typeof ob.mCustomScrollbar != 'undefined' ){
            ob.mCustomScrollbar({axis:"y",theme:"inset-2",scrollInertia:0});
        }
    }, 100 );
}
function fr_custom_select_set_value( ob, val ){
    $( ob ).find( 'input[value="' + val + '"]' ).prop( 'checked', 'checked' );
    $( ob ).find( 'input[value="' + val + '"]' ).trigger( 'change' );
}





function fr_input_format_hour_seconds( ob ){
    $( 'body' ).on( 'keyup', ob, function( e ){
        var val = $( this ).val();
        var keep = [':','0','1','2','3','4','5','6','7','8','9'];
        var x = 0;
        var new_val = [];
        while( x < val.length ){
            if( $.inArray( val[x], keep ) != -1 ){
                new_val.push( val[x] );
            }
            x++;
        }
        new_val = new_val.join( '' );
        new_val = new_val.split( ':' );
        if( new_val.length == 2 ){
            if( Number( new_val[1] ) > 59 ){
                new_val[1] = '00';
            }
            new_val[1] = new_val[1].substr( 0, 2 );
        }
        new_val = new_val.join( ':' );
        $( this ).val( new_val );
    });
}

function fr_input_format_int( ob ){
    $( 'body' ).on( 'keyup', ob, function( e ){
        var val = $( this ).val();
        var keep = ['0','1','2','3','4','5','6','7','8','9'];
        var x = 0;
        var new_val = [];
        while( x < val.length ){
            if( $.inArray( val[x], keep ) != -1 ){
                new_val.push( val[x] );
            }
            x++;
        }
        new_val = new_val.join( '' );
        $( this ).val( new_val );
    });
}




function fr_change_url( title, html, url ){
    if( fr_current_url() == url ){
        return;
    }
    window.history.pushState( { "html": html, "pageTitle": title }, "", url );
}



function fr_is_function( functionToCheck ) {
 return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

function fr_current_url( no_params ){
    var url = window.location.href;
    if( no_params ){
        url = url.split( '?' )[0];
    }
    return url;
}

var fr_is_redirect = false;
var fr_redirect_to_top = false;
function fr_redirect( link, no_is_redirect, timeout ){

    if( timeout ){
        setTimeout( redirect, timeout );
    } else {
        redirect();
    }

    function redirect(){
        if( fr_redirect_to_top ){
            window.top.location.href = link;
        } else {
            window.location.href = link;
        }
        if( !no_is_redirect ){
            fr_is_redirect = true;
        }
    }
}

function fr_doing_redirect(){
    return fr_is_redirect;
}

function fr_refresh(){
    window.location.reload( true ); 
}

var fr_current_pressed_key = [];
function fr_current_pressed_key_is( keys ){

    if( typeof keys != 'array' ){
        keys = [ keys ];
    }

    for( k in keys ){
        if( $.inArray( keys[k], fr_current_pressed_key ) != -1 ){
            return k;
        }
    }
}


function fr_key_pressed( e ){
    return e.keyCode || e.which;
}

$( "html" ).on( 'keydown',function( e ) {
    var code = e.keyCode || e.which;

    // Abort if the same key
    if( fr_current_pressed_key.length ){
        var last = fr_current_pressed_key.slice( - 1 )[0];
        if( last == code ){
            return;
        }
    }
    
    fr_current_pressed_key.push( code );
});

$( "html" ).on( 'keyup', function( e ) {
    var code = e.keyCode || e.which;
    if( code == 91 ){
        // If Ctrl/Cmd is pressed then remove all keys since they won't trigger the keyup
        fr_current_pressed_key = [];
    } else {
        fr_current_pressed_key = fr_array_remove_item( fr_current_pressed_key, code );
    }
});


function fr_merge_recursive( a, b ){
    if( typeof a == 'undefined' ){
        return b;
    }

    for( k in b ){
        if( typeof a[k] != 'undefined' && ( a[k].constructor === Array || a[k].constructor === Object ) ){
            if( b[k].constructor === Array || b[k].constructor === Object ){
                fr_merge_recursive( a[k], b[k] );
            }
        }else{
            a[k] = b[k];
        }
    }
    return a;
}


function fr_object_merge( a, b ){
    return { ...a, ...b };
}


function fr_init_google_charts(){

    // If it's not loaded yet check every second until it is
    if ((typeof google === 'undefined') || (typeof google.charts === 'undefined')) {
        setTimeout( fr_init_google_charts, 1000 );
        return;
    }

    $( '.fr_google_chart' ).each( init );
    $( window ).on( 'resize', function(){
        $( '.fr_google_chart' ).each( init );
    });

    function init(){
        var settings = $( this ).data( 'json' );
        var ob = $( this );
        var chart_type = settings.chart_type;

        google.charts.load('current', {packages: ['corechart', chart_type]});

        google.charts.setOnLoadCallback(function() {
            var chart_data = settings.rows;
            var chart_header = settings.header;
            var options = settings.options;

            for( x in chart_header ){
                if( chart_header[x]['type'] == 'date' ){
                    for( y in chart_data ){
                        chart_data[y][x] = new Date( chart_data[y][x] );
                    }
                }
            }

            var data = new google.visualization.DataTable();
            for( k in chart_header ){
                data.addColumn( chart_header[k].type, chart_header[k].name);
            }

            data.addRows(chart_data);

            if( chart_type == 'bar' ){
                var chart = new google.visualization.BarChart( ob[0] );
            }else{
                var chart = new google.visualization.LineChart( ob[0] );
            }
            
            chart.draw(data, options);
        });
    }
}




function fr_is_dev(){
    return fr_setting( 'is_dev_env' );
}

function fr_is_touch() { return !!('ontouchstart' in window) || !!('msmaxtouchpoints' in window.navigator); };


var fr_start_timer_time = 0;
function fr_start_timer(){
    var d = new Date();
    fr_start_timer_time = d.getTime();
}

function fr_get_timer(){
    var d = new Date();
    return d.getTime() - fr_start_timer_time;
}

function fr_show_timer(){
    console.log( fr_get_timer() );
}

function fr_time_log( message ){
    var time = 0;
    if( fr_start_timer_time ){
        time = fr_get_timer();
    }
    console.log( message, time );
    fr_start_timer(); 
}

function fr_debug(){
    console.log( new Error().stack );
}

function fr_is_ios(){
    return /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
}

function fr_is_iphone(){
    return /(iPhone)/g.test(navigator.userAgent);
}

function fr_is_safari(){
    return navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
                 navigator.userAgent &&
                 navigator.userAgent.indexOf('CriOS') == -1 &&
                 navigator.userAgent.indexOf('FxiOS') == -1;
}

function fr_is_IE(){
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
         // Edge (IE 12+) => return version number
         return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}

function fr_is_firefox(){
    return navigator.userAgent.indexOf("Firefox") > 0;
}


function fr_is_chrome(){
    var isChromium = window.chrome;
    var winNav = window.navigator;
    var vendorName = winNav.vendor;
    var isOpera = typeof window.opr !== "undefined";
    var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
    var isIOSChrome = winNav.userAgent.match("CriOS");

    if (isIOSChrome) {
        // is Google Chrome on IOS
        return true;
    } else if(
        isChromium !== null &&
        typeof isChromium !== "undefined" &&
        vendorName === "Google Inc." &&
        isOpera === false &&
        isIEedge === false
    ) {
        // is Google Chrome
        return true;
    } else { 
        // not Google Chrome 
        return false;
    }
}


var fr_lazy_load_interval;
var fr_lazy_load_started;
function fr_init_lazy_load(){

    fr_lazy_load_started = true;

    function interval( padding, show_all ){
        if( fr_lazy_load_interval ){
            clearInterval( fr_lazy_load_interval );
        }
        fr_lazy_load( padding, show_all );
        fr_lazy_load_interval = setInterval( function(){
            fr_lazy_load( padding, show_all );
        },250);
    }

    function load(){
        fr_lazy_load_started = true;

        fr_lazy_load( 1000000, true, 'at_load' );

        $( window ).on( 'scroll', fr_padding_interval );

        setTimeout( function(){
            $( window ).off( 'scroll', fr_no_padding_interval );
            $( window ).off( 'scroll', fr_padding_interval );
            fr_lazy_load_started = false;
            fr_lazy_load( 1000000, true, 'after_load' );
        }, 3000 );
    };

    function fr_no_padding_interval(){
        $( window ).off( 'scroll', fr_no_padding_interval );
        interval( 0, true );
    }

    function fr_padding_interval(){
        $( window ).off( 'scroll', fr_padding_interval );
        interval( 500, true );
    }

    fr_no_padding_interval();

    fr_lazy_load( 0, true );
    fr_on_load( load );
    $( window ).on( 'scroll', fr_no_padding_interval );


    function reinit(){
        fr_lazy_load( 0, true );
        if( !fr_lazy_load_started ){
            load();
        }
    }

    fr_add_filter( 'ready', reinit );
    fr_add_filter( 'fr_reinit_lazy_load', reinit );
}


function fr_lazy_load( range, show_hidden, load_time ){
    var obs = $("[data-lazy-src],[data-lazy-style],[data-lazy-href]");
    if( !show_hidden ){
        obs = obs.filter( ':visible' );
    }
    obs.each(function(){
        var ob = $(this);
        if( load_time != 'at_load' && ob.is( '[data-load-time="at_load"]' ) && ob.is( ':not(:visible)' ) ){
            return;
        }
        if( load_time == 'at_load' && !ob.is( '[data-load-time="at_load"]' ) ){
            return;
        }
        if( load_time != 'after_load' && ob.is( '[data-load-time="after_load"]' ) && ob.is( ':not(:visible)' ) ){
            return;
        }
        if(fr_in_viewport(ob,range)){
            if(ob.is("[data-lazy-src]")){
                ob.attr("src",ob.attr("data-lazy-src"));
                ob.removeAttr("data-lazy-src");
            }
            if( ob.is( '[data-lazy-href]' ) ){
                ob.attr( 'href', ob.data( 'lazy-href' ) );
                ob.removeAttr( 'data-lazy-href' );
            }
            if(ob.is("[data-lazy-srcset]")){
                ob.attr("srcset",ob.attr("data-lazy-srcset"));
                ob.removeAttr("data-lazy-srcset");
            }
            if(ob.is("[data-lazy-style]")){
                if(ob.is("[style]")){
                    var style=ob.attr("style") + ';' + ob.attr("data-lazy-style");
                }else{
                    var style=ob.attr("data-lazy-style");
                }
                ob.attr("style",style);
                ob.removeAttr("data-lazy-style");
            }
        }
    });
}

function fr_in_viewport(ob,range ){
    range = range ?? 0;
    ob = $( ob );
    var w = $(window);
    var wst=w.scrollTop();
    var wh=w.height();
    var top=ob.offset().top;
    var height=ob.outerHeight();
    var wsl=w.scrollLeft();
    var ww=w.width();
    var left=ob.offset().left;
    var width=ob.outerWidth();

    top=top-range;
    height=height+range*2;
    left=left-range;
    width=width+range*2;
    if( ((wst<=top && wst+wh>=top) || (wst<=top+height && wst+wh>=top+height) || (wst>top && wst+wh<top+height)) && 
        ((wsl<=left && wsl+ww>=left) || (wsl<=left+width && wsl+ww>=left+width) || (wsl>left && wsl+wh<left+width))){
        return 1;
    }
}


function fr_load_from_url( url, callback ){
    var request = new XMLHttpRequest();
    request.open( 'GET', url, true );
    request.responseType = "blob";        
    request.onload = function() {
        if ( this.status >= 200 && this.status < 300 ) {
            callback( URL.createObjectURL( this.response ) );
        } else {
            callback();
        }
    }
    request.onerror = function() {
        callback();
    }
    request.send();
    return request;
}

function fr_on_img_load( img, callback ){
        $("<img/>").on('load', callback ).on( 'error', callback ).attr("src", img );
}

function fr_on_style_load( href, callback ){
        var ob = $( "<link/>" ).on( 'load', callback ).attr( "rel", 'stylesheet' ).attr( "type", 'text/css' ).attr( "href", href );
}


/**
 * Function that rotates an image on a 90deg increment using canvas
 * @param    {string}     base64data The image data encoded as a base64 string including "data:" 
 * @param    {int}            degrees        The number of degrees to rotate. It only rotates in increments of 90 degrees
 * @param    {int}     max_width         The maxiumum width in pixels the returned image should have. A smaller number is more efficient
 * @param    {Function} callback     Callback with the result
 * @return {string}                            The rotated image data encoded in base64 string
 */
function fr_image_rotate( src, degrees, max_width, max_height, callback ) {
    if( degrees == 0 ){
     callback( src );
    }

    fr_image_rotate_90( src, max_width, max_height, function( src ){
        if( degrees - 90 ){
            fr_image_rotate( src, degrees - 90, max_width, max_height, callback );
        } else {
            callback( src );
        }
    } )
}

/**
 * Function that rotates 90 deg a base64 encoded image using canvas
 * @param    {string}     base64data The image data encoded as a base64 string including "data:" 
 * @param    {int}     max_width         The maxiumum width in pixels the returned image should have. A smaller number is more efficient
 * @param    {Function} callback     Callback with the result
 * @return {string}                            The rotated image data encoded in base64 string
 */
function fr_image_rotate_90( src, max_width, max_height, callback ) {
    degrees = 90;

    fr_image_get_real_size( src, function( width, height, image ){ 
        // For efficiency specify a max width and max_height to process the image
        if( max_width && width > max_width ){
            height = height * max_width / width;
            width = max_width;
        }
        if( max_height && height > max_height ){
            width = width * max_height / height;
            height = max_height;
        }

        var canvas = $( '<canvas width="' + height + '" height="' + width + '"/>' );
        canvas.width = height;
        canvas.height = width;
        var ctx = canvas[0].getContext( '2d' );
        ctx.rotate( degrees * Math.PI / 180 );
        ctx.translate( 0, -canvas.width );
        ctx.drawImage( image, 0, 0, width, height ); 
        var mime = fr_image_get_mime( src );
        if( !mime ){
            mime = 'image/jpg';
        }
        callback( canvas[0].toDataURL( mime ) );
    } );
}


/**
 * Converts an url to a base64 image using canvas
 * @param    {string}     src            The url of the image. It must be allowed by CORS
 * @param    {Function} callback The function to call after the process is completed
 * @return {string}                        The image data encoded in a base64 string
 */
function fr_image_to_base64( src, callback ) {
    if( is_src_base64( src ) ){
        return src;
    }
    fr_image_get_real_size( src, function( width, height, image ){ 
        var height = image.height;
        var width = image.width;

        var canvas = $( '<canvas width="' + height + '" height="' + width + '"/>' );
        canvas.width = height;
        canvas.height = width;
        var ctx = canvas[0].getContext( '2d' );
        ctx.drawImage(image, 0, 0, width, height ); 
        var mime = fr_image_get_mime( src );
        if( !mime ){
            mime = 'image/jpg';
        }
        callback( canvas[0].toDataURL( mime ) );
    } );
}


/**
 * Crop an image using canvas
 * @param    {string}     src            The src to crop. It supports bas64 and urls
 * @param    {int}     x                     The x position where to crop the image
 * @param    {int}     y                     The y position where to crop the image
 * @param    {int}     width             The width of the area to crop
 * @param    {int}     height            The height of the area to crop
 * @param    {Function} callback The function to be called back with the result
 * @return {string}                        The image data encoded in a base64 string
 */
function fr_image_crop( src, x, y, width, height, callback ) {
    fr_image_get_real_size( src, function( image_width, image_height, image ){
        var canvas = $( '<canvas width="' + width + '" height="' + height + '"/>' );
        canvas.width = height;
        canvas.height = width;
        var ctx = canvas[0].getContext( '2d' );
        ctx.drawImage( image, -x, -y, image_width, image_height ); 
        var mime = fr_image_get_mime( src );
        if( !mime ){
            mime = 'image/jpg';
        }
        var data = canvas[0].toDataURL( mime );
        callback( data );
    } );
}


/**
 * Function that gets the real size of an image
 * @param    {string}     src            The src of the image. It supports bas64 and urls     
 * @param    {Function} callback The function to be called back with the result
 * @return {mixed}        width, height, image        
 */
function fr_image_get_real_size( src, callback ){
    var image = new Image();
    image.src = src;
    image.onload = function() {
        var height = image.height;
        var width = image.width;
        callback( width, height, image );
    }
}

/**
 * Gets the mime type of an image. It has limited support
 * @param    {string} src The src of the image. It supports bas64 and urls 
 * @return {string}         The detected mime type. If the mime is not a valid image it will return false
 */
function fr_image_get_mime( src ){
    var mime = false;
    if( is_src_base64( src ) ){
        mime = src.split( 'data:' )[1].split( ';' )[0];
    } else {
        var ext = src.split( '.' ).pop();
        if( ext == 'jpg' || ext == 'jpeg' ){
            mime = 'image/jpeg';
        }
        if( ext == 'gif' ){
            mime = 'image/gif';
        }
        if( ext == 'png' ){
            mime = 'image/png';
        }
    }
    return mime;
}

/**
 * Detects if the src is a bas64 encoded string
 * @param    {string}    src The string to be checked
 * @return {Boolean}
 */
function is_src_base64( src ){
    return src.substr( 0, 5 ) == 'data:' && src.split( ';base64,' ).length == 2;
}

function fr_on_audio_load( src, callback ){
        var audio = new Audio();
        audio.addEventListener( 'canplaythrough', callback, false );
        audio.addEventListener( 'error', callback );
        audio.src = src;
        audio.load();
}

function fr_on_style_load( href, callback ){
        var ob = $( "<link/>" ).on( 'load', callback ).attr( "rel", 'stylesheet' ).attr( "type", 'text/css' ).attr( "href", href );
}


function fr_on_styles_load( urls, callback, nocache ){
    if (typeof nocache=='undefined') nocache=false; // default don't refresh
    $.when.apply($,
            $.map(urls, function(url){
                    if (nocache) url += '?_ts=' + new Date().getTime(); // refresh? 
                    return $.get(url, function(){                                        
                            $('<link>', {rel:'stylesheet', type:'text/css', 'href':url}).appendTo('head');                                        
                    });
            })
    ).then(function(){
            if (typeof callback=='function') callback();
    });
};


function fr_whatsapp_installed( succes_callback, false_callback ){
    $.ajax({
        type: 'HEAD',
        url: 'whatsapp://send?text=1',
        success: succes_callback,
        error: false_callback
    });
}



/**
 * Automatically scrolls down the element when it reaches the top of the browser
 * @param    string/object selector 
 * @param string/object/int offset_selectors It is usefull if there is a fixed header
 * @param string/object parent Dom object from which it will not go
 * @param int min_window_width Useful to disable for mobile devices
 */
var fr_fixed_on_scroll_last_position = 0;
function fr_fixed_on_scroll(selector, offset_selectors, parent, min_window_width, min_window_height ){

    var ob = $( selector );

    if( ob.is( '.init' ) ){
        return;
    }

    //omit mobile devices
    if(!min_window_width && !min_window_height){
        min_window_width = 768;
    }


    if(!parent){
        var parent_ob = ob.parent();
    } else {
        var parent_ob = $( parent );
    }


    ob.addClass( 'init' );

    init();

    $(window).on( 'scroll', function(){
        fr_delay( 100, 'fr_fixed_on_scroll_' + selector, init );
    } );
    $(window).on( 'resize', function(){
        fr_delay( 100, 'fr_fixed_on_scroll_' + selector, init );
    } );




    function init(){

        if(!ob.length){
            return;
        }

        if( !parent_ob.length ){
            return;
        }

        //clear position of element
        ob.removeClass("bottom");
        ob.removeAttr("style");

        //check window width
        if( min_window_width && $(window).width() <= min_window_width ){
            remove();
            return;
        }

        // check window height
        if( min_window_height && $(window).height() <= min_window_height ){
            remove();
            return;
        }

        //set variables
        var wst = $(window).scrollTop();
        var sot = ob.offset().top;
        var sh = ob.outerHeight();
        var sw = ob.outerWidth();
        var spot = parent_ob.offset().top;
        var sph = parent_ob.height();
        var offset = 0;

        if( ob.next().is( '.fr_fixed_on_scroll_placeholder' ) ){
            ob.next().show();
            sot = ob.next().offset().top;
            sw = ob.next().outerWidth();
            ob.next().css( 'display', '' );
        }

        if( sh >= sph ){
            remove();
            return;
        }

        //calculate offset
        if( offset_selectors ){
            if( Number.isInteger(offset_selectors) ){
                offset = offset_selectors;
            }else{
                $(offset_selectors).each(function(){
                    offset += $(this).outerHeight();
                });
            }
        }

        //position the element
        if( wst + offset > sot ){

            if( !ob.next().is( '.fr_fixed_on_scroll_placeholder' ) ){
                ob.after( '<div class="fr_fixed_on_scroll_placeholder" style="height: ' + sh + 'px"></div>' );
            }

            ob.outerWidth( sw );
            ob.addClass("fr_is_fixed_on_scroll");

            if( ob.offset().top + ob.outerHeight() > spot + sph ){
                ob.addClass( 'bottom' );
            }
        } else {
            remove();
        }
    }

    function remove(){
        ob.removeClass("fr_is_fixed_on_scroll");
        if( ob.next().is( '.fr_fixed_on_scroll_placeholder' ) ){
            ob.next().remove();
        }
    }
}






function fr_fixed_to_the_bottom( ob, top_ob ){
    ob = $( ob );
    top_ob = $( top_ob );

    function position(){
        fr_delay( 100, 'fr_fixed_to_the_bottom', function(){ 
            ob.removeClass( 'fixed' );
            ob.css( 'width', '' );
            if( ob.offset().top + ob.outerHeight() > $( window ).scrollTop() + $( window ).height() ){
                ob.width(ob.width() );
                ob.addClass( 'fixed' );
            }

            if( top_ob.length && top_ob.offset().top > ob.offset().top ){
                ob.removeClass( 'fixed' );
                ob.css( 'width', '' );
            }
        } );
    }
    position();
    $( window ).ready( position );
    $( window ).on( 'load', position );
    $( window ).on( 'scroll', position );
    $( window ).on( 'resize', position );
}



function fr_has_scroll( ob, direction ){
    if( !direction ){
        direction = 'vertical';
    }
    if( direction == 'vertical' ){
        return $( ob ).prop( 'clientHeight' ) < $( ob ).prop( 'scrollHeight' );
    } else {
        return $( ob ).prop( 'clientWidth' ) < $( ob ).prop( 'scrollHeight' );
    }
}

function fr_scroll_to( ob, offset, speed ){
    if( !speed ){
        speed = 250;
    }
    var offset_height = 0;
    if( offset ){
        $( offset ).each( function(){ 
            offset_height += $( this ).outerHeight();
        } );
    }
    $( 'body,html' ).stop().animate( { 'scrollTop': $( ob ).offset().top - offset_height }, speed ); 
}



function fr_show_json_content( parent ){
    $( parent ).find( '.fr_json_content' ).each( function(){
        var json_object = $( this );
        var data = json_object.html();
        json_object.after( data );
        json_object.remove();
    } );
}

function fr_to_time( nr ){
    nr = Number( nr );
    nr = Math.round( nr );
    var minutes = Math.floor( nr / 60 );
    var seconds = nr - minutes * 60;
    minutes = minutes.toString();
    seconds = seconds.toString();
    if( minutes.length == 1 ){
        minutes = '0' + minutes;
    }
    if( seconds.length == 1 ){
        seconds = '0' + seconds;
    }
    return minutes + ':' + seconds;
}


function fr_img_antialising( img ){
    var img = new Image();
    img = $( img );

    img.src = img.attr( 'src' );

    img.onload = function() {

            var scale = 2;

            var src_canvas = document.createElement( 'canvas' );
            src_canvas.width = this.width;
            src_canvas.height = this.height;

            var src_ctx = src_canvas.getContext('2d');
            src_ctx.drawImage(this, 0, 0);
            var src_data = src_ctx.getImageData(0, 0, this.width, this.height).data;

            var sw = this.width * scale;
            var sh = this.height * scale;

            img.after( '<canvas>' );
            var dst_canvas = img.next()[0];
            dst_canvas.width = sw;
            dst_canvas.height = sh;
            var dst_ctx = dst_canvas.getContext('2d');

            var dst_imgdata = dst_ctx.createImageData(sw, sh);
            var dst_data = dst_imgdata.data;

            var src_p = 0;
            var dst_p = 0;
            for (var y = 0; y < this.height; ++y) {
                    for (var i = 0; i < scale; ++i) {
                            for (var x = 0; x < this.width; ++x) {
                                    var src_p = 4 * (y * this.width + x);
                                    for (var j = 0; j < scale; ++j) {
                                            var tmp = src_p;
                                            dst_data[dst_p++] = src_data[tmp++];
                                            dst_data[dst_p++] = src_data[tmp++];
                                            dst_data[dst_p++] = src_data[tmp++];
                                            dst_data[dst_p++] = src_data[tmp++];
                                    }
                            }
                    }
            }

            dst_ctx.putImageData(dst_imgdata, 0, 0);

    };
}


var fr_global_data = {};
function fr_global( k, v, inc ){
    if( typeof v != 'undefined' ){
        if( inc ){
            var old = fr_global( k );
            if( typeof old == 'undefined' ){
                old = 0;
            }
            fr_global( k, old + v );
            return old + v;
        } else {
            fr_global_data[k] = v;
        }
    } else if( typeof fr_global_data[k] != 'undefined' ){
        return fr_global_data[k];
    }
}






// Normalize between mobile and desktop devices
function fr_mouse_touch_move( ob, callback ){
    $( ob ).on( 'mousemove touchmove', function( e ){
        if( typeof e.originalEvent.touches != 'undefined' ){
            var touch = e.originalEvent.touches[0];
            e.pageX = touch.pageX;
            e.pageY = touch.pageY;
        }
        callback( e );
    });
}

function fr_mouse_touch_down( parent, ob, callback ){

    if( !callback ){
        callback = ob;
        ob = '';
    }

    parent.on( 'mousedown touchstart', ob, function( e ){
        if( typeof e.originalEvent.touches != 'undefined' ){
            var touch = e.originalEvent.touches[0];
            e.pageX = touch.pageX;
            e.pageY = touch.pageY;
        }
        callback( e, $( this ) );
    });
}

function fr_mouse_touch_up( ob, callback ){
    $( ob ).on( 'mouseup touchend', callback );
}

function fr_supports( type ){
    if( type == 'canvas' ){
        var elem = document.createElement( 'canvas' );
        return !!( elem.getContext && elem.getContext( '2d' ) );
    }
}


function fr_array_sum( a ){
    return a.reduce( function( a, b ){ return a + b; } );
}



function fr_wait_for( ob, callback, resolution ){
    if( !resolution ){
        resolution = 250;
    }

    var init = function(){
        if( ( ob.length == 2 && typeof window[ob[0]] != 'undefined' && typeof window[ob[0]][ob[1]] != 'undefined' ) || ( ob.length == 1 && typeof window[ob[0]] != 'undefined' ) ){
            clearInterval( fr_wait_for_interval );
            callback();
        }
    }

    ob = ob.split( '.' );
    var fr_wait_for_interval = setInterval( init, resolution );
    init();
}



function fr_wp_ajax_form_with_files( action, form, add_data, remove_data, return_func, timeout = 60000, callback_status ){

    form.find( '[type="file"][value=""]' ).prop( 'disabled', true );

    var fd = new FormData( form[0] );

    form.find( '[type="file"][value=""]' ).prop( 'disabled', false );

    if( remove_data ){
        for( k in remove_data ){
            fd.delete( remove_data[k] );
        }
    }

    if( add_data ){
        for( k in add_data ){
            fd.append( k, add_data[k] );
        }
    }

    $( form ).find( '.submitted[type="submit"][name]' ).each( function(){ 
        fd.append( $( this ).attr( 'name' ), 1 );
        $( this ).removeClass( 'submitted' );
    } );

    if( !fd.has( 'action' ) ){
        fd.append( 'action', action );
    }


    $.ajax({ 
        url: ajax_url,
        type: 'post',
        dataType : "html",
        data: fd, 
        contentType: false, 
        processData: false,
        cache: false,
        timeout: timeout,
        success: function( data, textStatus, xhr ){
            if( return_func ){
                return_func( fr_json_decode( data ), xhr );
            }
        },
        error: function( xhr, textStatus ){
            if( return_func ){
                return_func( fr_json_decode( xhr.responseText ), xhr );
            }
        },
        xhr: function() {

            let xhr = new window.XMLHttpRequest();

            if( !callback_status ) {
                return xhr;
            }

            xhr.upload.addEventListener( 'progress', function( evt ) {

                if( evt.lengthComputable ) {
                    let percent_complete = parseInt( evt.loaded / evt.total * 100 );
                    callback_status( percent_complete );
                }

            }, false );

            return xhr;

        }
    });
}


function fr_json_decode( data ){
    var decoded = null;
    try {
        decoded = JSON.parse( data );
        if( typeof decoded !== 'object' && typeof decoded !== 'array' ){
            decoded = null;
        }
    } catch (e) {}
    return decoded;
}


var fr_supports_webp_result;

function fr_supports_webp() {
    return new Promise((resolve) => {
        if (typeof fr_supports_webp_result !== 'undefined') {
            resolve(fr_supports_webp_result);
        } else {
            const webpDataURI = 'data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAAAAAAfQ//73v/+BiOh/AAA=';
            
            fetch(webpDataURI)
                .then((response) => response.blob())
                .then(createImageBitmap)
                .then(() => {
                    fr_supports_webp_result = true;
                    resolve(true);
                })
                .catch(() => {
                    fr_supports_webp_result = false;
                    resolve(false);
                });
        }
    });
}




function fr_key_sort( unordered ){
    var ordered = {};
    Object.keys(unordered).sort().forEach(function(key) {
        ordered[key] = unordered[key];
    });
    return unordered;
}



function fr_mCustomScrollbar_top( ob ){
    var scrollTop = $( ob ).find( '.mCSB_container' ).css( 'top' ).replace( 'px', '' );
    scrollTop = Number( scrollTop ) * -1;
    return scrollTop;
}



/**
 * Allows form elements to show or hide based on options selected
 * Works only for radios currently
 */

function fr_form_enable_conditionals(){
    $( 'body' ).on( 'change', '[type="radio"],[type="hidden"]', function(){
        check( $( this ) );
    } );
    check( $( '[type="radio"]:checked,[type="hidden"]' ) );

    function check( obs ){
        obs.each( function(){
            var ob = $( this );
            var form = ob.parents( 'form' );
            var val = ob.val();
            show_hide( form, ob.attr( 'name' ), val );
        });
    }

    function show_hide( form, name, val ){
        form.find( '[data-fr-conditional^="' + name + ':"]' ).addClass( 'hide' );
        var conditionals = form.find( '[data-fr-conditional="' + name + ':' + $.escapeSelector( val ) + '"]' );
        if( conditionals.length ){
            conditionals.removeClass( 'hide' );
        }
    }
}



function fr_focus_input( ob ){
    $( ob ).trigger( 'focus' );
    var tmpStr = $( ob ).val();
    $( ob ).val('');
    $( ob ).val( tmpStr );
}


function fr_init_local_time( ob, month_names ){

    if( !ob ){
        ob = $( 'body' );
    } else {
        ob = $( ob );
    }

    ob.find( '[data-local-time][data-time-format]:not( [data-is-local-date] )' ).each( function(){
        var time = Number( $( this ).attr( 'data-local-time' ) );
        var format = $( this ).attr( 'data-time-format' );
        var new_date = fr_date( format, time * 1000, month_names );

        $( this ).text( new_date );
        $( this ).attr( 'data-is-local-date', 1 );
    } );
}


function fr_date( format, time, month_names ){

    format = format ?? 'Y-m-d H:i:s';

    var custom_month_names = month_names ? true : false;

    if( !month_names ){
        month_names = [
            "January", 
            "February", 
            "March", 
            "April", 
            "May", 
            "June",
            "July", 
            "August", 
            "September", 
            "October", 
            "November", 
            "December"
        ];
    }

    if( !time ){
        time = new Date().getTime();
    }

    var date = new Date( time );
    var year = date.getFullYear();
    var month = date.getMonth();
    var hours = date.getHours();
    var vars = {
        Y: year,
        y: year.toString().substr( 2, 2 ),
        m: ( month + 1 ).toString().padStart( 2, '0' ),
        F: month_names[month],
        M: month_names[month].toString().substr( 0, 3 ),
        d: date.getDate().toString().padStart( 2, '0' ),
        j: date.getDate(),
        H: hours.toString().padStart( 2, '0' ),
        g: hours <= 12 ? hours.toString().padStart( 2, '0' ) : hours - 12,
        i: date.getMinutes().toString().padStart( 2, '0' ),
        s: date.getSeconds().toString().padStart( 2, '0' ),
        a: hours <= 12 ? 'am' : 'pm'
    };

    new_date = format.split( '' );

    for( k in new_date ){
        if( typeof vars[new_date[k]] != 'undefined' ){
            new_date[k] = vars[new_date[k]];
        }
    }

    if( !custom_month_names ){
        var locale = fr_setting( 'locale' );
        if( locale ){
            vars.F = fr_uc_first( date.toLocaleDateString( locale, { month: 'long' } ) );
            vars.M = fr_uc_first( date.toLocaleDateString( locale, { month: 'short' } ) );
        }
    }

    return new_date.join( '' ) ;
}



function fr_uc_first( s ){
    return s.substr( 0, 1 ).toUpperCase() + s.substr( 1 );
}




function fr_extract_emails( text, only_valid = true ){

    var matches = [];
    text = " " + text + " ";

    while( 1 ){

        var res = text.match( /[^a-z0-9_\-\+.]([a-z0-9_\-\+.]+[ ]?(?:@|\(at\)|\[at\])[ ]?[a-z0-9\-]+[ ]?(?:\.|\(dot\)|\[dot\])[ ]?[\(\[]?\(?(?:[a-z]{2,3})(?:\.[a-z]{2})?[\)\]]?)[^a-z0-9]/mi );

        if( res && typeof res[1] != 'undefined' && res[1] != '' ){
                if( !only_valid || fr_is_email_valid( res[1] ) ){
                        matches.push( res[1] );
                }
                text = text.replace( res[1], '' );
                continue;
        }

        break;
    }

    return matches;
}

function fr_is_email_valid( email ) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
}

function fr_extract_phones( text ){

    text = " " + text + " ";
    matches = [];

    while( 1 ){

        var res = new RegExp( '[^0-9]((1-)?(([+][0-9]{10,11})|([+][0-9]{1,2}( |-)?[0-9]{1,3}( |-)?[0-9]{1,3}( |-)?[0-9]{1,3})|([0-9]{12})|([0-9]{3}( |-)[0-9]{3}( |-)[0-9]{3}( |-)[0-9]{3})|([0-9]{2,3}( |-)[0-9]{4}( |-)[0-9]{3}( |-)[0-9]{3})|(\\([0-9]{3}\\)( |-)[0-9]{4}( |-)[0-9]{3}( |-)[0-9]{3})))[^0-9]', 'gim' ).exec( text );

        if( res && typeof res[1] != 'undefined' && res[1] != '' ){
                matches.push( res[1] );
                text = text.replace( res[1], '' );
                continue;
        }

        break;
    }

    return matches;
}

function fr_clone_var( ob ){
    if( typeof ob == 'object' || typeof ob == 'array' ){
        return JSON.parse( JSON.stringify( ob ) );
    } else {
        return ob;
    }
}



var fr_is_page_unloading_result = false;
$( window ).on( 'unload', function(){ 
    fr_is_page_unloading_result = true;
} );
function fr_is_page_unloading(){
    return fr_is_page_unloading_result;
}



function fr_maybe_scroll_to_selector( ob, offset, speed, ob_to_scroll ){

    ob = $( ob );

    if( !ob.length ){
        return;
    }

    if( !offset ){
        offset = 0;
    }
    var top = $( window ).scrollTop() + offset;

    if( !ob_to_scroll ){
        ob_to_scroll = $( 'body,html' );
        var top = ob.offset().top;
    } else {
        var top = ob_to_scroll.scrollTop() + ob.position().top;
    }

    top -= offset;

    if( ob_to_scroll.scrollTop() > top || ob_to_scroll.scrollTop() > top + ob_to_scroll.height() ){
        ob_to_scroll.animate( { scrollTop: top }, speed );
        return true;
    }
}




function fr_scroll_to_selector( ob, offset, speed, ob_to_scroll ){

    if( !offset ){
        offset = 0;
    }

    var top = $( window ).scrollTop() + offset;

    if( !ob_to_scroll ){
        ob_to_scroll = $( 'body,html' );
        var top = $( ob ).offset().top;
    } else {
        var top = ob_to_scroll.scrollTop() + ob.position().top;
    }

    top -= offset;

    ob_to_scroll.animate( { scrollTop: top }, speed );
}





function fr_offset_to_parent( ob, parent ){
    var top = $( ob ).position().top;
    $( ob ).parents().each( function(){
        if( $( this ).is( parent ) ){
            return false;
        }
        top += $( this ).position().top;
    });
    return top;
}




function fr_acordeon( group, scroll_offset ){
    $( group ).find( '.handle' ).on( 'click', function( e ){

        if( e.isDefaultPrevented() ){
            return;
        }

        var ob = $( this );
        var item = ob.parent();

        if( !$( this ).parent().is( '.open' ) ){
            open( item, scroll_to_item );
            $( this ).next().slideDown( 200 );
        } else {
            close( item, scroll_to_item );
        }

    } );

    function open( ob, callback ){
        ob.find( '.content' ).slideDown( 200, function(){ 
            if( callback ){
                callback( ob );
            }
        } );
        ob.addClass( 'open' );
    }

    function close( ob, callback ){
        ob.find( '.content' ).slideUp( 200, function(){ 
            if( callback ){
                callback( ob );
            }
        } );
        ob.removeClass( 'open' );
    }

    function scroll_to_item( ob ){
        fr_maybe_scroll_to_selector( ob, scroll_offset, 200 );
    }

    $( group ).find( '.open .content' ).show();
}


function fr_ajax_update_selectors( selectors, callback ){

    if( typeof selectors != 'object' ){
        selectors = [ selectors ];
    }

    $.get( {
        url: fr_current_url(),
        cache: false,
        success: function( html ){
            $( 'body' ).attr( 'class', fr_get_body_class_from_string( html ) );
            for( k in selectors ){
                $( selectors[k] ).html( $( '<div>' + html + '</div>' ).find( selectors[k] ).html() );
            }

            if( callback ){
                callback( html );
            }
        }
    } );
}

function fr_get_body_class_from_string( html ){
    return html.match(/body\sclass=['|"]([^'|"]*)['|"]/)[1];
}

function fr_gf_remove_errors( ob ){
    ob.find( '.gfield_error' ).removeClass( 'gfield_error' );
    ob.find( '.gfield .validation_message' ).html( '' );
    ob.find( '.validation_error' ).remove();
}



function fr_add_class_on_drag( ob ){

    var ob = $( ob );
    var count = 0;

    $( document ).on( 'dragenter', function(){
        count++;
        ob.addClass( 'dragging' );
    } );

    $( document ).on( 'dragleave', function(){// dragend drop
        count--;
        if( count < 0 ){
            count = 0;
        }

        if( count > 0 ){
            return;
        }

        setTimeout( function(){
            ob.removeClass( 'dragging' );
        }, 100 );
    } );

    $( document ).on( 'dragend drop', function(){
        count = 0;
        setTimeout( function(){
            ob.removeClass( 'dragging' );
        }, 100 );
    } );
}



/**
 * Format a number as a price
 */
function fr_price( amount ){
    amount = Number( amount );
    return amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}


function fr_base64_encode( s ){
    return btoa( encodeURIComponent( s ) );
}


function fr_tmce_get_content( editor_id, textarea_id ) {
    if ( typeof editor_id == 'undefined' ) editor_id = wpActiveEditor;
    if ( typeof textarea_id == 'undefined' ) textarea_id = editor_id;
    
    if( typeof wp != 'undefined' && typeof wp.data != 'undefined' ){
        // Gutenberg
        return wp.data.select( "core/editor" ).getEditedPostContent();
    } else if ( jQuery('#wp-'+editor_id+'-wrap').hasClass('tmce-active') && tinyMCE.get(editor_id) ) {
        return tinyMCE.get(editor_id).getContent();
    }else{
        return jQuery('#'+textarea_id).val();
    }
}




function fr_isset( array, key ){
    return typeof array[key] != 'undefined';
}




function fr_same_height( obs ){

    init();
    $( window ).on( 'resize', init );
    fr_add_filter( 'fr_same_height', init );

    function init(){
        $( obs ).each( function(){
            var max_height = 0;
            $( this ).find( '>*' ).outerHeight( '' );
            $( this ).find( '>*' ).each( function(){
                var height = $( this ).outerHeight();
                if( height > max_height ){
                    max_height = height;
                }
            } );
            $( this ).find( '>*' ).outerHeight( max_height );
        } );
    }
}



function fr_on_scroll_select_links( links, container ){

    links = $( links );
    if( !container ){
        container = $( 'body' );
    } else {
        container = $( container );
    }

    $( window ).on( 'scroll', function(){

        var window_height = $( window ).height();
        var window_top = $( window ).scrollTop();

        links.removeClass( 'active' );

        $( '[id]', container ).each( function(){
            var ob = $( this );
            var top = ob.offset().top;
            var height = ob.outerHeight();


            // Middle is in view
            if( window_top > top && top + height > window_top + window_height / 2 ){
                var link = links.filter( '[href="#' + $( this ).attr( 'id' ) + '"]' );

                if( !link.length ){
                    return;
                }

                link.addClass( 'active' );
                return false;
            }


            // Top is in view
            if( window_top < top && top < window_top + window_height / 2 ){
                var link = links.filter( '[href="#' + $( this ).attr( 'id' ) + '"]' );

                if( !link.length ){
                    return;
                }

                link.addClass( 'active' );
                return false;
            }
        } );

    } );
}




function fr_onload( $f, timeout ){
    if( !timeout ){
        timeout = 1;
    }
    window.addEventListener( 'load', function(){
        setTimeout( $f, timeout );
    }, true);
}


function fr_on_scroll( ob, callback ){

    document.addEventListener( 'scroll', function ( event ) {
        var ob2 = $( ob    );

        if( !ob2.length ){
            return;
        }

        if ( event.target !== ob2[0] ) {
             return;
        }

        callback();

    }, true );
}



function fr_column( a, key, val ){
    var b = {};
    for( k in a ){
        b[a[k][key]] = a[k][val];
    }
    return b;
}





function fr_gutenberg_save_post( callback ){

    var int = setInterval( function(){
        if( !$( '.is-primary[class*="editor-post-"]:not(.is-busy):not([aria-disabled="true"])' ).length ){
            return;
        }
        clearInterval( int );
        
        if( callback ){
            callback();
        }
    }, 100 );
}



function fr_is( ob, k ){
    return typeof ob[k] != 'undefined';
}



function fr_trim( s, char ){
    if( !char ){
        char = ' ';
    }
    while( 1 ){
        if( s.substr( 0, 1 ) === char ){
            s = s.substr( 1, s.length );
        } else if( s.substr( s.length - 1, 1 ) === char ){
            s = s.substr( 0, s.length - 1 );
        } else {
            break;
        }
    }
    return s;
}


function fr_array_unique( a ){
    return Array.from( new Set( a ) );
}



function fr_allowed_characters( ob, allowed, not_allowed ){

    if( typeof allowed != 'object' ){
        allowed = [ allowed ];
    }

    var new_allowed = '';
    for( k in allowed ){
        if( allowed[k].length == 3 && allowed[k][1] == '-' ){
            var parts = allowed[k].split( '-' );
            var start = parts[0].charCodeAt( 0 );
            var end = parts[1].charCodeAt( 0 );
            for( x = start; x < end; x++ ){
                new_allowed += String.fromCharCode( x );
            }
        } else {
            new_allowed += allowed[k];
        }
    }
    allowed = new_allowed;

    $( 'body' ).on( 'keyup', ob, function( e ){
        var val = $( this ).val().split( '' );
        var orig = val;
        var new_val = '';
        for( k in val ){
            if( allowed && allowed.split( val[k] ).length == 1 ){
                continue;
            }
            if( not_allowed && not_allowed.split( val[k] ).length > 1 ){
                continue;
            }

            new_val += val[k];
        }

        if( orig === new_val ){
            return;
        }

        $( this ).val( new_val );
    } )

}



function fr_is_window_focused(){
    return document.hasFocus();
}





function fr_input_to_array( ob ){

    var a = {};

    $( ob ).each( function(){

        var val = $( this ).val();
        var name = $( this ).attr( 'name' );
        if( name ){
            name = name.replace( '[]', '' );
        }

        if( $( this ).is( '[type="checkbox"]' ) ){
            if( typeof a[name] == 'undefined' ){
                a[name] = [];
            }
            if( !$( this ).is( ':checked' ) ){
                return;
            }
            a[name].push( val );
            return;
        }

        if( $( this ).is( '[type="radio"]' ) ){
            if( typeof a[name] == 'undefined' ){
                a[name] = false;
            }
            if( !$( this ).is( ':checked' ) ){
                return;
            }
        }

        a[name] = val;
    } );

    return a;

}






/**
 * Returns the value of a css attribute as a numeric value
 */
function fr_css_nr( ob, attr ){
        return Number( $( ob ).css( attr ).replace( 'px', '' ) );
}






function fr_fix_for_jquery_block_execution_events(){

    jQuery.event.special.touchstart = {
        setup: function( _, ns, handle ){
            if ( $.inArray( 'noPreventDefault', ns ) != -1 ) {
                this.addEventListener( 'touchstart', handle, { passive: false });
            } else {
                this.addEventListener( 'touchstart', handle, { passive: true });
            }
        }
    };

    jQuery.event.special.touchmove = {
        setup: function( _, ns, handle ){
            if ( $.inArray( 'noPreventDefault', ns ) != -1 ) {
                this.addEventListener( 'touchmove', handle, { passive: false });
            } else {
                this.addEventListener( 'touchmove', handle, { passive: true });
            }
        }
    };

    jQuery.event.special.mousewheel = {
        setup: function( _, ns, handle ){
            if ( $.inArray( 'noPreventDefault', ns ) != -1 ) {
                this.addEventListener( 'mousewheel', handle, { passive: false });
            } else {
                this.addEventListener( 'mousewheel', handle, { passive: true });
            }
        }
    };

}










function fr_uppercase_first_letter( string ) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function fr_script_exists( s ){
    return $( '[src="' + s + '"]' ).length;
}


function fr_window_hash(){
    var hash = window.location.hash.replace( '#', '' );

    if( hash.split( '=' ).length > 1 || hash.split( '&' ).length > 1 ){
        return;
    }

    return hash;
}


function fr_array_diff( a, b ){
    return a.filter(function(i) {
        return b.indexOf( i ) < 0;
    });
}










function fr_popup_center( url, title, w, h ){

        var dualScreenLeft = window.screenLeft !==        undefined ? window.screenLeft : window.screenX;
        var dualScreenTop = window.screenTop !==        undefined         ? window.screenTop        : window.screenY;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        //var systemZoom = width / window.screen.availWidth;
        var systemZoom = 1;
        var left = (width - w) / 2 / systemZoom + dualScreenLeft;
        var top = (height - h) / 2 / systemZoom + dualScreenTop;
        
        var newWindow = window.open( url, title, 'scrollbars=yes,width=' + ( w / systemZoom ) + ', height=' + ( h / systemZoom ) + ', top=' + top + ', left=' + left );

        if (window.focus) newWindow.focus();
}





function fr_center_in_view( ob ) {

    if( !ob.is( ':visible' ) ){
        return;
    }

    let window_height = window.innerHeight;
    let element_height = ob.outerHeight();
    let element_offset = ob.offset().top;

    let center_position = element_offset - ( window_height - element_height ) / 2;

    window.scrollTo( 0, Math.max( 0, center_position ) );

}





function fr_rand( min, max ) {
    return Math.floor( min + Math.random() * ( max - min ) );
}






function fr_array_rand( a ) {
        var j, x, i;
        for (i = a.length - 1; i > 0; i--) {
                j = Math.floor(Math.random() * (i + 1));
                x = a[i];
                a[i] = a[j];
                a[j] = x;
        }
        return a;
}



function fr_keep_character_range( s, min_ord, max_ord ){

    s = s.split( '' );

    for( k in s ){

        var code = s[k].charCodeAt( 0 );

        if( code >= min_ord && code <= max_ord ){
            continue;
        }

        s[k] = '';

    }

    s = s.join( '' );

    return s;

}


function fr_remove_multiple_space_characters( s ){
    return s.replace(/\s+/g, ' ');
}


function fr_esc_html( s ){
    return s.replaceAll('&', '&amp;').replaceAll('<', '&lt;').replaceAll('>', '&gt;').replaceAll('"', '&quot;').replaceAll("'", '&#039;');
}


var fr_html_decode_textarea = document.createElement( 'textarea' );
function fr_html_decode( s ){
        fr_html_decode_textarea.innerHTML = s;
        return fr_html_decode_textarea.value;
}




function fr_flip( a ){

    var b = {};
    for( let k in a ){
        b[a[k]] = k;
    }
    return b;

}



function fr_replace( a, a2, string ){

    a = typeof a == 'object' ? a : [ a ];
    a2 = typeof a2 == 'object' ? a2 : [ a2 ];
    var last = a2[a2.length - 1];

    for( k in a ){

        a2[k] = typeof a2[k] == 'undefined' ? last : a2[k];
        string = string.split( a[k] ).join( a2[k] );

    }

    return string;

}


function fr_replace_loop( a, a2, s ){


    while( true ){
        var new_s = s.replace( a, a2 );

        if( new_s === s ){
            return s;
        }

        s = new_s;
    }

}


function fr_string_has_text( s ){
 
    s = fr_replace( [ '%s', '%1$s', '%2$s', '%3$s', '%d' ], '', s );
    return /[a-zA-Z]/.test( s );

}




function fr_closest_string( string, parts ){

    var found = [];

    for( var i = 0, l = parts.length; i < l; i++ ){

        if( string.indexOf( parts[i] ) !== -1 ){
            found.push( parts[i] );
        }
    }

    if( !found ){
        return false;
    }

    return found[0];

}



function fr_input_values_to_multidimensional_object( objects ){

    let serializedArr = objects.serializeArray();

    var obj = {};

    $.each(serializedArr, function() {
        var keys = this.name.replace(/\[/g, '.').replace(/\]/g, '').split('.');
        var lastObj = obj;

        for (var i = 0; i < keys.length; ++i) {
            var key = keys[i];

            if (!(key in lastObj)) {
                if (i !== keys.length - 1) {
                    lastObj[key] = {};
                } else {
                    lastObj[key] = this.value;
                }
            }

            if(i !== keys.length - 1){
                lastObj = lastObj[key];
            }
        }
    });

    return obj;

}





function fr_reload_content( url, selector, update_callback, before_on_change_callback, on_change_callback, callback ) {

    url = url ? url : window.location.href;

    $.get( url, function( data ) {

        var temp_div = $( '<div>' );
        temp_div.html( data );

        if( typeof selector === 'string' ) {
            update_content( selector, temp_div );
        } else if( Array.isArray( selector ) ) {
            selector.forEach( function( single_selector ) {
                update_content( single_selector, temp_div );
            } );
        }

        if( callback ) {
            callback();
        }

    } );



    function update_content( single_selector, temp_div ) {

        var new_content = temp_div.find( single_selector ).text();
        var current_content = $( single_selector ).text();

        if( new_content !== current_content ) {

            if( before_on_change_callback ){
                before_on_change_callback( single_selector, temp_div.find( single_selector ) );
            }

            if( update_callback ) {
                update_callback( single_selector, temp_div.find( single_selector ) );
            } else {
                $( single_selector ).html( temp_div.find( single_selector ).html() );
            }

            if( on_change_callback ){
                on_change_callback( single_selector, temp_div.find( single_selector ) );
            }
        }

    }

}





function track_inp_long_tasks(){

    const observer = new PerformanceObserver((list) => {
        list.getEntries().forEach((entry) => {

            if (entry.entryType === 'longtask') {

                const attribution = entry.attribution[0];
                  
                const scriptUrl = attribution.containerSrc;
                const startTime = entry.startTime;
                const duration = entry.duration;
                const endTime = startTime + duration;
                
                console.log(`Long task detected:`);
                console.log(`Script URL: ${scriptUrl}`);
                console.log(`Duration: ${duration}`);
                console.log(`Start Time: ${startTime}`);
                console.log(`End Time: ${endTime}`);

            }
        });
    });

    observer.observe({ entryTypes: ['longtask'], durationThreshold: 20 });

}






function fr_on_dom_change( target, callback ){

    var target = $( target );

    // Create a new MutationObserver instance
    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        callback( mutation );
      });
    });

    // Configure the observer
    var config = {
      childList: true, // Observe direct children
      subtree: true // Observe descendants
    };

    // Start observing the target element
    observer.observe(target[0], config);

}




