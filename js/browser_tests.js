
$( window ).ready( function(){ 
  if( $( '#fr_browser_tests' ).length ){
    fr_init_browser_tests();
  }
} );

function fr_init_browser_tests(){

  var instructions;
  var cookie;
  var response;
  var current_key = 0;
 
  if( $( '#fr_browser_tests' ).length ){
    var s = JSON.parse( $( '#fr_browser_tests' ).html() );
  } else {
    return;
  }


  $( window ).on( 'error', function( e ){
    response.js_errors.push( e.originalEvent.error.stack );
  } );


  loop( s );

  function loop( s ){

    var data = s;

    instructions = data.module;
    cookie = fr_object_to_array( data.cookie );
    for( k in cookie ){
      cookie[k] = Number( cookie[k] );
    }

    response = {
      executed_steps: {},
      js_errors: [],
      current_module: fr_clone_var( data.cookie ),
      current_url: fr_current_url()
    };


    for( key in instructions ){
      var ob;

      current_key = Number( key );

      if( cookie[3] > key ){
        continue;
      }

      var instruction = fr_clone_var( instructions[key] );
      response.executed_steps[key] = instruction;

      // Check IF
      if( typeof instruction.if != 'undefined' ){
        var ok = true;
        var ob;

        // Check conditions. They execute as AND
        for( k in instruction.if ){
          var condition = instruction.if[k];
          if( typeof condition.selector != 'undefined' ){
            ob = $( condition.selector );
            if( !ob.length ){
              response.executed_steps[key]['if'][k]['success'] = false;
              ok = false;
              break;
            } else {
              response.executed_steps[key]['if'][k]['success'] = true;
            }
          }
        }

        if( ok ){
          // If conditions are meet then execute "then"
          for( k in instruction.then ){
            var ob;
            var then = instruction.then[k];

            // Selector
            if( typeof then.selector != 'undefined' ){
              ob = $( then.selector );
              if( ob.length ){
                response.executed_steps[key]['if'][k]['success'] = true;
              } else {
                response.executed_steps[key]['then'][k]['error'] = 'Missing Selector';
                next_test_group();
                return;
              }
            }

            // Actions
            if( typeof then.action != 'undefined' ){

              // Click
              if( then.action == 'click' ){
                if( ob && ob.length ){
                  response.executed_steps[key]['then'][k]['success'] = true;
                  if( ob.is( '[href]' ) ){
                    var href = ob.attr( 'href' );
                    if( redirect( href ) ){
                      unload();
                      return;
                    }
                  } else {
                    ob.first().trigger( 'click' );
                  }
                } else {
                  response.executed_steps[key]['then'][k]['success'] = false;
                }
              }

            }

          }
        } else {
          response.executed_steps[key]['if']['success'] = false;
        }
      }

      // Selector
      if( typeof instruction.selector != 'undefined' ){
        ob = $( instruction.selector );
        if( ob.length ){
          response.executed_steps[key]['success'] = true;
        } else {
          response.executed_steps[key]['error'] = 'Missing Selector';
          next_test_group();
          return;
        }
      }

      // Selector Wait
      if( typeof instruction.selector_wait != 'undefined' ){
        var interval = setInterval( function(){ 
          ob = $( instruction.selector_wait );

          // When it finds the selector continue with testing
          if( ob.length ){
            clearInterval( interval );
            clearTimeout( timeout );
            cookie[3] = current_key + 1;
            response.executed_steps[key]['success'] = true;
            save( function( data ){ 
              if( data ){
                fr_init_browser_tests( data );
              }
            } );
          }
        }, 100 );

        // If 10 seconds pass then issue an error and move to the next test group
        var timeout = setTimeout( function(){
          response.executed_steps[key]['error'] = 'Timeout. Missing Selector';
          clearInterval( interval );
          next_test_group();
        }, 10000 );

        return;
      }

      // Set value
      if( typeof instruction.value != 'undefined' ){
        if( ob && ob.length ){

          if( ob.is( '[type="file"]' ) ){
            ob.attr( 'type', 'text' );
            instruction.value = 'fr_test_file=' + instruction.value;
          }

          ob.val( instruction.value );
          ob.trigger( 'change' );
          ob.trigger( 'input' );

          // Check if values was set
          if( ob.val() == instruction.value ){
            response.executed_steps[key]['success'] = true;
          } else {
            response.executed_steps[key]['error'] = 'Value not set';
            next_test_group();
            return;
          }
        } else {
          response.executed_steps[key]['success'] = false;
        }
      }

      // Actions
      if( typeof instruction.action != 'undefined' ){

        // Click
        if( instruction.action == 'click' ){
          if( ob && ob.length ){
            response.executed_steps[key]['success'] = true;
            cookie[3] = current_key + 1;

            if( ob.is( '[href]' ) ){
              var href = ob.attr( 'href' );
              unload();
              if( redirect( href ) ){
                return;
              }
            }
            ob.trigger( 'click' );
          } else {
            response.executed_steps[key]['success'] = false;
          }
        }

        // Check
        if( instruction.action == 'check' ){
          if( ob && ob.length ){
            if( ob.is( '[type="checkbox"]' ) || ob.is( '[type="radio"]' ) ){
              response.executed_steps[key]['success'] = true;
              ob.prop( 'checked', true );
            } else {
              response.executed_steps[key]['error'] = 'Element does not support prop checked';
              next_test_group();
              return;
            }
          } else {
            response.executed_steps[key]['success'] = false;
          }
        }


        // Uncheck
        if( instruction.action == 'uncheck' ){
          if( ob && ob.length ){
            if( ob.is( '[type="checkbox"]' ) || ob.is( '[type="radio"]' ) ){
              response.executed_steps[key]['success'] = true;
              ob.prop( 'checked', false );
            } else {
              response.executed_steps[key]['error'] = 'Element does not support prop checked';
              next_test_group();
              return;
            }
          } else {
            response.executed_steps[key]['success'] = false;
          }
        }


        // Submit
        if( instruction.action == 'submit' ){
          if( ob && ob.length ){
            if( ob.is( 'form' ) ){
              response.executed_steps[key]['success'] = true;
              unload();
              ob.trigger( 'submit' );
            } else {
              response.executed_steps[key]['error'] = 'Can\'t submit because element is not a form';
              next_test_group();
              return;
            }
          } else {
            response.executed_steps[key]['success'] = false;
          }
          return;
        }

        // Wait
        if( instruction.action == 'wait' ){
          unload();
          return;
        }

        // Page load
        if( instruction.action == 'wait_page_load' ){
          fr_on_load( function(){
            cookie[3] = current_key + 1;
            data.cookie = cookie;
            loop( data );
          } );
          return;
        }
      }


      // Go to another page
      if( typeof instruction.go_to != 'undefined' ){
        response.executed_steps[key]['success'] = true;
        cookie[3] = current_key + 1;
        if( redirect( instruction.go_to ) ){
          unload();
          return;
        }
      }


      // Scroll to
      if( typeof instruction.scroll_to != 'undefined' ){
        if( $( instruction.scroll_to ).length ){
          response.executed_steps[key]['success'] = true;
          $( window ).scrollTop( $( instruction.scroll_to ).offset().top );
        } else {
          response.executed_steps[key]['error'] = 'Missing Selector';
          next_test_group();
          return;
        }
      }


      // Contains
      if( typeof instruction.contains != 'undefined' ){
        if( ob && ob.length ){
          var text = ob.text();
          if( text.split( instruction.contains ).length > 1 ){
            response.executed_steps[key]['success'] = true;
          } else {
            response.executed_steps[key]['error'] = 'Text not found';
            next_test_group();
            return;
          }
        } else {
          response.executed_steps[key]['success'] = false;
        }
      }


      if( typeof instruction.wait != 'undefined' ){
        setTimeout( function(){
          cookie[3] = current_key + 1;
          data.cookie = cookie;
          loop( data );
        }, instruction.wait );
        return;
      }
    }

    cookie[2]++;
    cookie[3] = 0;
    save( function( data ){ 
      if( data ){
        loop( data );
        return;
      }
    } );
  }


  function unload(){
    cookie[3] = current_key + 1;
    save();
  }

  function redirect( link ){
    if( link != fr_current_url() ){
      fr_redirect( link );
    }
    if( link.split( '#' )[0] != fr_current_url().split( '#' )[0] ){
      return true;
    } else {
      return false;
    }
  }

  function save( callback ){
    fr_wp_ajax( 'fr_browser_tests_save', { data: response, cookie: cookie }, callback, 'post', { 'async': false } );
  }

  function next_test_group(){
    cookie[1]++;
    cookie[2] = 0;
    cookie[3] = 0;
    save( function( data ){ 
      if( data ){
        loop( data );
      }
    } );
  }
}
