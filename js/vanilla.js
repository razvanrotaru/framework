var fr_lazy_load_interval;
var fr_lazy_load_started;
function fr_init_lazy_load(){

    fr_lazy_load_started = true;

    function interval( padding, show_all ){
        if( fr_lazy_load_interval ){
            clearInterval( fr_lazy_load_interval );
        }

        fr_lazy_load_interval = setInterval( function(){
            fr_lazy_load( padding, show_all );
        }, 250 );
    }



    function load(){
        
        fr_lazy_load_started = true;

        setTimeout( function(){
            fr_lazy_load( 1000000, 'at_load' );
        }, 1 );

        fr( window ).on( 'scroll', fr_padding_interval );

        setTimeout( function(){

            // Clear interval if already running
            if( fr_lazy_load_interval ){
                clearInterval( fr_lazy_load_interval );
            }
            fr_lazy_load_interval = '';

            fr( window ).unbind( 'scroll', fr_no_padding_interval );
            fr( window ).unbind( 'scroll', fr_padding_interval );

            fr_lazy_load_started = false;
            fr_lazy_load( 1000000, 'after_load' );

            setInterval( function(){
                fr_lazy_load( 1000000, 'on_view' );
            }, 500 );
        }, 3000 );
    };

    function fr_no_padding_interval(){
        fr( window ).unbind( 'scroll', fr_no_padding_interval );
        interval( 0, true );
    }

    function fr_padding_interval(){
        fr( window ).unbind( 'scroll', fr_padding_interval );
        interval( 500 );
    }

    fr_lazy_load( 0 );
    fr_no_padding_interval();
    fr_on_load( load );


    function reinit(){
        fr_lazy_load( 0 );
        if( !fr_lazy_load_started && !fr_lazy_load_interval ){
            load();
        }
    }

    fr( window ).on( 'fr_ready', reinit );
    fr( window ).on( 'fr_reinit_lazy_load', reinit );
}


function fr_lazy_load( range, load_time ){

    fr( '[data-lazy-src],[data-lazy-style],[data-lazy-href],[data-lazy-style]' ).each(function( ob ){
        
        if( load_time != 'after_load' && ob.context.tagName != 'SCRIPT' ){
            var parents = ob.parents( false, true );
            for( k in parents ){
                if( !fr( parents[k] ).visible() ){
                    return;
                }
            }
        }


        var load_time_setting = ob.attr( 'data-load-time' );

        if( ( load_time != 'at_load' && load_time != 'after_load' ) && load_time_setting == 'at_load' ){
            return;
        }
        if( load_time == 'at_load' && load_time_setting != 'at_load' ){
            return;
        }
        if( load_time != 'after_load' && load_time_setting == 'after_load' ){
            return;
        }
        if( load_time_setting == 'on_view' ){

            var view_range = ob.attr( 'data-load-range' );

            if( !view_range ){
                view_range = 0;
            }

            view_range = Number( view_range );

            if( !fr_in_viewport( ob, 1000000, view_range ) ){
                return;
            }
        }
        if( range == 1000000 || fr_in_viewport( ob, 1000000, range ) ){
            if( ob.attr( 'data-lazy-src' ) ){

                if( ob.context.tagName == 'SCRIPT' ){
                    fr_lazy_load_script( ob );
                } else {
                    ob.attr("src",ob.attr("data-lazy-src"));
                    ob.removeAttr("data-lazy-src");
                }
            }
            if( ob.attr( 'data-lazy-href' ) ){
                ob.attr( 'href', ob.attr( 'data-lazy-href' ) );
                ob.removeAttr( 'data-lazy-href' );
            }
            if( ob.attr( 'data-lazy-srcset' ) ){
                ob.attr("srcset",ob.attr("data-lazy-srcset"));
                ob.removeAttr("data-lazy-srcset");
            }
            if( ob.attr( 'data-lazy-style' ) ){

                /* 
                Load background images as img tags
                Phone browsers do not resume background image download on back or forward navigation
                But they do for <img>
                */
                
                var style = ob.attr("data-lazy-style");
                if( style.split( 'url(' ).length > 1 ){
                    var src = style.split( 'url(' )[1].split( ')' )[0].split( '"' ).join( '' ).split( "'" ).join( '' ).trim();
                    var title = ob.attr( 'title' );
                    if( !title ){
                        title = ob.attr( 'data-alt' );
                    }
                    if( !title ){
                        title = ob.parent().attr( 'title' );
                    }

                    var img =document.createElement( 'img' );

                    img.setAttribute( 'src', src );
                    img.setAttribute( 'alt', title );

                    fr( 'body' ).append( '<div style="width:0;height:0;overflow:hidden">' + img.outerHTML + '</div>' );
                }

                if(ob.attr( 'style' ) ){
                    var style=ob.attr( "style" ) + ';' + ob.attr("data-lazy-style");
                }else{
                    var style=ob.attr("data-lazy-style");
                }
                ob.attr("style",style);
                ob.removeAttr("data-lazy-style");
            }
        }
    });

}




function fr_lazy_load_script( ob ){

    var script = document.createElement( 'script' );
    script.src = ob.attr( 'data-lazy-src' );
 
    for( k in ob.context.attributes ){
        if( ob.context.attributes[k].nodeName == 'data-lazy-src' ){
            continue;
        }
        if( typeof ob.context.attributes[k].nodeName == 'undefined' ){
            continue;
        }
        script.setAttribute( ob.context.attributes[k].nodeName, ob.context.attributes[k].nodeValue );
    }
    
    ob.before( script );
    ob.remove();
}




function fr_native_lazy_loading(){

    // Check if browser supports native image loading
    if( !( 'loading' in document.createElement( 'img' ) ) ){
        return;
    }

    fr( 'img.lazyload' ).each( function( ob ){

        ob.removeClass( 'lazyload' );
        ob.attr( 'loading', 'lazy' );

        if( ob.hasAttr( 'data-lazy-srcset' ) ){
            ob.attr( 'srcset', ob.attr( 'data-lazy-srcset' ) );
            ob.removeAttr( 'data-lazy-srcset' );
        }

        if( ob.hasAttr( 'data-lazy-src' ) ){
            ob.attr( 'src', ob.attr( 'data-lazy-src' ) );
            ob.removeAttr( 'data-lazy-src' );
        }

        if( ob.hasAttr( 'data-lazy-style' ) ){
            ob.attr( 'style', ob.attr( 'data-lazy-style' ) );
            ob.removeAttr( 'data-lazy-style' );
        }
    } );
}




function fr_in_viewport( ob, range_before, range_after ){

    range_before = typeof range_before != 'undefined' ? range_before : 0;
    range_after = typeof range_after != 'undefined' ? range_after : range_before;

    var html = document.scrollingElement || document.documentElement;
    var wst = html.scrollTop;
    var wh = window.innerHeight;
    var wsl = html.scrollLeft;
    var ww = window.innerWidth;
    var type = ob.context.tagName.toLowerCase();

    if( type == 'script' ){
        ob.css( 'display', 'block' );
    }

    var top = ob.offset().top;
    var height = ob.outerHeight();
    var left = ob.offset().left;
    var width = ob.outerWidth();

    if( type == 'script' ){
       ob.css( 'display', '' );
    }

    var top_window_bound = wst - range_before;
    var bottom_window_bound = wst + wh + range_after;
    var left_window_bound = wsl - range_before;
    var right_window_bound = wsl + ww + range_after;

    if(
        ( 
            ( top_window_bound <= top && bottom_window_bound >= top ) || 
            ( top_window_bound <= top + height && bottom_window_bound >= top + height ) || 
            ( top_window_bound > top && bottom_window_bound < top + height ) 
        ) && 
        ( 
            ( left_window_bound <= left && right_window_bound >= left ) || 
            ( left_window_bound <= left + width && right_window_bound >= left + width ) || 
            ( left_window_bound > left && right_window_bound < left + width ) 
        ) ){
        return 1;
    }
}


function fr_setting( k ){
    if( typeof fr_settings != 'undefined' && typeof fr_settings[ k ] != 'undefined' ){
        return fr_settings[ k ];
    }
}


var fr_start_timer_time;
function fr_start_timer(){
    fr_start_timer_time = window.performance.now();
}

function fr_get_timer(){
    return window.performance.now() - fr_start_timer_time;
}

function fr_show_timer(){
    console.log( fr_get_timer() );
}
function fr_get_load_time(){
    return window.performance.now();
}


function fr_debug(){
    console.log(new Error().stack);
}


function fr_on_load( callback, delay ){
    if( document.readyState == 'complete' ){
        if( delay ){
            setTimeout( callback, delay );
        } else {
            callback();
        }
    }else{
        fr( window ).on( 'load', function(){
            if( delay ){
                setTimeout( callback, delay );
            } else {
                callback();
            }
        } );
    }
}




/**
 * Makes two elements the same height dynamically
 * @param    ob rows            Object elements to be used as the parent row
 * @param    string cols    The columns of the row
 * @param    string height_ob The selector to modify the height
 */
function fr_make_same_height( rows, cols, height_ob ){

    fr( rows ).each( function( ob ){

        var groups = [];
        var group_index = 0;

        fr( cols, ob ).each( function( sub_ob ){

            if( sub_ob.index() && sub_ob.prev().exists() && sub_ob.offset().top != sub_ob.prev().offset().top ){
                group_index++;
            }

            if( typeof groups[group_index] == 'undefined' ){
                groups[group_index] = [];
            }

            if( height_ob ){
                sub_ob = sub_ob.find( height_ob );
            }

            groups[group_index].push( sub_ob );
        } );
        
        for( k in groups ){

            for( k2 in groups[k] ){
                groups[k][k2].css( 'min-height', '' );
            }

            if( groups[k].length <= 1 ){
                continue;
            }

            var max_height = 0;
            for( k2 in groups[k] ){
                var height = groups[k][k2].height();
                if( height > max_height ){
                    max_height = height;
                }
            }

            for( k2 in groups[k] ){
                groups[k][k2].css( 'min-height', max_height + 'px' );
            }
        }
    } );
}




function fr_add_filter(filter,func){
    if( typeof fr_filters == 'undefined' ){
        window.fr_filters=[];
    }
    if( typeof fr_filters[filter]=="undefined"){
        fr_filters[filter]=[];
    }
    fr_filters[filter].push( func );
}




function fr_apply_filter(filter,res,args){
    if( typeof fr_filters == 'undefined' ){
        window.fr_filters=[];
    }

    var args2=[res].concat(args);
    if(typeof fr_filters[filter]!="undefined"){
        for(k in fr_filters[filter]){
            if( fr_is_function( fr_filters[filter][k] ) ){
                res=fr_filters[filter][k].apply(null,args2);
            }else{
                res=fr_filters[filter][k];
            }
        }
    }
    return res;
}


function fr_is_function( functionToCheck ) {
 return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}


var delay_tmp=[];
function fr_delay(time,name,func){
    if(typeof delay_tmp[name]!="undefined"){
        return;
    }
    delay_tmp[name]=setTimeout(function(){
        delay_tmp[name]=undefined;
        if(!func){
            eval(name+"()");
        }else{
            func();
        }
    },time);
}

function fr_slice_obj( ob, start, end ){
    var ob_length = fr_object_length( ob );
    var ob_new = {};
    var x = -1;
    if( start < 0 ){
        start = ob_length + start;
    }
    if( !end ){
        end = ob_length;
    } else {
        end = start + end;
    }

    for( k in ob ){
        x++;
        if( x < start ){
            continue;
        }
        if( x >= end ){
            break;
        }
        ob_new[k] = ob[k];
    }

    return ob_new;
}


function fr_object_length( obj ) {
        var size = 0, key;
        for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
        }
        return size;
};


function fr_on_ready( callback ){
    if ( document.readyState === 'complete' || document.readyState === 'interactive' ) {
            setTimeout( callback, 1 );
    } else {
            document.addEventListener( 'DOMContentLoaded', callback );
    }
}


function fr_maybe_scroll_to_selector( ob, offset, speed ){
    if( !offset ){
        offset = 0;
    }
    var top = fr_scrolling_element().scrollTop() + offset;
    if( ob.context && ob.offset().top < top ){
        fr_scroll_to_selector( ob, offset, speed );
        return true;
    }
}


function fr_scroll_to_selector( ob, offset, speed ){
    if( !offset ){
        offset = 0;
    }
    if( !speed ){
        speed = 200;
    }
    fr_scrolling_element().animate( { scrollTop: ob.offset().top - offset }, speed );
}

function fr_scrolling_element() {
    return fr( document.scrollingElement || document.documentElement );
}



/**
 * It needs the object because on IPhone the browser will automatically scroll to the textarea
 */
function fr_copy_to_clipboard( ob, string ) {
    var textarea;
    var result;

    try {

        if( string ){
            textarea = document.createElement('textarea');
            textarea.setAttribute('readonly', true);
            textarea.setAttribute('contenteditable', true);
            textarea.style.position = 'fixed';
            textarea.value = string;
            ob.prepend( textarea );
        } else {
            textarea = ob.context;
        }

        textarea.select();

        var range = document.createRange();
        range.selectNodeContents( textarea );

        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);

        textarea.setSelectionRange( 0, textarea.value.length );
        result = document.execCommand( 'copy' );
    } catch (err) {
        return false;
    } finally {
        if( string ){
            ob.context.removeChild( textarea );
        }
    }
    return true;
}



function fr_popup_center( url, title, w, h ){

    var dualScreenLeft = window.screenLeft !==    undefined ? window.screenLeft : window.screenX;
    var dualScreenTop = window.screenTop !==    undefined     ? window.screenTop    : window.screenY;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var systemZoom = width / window.screen.availWidth;
    var left = (width - w) / 2 / systemZoom + dualScreenLeft;
    var top = (height - h) / 2 / systemZoom + dualScreenTop;
    
    var newWindow = window.open( url, title, 'scrollbars=yes,width=' + ( w / systemZoom ) + ', height=' + ( h / systemZoom ) + ', top=' + top + ', left=' + left );

    if (window.focus) newWindow.focus();
}




function fr_center_item( ob, parent, scroll_pos ){

    var inner_ob = ob.parents( parent );

    if( scroll_pos == 'width' ){
        var new_pos = ob.position().left + ob.outerWidth() / 2 - inner_ob.width() / 2;
        inner_ob.animate( { scrollLeft: new_pos }, 200 );
    } else {
        var new_pos = ob.position().top + ob.outerHeight() / 2 - inner_ob.height() / 2;
        inner_ob.animate( { scrollTop: new_pos }, 200 );
    }
}






function fr_object_remove_key( ob, index ){
    var new_ob = [];
    for( k in ob ){
        if( k == index ){
            continue;
        }
        new_ob[k] = ob[k];
    }
    return new_ob;
}




function fr_validate_ajax_response( xhr, error_messages ){
    var validation = true;
    var response = fr_json_decode( xhr.response );

    if( xhr.status === 0 ){
        if( xhr.statusText == 'timeout' ){
            validation = error_messages.failed_timeout;
        } else {
            validation = error_messages.failed_internet_connection + ' (' + xhr.statusText + ')';
        }
    } else    if( xhr.status < 200 || xhr.status >= 400 && xhr.status < 500 ){
        validation = error_messages.failed_to_reach_server + ' (' + xhr.status + ' - ' + xhr.statusText + ')';
    } else    if( xhr.status >= 500 ){
        validation = error_messages.failed_server_error + ' (' + xhr.status + ' - ' + xhr.statusText + ')';
    } else if( xhr.responseText === null || !response || typeof response.success == 'undefined' ){
        validation = error_messages.failed_unexpected_response;
        if( xhr.status != 200 ){
            validation += ' (' + xhr.status + ' - ' + xhr.statusText + ')';
        }
    }

    return validation;
}



function fr_serialize_form( form, data ){

    if( !data ){
        data = [];
    }

    fr( 'input,textarea,select', form ).each( function( ob ){

        var name = ob.attr( 'name' );
        var val = ob.val();

        if( !name ){
            return;
        }

        if( ob.attr( 'type' ) == 'checkbox' ){

            if( !ob.hasProp( 'checked' ) ){
                return;
            }

            if( typeof data[name] == 'undefined' ){
                data[name] = [];
            }

            data[name].push( val );



        } else    if( ob.attr( 'type' ) == 'radio' ){

            if( !ob.hasProp( 'checked' ) ){
                return;
            }

            data[name] = val;


        } else {
            data[name] = val;
        }
    } );


    return data;
}


function fr_json_decode( data ){
    var decoded = null;
    try {
        decoded = JSON.parse( data );
        if( typeof decoded !== 'object' && typeof decoded !== 'array' ){
            decoded = null;
        }
    } catch (e) {}
    return decoded;
}


function fr_object_remove_key( ob, index ){
    var new_ob = [];
    for( k in ob ){
        if( k == index ){
            continue;
        }
        new_ob[k] = ob[k];
    }
    return new_ob;
}


function fr_validate_ajax_response( xhr, error_messages ){
    var validation = true;
    var response = fr_json_decode( xhr.response );

    if( xhr.status === 0 ){
        if( xhr.statusText == 'timeout' ){
            validation = error_messages.failed_timeout;
        } else {
            validation = error_messages.failed_internet_connection + ' (' + xhr.statusText + ')';
        }
    } else    if( xhr.status < 200 || xhr.status >= 400 && xhr.status < 500 ){
        validation = error_messages.failed_to_reach_server + ' (' + xhr.status + ' - ' + xhr.statusText + ')';
    } else    if( xhr.status >= 500 ){
        validation = error_messages.failed_server_error + ' (' + xhr.status + ' - ' + xhr.statusText + ')';
    } else if( xhr.responseText === null || !response || typeof response.success == 'undefined' ){
        validation = error_messages.failed_unexpected_response;
        if( xhr.status != 200 ){
            validation += ' (' + xhr.status + ' - ' + xhr.statusText + ')';
        }
    }

    return validation;
}



function fr_serialize_form( form, data ){

    if( !data ){
        data = [];
    }

    fr( 'input,textarea,select', form ).each( function( ob ){

        var name = ob.attr( 'name' );
        var val = ob.val();

        if( !name ){
            return;
        }

        if( ob.attr( 'type' ) == 'checkbox' ){

            if( !ob.hasProp( 'checked' ) ){
                return;
            }

            if( typeof data[name] == 'undefined' ){
                data[name] = [];
            }

            data[name].push( val );



        } else    if( ob.attr( 'type' ) == 'radio' ){

            if( !ob.hasProp( 'checked' ) ){
                return;
            }

            data[name] = val;


        } else {
            data[name] = val;
        }
    } );


    return data;
}


function fr_json_decode( data ){
    var decoded = null;
    try {
        decoded = JSON.parse( data );
        if( typeof decoded !== 'object' && typeof decoded !== 'array' ){
            decoded = null;
        }
    } catch (e) {}
    return decoded;
}


function fr_stop_scrolling( ob, options ){

    if( ob.hasClass( 'fr_stop_scrolling' ) ){
        return;
    }

    options = options ? options : [];
    var scroll = ob.scrollTop();
    var width = ob.width();
    ob.addClass( 'fr_stop_scrolling' );
    ob.attr( 'data-initial-scroll', scroll );
    ob.attr( 'data-settings', JSON.stringify( options ) );

    var new_width = ob.width();
    if( new_width > width ){
        ob.css( 'padding-right', new_width - width + 'px' );
        if( typeof options.fixed_obs != 'undefined' ){
            fr( options.fixed_obs ).each( function( ob2 ){
                ob2.css( 'padding-right', new_width - width + 'px' );
            } );
        }
    }
}



function fr_stop_scrolling_remove( ob ){

    if( !ob.hasClass( 'fr_stop_scrolling' ) ){
        return;
    }

    var options = JSON.parse( ob.attr( 'data-settings' ) );
    var scroll = ob.attr( 'data-initial-scroll' );

    if( !options ){
        return;
    }

    ob.removeClass( 'fr_stop_scrolling' );
    ob.css( 'margin-top', '' );
    ob.css( 'padding-right', '' );
    ob.removeAttr( 'data-fixed-obs' );
    ob.removeAttr( 'data-initial-scroll' );

    ob.scrollTop( scroll );

    if( typeof options.fixed_obs != 'undefined' ){
        fr( options.fixed_obs ).each( function( ob2 ){
            ob2.css( 'padding-right', '0' );
        } );
    }
}




function fr_css_supported( prop, value ) {
    var d = document.createElement( 'div' );
    d.style[prop] = value;
    return d.style[prop] === value;
}


function fr_merge( a1, a2 ){
    for( k in a2 ){
        a1[k] = a2[k];
    }
    return a1;
}



function fr_has_scrollbar_add_class( ob_scrolled_parent, ob_scrolled, ob_with_class ){

    if( ob_scrolled_parent.outerWidth() == ob_scrolled.outerWidth() ){
        ob_with_class.removeClass( 'has_scrollbar_y' );
    } else {
        ob_with_class.addClass( 'has_scrollbar_y' );
    }

    if( ob_scrolled_parent.outerHeight() == ob_scrolled.outerHeight() ){
        ob_with_class.removeClass( 'has_scrollbar_x' );
    } else {
        ob_with_class.addClass( 'has_scrollbar_x' );
    }

}

/**
 * Returns the value of a css attribute as a numeric value
 */
function fr_css_nr( ob, attr ){
    return parseFloat( fr( ob ).css( attr ) );
}



function fr_parse_html( html ){
    var parser = new DOMParser();
    var dom = parser.parseFromString( html, 'text/html' );
    return fr( dom );
}






















var fr_calls = 0;
function fr( context, ob, constructing ){
    
    if ( !constructing ) {
            return new fr( context, ob, true );
    }

    fr_calls++;

    var root = document;

    this.is_fr = function( ob ){
        return typeof ob == 'object' && typeof ob.is_fr != 'undefined';
    }

    if( ob ){
        if ( this.is_fr( ob ) ) {
            root = ob.context;
        } else {
            root = ob;
        }
    }

    if( !context ){
        context = false;
    }

    if( this.is_fr( context ) ){
        context = context.context;
    }

    this.original_context = [ context, root ];
    this.context = context;
    if( typeof context == 'string' ){
        this.context = root.querySelector( context );
    }

    this.on = function( event, callback ) {

        if( !this.context ){
            return this;
        }

        event = event.split( ' ' );
        for( k in event ){
            this.context.addEventListener( event[k], callback );
        }

        return this;
    }

    this.unbind = function( event, callback ){
        this.context.removeEventListener( event, callback );
    }

    this.addClass = function( c ){
        this.context.classList.add( c );
        return this;
    }

    this.removeClass = function( c ){
        this.context.classList.remove( c );
        return this;
    }

    this.hasClass = function( c ){
        return this.context.classList.contains( c );
    }

    this.remove = function(){
        this.parent().context.removeChild( this.context );
    }

    this.toggleClass = function( c ){
    if( this.hasClass( c ) ){
        this.removeClass( c );
    } else {
        this.addClass( c );
    }
    return this;
    }

    this.val = function( val ){
        if( typeof val == 'undefined' ){
            return this.context.value;
        } else {
            this.context.value = val;
            return this;
        }
    }

    this.select = function( val ){
        this.context.select();
    }

    this.index = function(){
        if( !this.context ){
            return -1;
        }
        return Array.prototype.indexOf.call( this.context.parentElement.children, this.context );
    }

 
    this.ajax = function( options ){

        if( typeof window.fr_ajax_cache == 'undefined' ){
            window.fr_ajax_cache = {};
        }

        options = fr_merge( {
            url: '',
            method: 'POST',
            cache: false,
            cache_length: 1000,
            data: {},
            callback: false,
            form: false
        }, options );

        if( options.method == 'GET' && fr_object_length( options.data ) ){
            options.url += '?' + new URLSearchParams( options.data ).toString();
        }

        var request = new XMLHttpRequest();
        request.open( options.method, options.url );

        if( options.form ){
            if( typeof options.form == 'string' ){
                options.form = fr( options.form );
            }
            var form_data = new FormData( options.form.context );
        } else {
            var form_data = new FormData();
        }

        if( options.data ){
            for( k in options.data ){
                form_data.append( k, options.data[k] );
            }
        }


        if( options.cache && options.callback ){

            var values = {};
            if( typeof form_data.values != 'undefined' ){
                values = Array.from( form_data.values() );
            } else {
                if( options.form ){
                    values = fr_serialize_form( options.form, options.data );
                } else {
                    values = options.data;
                }
            }

            var cache_key = this.hash( [ options.url, options.method, values ] );
            if( typeof window.fr_ajax_cache[cache_key] != 'undefined' ){
                options.callback( window.fr_ajax_cache[cache_key], 200 );
                return;
            }
        }

        request.onreadystatechange = function() {
            
            if( this.readyState === 4 && options.callback ) {
                if( options.cache && this.status == 200 ){
                    window.fr_ajax_cache[cache_key] = this.responseText; 
                    window.fr_ajax_cache = fr_slice_obj( window.fr_ajax_cache, - options.cache_length, options.cache_length );
                }
                options.callback( this.responseText, this.status, this );
            }
        };

        request.send( form_data );

        return this;
    }



    this.width = function( val ){
        if( typeof val != 'undefined' ){
            this.css( 'width', val + 'px' );
            return this;
        } else {
            if( this.context == window ){
                return window.innerWidth;
            }
            return this.context.offsetWidth;
        }
    }

    this.outerWidth = function( val ){
        if( typeof val != 'undefined' ){
            return this;
        } else {
            if( this.context == window ){
                return this.context.outerWidth;
            } else {
                return this.context.clientWidth;
            }
        }
    }

    this.height = function( val ){
        if( typeof val != 'undefined' ){
            this.css( 'height', val + 'px' );
            return this;
        } else {
            if( this.context == window ){
                return window.innerHeight;
            }
            return this.context.offsetHeight;
        }
    }

    this.outerHeight = function(){
        if( this.context == window ){
            return this.context.outerHeight;
        } else {
            return this.context.clientHeight;
        }
    }

    this.html = function( html ){
        if( typeof html != 'undefined' ){
            this.context.innerHTML = html;
            return this;
        } else {
            return this.context.innerHTML;
        }
    }

    this.text = function( text ){
        if( typeof html != 'undefined' ){
            this.context.textContent = text;
            return this;
        } else {
            return this.context.textContent;
        }
    }
    
    this.each = function( callback ){

        if( arguments.length > 1 ){
            var args = fr_object_to_array( arguments );
            var method = args[0];
            args = args.slice( 1 );
            args = args.concat( [ null, null, null, null, null, null ] );

            callback = function( ob ){
                ob[method]( args[0], args[1], args[2], args[3], args[4], args[5] );
            }
        }

        if( typeof this.original_context[0] == 'string' ){
            var elements = this.original_context[1].querySelectorAll( this.original_context[0] );
            for( k in elements ){
                if( elements.hasOwnProperty( k ) ){
                    var res = callback( fr( elements[k] ) );
                    if( res === false ){
                        break;
                    }
                }
            }
        } else {
            if( this.original_context[0] ){
                callback( fr( this.original_context[0] ) );
            }
        }
        return this;
    }

    this.length = function(){
        
        var total = 0;

        if( typeof this.original_context[0] == 'string' ){
            var elements = this.original_context[1].querySelectorAll( this.original_context[0] );
            for( k in elements ){
                if( elements.hasOwnProperty( k ) ){
                    total++;
                }
            }
        } else if( typeof this.original_context[0] == 'object' ){
            total = 1;
        }

        return total;
    }

    this.children = function(){
        return this.context.children();
    }

    this.firstChild = function(){
        return fr( this.context.firstElementChild );
    }

    this.parent = function(){
        return fr( this.context.parentElement );
    }

    this.hasAttr = function( attr ){
        return this.context.hasAttribute( attr );
    }

    this.parents = function( selector, self ){
        if( selector ){
            return fr( this.context.closest( selector ) );
        } else {
            var nodes = [];
            var element = this.context;

            if( self ){
                nodes.push( element );
            }

            while( element.parentNode ) {
                if( element.parentNode == document.body ){
                    break;
                }
                nodes.push( element.parentNode );
                element = element.parentNode;
            }

            return nodes;
        }
    }

    this.prev = function(){
        return fr( this.context.previousElementSibling );
    }

    this.next = function(){
        return fr( this.context.nextElementSibling );
    }

    this.before = function(content) {
        if (typeof content === 'string') {
            this.context.insertAdjacentHTML('beforebegin', content);
        } else if (content instanceof Node) {
            this.context.parentNode.insertBefore(content, this.context);
        }
    };

    this.after = function(content) {
        if (typeof content === 'string') {
            this.context.insertAdjacentHTML('afterend', content);
        } else if (content instanceof Node) {
            if (this.context.nextSibling) {
                this.context.parentNode.insertBefore(content, this.context.nextSibling);
            } else {
                this.context.parentNode.appendChild(content);
            }
        }
    };

    this.prepend = function( html ){
        this.context.insertBefore( this.createElement( html ), this.context.firstChild );
    }
    this.append = function( html ){
        this.context.appendChild( this.createElement( html ) );
    }

    this.hide = function(){
        this.css( 'display', 'none' );
    }

    this.show = function(){
        this.css( 'display', 'block' );
    }

    this.createElement = function( html ){

        if( typeof html != 'string' ){
            return html;
        }

        var template = document.createElement( 'div' );
        html = html.trim();
        template.innerHTML = html;
        return template.firstChild;
    }

    this.css = function( k, val ){
        if( typeof val == 'undefined' ){
            return window.getComputedStyle( this.context )[k];
        } else {
            if( val === '' ){
                return this.context.style.removeProperty( k );
            } else {
                return this.context.style[k] = val;
            }
        }
    }

    this.css_to_number = function( k ){
        return Number( this.css( k ).replace( 'px', '' ) );
    }

    this.find = function( selector ){
        return fr( this.context.querySelector( selector ) );
    }

    this.attr = function( k, v ){
        if( typeof v == 'undefined' ){
            return this.context.getAttribute( k );
        } else {
            this.context.setAttribute( k, v );
            return this;
        }
    }

    this.removeAttr = function( k ){
        this.context.removeAttribute( k );
    }

    this.offset = function(){
        var rect = this.context.getBoundingClientRect();
        var win = this.context.ownerDocument.defaultView;
        return {
                top: rect.top + win.pageYOffset,
                left: rect.left + win.pageXOffset
        };
    }

    this.scrollTop = function( val ){
        if( typeof val == 'undefined' ){
            return this.context.scrollTop;
        } else {
            this.context.scrollTop = val;
            return this;
        }
    }

    this.scrollLeft = function( val ){
        if( typeof val == 'undefined' ){
            return this.context.scrollLeft;
        } else {
            this.context.scrollLeft = val;
            return this;
        }
    }

    this.visible = function(){
        return this.css( 'display' ) != 'none' && this.css( 'visibility' ) != 'hidden' &&    this.css( 'opacity' ) > 0;
    }

    this.hash = function( string ){

        if( typeof string == 'object' || typeof string == 'array' ){
            string = JSON.stringify( string );
        }

        var hash = 0, i, chr;
        for (i = 0; i < string.length; i++) {
            chr     = string.charCodeAt( i );
            hash    = ( ( hash << 5 ) - hash ) + chr;
            hash |= 0;
        }
        hash = hash.toString();
        return hash;
    }

    this.clone_ob = function( ob ){
        return JSON.parse( JSON.stringify( ob ) );
    }

    this.trigger = function( event, callback ){

        if( typeof document.createEvent != 'undefined' ) {
            var event_ob = document.createEvent( 'Event' );
            event_ob.initEvent( event, true, true);
            this.context.addEventListener( event, callback, false);
            this.context.dispatchEvent( event_ob );
        } else {
            event = new CustomEvent( event );
            this.context.dispatchEvent( event );
        }

        return this;
    }




    this.animate = function( keyframes, duration, callback ){

        var ob = this;

        if( typeof keyframes.scrollTop != 'undefined' || typeof keyframes.scrollLeft != 'undefined' ){
            if( callback ){
                setTimeout( callback, duration );
            }
        }

        if( typeof keyframes.scrollTop != 'undefined' ){
            this.animation_steps( this.context.scrollTop, keyframes.scrollTop, duration, function( val ){
                ob.context.scrollTop = val;
            } );
            return this;
        }


        if( typeof keyframes.scrollLeft != 'undefined' ){
            this.animation_steps( this.context.scrollLeft, keyframes.scrollLeft, duration, function( val ){
                ob.context.scrollLeft = val;
            } );
            return this;
        }



        for( key in keyframes ){

            if( key != 'height' && key != 'width' ){
                continue;
            }

            start = 0;

            if( key == 'height' ){
                start = this.height();
            }

            if( key == 'width' ){
                start = this.width();
            }

            this.css( key, start + 'px' );
        }


        setTimeout( function(){

            ob.css( 'transition', 'all ' + duration / 1000 + 's ease' );

            for( key in keyframes ){
                ob.css( key, keyframes[key] );
            }

            var animation_end = function(){
                ob.unbind( 'transitionend', animation_end );
                ob.css( 'transition', '' );
                if( callback ){
                    callback();
                }
            }

            ob.on( 'transitionend', animation_end );

        }, 1 );

        return this;
    }



    this.animation_steps = function( start, end, duration, callback, data ){
        
        var ob = this.context;
        var change = end - start
        var currentTime = 0
        var increment = 15;
        var val = 0;

        var step = function (){
            currentTime += increment;
            val = easeInOutQuad( currentTime, start, change, duration );
            val = Math.round( val * 1000 ) / 1000;
     
            if ( currentTime >= duration ) {
                val = end;
                clearInterval( interval_ob );
            }

            callback( val, data );
        }

        function easeInOutQuad( t, b, c, d ){
            t /= d / 2
            if (t < 1) return c / 2 * t * t + b
            t--
            return -c / 2 * (t * (t - 2) - 1) + b
        }

        var interval_ob = setInterval( step, increment );
        step();
    }

    

    this.exists = function(){
        return this.context;
    }

    this.focus = function(){
        return this.context.focus();
    }

    this.click = function(){
        return this.context.click();
    }

    this.hasProp = function( k ){
        return this.context.hasOwnProperty( k );
    }

    this.setProp = function( k ){
        return this.context.setProperty( k );
    }

    this.position = function(){
        var parent_offset = this.parent().offset();
        var this_offset = this.offset();
        var scroll_top = this.parent().scrollTop();
        var scroll_left = this.parent().scrollLeft();
        return { top: this_offset.top - parent_offset.top + scroll_top, left: this_offset.left - parent_offset.left + scroll_left };
    }

    this.on_click_enter = function ( callback ){
        this.on( 'click', callback );
        this.on( 'keyup', function( e ){
            if( e.keyCode !== 13 ){
                return;
            }
            callback();
        } )
    }


    this.slideDown = function( speed, callback ){

        if( !speed ){
            speed = 200;
        }

        this.css( 'height', 'auto' );
        this.show();

        var ob = this;
        var height = this.height();
        var padding_top = this.css( 'padding-top' );
        var padding_bottom = this.css( 'padding-bottom' );
        var margin_top = this.css( 'margin-top' );
        var margin_bottom = this.css( 'margin-bottom' );

        this.height( 0 );
        this.css( 'padding-top', '0px' );
        this.css( 'padding-bottom', '0px' );
        this.css( 'margin-top', '0px' );
        this.css( 'margin-bottom', '0px' );

        this.animate( { height: height + 'px',  'padding-top': padding_top, 'padding-bottom': padding_bottom,  'margin-top': margin_top, 'margin-bottom': margin_bottom }, speed, function(){
            ob.css( 'height', '' );
            ob.css( 'padding-top', '' );
            ob.css( 'padding-bottom', '' );
            ob.css( 'margin-top', '' );
            ob.css( 'margin-bottom', '' );
            if( callback ){
                callback();
            }
        } );
    }



    this.slideUp = function( speed, callback ){

        if( !speed ){
            speed = 200;
        }

        var ob = this;
        
        ob.show();
        ob.css( 'height', ob.css( 'height' ) );
        ob.css( 'padding-top', ob.css( 'padding-top' ) );
        ob.css( 'padding-bottom', ob.css( 'padding-bottom' ) );
        ob.css( 'margin-top', ob.css( 'margin-top' ) );
        ob.css( 'margin-bottom', ob.css( 'margin-bottom' ) );

        this.animate( { height: '0px', 'padding-top': '0px', 'padding-bottom': '0px',  'margin-top': '0px', 'margin-bottom': '0px' }, speed, function(){
            ob.css( 'height', '' );
            ob.css( 'padding-top', '' );
            ob.css( 'padding-bottom', '' );
            ob.css( 'margin-top', '' );
            ob.css( 'margin-bottom', '' );
            ob.hide();
            if( callback ){
                callback();
            }
        } );
    }
}









// Polyfils
fr_polifill_closest();

function fr_polifill_closest(){
    if (!Element.prototype.matches) {
        Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
    }

    if (!Element.prototype.closest) {
        Element.prototype.closest = function(s) {
            var el = this;

            do {
                if (Element.prototype.matches.call(el, s)) return el;
                el = el.parentElement || el.parentNode;
            } while (el !== null && el.nodeType === 1);
            return null;
        };
    }
}
