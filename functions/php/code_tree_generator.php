<?php


class Fr_Code_Tree_Generator {

    static  function generate($path, $excludeDirs = [] ) {

        ?>
        <html>
        <head>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        </head>
        <body>
            <?php  self::scanDirectory($path, $excludeDirs ); ?>
        </body>
        <?php 
        exit;
    }

    static function scanDirectory($dir, $excludeDirs ) {


        $files = scandir($dir);
        echo '<ul class="list-group">';

        foreach ($files as $file) {
            if ($file != "." && $file != "..") {

                $path = $dir . '/' . $file;

                if (in_array(basename($path), $excludeDirs)) {
                    continue;
                }

                echo '<li class="list-group-item">';

                if (is_dir( $path )) {
                    echo htmlspecialchars($file);
                    self::scanDirectory($path, $excludeDirs );
                } else {
                    echo htmlspecialchars($file);
                    self::parseFileForFunctionsAndClasses($path);
                }

                echo '</li>';
            }
        }

        echo '</ul>';
    }

    static function parseFileForFunctionsAndClasses($filePath) {
        $content = file_get_contents($filePath);
        if (preg_match_all('/function\s+(\w+)/', $content, $matches)) {
            foreach ($matches[1] as $functionName) {
                echo '<ul class="list-group"><li class="list-group-item">' . htmlspecialchars($functionName) . '</li></ul>';
            }
        }
    }
}
