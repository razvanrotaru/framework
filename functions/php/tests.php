<?php

function fr_ab_speed_test(){

	$args = func_get_args();
	$max = array_shift( $args );
	$func = array_shift( $args );

	$funcs = [ $func, $func . '2' ];
	$res = [];
	$error = false;
	$timer = [];

	foreach( $funcs as $index => $func ){
		fr_start_timer( 'fr_ab_speed_test_func' );

		for( $x = 0; $x < $max; $x++ ){
			$res[$index] = call_user_func_array( $func, $args );
		}

		$timer[$index] = fr_get_timer( 'fr_ab_speed_test_func' );

		if( $index && $res[$index] !== $res[$index -1] ){
			fr_p( [ 'Values do not match', $res[$index -1], $res[$index] ], 'error' );
			$error = true;
		}
	}

	if( !$error ){
		fr_p( [
			'diff' => round( 100 / $timer[0] * $timer[1] - 100, 2 ) . '%',
			'old_time' => fr_show_full_nr( $timer[0] / $max ),
			'new_time' => fr_show_full_nr( $timer[1] / $max ),
			'total_exec_time' => array_sum( $timer ),
			'res' => $res[1]
		], 'success' );
	}

}


function fr_speed_test( $max, $func ){

	fr_start_timer( 'fr_speed_test' );

	for( $x = 0; $x < $max; $x++ ){
		call_user_func( $func );
	}

	$time = fr_get_timer( 'fr_speed_test' );

	fr_p( $time );

}






class fr_tests {

	static $failed = false;
	static $exec_time = 0;
	static $tests_performed = 0;

	static function log_test( $test_name, $expected, $callback, $operator = '==' ) {

		$backtrace = debug_backtrace();
		$file_name = pathinfo( $backtrace[0]['file'] )['filename'];
		self::do_unit_tests( $callback, [
			"$file_name -> $test_name" => [
				'input' => [],
				'output' => $expected
			]
		], $operator );

	}

	static function echo_unit_tests( $callback, $tests, $operator = '==', $normalization_func = false ) {

		return print_r( self::do_unit_tests( $callback, $tests, $operator, $normalization_func, true ), true );

	}

	static function do_unit_tests( $callback, $tests, $operator = '==', $result_normalization_func = false, $return = false ) {

		if( self::$failed ) {
			return;
		}

		foreach( $tests as $test_name => $test ) {

			self::$tests_performed++;
			$general_test_name = is_numeric( $test_name ) ? $callback : "$callback -> $test_name";

			$function_code = self::extract_function_code( $callback );
			if( fr_is_error( $function_code ) ) {
				return self::handle_failed_test( $general_test_name, $test, $function_code->message, $result_normalization_func, $return );
			}

			$prepared_code = self::prepare_function_with_dependencies( $callback, $function_code, $test['dependencies'] ?? [] );
			$test_script = self::create_test_script( $test, $prepared_code, $callback );
			$res = self::execute_and_decode_test_script( $test_script );
			$res = $res['result'] ?? $res; 

			if( is_array( $test['output'] ) ){
				$test['output'] = self::process_output_functions( $test['output'], $res );
			}

			$local_operator = $test['operator'] ?? $operator;
			if( !self::compare( $res, $test['output'], $local_operator ) ) {
				return self::handle_failed_test( $general_test_name, $test, $res, $result_normalization_func, $return );
			}

			if( function_exists( 'fr_test' ) ) {
				fr_test( $general_test_name, fn() => [true, true, []] );
			}

		}

	}

	static function do_tests( $callback, $tests, $operator = '==', $result_normalization_func = false, $return = false ) {

		if( self::$failed ) {
			return;
		}

		foreach( $tests as $test_name => $test ) {

			self::$tests_performed++;
			$general_test_name = is_numeric( $test_name ) ? $callback : "$callback -> $test_name";
			$res = $callback( ...array_values( $test['input'] ) );

			if( is_array( $test['output'] ) ){
				$test['output'] = self::process_output_functions( $test['output'], $res );
			} else if( $test['output'] instanceof Closure ){
				$test['output'] = $test['output']( $res );
			}

			$local_operator = $test['operator'] ?? $operator;

			if( !self::compare( $res, $test['output'], $local_operator ) ) {
				return self::handle_failed_test( $general_test_name, $test, $res, $result_normalization_func, $return );
			}

			if( function_exists( 'fr_test' ) ) {
				fr_test( $general_test_name, fn() => [true, true, []] );
			}

		}

	}

	static function handle_failed_test( $general_test_name, $test, $res, $result_normalization_func, $return ) {

		if( $result_normalization_func ) {
			$test = fr_array_map_recursive( $result_normalization_func, $test );
		}
		self::$failed = [
			'failed_test' => $general_test_name,
			'test' => [
				'input' => $test['input'],
				'expected_output' => $test['output'],
				'actual_output' => $res
			]
		];
		if( $return ) {
			return self::$failed;
		}

		if( function_exists( 'fr_test' ) ) {
			fr_test( $general_test_name, fn() => [true, false, [
				'input' => self::$failed['test']['input'],
				'output' => self::$failed['test']['actual_output'],
				'received' => $res
			]] );
		}
		self::show_results();
		exit;

	}

	static function execute_and_decode_test_script( $script ) {

		$raw_result = self::execute_test_script( $script );

		return json_decode( $raw_result, true ) ?? $raw_result;

	}

	static function execute_test_script( $script ) {

		$namespace = 'IsolatedNamespace_' . uniqid();

		$fullCode = "
namespace $namespace {
set_error_handler(function(\$errno, \$errstr, \$errfile, \$errline) {
throw new \ErrorException(\$errstr, 0, \$errno, \$errfile, \$errline);
});
ob_start();
try {
" . $script . "
} catch (\\Throwable \$e) {
ob_end_clean();
\$script = <<<'EOD'
" . $script . "
EOD;
\$lines = explode(\"\\n\", \$script);
\$errorLine = \$e->getLine() - 7;
\$codeLine = isset(\$lines[\$errorLine - 1]) ? trim(\$lines[\$errorLine - 1]) : 'Unable to retrieve the line of code';
return 'Error: ' . \$e->getMessage() . \"\\nLine \" . \$errorLine . \": \" . \$codeLine;
}
return ob_get_clean();
}
		";
		
		try {
			$res = eval( $fullCode );
		} catch (Throwable $e) {
			$lines = explode( "\n", $script );
			$errorLine = $e->getLine();
			$codeLine = isset( $lines[$errorLine - 1] ) ? trim( $lines[$errorLine - 1] ) : 'Unable to retrieve the line of code';
			$res = 'Error:2 ' . $e->getMessage() . PHP_EOL . "Line $errorLine: $codeLine";
		
		}

		if( str_starts_with( $res, 'Error:' ) ) {
			$res .= PHP_EOL . fr_enumerate_lines( $script );
		}

		return $res;

	}

	static function compare( $received, $expected, $operator = '==' ) {

		switch( $operator ) {
			case '==':
				return $received == $expected;
			case '===':
				return $received === $expected;
			case '!=':
				return $received != $expected;
			case '!==':
				return $received !== $expected;
			case '>':
				return $received > $expected;
			case '<':
				return $received < $expected;
			case '>=':
				return $received >= $expected;
			case '<=':
				return $received <= $expected;
			default:
				return true;
		}

	}

	static function process_output_functions( $expected, $received ){

		if( fr_is_error( $received ) ){
			$received = [];
		}

		foreach( $expected as $k => $v ){

			if( $v instanceof Closure ){

				if( isset( $received[$k] ) ){
					$res = $v( $received[$k] );
					if( $res ){
						$expected[$k] = $received[$k];
					} else {
						$expected[$k] = null;
					}
				} else {
					$expected[$k] = false;
				}

			} else if( is_array( $v ) ){

				$expected[$k] = self::process_output_functions( $v, $received[$k] ?? [] );

			}

		}

		return $expected;

	}


	static function show_results() {

		if( self::$failed ) {
			fr_p( self::$failed, 'error' );
		} else {
			fr_p( 'All tests are ok', 'green' );
			fr_p( 'Tests performed: ' . self::$tests_performed );
			fr_p( 'Time taken: ' . round( self::$exec_time, 4 ) );
		}

	}

	static function extract_function_code( $function_name ) {

		try {
			$reflection = self::get_reflection( $function_name );
			$filename = $reflection->getFileName();
			$start_line = $reflection->getStartLine() - 1;
			$end_line = $reflection->getEndLine();
			$source = file( $filename );
			$body = implode( '', array_slice( $source, $start_line, $end_line - $start_line ) );
			return $reflection instanceof ReflectionFunction ? "function $function_name" . substr( $body, strpos( $body, '(' ) ) : $body;
		} catch( ReflectionException $e ) {
			return new fr_error( $e->getMessage() );
		}

	}

	static function get_reflection( $function_name ) {

		if( strpos( $function_name, '::' ) !== false || strpos( $function_name, '->' ) !== false ) {
			list( $class_name, $method_name ) = preg_split( '/::|->/', $function_name );
			return new ReflectionMethod( $class_name, $method_name );
		}

		return new ReflectionFunction( $function_name );

	}

	static function prepare_function_with_dependencies( $function_name, $function_code, $dependencies = [] ) {

		$classes = ['functions' => []];
		list( $test_class, $test_method ) = self::parse_function_name( $function_name );

		foreach( $dependencies as $dep_name => $dep_cases ) {
			list( $dep_class_name, $dep_method_name, $static ) = self::parse_function_name( $dep_name );
			if( in_array( $dep_class_name, ['self', '$this'] ) ) {
				if( !$test_class ) {
					continue;
				}
				$dep_class_name = $test_class;
			}
			
			if( !isset( $classes[$dep_class_name] ) ){
				$classes = array_merge( [ $dep_class_name => [] ], $classes );
			}

			$is_global = in_array( $dep_class_name, [ 'functions' ] );
			$classes[$dep_class_name][$dep_method_name] ??= '';
			$classes[$dep_class_name][$dep_method_name] .= PHP_EOL . ( $static ? 'static ' : '' ) . self::create_dummy_function( $dep_method_name, $dep_cases, $is_global, $static );

			if( $is_global ){
				$classes[$dep_class_name][$dep_method_name] = rtrim( $classes[$dep_class_name][$dep_method_name] ) . ';' . PHP_EOL;
			}
		}

		if( $test_class ) {
			$classes[$test_class] ??= [];
			$classes[$test_class][$test_method] = $function_code;
		}

		return self::generate_code( $classes );

	}

	static function parse_function_name( $function_name ) {

		if( preg_match( '/^(.+?)(->|::)(.+)$/', $function_name, $matches ) ) {
			return [$matches[1], $matches[3], $matches[2] === '::'];
		}

		return ['functions', $function_name, false];

	}

	static function generate_code( $classes ) {

		$code = '';
		foreach( $classes as $class_name => $methods ) {
			if( $class_name !== 'functions' ) {
				if( str_starts_with( $class_name, '$' ) ){
					$code .= "\$f = function(){ global $class_name; $class_name = new class {\n\n";
				} else {
					$code .= "class $class_name {\n\n";
				}
			}
			$code .= implode( "\n\n", $methods ) . "\n\n";
			if( $class_name !== 'functions' ) {
				if( str_starts_with( $class_name, '$' ) ){
					$code .= "};};\$f();\n\n";
				} else {
					$code .= "};\n\n";
				}
			}
		}

		return $code;

	}

	static function create_test_script( $test_case, $full_code, $function_name ) {

		$input = var_export( $test_case['input'], true );
		list( $class_name, $method_name, $is_static ) = self::parse_function_name( $function_name );

		if ($is_static) {
		    $call_code = "\$result = $function_name(...array_values( $input ) );";
		} else {
		    if ($class_name === 'functions') {
		        $call_code = "\$result = $function_name(...array_values( $input ));";
		    } else {
		        $call_code = "
		            \$instance = new $class_name();
		            \$result = \$instance->$method_name(...array_values( $input ));
		        ";
		    }
		}

		return "\n{$full_code}\n$call_code;\necho json_encode(['result' => \$result]);";

	}

	static function create_dummy_function( $function_name, $cases, $is_global, $is_static ) {

		if( str_starts_with( $function_name, '$$' ) ){
			$function_name = substr( $function_name, 1 );
			if( $is_global ){
				$function_body = "\$f = function(){ global $function_name; $function_name = " . var_export( $cases, true ) . ";\n}; \$f();\n\n";
			} else {
				if( $is_static ){
					$function_body = "$function_name = " . var_export( $cases, true ) . ";\n\n";
				} else {
					$function_body = "var $function_name = " . var_export( $cases, true ) . ";\n\n";
				}
			}
			return $function_body;
		} else if( str_starts_with( $function_name, '$' ) ){
			$function_body = "global $function_name; $function_name = function() {\n";
		} else {
			$function_body = "function $function_name() {\n";
		}

		$function_body .= '$args = func_get_args();' . "\n";

		if( is_array( $cases ) ){
			foreach( $cases as $case ) {

				if( !empty( $case['input'] ) && is_array( $case['input'] ) ){
					$condition = self::create_condition( $case['input'] );
					$return_value = var_export( $case['output'] ?? null, true );
					$function_body .= "    if( $condition ) return $return_value;\n";
				} else {
					$function_body .= "    return " . var_export( $case['output'] ?? null, true ) . ";\n";
				}

			}
		}

		return $function_body . "    return null;\n}\n";

	}

	static function create_condition( $input ) {

		return implode( ' && ', array_map(
			fn( $index, $value ) => "\$args[$index] === " . var_export( $value, true ),
			array_keys( $input ),
			$input
		) );

	}




	static function view_diff( $test ){

		if( !$test ){
			return;
		}

		if( fr_is_error( $test ) ){
			fr_p( $test, 'error' );
			return;
		}

		echo fr_diff_compare( $test['test']['expected_output'], $test['test']['actual_output'] );

	}


}


