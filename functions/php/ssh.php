<?php
function fr_ssh_disk_usage_data_to_array( $s ){
	if( !$s ){
		return [];
	}

	$s = trim( $s );
	$s = explode( "\n", $s );
	unset( $s[0] );
	foreach( $s as $k => $v ){
		$v = str_replace( "\t", " ", $v );
		$v = fr_replace_loop( '  ', ' ', $v );
		$v = explode( ' ', $v );
		$s[$k] = [
			'dir' => $v[0],
			'size' => $v[1],
			'used' => $v[2],
			'available' => $v[3],
			'usage' => str_replace( '%', '', $v[4] ),
			'mounted' => $v[5]
		];
	}

	usort( $s, function( $a, $b ){ 
		return fr_to_bytes( $a['size'] ) <=> fr_to_bytes( $b['size'] );
	} );

	return $s;
}