<?php
function fr_exec( $cmd, $return_error = false ){
	if( fr_is_windows() ){
		$cmd = str_replace( '/', '\\', $cmd );
	}

	if( $return_error && !strstr( $cmd, ' 2>&1' ) ){
		$cmd .= ' 2>&1';
	}

	return shell_exec( $cmd );
}

function fr_is_windows(){
	return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
}