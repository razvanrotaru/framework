<?php
// Special file that is used only in with a standalone file, with no WP dependencies
// Its needed to speed up certain ajax requests

class FR_Standalone {

	var $db;
	var $db_settings;
	var $db_prefix;
	var $wp_root;

	function __construct( $wp_root ){

		$this->wp_root = $wp_root;

		$this->db_settings = fr_wp_get_settings_from_file( $this->wp_root . '/wp-config.php' );

		if( $this->db_settings ){
			$this->db_prefix = $this->db_settings['prefix'];
			$this->db = new FR_DB( $this->db_settings );
		} else {
			die( 'DB settings are missing' );
		}

	}


    function get_option( $key ){
        return  $this->db->query_var( 'option_value', 'SELECT option_value FROM ' . $this->db_prefix . 'options WHERE option_name = "' . $this->db->esc( $key ) . '"' );
    }

    function get_post_meta( $pid, $key ){
        return $this->db->query_var( 'meta_value', 'SELECT meta_value FROM ' . $this->db_prefix . 'postmeta WHERE post_id = "' . $this->db->esc( $pid ) . '" AND meta_key = "' . $key . '"' );
    }

    function update_post_meta( $pid, $key, $val ){
        return $this->db->update( $this->db_prefix . 'postmeta', [ 
        	'meta_value' => $val
        ], [ 
        	'post_id' => $pid, 
        	'meta_key' => $key
        ] );
    }

    function get_user_meta( $pid, $key ){
        return $this->db->query_var( 'meta_value', 'SELECT meta_value FROM ' . $this->db_prefix . 'usermeta WHERE user_id = "' . $this->db->esc( $pid ) . '" AND meta_key = "' . $key . '"' );
    }

    function get_user_field( $pid, $key ){
        return $this->db->query_var( $key, 'SELECT ' . $key . ' FROM ' . $this->db_prefix . 'users WHERE ID = "' . $this->db->esc( $pid ) . '"' );
    }

    function get_posts_with_taxonomy( $taxonomy, $where ){

    	$term_tax = $this->db->query( 'SELECT term_taxonomy_id FROM ' . $this->db_prefix . 'term_taxonomy WHERE taxonomy = "' . $this->db->esc( $taxonomy ) . '"' );
    	$term_tax = array_column( $term_tax, 'term_taxonomy_id' );
    	$posts = $this->db->query( 'SELECT object_id FROM ' . $this->db_prefix . 'term_relationships WHERE term_taxonomy_id IN ( ' . implode( ',', $term_tax ) .  ' )' );
    	$posts = array_column( $posts, 'object_id' );
		$posts = $this->db->query( 'SELECT * FROM ' . $this->db_prefix . 'posts WHERE ID IN ( ' . implode( ',', $posts ) .  ' ) ' . $where );

    	return $posts;
    }

    function get_posts_with_terms( $taxonomies, $term_ids, $where, $col = false){

        if( !$term_ids ){
            return [];
        }

    	$term_ids = fr_make_array( $term_ids );
        $taxonomies = fr_make_array( $taxonomies );

        foreach( $taxonomies as $key => $taxonomy ){
            $taxonomies[$key] = $this->db->esc( $taxonomy );
        }

        $q = 'term_id IN ( ' . implode( ',', $term_ids ) . ' )';
        $term_tax = $this->db->query( 'SELECT term_taxonomy_id, taxonomy FROM ' . $this->db_prefix . 'term_taxonomy WHERE taxonomy IN ( "' . implode( '","', $taxonomies ) . '" ) AND ' . $q );
        $term_tax = array_column( $term_tax, 'taxonomy', 'term_taxonomy_id' );
    	$posts = $this->db->query( 'SELECT object_id FROM ' . $this->db_prefix . 'term_relationships WHERE term_taxonomy_id IN ( ' . implode( ',', array_keys( $term_tax ) ) .  ' )' );
    	$posts = array_column( $posts, 'object_id' ); 
        $select_col = $col ? $col : '*';
		$posts = $this->db->query( 'SELECT ' . $select_col . ' FROM ' . $this->db_prefix . 'posts WHERE ID IN ( ' . implode( ',', $posts ) .  ' ) ' . $where, $col );

    	return $posts;
    }

    function get_post_terms( $pids, $taxonomies ){

        $taxonomies = fr_make_array( $taxonomies );
        $pids = fr_make_array( $pids );

    	$terms = $this->db->query( 'SELECT term_taxonomy_id FROM ' . $this->db_prefix . 'term_relationships WHERE object_id = "' . implode( ',', $pids ) . '"' );
    
    	if( !$terms ){
    		return [];
    	}

    	$terms = array_column( $terms, 'term_taxonomy_id' );

        foreach( $taxonomies as $key => $taxonomy ){
            $taxonomies[$key] = $this->db->esc( $taxonomy );
        }

    	$terms = $this->db->query( 'SELECT term_id, taxonomy FROM ' . $this->db_prefix . 'term_taxonomy WHERE taxonomy IN ("' . implode( '","', $taxonomies ) . '") AND term_taxonomy_id IN ( ' . implode( ',', $terms ) . ' )' );

    	if( !$terms ){
    		return [];
    	}

    	$terms = array_column( $terms, 'taxonomy', 'term_id' );

    	$terms_data = $this->db->query( 'SELECT term_id,name,slug FROM ' . $this->db_prefix . 'terms WHERE term_id IN ( ' . implode( ',', array_keys( $terms ) ) . ' )' );

    	$new_terms = [];
    	foreach( $terms_data as $term ){
            $term['taxonomy'] = $terms[$term['term_id']];
    		$new_terms[$term['term_id']] = $term;
    	}

    	return $new_terms;
    }



    function get_posts_terms( $pids, $taxonomies ){

        return $this->get_post_terms( $pids, $taxonomies );

    }


    function get_term_by( $type, $value, $taxonomy = 'category' ){

    	if( $type == 'id' ){
    		$type = 'term_id';
    	}

    	$terms = $this->db->query( 'SELECT term_id,name,slug FROM ' . $this->db_prefix . 'terms WHERE ' . $type . ' = "' .$this->db->esc( $value ) . '"' );
    	
    	if( !$terms ){
    		return false;
    	}

    	$term_ids = array_column( $terms, 'term_id' );
    	$terms = fr_array_unique_on_values( $terms, 'term_id' );

    	$terms_with_taxonomy = $this->db->query( 'SELECT term_id FROM ' . $this->db_prefix . 'term_taxonomy WHERE taxonomy = "' . $this->db->esc( $taxonomy ) . '" AND term_taxonomy_id IN ( ' . implode( ',', $term_ids ) . ' )' );

    	$terms_with_taxonomy = array_column( $terms_with_taxonomy, 'term_id' );
    	$terms_with_taxonomy = array_flip( $terms_with_taxonomy );

    	$terms = array_intersect_key( $terms, $terms_with_taxonomy );

    	return reset( $terms );
    }


    function get_terms( $taxonomies = [ 'category' ], $query = '', $term_ids = [] ){

        if( $term_ids ){
            $term_ids = array_map( 'intval', $term_ids );
            $query .= ' AND tt.term_id IN ( ' . implode( ',', $term_ids ) . ')';
        }

        $taxonomies = fr_make_array( $taxonomies );

        foreach( $taxonomies as $key => $taxonomy ){
            $taxonomies[$key] = $this->db->esc( $taxonomy );
        }

        $terms = $this->db->query( 'SELECT * FROM ' . $this->db_prefix . 'term_taxonomy tt INNER JOIN ' . $this->db_prefix . 'terms t ON tt.term_id = t.term_id WHERE tt.taxonomy IN ( "' . implode( '","', $taxonomies ) . '" ) ' . $query );

        return $terms;

    }


    function get_terms_with_posts( $taxonomy = 'category', $pids = [], $query = '', $group_on = 'terms' ){

        if( !$pids ){
            return [];
        }
        
        $terms = $this->db->query( 'SELECT * FROM ' . $this->db_prefix . 'term_taxonomy tt INNER JOIN ' . $this->db_prefix . 'terms t ON tt.term_id = t.term_id INNER JOIN ' . $this->db_prefix . 'term_relationships ts ON ts.term_taxonomy_id = tt.term_taxonomy_id WHERE tt.taxonomy = "' . $this->db->esc( $taxonomy ) . '" AND ts.object_id IN ( ' . implode( ',', $pids ) . ' )' . $query );

        $new_terms = [];

        if( fr_is_error( $terms ) ){
            return $new_terms;
        }

        foreach( $terms as $term ){
            if( $group_on == 'terms' ){
                if( !isset( $new_terms[$term['term_id']] ) ){
                    $term['post_count'] = 1;
                    $term['posts'] = [ $term['object_id'] ];
                    $new_terms[$term['term_id']] = $term;
                } else {
                    $new_terms[$term['term_id']]['post_count']++;
                    $new_terms[$term['term_id']]['posts'][] = $term['object_id'];
                }
            } else {
                if( !isset( $new_terms[$term['object_id']] ) ){
                    $new_terms[$term['object_id']] = [
                        'term_count' => 1,
                        'terms' =>   [ $term['term_id'] ]
                    ];
                } else {
                    $new_terms[$term['object_id']]['term_count']++;
                    $new_terms[$term['object_id']]['terms'][] = $term['term_id'];
                }
            }
        }

        return $new_terms;

    }


	function get_image_src( $image, $size ){

		global $fr;

		if( $image ){
			$sizes = $fr->get_post_meta( $image, '_wp_attachment_metadata' );
			$sizes = fr_is_serialized( $sizes ) ? @unserialize( $sizes ) : false;

			if( $sizes ){

				if( isset( $sizes['sizes'][$size] ) ){

					$dir = explode( '/', $sizes['file'] );
					array_pop( $dir );
					$dir = implode( '/', $dir );

					$image = CDN_URL . $dir . '/' . $sizes['sizes'][$size]['file'];
				} else {
					$image = CDN_URL . $sizes['file'];
				}
			} else {
				$image = false;
			}
		} else {
			$image = false;
		}
		return $image;
	}

	function esc_html( $s ){
		return htmlspecialchars( $s );
	}

	function get_excerpt( $post, $max_length = false, $after = '' ){
		$excerpt = $post['post_excerpt'];
		if( !$excerpt ){
			$excerpt = str_Replace( [ "\n", "\r" ], ' ', strip_tags( $post['post_content'] ) );
			$excerpt = fr_replace_recursive( '  ', ' ', $excerpt );
			$excerpt = trim( $excerpt );
		}

		if( $max_length ){
			$excerpt = mb_substr( $excerpt, 0, $max_length, 'utf-8' ) . $after;
		}
		return $excerpt;
	}



    function get_user(){

        global $fr;

        $cookie = '';

        foreach( $_COOKIE as $k => $v ){
            if( fr_str_starts_with( $k, 'wordpress_logged_in_' ) ){
                $cookie = $v;

                break;
            }
        }
            
        if( !$cookie ){
            return;
        }

        $cookie = explode( '|', $cookie );

        if( count( $cookie ) != 4 ){
            return;
        }

        $username = $cookie[0];
        $expiration = $cookie[1];
        $token = $cookie[2];
        $hmac = $cookie[3];


        if( $expiration < time() ){
            return;
        }

        $res = $this->db->query( 'SELECT * FROM ' . $this->db_prefix . 'users WHERE user_login = "' . $this->db->esc( $username ) . '"' );
        
        if( !$res ){
            return;
        }

        $pass = $res[0]['user_pass'];

        $pass_frag = substr( $pass, 8, 4 );

        $salt = $this->db_settings['LOGGED_IN_KEY'] . $this->db_settings['LOGGED_IN_SALT']; 

        $key = hash_hmac( 'md5', $username . '|' . $pass_frag . '|' . $expiration . '|' . $token, $salt );
        $algo = function_exists( 'hash' ) ? 'sha256' : 'sha1';
        $hash = hash_hmac( $algo, $username . '|' . $expiration . '|' . $token, $key );

        if ( !hash_equals( $hash, $hmac ) ) {
            return;
        }

        return $res[0]['ID'];
    }
}



