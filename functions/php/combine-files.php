<?php
// Remove characters that are not needed
function fr_css_minify( $css, $optimize_font_names = [], $light_optimization = false, $remove_all_spaces = false ){

	$old_len = strlen( $css );
	// Remove comments
	$css = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css );
	
	$css = str_replace( "\r", "", $css );
	$css = fr_replace_loop( "\n\n\n", "\n\n", $css );
	$css = str_replace(  "\t", " ", $css );
	$css = fr_replace_loop( '  ', ' ', $css );
	$css = str_replace( "\n \n", "\n\n", $css );
	$css = fr_replace_loop( "\n\n\n", "\n\n", $css );
	$css = str_replace( "\n ", "\n", $css );
	if( $remove_all_spaces ){
		$css = str_replace( "\n", "", $css );
	}

	$css = str_replace( "{ ", "{", $css );
	$css = str_replace( " }", "}", $css );
	$css = str_replace( " {", "{", $css );
	$css = str_replace( "} ", "}", $css );
	$css = str_replace( "; ", ";", $css );
	$css = str_replace( ": ", ":", $css );
	$css = str_replace( ";}", "}", $css );
	$css = str_replace( ", ", ",", $css );
	$css = str_replace( ",\n", ",", $css );

	if( $light_optimization ){
		return $css;
	}
	$css = str_replace( " >", ">", $css );
	$css = str_replace( "> ", ">", $css );
	$css = str_replace( "( ", "(", $css );
	$css = str_replace( " )", ")", $css );


	$css = str_replace( "#000000", "#000", $css );
	$css = str_replace( "#111111", "#111", $css );
	$css = str_replace( "#222222", "#222", $css );
	$css = str_replace( "#333333", "#333", $css );
	$css = str_replace( "#444444", "#444", $css );
	$css = str_replace( "#555555", "#555", $css );
	$css = str_replace( "#666666", "#666", $css );
	$css = str_replace( "#777777", "#777", $css );
	$css = str_replace( "#888888", "#888", $css );
	$css = str_replace( "#999999", "#999", $css );
	$css = str_replace( "#aaaaaa", "#aaa", $css );
	$css = str_replace( "#bbbbbb", "#bbb", $css );
	$css = str_replace( "#cccccc", "#ccc", $css );
	$css = str_replace( "#dddddd", "#ddd", $css );
	$css = str_replace( "#eeeeee", "#eee", $css );
	$css = str_replace( "#ffffff", "#fff", $css );
	$css = str_replace( "#F00", "red", $css );
	$css = str_replace( "#FF0000", "red", $css );
	$css = str_replace( "33.33333333%", "33.33%", $css );
	$css = str_replace( " 0.", " .", $css );
	$css = str_replace( " @", "@", $css );
	$css = str_replace( " 0px", " 0", $css );
	$css = str_replace( ":white", ":#fff", $css );
	$css = str_replace( ":black", ":#000", $css );
	$css = str_replace( "black;", "#000;", $css );
	$css = str_replace( "black}", "#000}", $css );
	$css = str_replace( ":bold;", ":700;", $css );
	$css = str_replace( ":bold}", ":700}", $css );
	$css = str_replace( ",0.", ",.", $css );
		

	// Optimize font names
	if( $optimize_font_names ){
		$chars = fr_chars( 'alpha,numeric' );
		$chars = implode( '', $chars );
		$fonts = fr_single_loop_extract( $css, 'font-family:', [ ';', '}', ',' ] );

		$new_names = [];
		$index = '';
		$len_chars = strlen( $chars );
		foreach( $optimize_font_names as $name ){
			$new_names["'" . $name . "'"] = $index = fr_increment( $index, $chars, $len_chars );
		}

		foreach( $fonts as $font ){
			if( !isset( $new_names[$font[0]] ) ){
				continue;
			}
			$css = str_replace( 'font-family:' . $font[0] . $font[1], 'font-family:\'' . $new_names[$font[0]] . "'" . $font[1], $css );
		}
	}

	// Remove the domain
	$root_url = fr_get_url_root();
	$css = str_replace( $root_url, '', $css );

	return $css;
}


function fr_html_minify( $html ){

	$styles = explode( '<style', $html );

	foreach( $styles as $k => $style ){

		if( !$k ){
			continue;
		}

		$tag = strstr( $style, '</style>', true );
		$attr = strstr( $tag, '>', true );
		$css = substr( $tag, strlen( $attr ) + 1 );

		if( !$css ){
			continue;
		}

		$css = fr_css_minify( $css, '', true );
		$styles[$k] = $attr . '>' . $css . substr( $style, strlen( $tag )  );
	}

	$html = implode( '<style', $styles );

	$html = str_replace( "\t", " ", $html );
	$html = str_replace( "\r", "\n", $html );

	while( 1 ){
		$original_html = $html;
		$html = str_replace( "\n\n", "\n", $html );
		$html = str_replace( ">\n", "> ", $html );
		$html = str_replace( "  ", " ", $html );
		$html = str_replace( "\n ", "\n", $html );
		$html = str_replace( " \n", "\n", $html );
		if( strlen( $original_html ) == strlen( $html ) ){
			break;
		}
	}

	$html = str_replace( ">\n<", "><", $html );
	$html = trim( $html );

	return $html;
}


/**
 * Requires https://github.com/matthiasmullie/minify
 */
function fr_js_minify( $s ){

	$path_to_minify = FRAMEWORK_DIR .'/resources/js_minify';

	require_once $path_to_minify . '/src/Minify.php';
	require_once $path_to_minify . '/src/JS.php';
	require_once $path_to_minify . '/src/Exception.php';
	require_once $path_to_minify . '/src/Exceptions/BasicException.php';
	require_once $path_to_minify . '/src/Exceptions/FileImportException.php';
	require_once $path_to_minify . '/src/Exceptions/IOException.php';
	require_once $path_to_minify . '/init.php';

	$minifier = new MatthiasMullie\Minify\JS( $s );
	$s = $minifier->minify();

	return $s;
}



$_ENV['fr_combine_enque'] = array(
	'css' => array(),
	'js' => array()
);
function fr_combine_enque( $src, $type, $root, $url ) {
    if( !strstr( $src, $url ) || strstr( $src, "#dont_combine" ) ){ 
    	return;
    }
    
    $src = fr_relative_url_to_absolute( $src, $url );
    $file = fr_url_to_path( $src, $root, $url );

    if( file_exists( $file ) && ( substr( $file, -3, 3 ) == '.js' || substr( $file, -4, 4 ) == '.css' ) ){
    	$hash = md5_file( $file );
    	$_ENV['fr_combine_enque'][$type][$hash] = array(
			'type' => 'src',
			'url' => $url,
			'src' => $src,
			'file' => $file
		);
    }
}

function fr_combine_enque_inline( $content, $type, $url ){
	$hash = md5( $content );
	$_ENV['fr_combine_enque'][$type][$hash] = array(
		'type' => 'inline',
		'content' => $content,
		'url' => $url
	);
}

function fr_combine( $type, $folder, $debug = false, $version = 'v1' ){

	if( !empty( $_ENV['fr_combine_enque'][$type] ) ){

	    // Get all the content from the files
		$content = '';
		foreach( $_ENV['fr_combine_enque'][$type] as $script ){
			if( $script['type'] == 'src' ){
				$c = file_get_contents( $script['file'] );

				// Make sure all depended resources are replaced with the new links
			    if( $type == 'css' ){
			    	$urls = explode( 'url(', $c );
			    	if( $urls ){
			    			
			    		$relative_url = fr_url_dirname( $script['src'] ) . '/';
			    		$info = parse_url( $relative_url );
						$scheme = $info['scheme'];

			    		foreach( $urls as $k => $part ){

			    			if( !$k ){
			    				continue;
			    			}

			    			$src = strstr( $part, ')', 1 );
			    			$url = $src;
			    			$url = trim( $url );

			    			$wrap_char = substr( $url, 0, 1 );
			    			if( $wrap_char == '"' || $wrap_char == "'" ){
			    				$url = substr( $url, 1, -1 );
			    			} else {
			    				$wrap_char = '';
			    			}

			    			if( substr( $url, 0, 5 ) == 'data:' ){
			    				continue;
			    			}
							
			    			$new_url = fr_relative_url_to_absolute( $url, $relative_url );
			    			$new_url = apply_filters( 'fr_wp_combined_file_asset', $new_url, $type, $relative_url );
			    	
			    			if( $url !== $new_url ){
			    				$urls[$k] = $wrap_char . $new_url . $wrap_char . substr( $part, strlen( $src ) );
			    			}
			    		}
			    		$c = implode( 'url(', $urls );
				    }
			    }

				$content .= "\n\n\n\n/* " . fr_url_without_params( $script['src'] ) . " */\n\n" . $c;

			}else{
				$content .= "\n\n\n\n/* Inline content */\n\n" . $script['content'];
			}
		}


		if( $folder === false ){

		    // Minify only if the file doesn't exist
			if( $type == 'css' ){
				$content = fr_css_minify( $content, '', true );
			}

			if( $type == 'js' ){
				$content = fr_js_minify( $content, THEME_DIR . '/technadu-shared/resources/minify' );
			}

			return $content;
		}



		if( !file_exists( $folder ) ){
			mkdir( $folder, 0755, true );
		}


		$file_name = md5( implode( '|', array_keys( $_ENV['fr_combine_enque'][$type] ) ) . $debug ) . '-' . $version . '.' . $type;
	    $file = trim( $folder, '/' ) . '/' . $file_name;

	    if( $type == 'js' && file_exists( $file ) && ( !defined( 'FR_FORCE_COMBINE' ) || !FR_FORCE_COMBINE ) ){
	    	return $file;
	    }



	    if( !file_exists( $file ) || ( defined( 'FR_FORCE_COMBINE' ) && FR_FORCE_COMBINE ) ){

		    // Minify only if the file doesn't exist
			if( $type == 'css' ){
				$content = fr_css_minify( $content, '', true );
			}

			if( $type == 'js' ){
				$content = fr_js_minify( $content, THEME_DIR . '/technadu-shared/resources/minify' );
			}

			fr_required_dir( dirname( $file ) );
			file_put_contents( $file, $content );
		}
		
		touch( $file );
		
		return $file;
	}
}

function fr_combine_clear_old_files( $folder, $duration = '1 week' ){

	$files = scandir( $folder, SCANDIR_SORT_NONE );
	$duration = strtotime( '-' . $duration );

	foreach( $files as $file ){

		$file = $folder . '/' . $file;

		if( strlen( $file ) <= 32 ){
			continue;
		}

		if( !is_file( $file ) ){
			continue;
		}
		if( !fr_file_has_ext( $file, 'css' ) && !fr_file_has_ext( $file, 'js' ) ){
			continue;
		}
		if( filemtime( $file ) >= $duration ){
			continue;
		}

		@unlink( $file );
	}
}

function fr_combine_enqueue_from_html( $html, $types, $url, $omit = array(), $exclude_inline = false ){

	$doc = new DOMDocument();
	libxml_use_internal_errors( true );
	$doc->loadHTML( '<?xml encoding="utf-8" ?>' . $html );
	libxml_use_internal_errors( false );
	$relative_url = fr_get_current_url( true );


	// Add back body attributes
	// For some reason it removes them
	$body_attr = fr_extract( $html, '<body', '>' );
	$body_attr = fr_html_attr_to_array( $body_attr );

	foreach( $body_attr as $attr => $value ){
		$doc->getElementsByTagName( 'body' )[0]->setAttribute( $attr, $value );
	}

	$omit[] = '#dont_combine';

	$remove = array();
	foreach( $types as $type ){

		if( $type == 'js' ){
			$scripts = $doc->getElementsByTagName( 'script' );
			foreach ( $scripts as $script ) {

				if( strstr( $script->getAttribute( 'class' ), 'exclude_combining' ) ){
					continue;
				}

				$content = $script->nodeValue;
				$src = $script->getAttribute( 'src' );
				if( $src ){
					foreach( $omit as $string ){
			        	if( strstr( $src, $string ) ){
			        		continue 2;
			        	}
			        }
					$src = fr_relative_url_to_absolute( $src, $relative_url );
			        if( !strstr( $src, $url ) ){
			        	continue;
			        }
			        fr_combine_enque( $src, $type, ABSPATH, $url );
			        $remove[] = $script;
				}
				if( $exclude_inline != 'all' && $exclude_inline != $type && $content ){
					// Include inline scripts that use jQuery
					if( !strstr( $content, 'jQuery' ) && !strstr( $content, '$(' ) ){
						continue;
					}
					fr_combine_enque_inline( $content, $type, $url );
					$remove[] = $script;
				}
			}
		}

		if( $type == 'css' ){

			$styles = $doc->getElementsByTagName( 'link' );
			foreach ( $styles as $style ) {
				
				if( strstr( $script->getAttribute( 'class' ), 'exclude_combining' ) ){
					continue;
				}

				if( $style->getAttribute( 'rel' ) == 'stylesheet' ){
					$src = $style->getAttribute( 'href' );
					if( $src ){
						foreach( $omit as $string ){
				        	if( strstr( $src, $string ) ){
				        		continue 2;
				        	}
				        }
						$src = fr_relative_url_to_absolute( $src, $relative_url );
				        if( !strstr( $src, $url ) ){
				        	continue;
				        }
				        fr_combine_enque( $src, $type, ABSPATH, $url );
						$remove[] = $style;
					}
				}
			}

			if( $exclude_inline != 'all' && $exclude_inline != $type ){
				$styles = $doc->getElementsByTagName( 'style' );
				foreach ( $styles as $style ) {
					$content = $style->nodeValue;
					fr_combine_enque_inline( $content, $type, $url );
					$remove[] = $style;
				}
			}
		}
	}

	$remove = array_reverse( $remove );
	foreach( $remove as $script ){
		$script->parentNode->removeChild( $script );
	}
	$html = $doc->saveHTML();

	$html = str_replace( '<?xml encoding="utf-8" ?>', '', $html );

	return $html;
}




/**
 * CSS Optimizer
 * v2.0
 * - update for multi level CSS support
 */
function fr_css_optimizer( $html, $css, $exclude_classes = [], $exclude_selectors = [] ){

	$html_classes = fr_html_extract_class_names( $html );

	if( !is_array( $css ) ){
		$css = fr_css_decode( $css );
	}

	$css_classes = [];
	$grouped_selectors_on_classes = [];


	$grouped_selectors_on_classes = fr_css_optimizer_walk( $css );
	$css_classes = array_keys( $grouped_selectors_on_classes );
	$css_classes = array_combine( $css_classes, $css_classes );

	$html_classes = array_flip( $html_classes );
	$remove_classes = array_diff_key( $css_classes, $html_classes );
	$remove_classes = array_diff_key( $remove_classes, array_combine( $exclude_classes, $exclude_classes ) );
	$remove_selectors = array_intersect_key( $grouped_selectors_on_classes, $remove_classes );
	$remove_selectors = fr_array_flaten( $remove_selectors );
	$remove_selectors = array_unique( $remove_selectors );
	$remove_selectors = array_flip( $remove_selectors );
	$css = fr_css_optimizer_remove_selectors( $css, $remove_selectors );

	$css = fr_css_encode( $css );

	return $css;
}




/**
 * Remove selectors
 * v1.0
 */
function fr_css_optimizer_remove_selectors( $css, $remove_selectors, $lvl = 0 ){

	$occurence = [];
	$spaces = [];
	$new_css = [];

	foreach( $css as $k => $v ){

		if( strstr( $k, '@' ) ){
			$v = fr_css_optimizer_remove_selectors( $v, $remove_selectors, $lvl + 1 );
			if( $v ){
				$new_css[$k] = $v;
			}
			continue;
		}

		if( !strstr( $k, '.' ) ){
			$new_css[$k] = $v;
			continue;
		}

		// Doesn't work well with ::not()
		if( strstr( $k, ':not(' ) ){
			$new_css[$k] = $v;
			continue;
		}

		$key = trim( $k );
		$key = explode( ',', $key );

		foreach( $key as $k2 => $v2 ){

			if( isset( $remove_selectors[$v2] ) ){
				unset( $key[$k2] );
			}
		}

		if( $key ){

			$key = implode( ',', $key );

			if( !isset( $occurence[$key] ) ){
				$occurence[$key] = 0;
			}
			if( !isset( $spaces[$occurence[$key]] ) ){
				$spaces[$occurence[$key]] = str_repeat( ' ', $occurence[$key] );
			}

			$space = $spaces[$occurence[$key]];
			$occurence[$key]++;
			$key .= $space;
			$new_css[$key] = $v;
		}
	}

	return $new_css;
}


function fr_css_optimizer_walk( $a, $values = [] ){

	$new = [];
	foreach( $a as $k => $v ){
		if( strstr( $k, '@' ) ){
			foreach( $v as $k => $v ){
				$new[$k] = $v;
			}
		} else {
			$new[$k] = $v;
		}
	}
	$a = $new;
	
	foreach( $a as $k => $v ){

		if( strstr( $k, '@' ) ){
			$values = fr_css_optimizer_walk( $v, $values );
			continue;
		}

		if( !strstr( $k, '.' ) ){
			continue;
		}

		$k = trim( $k );
		$k = explode( ',', $k );

		foreach( $k as $v ){

			if( !strstr( $v, '.' ) ){
				continue;
			}

			$s = trim( $v );
			$s = strstr( $s, '.' );
			$s = substr( $s, 1 );
			$s = str_replace( [ ':', '>', ',', '#', ')', '[' ], ' ', $s );
			$s = explode( '.', $s );

			foreach( $s as $class ){
				if( strstr( $class, ' ' ) ){
					$class = strstr( $class, ' ', true );
				}
				if( !isset( $values[$class] ) ){
					$values[$class] = [];
				}
				$values[$class][] = $v;
			}
		}


	}

	return $values;
}






function fr_html_extract_class_names( $html ){

	$html_class_names = fr_single_loop_extract( $html, 'class="', '"' );
	$html_class_names = implode( ' ', $html_class_names );
	$html_class_names = explode( ' ', $html_class_names );
	$html_class_names = array_unique( $html_class_names );
	$html_class_names = fr_array_remove_empty( $html_class_names );
	
	return $html_class_names;
}


function fr_class_name_shortner( $html, $css = array(), $exclude = [], $exclude_unused_class_names = false ){
	
	if( $exclude_unused_class_names ){
		$html_class_names = fr_html_extract_class_names( $html );
		$html_class_names = array_flip( $html_class_names );
	}

	if( !is_array( $exclude ) ){
		$exclude = explode( ',', $exclude );
		$exclude = array_map( 'trim', $exclude );
	}

	// Only supports wildcards at the end, eg: fa-*
	$exclude_with_wildcards = [];
	foreach( $exclude  as $rule ){
		if( strstr( $rule, '*' ) ){
			$exclude_with_wildcards[] = str_replace( '*', '', $rule );
		}
	}
	$exclude_flipped = array_flip( $exclude );
	$exclude_with_wildcards_flipped = array_flip( $exclude_with_wildcards );
	foreach( $css as $k => $v ){
		$css[$k] = fr_css_decode( $v );

		$classes = array();
		foreach( $css[$k] as $selector => $attr ){
			if( !strstr( $selector, '@media' ) ){
				$extracted = fr_class_extract_from_selector( $selector );

				foreach( $extracted as $class ){
					foreach( $exclude_with_wildcards as $rule ){
						if( substr( $class, 0, strlen( $rule ) ) === $rule ){
							$exclude_flipped[$class] = '';
							continue 3;
						}
					}
				}

				if( $exclude_unused_class_names && !strstr( $selector, ',' ) && !strstr( $selector, 'not(' ) && $extracted ){
					if( $diff = array_diff_key( array_flip( $extracted ), $html_class_names ) ){
						if( !array_intersect_key( $diff, $exclude_flipped ) ){
							unset( $css[$k][$selector] );
							continue;
						}
					}
				}

				foreach( $extracted as $class ){
					if( !isset( $classes[$class] ) ){
						$classes[$class] = 0;
					}
					$classes[$class]++;
				}
			}else{
				// Media queries
				foreach( $attr as $selector2 => $attr2 ){
					$extracted = fr_class_extract_from_selector( $selector2 );
					foreach( $extracted as $class ){
						if( !isset( $classes[$class] ) ){
							$classes[$class] = 0;
						}
						$classes[$class]++;
					}
				}
			}
		}
	}


	asort( $classes, SORT_NUMERIC );
	$classes = array_reverse( $classes, 1 );


	// Define characters to be used in class names
	$chars = fr_chars( 'alpha,numeric' );
	$chars = implode( '', $chars );

	

	// Generate short names for each class
	$index = '9';


	foreach( $classes as $class => $count ){

		if( isset( $exclude_flipped[$class] ) ){
			$classes[$class] = $class;
			continue;
		}

		while( 1 ){
			$index = fr_increment( $index, $chars );

			if( isset( $exclude_flipped[$index] ) ){
				continue;
			}
			
			break;
		}

		while( is_numeric( substr( $index, 0, 1 ) ) || $index == 'ad' ){// remove "ad" as class name so ad blockers don't remove the dom element
			$index = fr_increment( $index, $chars );
		}
		$classes[$class] = $index;
	}

	uksort( $classes, function( $a, $b ){
		return strlen( $a ) <=> strlen( $b );
	} );
	

	// Replace old class names with the shorter ones
	$targets = array();
	$replacements = array();
	$separators = array( '.', ',', ' ', '{','>', ':', '#', ')' );
	$separators2 = array( "\t.", "\t,", "\t ", "\t{", "\t>", "\t:", "\t#", "\t)" );
	foreach ($classes as $class => $shortname ){
		$targets[] = '.' . $class . "\t";
		$replacements[] = '.' . $shortname . "\t";
	}
	foreach( $css as $k => $v ){
		$new = array();
		foreach( $css[$k] as $selector => $attr ){
			if( !strstr( $selector, '@media' ) ){
				$selector = str_replace( $separators, $separators2, $selector ) . "\t";
				$key = substr( $selector, 1 );
				if( substr( $selector, 0, 1 ) == '.' && isset( $classes[$key] ) ){
					$selector = '.' . $classes[$key];
				}else{
					$selector = str_replace( $targets, $replacements, $selector );
				}
				$selector = str_replace( "\t", "", $selector );
				$new[$selector] = $attr;
			}else{
				// Media queries
				$new2 = array();
				foreach( $attr as $selector2 => $attr2 ){
					$selector2 = str_replace( $separators, $separators2, $selector2 ) . "\t";
					$key = substr( $selector2, 1 );
					if( substr( $selector2, 0, 1 ) == '.' && isset( $classes[$key] ) ){
						$selector2 = '.' . $classes[$key];
					}else{
						$selector2 = str_replace( $targets, $replacements, $selector2 );
					}
					$selector2 = str_replace( "\t", "", $selector2 );
					$new2[$selector2] = $attr2;
				}
				$new[$selector] = $new2;
			}
		}
		$css[$k] = $new;

		$css[$k] = fr_css_encode( $css[$k] );
	}


	// Replace old classes with new classes in html
	$doc = new fr_dom( $html );
	$selectors = array();
	foreach( $classes as $class => $shortname ){
		$selectors[] = '.' . $class;
	}
	$selectors = implode( ',', $selectors );
	$el = $doc->find( '[class]' );
	$el->each( function( $ob ) use ( $classes ) {
		$class = fr_dom( $ob )->attr( 'class' );
		$class = trim( $class );
		if( $class ){
			$class = explode( ' ', $class );
			foreach( $class as $k => $v ){
				if( isset( $classes[$v] ) ){
					$class[$k] = $classes[$v];
				}
			}
			$class = implode( ' ', $class );
			fr_dom( $ob )->attr( 'class', $class );
		}
	} );

	$html = $doc->output();

	return array( $html, $css );
}



/**
 * Class extractor
 * v2.0
 * - Optimized
 */
function fr_class_extract_from_selector( $selector ){

	$selector = strstr( $selector, '.' );
	$selector = str_replace( [ ':', '>', ',', '#', ')', '[' ], ' ', $selector );
	$s = explode( '.', $selector );
	array_shift( $s );

	$classes = [];
	foreach( $s as $k => $class ){
		if( strstr( $class, ' ' ) ){
			$class = strstr( $class, ' ', true );
		}
		$classes[] = $class;
	}

	return $classes;
}


/**
 * CSS Encode
 * v2.0
 * - allow more than 2 levels of hierarchy
 */
function fr_css_encode( $css, $inline = false, $lvl = 0 ){

	$all = [];
	$tabs = str_repeat( "\t", $lvl );
	
	foreach( $css as $k => $v ){

		$all[] = $tabs . trim( $k ) . ' {';

		foreach( $v as $k2 => $v2 ){

			if( is_array( $v2 ) ){
				$all[] = fr_css_encode( [ $k2 => $v2 ], false, $lvl + 1 );
			}else{
				$all[] = $tabs . "\t" . $k2 . ': ' . $v2 . ';';
			}
		}

		$all[] = $tabs . '}';

	}

	$all = implode( "\n", $all );

	if( $inline ){
		$all = fr_css_minify( $all );
	}

	return $all;
}


/**
 * CSS Decode
 * v2.0
 * - allow more than 2 levels of hierarchy
 */
function fr_css_decode( $css ){

	// Normalize
	$css = fr_css_minify( $css, false, true );
	$css = str_replace( ';', ";\n", $css );
	$css = str_replace( ";\n\"", ";\"", $css );
	$css = str_replace( ";\n'", ";'", $css );
	$css = str_replace( "\nbase64,", "base64,", $css );
	$css = str_replace( "\nutf8,", "utf8,", $css );
	$css = str_replace( "\ncharset=utf-8,", "charset=utf-8,", $css );


	$css = str_replace( '}', "\n}\n", $css );
	$css = str_replace( '{', "{\n", $css );
	$css = str_replace( '}', "}\n", $css );
	$css = explode( "\n", $css );

	list( $ob, $x ) = fr_css_decode_recursive( $css );

	return $ob;
}



/**
 * Decode per one CSS lvl
 * v1.0
 */
function fr_css_decode_recursive( $css, $x = 0, $lvl = 0 ){

	// If lvl is to high then end decoding
	if( $lvl > 10 ){
		return [ [], 10000000000 ];
	}

	$ob = [];
	$selector = false;
	$occurence = [];
	$spaces_cache = [];

	while( isset( $css[$x] ) ){

		$v = $css[$x];

		if( strstr( $v, '{' ) ){

			if( $selector ){
				list( $ob[$selector], $x ) = fr_css_decode_recursive( $css, $x, $lvl + 1 );
				$selector = false;
			} else {

				$selector = trim( substr( $v, 0, -1 ) );

				if( !isset( $occurence[$selector] ) ){
					$occurence[$selector] = 0;
				}

				if( isset( $spaces_cache[$occurence[$selector]] ) ){
					$spaces = $spaces_cache[$occurence[$selector]];
				} else {
					$spaces = str_repeat( ' ', $occurence[$selector] );
				}
				
				$occurence[$selector]++;
				$selector .= $spaces;

				$ob[$selector] = [];
			}
		} else if( strstr( $v, '}' ) ){

			if( !$selector ){
				$x++;
				break;
			}

			$selector = false;

		} else if( strstr( $v, ':' ) ){
			$attr = strstr( $v, ':', true );
			$val = strstr( $v, ':' );
			$val = substr( $val, 1 );
			$val = rtrim( $val, ';' );
			$ob[$selector][$attr] = $val;
		}

		$x++;
	}

	return [ $ob, $x ];
}



function fr_css_extract_imports_recursive( $file, $found = [] ){
	$css = file_get_contents( $file );
	$import_files = fr_css_extract_imports( $css, dirname( $file ) );

	$found[] = $file;
	$files = [];
	foreach( $import_files as $import_file ){
		if( in_array( $import_file, $found ) ){
			continue;
		}
		$res = fr_css_extract_imports_recursive( $import_file, $found );
		$files = array_merge( $files, $res );
		$files[] = $import_file;
	}

	return $files;
}

function fr_css_extract_imports( $css, $dir ){
	$import_files = [];
	$css = explode( '@import', $css );
	foreach( $css as $k => $import ){
		if( $k ){
			$import = strstr( $import, ';', 1 );
			$import = str_replace( [ ' ', '"', "'" ], '', $import );
			$import = trim( $import );
			$import = trim( $import, '/' );
			$file = $dir . '/' . $import;
			if( file_exists( $file ) ){
				$import_files[] = realpath( $file );
			}
		}
	}
	return $import_files;
}





// https://developers.google.com/closure/compiler/docs/api-tutorial1
function fr_js_compress( $file, $file_contents = false ){

    $js = $file_contents ? $file_contents : file_get_contents( $file );

	return fr_js_minify( $js );

    $url = 'https://closure-compiler.appspot.com/compile';
    $js = $file_contents ? $file_contents : file_get_contents( $file );
    $ch = curl_init();

    $compressed = fr_curl( $url, [
    	'js_code' => $js,
    	'compilation_level' => 'SIMPLE_OPTIMIZATIONS',
    	'output_format' => 'text',
    	'output_info' => 'compiled_code'
    ], [
    	'Content-type' => 'application/x-www-form-urlencoded'
    ] );

    return $compressed['body'];
}

