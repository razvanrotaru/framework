<?php
// Docs: https://code.google.com/archive/p/phpquery/
include_once( FRAMEWORK_DIR . '/resources/phpQuery-onefile.php' );
class fr_dom extends phpQuery{
	var $dom;
	var $has_html;
	function __construct( $html ){

		$html = str_replace( '[class]=', 'data-amp-attr-class=', $html );
		$html = str_replace( '[hidden]=', 'data-amp-attr-hidden=', $html );

		// Has problems with HTML comments. Keep WP comments as they are required for rendering
		$tags = fr_single_loop_extract( $html, '<!-- wp:', '-->' );
		foreach( $tags as $tag ){
			$html = str_replace( '<!-- wp:' . $tag . '-->', '<wp_comment>' . $tag . '</wp_comment>', $html );
		}
		$tags = fr_single_loop_extract( $html, '<!-- /wp:', '-->' );
		foreach( $tags as $tag ){
			$html = str_replace( '<!-- /wp:' . $tag . '-->', '<wp_end_comment>' . $tag . '</wp_end_comment>', $html );
		}

		$this->has_html = true;
		if( !strstr( $html, '<html' ) ){
			$html = '<html>' . $html . '</html>';
			$this->has_html = false;
		}
		$this->dom = phpQuery::newDocumentHTML( $html );
		return $this->dom;
	}
	function html(){
		return $this->dom->html();
	}
	function find( $s ){
		return $this->dom->find( $s );
	}
	function append( $s ){
		return $this->dom->append( $s );
	}
	function remove(){
		return $this->dom->remove();
	}
	function output(){
		if( $this->has_html ){
			$html = $this->dom->htmlOuter();
		}else{
			$html = $this->dom->find( 'body' )->html();
		}

		$html = str_replace( '<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">', '', $html );
		$html = str_replace( 'data-amp-attr-class=', '[class]=', $html );
		$html = str_replace( 'data-amp-attr-hidden=', '[hidden]=', $html );

		// Revert WP comments
		$html = str_replace( '<wp_comment>', '<!-- wp:', $html );
		$html = str_replace( '</wp_comment>', '-->', $html );
		$html = str_replace( '<wp_end_comment>', '<!-- /wp:', $html );
		$html = str_replace( '</wp_end_comment>', '-->', $html );
		
		return $html;
	}		
}

function fr_dom( $s ){
	return pq( $s );
}




class fr_dom_custom {

	var $data = [];
	var $void_tags = [];
	var $cache = [];

	function __construct( $html = false ){

		$this->void_tags = array_flip( fr_data_html_void_tags() );

		if( $html !== false ){
			return $this->init( $html );
		}

	}


	function init( $html ){

		$this->data = [];
		$this->cache = [
			'start' => [],
			'end' => []
		];

		$pointer = 0;
		$index = 0;

		while( 1 ){

			$pos = strpos( $html, '<', $pointer );

			if( $pos === false ){
				break;
			}

			$pos2 = strpos( $html, '>', $pos );

			if( $pos2 === false ){
				break;
			}

			$tag = substr( $html, $pos + 1, $pos2 - $pos - 1 );
			$type = substr( $tag, 0, 1 ) === '/' ? 'end' : 'start';
			$tag = trim( $tag, '/' );
			
			$space_pos = strpos( $tag, ' ' );
			$tag_name = $space_pos === false ? $tag : substr( $tag, 0, $space_pos );
			$attr = substr( $tag, strlen( $tag_name ) );

			if( isset( $this->void_tags[$tag_name] ) ){
				$type = 'void';
			}

			// Add text nodes

			if( $pointer < $pos ){
				$text = substr( $html, $pointer, $pos - $pointer );
				$this->data[$index] = [
					'name' => $text,
					'type' => 'text',
					'start' => $pointer,
					'end' => $pos - $pointer,
					'attr' => false,
				];

				$this->cache['text'][$text] = $this->cache[$type][$text] ?? [];
				$this->cache['text'][$text][] = $index;
				$index++;
			}


			// Add tag

			$this->data[$index] = [
				'name' => $tag_name,
				'type' => $type,
				'start' => $pos,
				'end' => ( $pos2 + 1 ) - $pos,
				'attr' => $attr,
				'start_tag' => false,
				'end_tag' => false
			];

			$this->cache[$type][$tag_name] = $this->cache[$type][$tag_name] ?? [];
			$this->cache[$type][$tag_name][] = $index;

			$pointer = $pos2 + 1;
			$index++;

		}

		foreach( $this->cache['start'] as $tag_name => $indexes ){

			$end_tags = $this->cache['end'][$tag_name] ?? [];
			$start_tags = array_reverse( $indexes );

			foreach( $start_tags as $start_index ){
				foreach( $end_tags as $k => $end_index ){
					if( $this->data[$end_index]['start'] < $this->data[$start_index]['start'] ){
						continue;
					}
					$this->data[$start_index]['end_tag'] = $end_index;
					$this->data[$end_index]['start_tag'] = $start_index;
					unset( $end_tags[$k] );
					continue 2;
				}
				break;
			}
		}

		return $this->data;

	}	
}



