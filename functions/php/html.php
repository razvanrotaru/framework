<?php 

//Remove tags, including their content without modfying the rest of the HTML

function fr_strip_html_tags_and_content( $html, $tags = [] ){

	$tags = fr_make_array( $tags );

	if( !$tags ){
		$tags = fr_data_html_tags();
	}

	$void_tags = fr_data_html_void_tags();
	$void_tags = array_flip( $void_tags );
	$html_len = strlen( $html );

	foreach( $tags as $tag ){

		while( true ){
			$start_pos = strripos( $html, '<' . $tag );

			if( $start_pos === false ){
				// Remove stray end tags
				if( !isset( $void_tags[$tag] ) ){
					$start_pos = stripos( $html, '</' . $tag );
					while( $start_pos !== false ){
						$end_pos = strpos( $html, '>', $start_pos );
						if( $end_pos == false ){
							$end_pos = $html_len;
						}
						$html = substr( $html, 0, $start_pos ) . substr( $html, $end_pos + 1 );
						$start_pos = stripos( $html, '</' . $tag );
					}
				}
				continue 2;
			}


			if( isset( $void_tags[$tag] ) ){
				$end_pos = stripos( $html, '>', $start_pos );
			} else {
				if( $tag == '!--' ){
					$end_pos = stripos( $html, '-->', $start_pos );
					if( $end_pos === false ){
						$end_pos = $html_len;
					}
					$end_pos += 2;
				} else {
					$end_pos = stripos( $html, '</' . $tag, $start_pos );
					if( $end_pos === false ){
						$end_pos = $html_len;
					} else {
						$end_pos = strpos( $html, '>', $end_pos );
						if( $end_pos === false ){
							$end_pos = $html_len;
						}
					}
				}
			}

			$html = substr( $html, 0, $start_pos ) . substr( $html, $end_pos + 1 );
		}
	}


	return $html;
}


function fr_strip_only_tags($html,$strip_list){
  $strip_list = explode(",",$strip_list);
  foreach ($strip_list as $tag){
      $html = preg_replace('/<\/?' . $tag . '(.|\s)*?>/', '', $html);
  }
  return $html;
}


/**
 * Converts an associative array to a string of HTML attributes
 * @param  array $a
 * @return string
 */
function fr_array_to_tag_attr( $a ){

    if( !is_array( $a ) ){
        return $a;
    }

    $r = [];
    foreach( $a as $k => $v ){
        if( is_string( $v ) && !strlen( $v ) ){
            continue;
        }
        if( is_array( $v ) ){
	        if( $k == 'style' ){
	        	if( !is_numeric( key( $v ) ) ){
		        	$new = [];
		        	foreach( $v as $k2 => $v2 ){
		        		$new[] = $k2 .': ' . $v2;
		        	}
		        	$v = $new;
		        }
		        $v = implode( '; ', $v );
	        } else {
            	$v = implode( ' ', $v );
            }
        }
        if( function_exists( 'esc_attr' ) ){
        	$r[] = $k . '="'. esc_attr( $v ) . '"';
        } else {
        	$r[] = $k . '="'. htmlspecialchars( $v ) . '"';
        }
    }
    $r = implode( ' ', $r );
  
    return $r;
}




/**
 * Converts a string of html attributes to an associative array
 * @param  string $atts The string with attributes
 * @return array
 */
 function fr_html_attr_to_array( $atts ){

 	if( !$atts ){
 		return [];
 	}

    if( is_array( $atts ) ){
        return $atts;
    }

    $a = [];
    $in_key = false;
    $in_value = false;
    $equal_chr = false;
    $value_wrap_chr = false;

    for( $x = 0; $x < strlen( $atts ); $x++ ){

    	$chr = substr( $atts, $x, 1 );
    	$white_space = ctype_space( $chr );
    	
    	// Space before key
    	if( $white_space && $in_key === false ){
    		continue;
    	}

    	// Key
    	if( !$white_space && $chr !== '=' && $equal_chr === false ){
    		$in_key .= $chr;
    		continue;
    	}

    	// First space after key
    	if( $white_space && $equal_chr === false ){
    		$equal_chr = '';
    		continue;
    	}

    	// Equal char
    	if( $value_wrap_chr === false && $chr === '=' ){
    		$equal_chr = '=';
    		continue;
    	}

    	// No equal, next attribute key
    	if( !$white_space && !$equal_chr ){
    		$a[$in_key] = '';
    		$in_key = $chr;
    		continue;
    	}
    	
    	// Start wrapper char
    	if( $value_wrap_chr === false && ( $chr === '"' || $chr === "'" ) ){
    		$value_wrap_chr = $chr;
    		continue;
    	}
    	
    	// Missing wrap char, direct value
    	if( $value_wrap_chr === false && !$white_space ){
    		$value_wrap_chr = '';
    	}


    	// Value
    	if( $value_wrap_chr !== false && $chr !== $value_wrap_chr ){
    		$in_value .= $chr;
    		continue;
    	}

    	// End wrapper, reset
    	if( $value_wrap_chr !== false && $chr === $value_wrap_chr ){
    		$a[$in_key] = $in_value;

    		$in_key = false;
		    $in_value = false;
		    $equal_chr = false;
		    $value_wrap_chr = false;

		    continue;
    	}
    }

    // Missing end wrapper, add last key value
    if( $in_key !== false ){
    	$a[$in_key] = $in_value;
    }

    return $a;
}



// Adds a div that eficiently splits items into rows

function fr_html_separator($x,$max=2){
  for($y=2;$y<=$max;$y++){
    if( fr_is_divisible( $x, $y ) ){
      ?><div class="separator<?php echo $y ?>"></div><?php 
    }
  }
}


//Icon helper function

function fr_html_icon($s,$class="",$type="fa"){
	$attr=array();
	$style="";
	if(strstr($class,":")){
		$attr["style"]=$class;
		$class="";
	}
	$attr["class"]=trim("fa fa-".$s." ".$class);
	return '<i '.fr_array_to_tag_attr($attr).'></i>';
}


function fr_html_a( $link, $title="", $attr = [], $admin_link = false ){

	$attr = fr_html_attr_to_array( $attr );

	if(!$title && is_numeric( $link ) ){
		$title=get_the_title($link);
	}

	if( !$admin_link ){
		$link=fr_get_permalink($link);
	} else {
		if( is_numeric( $link ) ) {
			$link=get_edit_post_link($link);
		} else if( !strstr( $link, '://' ) && $link && $link != '#' ){
			$link = admin_url( $link );
		}
	}

	$attr['href'] = $link;
	return '<a ' . fr_array_to_tag_attr( $attr ) . '>'.$title.'</a>';
}



function fr_html_table( $a, $preformatted = false, $escape = true ) {

	if( !$a ){
		return;
	}

	$headers = array_keys( reset( $a ) );
	$html = '<table border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse; ' . ( $preformatted ? 'white-space: pre' : '' ) . '"><tr>';

	foreach( $headers as $header ) {
		$html .= '<th>' . fr_esc_html( ucwords( str_replace( '_', ' ', $header ) ) ) . '</th>';
	}

	$html .= '</tr>';

	foreach( $a as $row ) {
		$html .= '<tr>';
		foreach( $headers as $header ) {

			$value = $row[$header] ?? '';

			if( $escape ){
				$value = fr_esc_html( $value );
			}

			$html .= '<td>' . $value . '</td>';
		}
		$html .= '</tr>';
	}

	return $html . '</table>';

}



function fr_html_tag( $tag, $content = '', $attr = [] ){
	if( $content === false ){
		return '<' . $tag . ' ' . fr_array_to_tag_attr( $attr ) . '/>';
	}
	return '<' . $tag . ' ' . fr_array_to_tag_attr( $attr ) . '>' . $content . '</' . $tag . '>';
}


function fr_html_array_a( $a ){
	$res = [];
	foreach( $a as $v ){
		$res[] = fr_html_a( $v );
	}
	$res = implode( '</br>', $res );
	return $res;
}


/**
 * Returns a html select element
 * @param  string $name             	Name of the input
 * @param  array  $options          	An associative array of items to be used as the select options
 * @param  mixed  $options_selected   	The lists of selected options
 * @param  mixed $attributes        	A string or associative array of html attributes
 * @return string
 */
function fr_html_select( $name, $options = [], $options_selected = [], $attributes = [] ){
	$options_selected = fr_make_array( $options_selected );
	$attributes = fr_html_attr_to_array( $attributes );

	$attributes['name'] = $name;

	ob_start();
	?>
	<select <?php echo fr_array_to_tag_attr( $attributes ) ?>>
		<?php 
		foreach($options as $k=>$v){
			?><option  <?php if( in_array( $k, $options_selected ) ){ echo "selected='selected'"; } ?> value="<?php echo $k ?>"><?php echo $v ?></option><?php 
		} ?>
	</select>
	<?php 
	return ob_get_clean();
}



/**
 * If the value matches the expected value it returns the selected attribute
 */
function fr_html_selected( $value, $expected ){
	if( $value == $expected ){
		return 'selected="selected"';
	}
}


/**
 * If the value matches the expected value it returns the checked attribute
 */
function fr_html_checked( $value, $expected ){
	if( $value == $expected ){
		return 'checked="checked"';
	}
}




//Helper Function to display  dinamically the ACF flexible content field with content from the /layouts folder

function fr_show_content($groups){
	if($groups){
		foreach($groups as $group){
			/* Structure:
			Visibility(select): Desktop & Mobile, Desktop Only, Mobile Only
			*/
			$file=get_template_directory()."/layouts/".$group["acf_fc_layout"].".php";
			if(file_exists($file)){
				$class=array();
				$class[]="layout_".$group["acf_fc_layout"];
				if(!empty($group["visibility"])){
					$class[]="on_".$group["visibility"];
				}
				?>
				<div class="<?php echo implode(" ",$class) ?>">
					<?php include($file) ?>
				</div>
				<?php 
			}
		}
	}
}


/**
 * Generates html for displaying a font icon
 * @param  string  			$name 	This is the name of the icon. If a space character is introduced it won't add the global prefix
 * @param  string/array   	$attr   An array or string of additional attributes to add to the HTML element
 * @param  string 			$html 	Optional HTML to be included in the tag
 * @return string 					Returns <i/>
 */
function fr_icon( $name, $attr = [], $html = '' ){
	$prefix = fr_global( 'fr_fa_icon_prefix' );
	$prefix = $prefix ? $prefix : 'fa fa-';

	if( strstr( $name, ' ' ) ){
		$prefix = '';
	}
	if( !isset( $attr['class'] ) ){
		$attr['class'] = '';
	} else {
		$attr['class'] .= ' ';
	}
	$attr['class'] .= $prefix . $name;

	return '<i ' . fr_array_to_tag_attr( $attr ) . '>' . $html . '</i>';
}

/**
 * Sets a global default icon prefix
 * @param  string $prefix The prefix of the icon
 */
function fr_icon_set_prefix( $prefix ){
	fr_global( 'fr_fa_icon_prefix', $prefix );
}


function fr_price( $price, $formated = true, $remove_zeros = false ){
	if( !$formated ){
		$number = number_format( (float)$price, 2, '.', '' );
	} else {
		$number = number_format( (float)$price, 2 );

		// Add a currency in front of the number
		if( is_string( $formated ) ){
			$number = $formated . $number;
		}
	}

	if( $remove_zeros ){
		$number = rtrim( $number, 0 );
		if( substr( $number, -1, 1 ) == '.' ){
			$number = substr( $number, 0, -1 );
		}
	}

	return $number;
}

function fr_positive_price( $price ){
	if( $price < 0 ){
		$price = 0;
	}
	return fr_price( $price, false );
}


function fr_custom_select( $name, $options, $values=array(), $default_label = '', $multiple=0 ){

	$options = (array)$options;
	if( !is_array( $values ) ){
		$values = array( $values );
	}
	$label = implode( ', ', array_intersect_key( $values, $options ) );
	if( !$label ){
		if( isset( $options[''] ) ){
			$label = $options[''];
			unset( $options[''] );
		}else if( $default_label ){
			$label = $default_label;
		}else{
			$label = __( 'Select', 'framework' );
		}
	}
	$values = array_map( 'strval', $values );
	?>
	<div class="fr_custom_select multiple-<?php echo $multiple ?>" data-multiple="<?php echo $multiple ?>">
		<button class="view" type="button">
			<span class="inner" data-default-text="<?php echo $label ?>"><?php echo $label ?></span>
			<span class="arrow"><?php echo fr_icon( 'angle-down' ) ?></span>
		</button>
		<div class="list fr_custom_scroll">
			<?php foreach($options as $k=>$v){
				$lvl = 0;
				if( strstr( $v, '•' ) ){
					$lvl = count( explode( '•', $v ) ) - 1;
					$v = trim( str_replace( '•', '', $v ) );
				}
				?>
				<label class="item" data-lvl="<?php echo $lvl ?>">
					<input <?php if( in_array( (string)$k, $values, 1 ) ){echo "checked='checked'";} ?> name="<?php echo $name.($multiple && $name?"[]":""); ?>" type="<?php echo $multiple?"checkbox":"radio" ?>" value="<?php echo $k ?>">
					<span><?php echo $v ?></span>
				</label>
			<?php } ?>
		</div>
	</div>
	<?php 
}



function fr_table( $header, $rows, $options = [] ){
	$default_options = [
		'type' => [ 'desktop' ],
		'class' => '',
		'hide_empty' => false
	];
	$options = array_merge( $default_options, $options );
	if( !is_array( $options['type'] ) ){
		$options['type'] = explode( ' ', $options['type'] );
	}

	if( in_array( 'desktop', $options['type'] ) ){ ?>
		<table class="fr_table fr_table_desktop <?php echo $options['class'] ?>">
			<?php if( $header ){ ?>
				<thead>
					<tr>
						<?php foreach( $header as $key => $val ){ ?>
							<th class="fr_table_col_<?php echo $key ?>">
								<?php echo $val ?>
							</th>
						<?php } ?>
					</tr>
				</thead>
			<?php } ?>
			
			<tbody>
				<?php foreach( $rows as $row ){
					$class = '';
					if( isset( $row['row_class'] ) ){
						$class = $row['row_class'];
						unset( $row['row_class'] );
					}
					?>
					<tr class="<?php echo esc_attr( $class ) ?>">
						<?php foreach( $row as $key => $val ){
							$val = fr_call_if_func( $val, [$row], 1 );
							?>
							<td class="fr_table_col fr_table_col_<?php echo esc_attr( $key ) ?>"><?php echo $val ?></td>
						<?php } ?>
					</tr>
					<?php 
				}
				?>
			</tbody>
		</table>
	<?php } ?>
	
	<?php if( in_array( 'mobile', $options['type'] ) ){ ?>
		<table class="fr_table fr_table_mobile <?php echo $options['class'] ?>">
			<?php 
			foreach( $rows as $k => $row ){
				$keys = array_keys( $row );

				$class = '';
				if( isset( $row['row_class'] ) ){
					$class = $row['row_class'];
					unset( $row['row_class'] );
				}

				foreach( $row as $key => $val ){
					$val = fr_call_if_func( $val, [$row], 1 );
					if( $options['hide_empty'] && strlen( $val ) == 0 ){
						continue;
					}
					$header_key = array_search( $key, $keys );
					?>
					<tr class="fr_table_row fr_table_row_<?php echo esc_attr( $key ) ?> <?php echo $class ?>">
						<td><?php echo isset( $header[$key] ) ? $header[$key] : '' ?></td>
						<td><?php echo $val ?></td>
					</tr>
				<?php } ?>

				<?php if( !fr_array_is_last( $rows, $k ) ){ ?>
					<tr class="fr_table_spacing">
						<td colspan="2">&nbsp;</td>
					</tr>
				<?php } ?>
			<?php } ?>
		</table>
	<?php } ?>
	<?php 
}



function fr_html_apply_css( $html, $css ){
	$dom = new fr_dom( $html );
	$css = fr_css_decode( $css );
	foreach( $css as $selector => $attributes ){
		$selectors = $dom->find( $selector );
		$dom->each( $selectors, function( $index, $selector_ob ) use ( $attributes, $dom ){ 
			$new = [];
			foreach( $attributes as $k => $v ){
				$new[] = $k . ':' . $v . ';';
			}
			$new = implode( "", $new );
			$style = fr_dom( $selector_ob )->attr( 'style' );
			fr_dom( $selector_ob )->attr( 'style', trim( $style . ';' . $new, ';' ) );
		} );
	}
	$html = $dom->output();
	return $html;
}






function fr_html_json_content( $content ){
	return '<script type="text/html" class="fr_json_content">' . fr_html_minify( $content ) . '</script>';
}

function fr_html_remove_comments( $html ){
	return preg_replace('/<!--(.*)-->/Uis', '', $html);
}

// It is used to add new lines before block elements to keep the visual positioning after stripping tags
function fr_html_format_text_based_on_tags( $s ){
	$block_tags = 'address,article,aside,blockquote,canvas,dd,div,dl,dt,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hr,li,main,nav,noscript,ol,p,pre,section,table,tfoot,ul,video';
	$block_tags = fr_explode( $block_tags );
	foreach( $block_tags as $tag ){
		$s = str_replace( '</' . $tag . '>', '</' . $tag . '>' . "\n", $s );
		$s = str_replace( '<' . $tag, "\n" . '<' . $tag, $s );
	}
	$s = fr_replace_loop( "\r", "\n", $s );
	$s = fr_replace_loop( "\n\t", "\n", $s );
	$s = fr_replace_loop( "\n ", "\n", $s );
	$s = fr_replace_loop( "\n\n", "\n", $s );
	$s = trim( $s );

	return $s;
}


// Wrap a string in an html tag
function fr_html_add_tag( $tag, $s, $attributes = [] ){
	$attributes = fr_array_to_tag_attr( $attributes );
	return sprintf( '<%1$s %2$s>%3$s</%1$s>', $tag, $attributes, $s );
}



function fr_html_target_blank_external_links( $content, $local_domains ){
	$local_domains = fr_make_array( $local_domains );
	$local_domains = array_flip( $local_domains );
	$dom = new fr_dom( $content );
	$links = $dom->find( 'a[href]' );
	$changed = false;
	foreach( $links as $link ){
		$href = fr_dom( $link )->attr( 'href' );
		if( !$href || $href == '#' ){
			continue;
		}
		$domain = fr_get_domain( $href );
		if( $domain && !isset( $local_domains[$domain] ) && fr_dom( $link )->attr( 'target' ) != '_blank' ){
			$changed = true;
			fr_dom( $link )->attr( 'target', '_blank' );
		}
	}
	if( $changed ){
		$content = $dom->output();
	}
	return $content;
}


function fr_html_convert_text_to_links( $s, $link_attr = [ 'target' => '_blank' ] ){
	$links = fr_extract_urls( $s );
	$links = array_unique( $links );
	foreach( $links as $link ){
		$s = str_replace( $link, fr_html_a( $link, $link, $link_attr ), $s );
	}
	return $s;
}



function fr_html_star_rating( $rating, $max, $full_star, $empty_star, $half_star = false ){

	if( $rating < 1 ){
		$rating = 1;
	}

	if( $half_star == false ){
		$rating = round( $rating );
	}

	$s = '<span class="fr_rating">';
	for( $x = 1; $x <= $max; $x++ ){
		if( $x <= $rating ){
			$s .= $full_star;
		} else if( 1 - ( $x - $rating ) >= 0.3 ){
			$s .= $half_star;
		} else {
			$s .= $empty_star;
		}
	}
	$s .= '</span>';

	return $s;
}



function fr_html_star_rating_precise( $rating, $max, $full_star, $empty_star ){

	if ($rating < 0) {
		$rating = 0;
	} elseif ($rating > $max) {
		$rating = $max;
	}

	$half_star = str_replace( '<svg', '<svg style="display: block; width: 0.9em;height: 0.9em;margin-top: 0.05em;margin-left: 0.05em;"', $full_star );
	$full_star = str_replace( '<svg', '<svg style="float: left; display: block;"', $full_star );
	$empty_star = str_replace( '<svg', '<svg style="float: left; display: block;"', $empty_star );

	$s = '<span class="fr_rating">';
	for ($x = 1; $x <= $max; $x++) { 
		$difference = $rating - ($x - 1);
		if ($difference >= 1) { 
			$s .= $full_star;
		} elseif ($difference > 0) {
			$percentage = $difference * 100; 
			$s .= '<span style="position:relative;display:inline-block;float: left;">';
			$s .= '<span style=" margin-left: -1em;position:absolute;overflow:hidden;width:' . $percentage . '%;">' . $half_star . '</span>';
			$s .= '<span>' . $empty_star . '</span>';
			$s .= '</span>';
		} else {
			$s .= $empty_star;
		}
	}
	$s .= '</span>';

	return $s;
}






function fr_html_esc_attr( $s ){
	return htmlspecialchars( $s, ENT_QUOTES );
}

function fr_html_esc( $s ){
	return htmlspecialchars( $s );
}


function fr_html_img( $url ){
	return '<img src="' . fr_html_esc_attr( $url ) . '">';
}


function fr_html_audio( $url ){
	return '<audio controls><source src="' . fr_html_esc_attr( $url ) . '"></audio>';
}



function fr_goal_bar( $total, $done, $goal_label ){

	$percent_done = round( 100 / $total * $done, 2 );

	?>
	
	<div style="background-color: lightgray; position: fixed; bottom: 0; left: 0; width: 100%; z-index: 100000" onclick="this.remove()">
		<h1 style="text-align: center; position: absolute; width: 100%; margin-top: 5px; font-size: 35px;"><?php echo $goal_label ?></h1>
		<div style="position: relative; background-color: green; width: <?php echo $percent_done ?>%; white-space: nowrap; color: white; padding: 3px 0; text-align: center; font-weight: bold; border-right: 3px solid white; font-size: 20px">
			<?php echo $done ?>
			/
			<?php echo $total ?>
			(
			<?php echo $percent_done ?>%
			)
		</div>
	</div>
	<?php 

}






