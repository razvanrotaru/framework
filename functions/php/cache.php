<?php

function fr_cache( $keys, $v = null ){

  $keys = fr_make_array( $keys );
  $is_string = is_string( $keys[0] );
  $main_key = $is_string  ? $keys[0] : '';
  $_ENV['fr_cache'] = $_ENV['fr_cache'] ?? [];
  $_ENV['fr_cache'][$main_key] = $_ENV['fr_cache'][$main_key] ?? [];

  if( $is_string && count( $keys ) == 1 ){
    $key = $keys[0];
  } else {
    $key = md5( json_encode( $keys ) );
  }

  if( $v !== null ){
    $_ENV['fr_cache'][$main_key][$key] = $v;
    return $v;
  }

  if( isset( $_ENV['fr_cache'][$main_key][$key] ) ){
    return $_ENV['fr_cache'][$main_key][$key];
  }
  
  return false;
}




function fr_cache_clear( $keys ){
  $keys = fr_make_array( $keys );
  foreach( $keys as $key ){
    if( isset( $_ENV['fr_cache'][$key] ) ){
      unset( $_ENV['fr_cache'][$key] );
    }
  }
}