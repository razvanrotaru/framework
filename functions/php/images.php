<?php
function fr_img_get_size( $path ){

	if( !file_exists( $path ) ){
		return false;
	}


	// For SVG files
	if( fr_file_ext( $path ) == 'svg' ){
		
		$c = file_get_contents( $path );
		$data = fr_extract( $c, 'viewBox="', '"' );

		if( !$data ){
			return false;
		}

		$data = explode( ' ', $data );
		$data = array_slice( $data, 2 );

		if( count( $data ) != 2 ){
			return false;
		}

		$size = [];
		list( $size['width'], $size['height'] ) = $data;

	} else {
		// For other images

		$data = getimagesize( $path );

		if( !$data || count( $data ) < 2 ){
			return false;
		}
		$size = [];
		list( $size['width'], $size['height'] ) = $data;
	}

	return $size;
}



function fr_svg( $file, $attr = [] ){

	$c = file_get_contents( $file );
	$c = str_replace( '<svg ', '<svg ' . fr_array_to_tag_attr( $attr ), $c );

	return $c;
}



function fr_image_base64( $image ){
    if( file_exists( $image ) ){
        $type = pathinfo( $image, PATHINFO_EXTENSION );
        $data = file_get_contents( $image );
        return 'data:image/' . $type . ';base64,' . base64_encode( $data );
    }
}




