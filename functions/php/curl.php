<?php 

function fr_curl( $url, $post = array(), $headers = array(), $add_options = array() ){
  $ch = curl_init();
  $options = [];

  $parse_url = parse_url( $url );
  if( !empty( $parse_url['user'] ) && !empty( $parse_url['pass'] ) ){
    $options[CURLOPT_USERPWD] = $parse_url['user'] . ':' . $parse_url['pass'];
    $options[CURLOPT_HTTPAUTH] = CURLAUTH_BASIC;
    $url = str_replace( $options[CURLOPT_USERPWD] . '@', '', $url );
  }

  $options[CURLOPT_URL] = $url;

  if( $post ){
    $options[CURLOPT_POST] = true;
    if( is_array( $post ) ){
      // Send as json
      if( isset( $headers['Content-Type'] ) && $headers['Content-Type'] == 'application/json' ){
        $headers['Content-Length'] = strlen( json_encode( $post ) );
        $options[CURLOPT_POSTFIELDS] = json_encode( $post );

      }else{
        // Send as normal post fields
        $options[CURLOPT_POSTFIELDS] = $post;
      }
    }else{
      // Send as string
      $options[CURLOPT_POSTFIELDS] = $post;
      $headers['Content-Length'] = strlen( $post );
    }
  }

  // Set Headers
  foreach( $headers as $key => $header ){
    if( !is_numeric( $key ) ){
      $headers[] = $key . ': ' . $header;
      unset( $headers[$key] );
    }
  }

  $options[CURLOPT_HTTPHEADER] = $headers;
  $options[CURLOPT_RETURNTRANSFER] = true;
  $options[CURLOPT_VERBOSE] = true;
  $options[CURLOPT_HEADER] = true;
  $options[CURLOPT_FOLLOWLOCATION] = true;
  $options[CURLOPT_FRESH_CONNECT] = true;

  foreach( $add_options as $k => $v ){
    $options[$k] = $v;
  }

  foreach( $options as $key => $option ){
    curl_setopt($ch, $key, $option );
  }

  $response = curl_exec( $ch );

  $header_size = curl_getinfo( $ch, CURLINFO_HEADER_SIZE );
  $header = substr( $response, 0, $header_size );
  $body = substr( $response, $header_size );
  $info = curl_getinfo($ch);
  $error = curl_error($ch);
  $error_code = curl_errno($ch);

  curl_close ($ch);
  
  return array( 
    "header" => $header,
    "body" => $body,
    "info" => $info,
    "post" => $post,
    "headers" => $headers,
    "options" => $options,
    'error_message' => $error,
    'error_code' => $error_code
  );
}

function fr_http_headers_to_array( $header ){
  $header = explode( "\n", $header );
  $data = [];
  foreach( $header as $v ){
    $v = trim( $v );
    if( !$v ){
      continue;
    }
    $v = explode( ':', $v );

    if( !isset( $v[1] ) ){
      $data[] = trim( $v[0] );
    } else {
      $data[trim( $v[0] )] = trim( $v[1] );
    }
  }
  return $data;
}