<?php
function fr_radial_matrix( $space, $y, $x, $radius, $callback ){
    for( $yy = -$radius; $yy <= $radius; $yy++ ){
		for( $xx = -$radius; $xx < $radius; $xx++ ){
            if( isset( $space[$y + $yy][$x + $xx] ) ){
            	$space[$y + $yy][$x + $xx] = call_user_func_array( $callback, array( $space[$y + $yy][$x + $xx], $y + $yy, $x + $xx, $yy, $xx ) );
            }
        }
    }
    return $space;
}

function fr_ai_get_radial_matrix( $space, $y, $x, $radius ){
	$new_space = array();
    for( $yy = -$radius; $yy <= $radius; $yy++ ){
		for( $xx = -$radius; $xx <= $radius; $xx++ ){
            if( isset( $space[$y + $yy][$x + $xx] ) ){
            	$new_space[$y + $yy][$x + $xx] = $space[$y + $yy][$x + $xx];
            }
        }
    }
    return $new_space;
}

function fr_ai_fallow_path( $space, $y, $x, $start_path = array(), $callback = false, $top_diff = 0, $lvl = 0 ){
	global $fr_ai_fallow_path;
	global $rec_count;
	if( $lvl == 0 ){
		$fr_ai_fallow_path = $start_path;
		$rec_count = 0;
	}

	$rec_count++;

	$new_space = fr_ai_get_radial_matrix( $space, $y, $x, 1 );

	$to_explore = array();

	foreach( $new_space as $yy => $row ){
		foreach( $row as $xx=> $val ){

			if( isset( $fr_ai_fallow_path[$yy][$xx] ) && !$fr_ai_fallow_path[$yy][$xx] ){
				continue;
			}

			$diff = abs( $y - $yy ) + abs( $x - $xx );
			$score = $top_diff + $diff;
			
			$score = call_user_func_array( $callback, array( $score, $val, $yy, $xx ) );
			if( isset( $fr_ai_fallow_path[$yy][$xx] ) ){
				$ors = $fr_ai_fallow_path[$yy][$xx];
				$fr_ai_fallow_path[$yy][$xx] = min( $fr_ai_fallow_path[$yy][$xx], $score );
			}else{
				$ors = false;
				$fr_ai_fallow_path[$yy][$xx] = $score;
			}

			if( $fr_ai_fallow_path[$yy][$xx] && ( $ors == false || $ors > $score ) ){
				$to_explore[] = array( $yy, $xx, $fr_ai_fallow_path[$yy][$xx], $lvl + 1 );
			}
		}
	}

	if( $lvl == 0 ){
		while( count( $to_explore ) ){
			$next = array();
			foreach( $to_explore as $k => $v ){
				$res = fr_ai_fallow_path( $space, $v[0], $v[1], $start_path, $callback, $v[2], $v[3] );
				$next = array_merge( $next, $res );
			}
			$to_explore = $next;
		}
		return $fr_ai_fallow_path;
	}else{
		return $to_explore;
	}
}