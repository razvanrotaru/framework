<?php
class FR_FTP {

	var $ftp;

	function __construct( $host, $user, $pass ){
		$this->ftp = $this->connect( $host, $user, $pass );
		return $this->ftp;
	}

	function connect( $host, $user, $pass ){
		$ftp = ftp_connect( $host );

		if( !$ftp ){
			return false;
		}

		$login = ftp_login( $ftp, $user, $pass );

		if( !$login ){
			return false;
		}

		return $login;
	}

	function upload( $local_file, $remote_file ){
		return ftp_put( $this->ftp, $remote_file, $local_file, FTP_BINARY );
	}

	function download( $local_file, $remote_file ){
		return ftp_get( $this->ftp, $local_file, $remote_file, FTP_BINARY );
	}

	function delete( $destination_file ){
		return ftp_delete( $this->ftp, $destination_file );
	}

	function close(){
		ftp_close( $this->ftp );
	}

}