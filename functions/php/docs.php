<?php
function fr_docs_activate_display(){

	$exclude = isset( $_GET['exclude'] ) ? explode( ',', $_GET['exclude'] ) : [];
	$exclude = array_merge( $exclude, [ 'resources', 'testing-ground.php' ] );

	fr_GET_trigger( 'framework', function() use ( $exclude ){ 
		new fr_show_docs( FRAMEWORK_DIR . '/functions/', $exclude );
	}, true );

	fr_GET_trigger( 'theme_functions', function() use ( $exclude ){ 
		new fr_show_docs( THEME_DIR, $exclude );
	}, true );

	fr_GET_trigger( 'wp_functions', function() use ( $exclude ){ 
		new fr_show_docs( [ ABSPATH . '/wp-admin/', ABSPATH . '/wp-includes/' ], $exclude );
	}, true );

	fr_GET_trigger( 'plugin_functions', function( $dirs ) use ( $exclude ){ 
		$dirs = explode( ',', $dirs );
		foreach( $dirs as $k => $v ){
			$dirs[$k] = ABSPATH . 'wp-content/plugins/' . sanitize_title( $v ) . '/';
		}
		new fr_show_docs( $dirs, $exclude );
	}, true );
}



class fr_show_docs{

	function __construct( $dir, $exclude = [] ){
		$func = $this->parse( $dir, $exclude );
		$this->display( $func );
		exit;
	}

	function parse( $dirs, $exclude ){

		$dirs = fr_make_array( $dirs );
		$files = [];

		foreach( $dirs as $dir ){
			$files = array_merge( $files, fr_scan_dir( $dir, $exclude, [ 'php' ] ) );
		}

		$all_funcs = [];

		foreach( $files as $file ){

			$code = file_get_contents( $file['path'] );
			$code = fr_strstra( $code, '<?php' );
			
			$classes = fr_single_loop_extract( $code, "\nclass ", "\n}" );
			$code = fr_single_loop_extract_remove( $code, "\nclass ", "\n}" );
			$traits = fr_single_loop_extract( $code, "\ntrait ", "\n}" );
			$code = fr_single_loop_extract_remove( $code, "\ntrait ", "\n}" );
			$all_funcs = $this->extract_functions( $code, $all_funcs, $file );;
			$classes = array_merge( $classes, $traits );

			foreach( $classes as $class ){

				$name = strstr( $class, '{', true );
				$name = strpos( $name, ' extends ' ) === false ? $name : strstr( $name, ' extends ', true );
				$name = trim( $name );
				$code = fr_strstra( $class, '{' );
				$code = explode( "\n", $code );

				foreach( $code as $k => $v ){
					if( strpos( $v, "\t" ) !== 0 ){
						continue;
					}
					$code[$k] = substr( $v, 1 );
				}

				$code = implode( "\n", $code );
				$all_funcs = $this->extract_functions( $code, $all_funcs, $file, $name );
			}

		}

		ksort( $all_funcs );


		return $all_funcs;
	}






	function extract_functions( $code, $all_funcs, $file, $name_prefix = '' ){

		$code = str_replace( "\r", "", $code );
		$lines = explode( "\n", $code );
		$spaces = '';
		$func_name = '';
		$func_lines = [];
		$comments = [];
		$comments_started = false;

		foreach( $lines as $line ){

			if( !$func_name ){

				if( !$comments_started ){

					if( strpos( $line, 'function ' ) !== false ){

						$func_name = fr_extract( $line, 'function ', '(' );
						$func_name = trim( $func_name );

						if( strpos( $func_name, ' ' ) === false ){

							if( !$func_name ){
								$func_name = 'anonymous_tmp';
								continue;
							}

							if( $name_prefix ){
								$sep = strpos( $line, 'static ' ) === false ? '->' : '::';
								$func_name = $name_prefix . $sep . $func_name;
							}

							$params = fr_extract( $line, '(', '{' );
							$params = rtrim( trim( $params ), ')' );

							$all_funcs[$func_name] = [
								'comments' => trim( implode( "\n", $comments ) ),
								'func_prefix' => trim( strstr( $line, 'function ', true ) ),
								'params' => $params,
								'file' => str_replace( FRAMEWORK_DIR . '/functions', '', $file['path'] ),
								'contents' => ''
							];
							$comments = [];
							$spaces = substr( $line, 0, -strlen( ltrim( $line ) ) );
							$func_lines[] = trim( $line );
							continue;
						}

					}

					if( trim( $line ) && !fr_str_starts_with( $line, $spaces . '/' ) ){
						$comments = [];
						continue;
					}
				}

				if( strpos( $line, '/*' ) !== false ){
					$comments_started = true;
				}

				if( strpos( $line, '*/' ) !== false ){
					$comments_started = false;
				}

				$comments[] = trim( $line );

			} else {

				if( fr_str_starts_with( $line, $spaces . '}' ) ){

					if( isset( $all_funcs[$func_name] ) ){
						$func_lines[] = '}';
						$all_funcs[$func_name]['contents'] = implode( "\n", $func_lines );
					}

				} else {
					$len = strlen( substr( $line, 0, -strlen( trim( $line ) ) ) );
					$len_spaces = strlen( $spaces );
					$func_lines[] = $len >= $len_spaces ? substr( $line, $len_spaces ) : trim( $line );
				}

			}

			if( fr_str_starts_with( $line, $spaces . '}' ) ){
				$func_name = '';
				$func_lines = [];
				$comments = [];
			}
		}

		return $all_funcs;

	}



	function display( $functions ){
		?>
		<html>
			<head>
				<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
				<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
				<script src="<?php echo FRAMEWORK_URL ?>/resources/prism/prism.js"></script>
				<link href="<?php echo FRAMEWORK_URL ?>/resources/prism/prism.css" rel="stylesheet">
				<style>
					body {
						background-color: #333;
					}
					.container {
						margin-top: 50px;
					}
					.card-title {
						cursor: pointer;
					}
					.card-title:last-child {
						margin-bottom: 0;
					}
					.functions {
						display:flex;
        				flex-flow: column;
					}
					.function {
						margin-bottom: 5px;
					}
					code .token.function {
						display: inline!important;
						cursor: pointer;
					}
					code .token.delimiter:first-child,
					code .token.delimiter:last-child {
						display: none!important;
					}
					.function pre code {
                        white-space: pre-wrap;
                    }
				</style>
				<script>

					$( window ).ready( function(){ 

						function throttle(func, limit) {
                            var inThrottle;
                            return function() {
                                var args = arguments;
                                var context = this;
                                if (!inThrottle) {
                                    func.apply(context, args);
                                    inThrottle = true;
                                    setTimeout(() => inThrottle = false, limit);
                                }
                            }
                        }


						// Lazy load Prism formatting
                        function formatVisibleCode() {
                            $('.function pre code2:visible:not(.formatted)').each(function(i, block) {
                                var $block = $(block);
                                var $newCode = $('<code>').attr('class', $block.attr('class') + ' init' ).html($block.html());
                                $block.replaceWith($newCode);

                                Prism.highlightElement($newCode[0]);
                                $newCode.addClass('formatted');
                                
                            });
                        }

                        // Format visible code on initial load
                        formatVisibleCode();

                        // Format code after search
                        $('.search').on('keyup', function() {
                            setTimeout(formatVisibleCode, 100);
                        });

                        // Format code on scroll using our custom throttle function
                        $(window).on('scroll', throttle(formatVisibleCode, 250));







						$.expr[":"].contains = $.expr.createPseudo(function(arg) {
						    return function( elem ) {
						        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
						    };
						});

						$( '.search' ).focus();

						var time;
						$( '.search' ).keyup( function(){ 
							var val = $( this ).val();
							if( time ){
								clearTimeout( time );
							}
							time = setTimeout( function(){
								time = '';
								if( val ){

									$( '.function .collapse:visible' ).hide();
									$( '.function' ).hide();
									$( '.function' ).css( 'order', '' );
									
									var vals = val.includes( ';' ) ? [ val ] : val.toUpperCase().split( ' ' );
									var matches = [];
									var title_matches = 0;
									var scores = [];
									var searches_file_path = val.includes( '/' );
									
									$( '.function' ).each( function(){

										var score = 0;
										var func_name = $( this ).find( '.name' ).text();
										var comments = $( this ).find( '.comments' ).text();
										var code = $( this ).find( 'code' ).text().split( '{' ).slice( 1 ).join( '{' );
										var file_path = $( this ).find( 'small' ).text();

										for( k in vals ){
											if( func_name.toUpperCase().split( vals[k] ).length > 1 ){
												score += 100;
											}
											if( searches_file_path && file_path.toUpperCase().split( vals[k] ).length > 1 ){
												score += 10;
											}
											if( code.toUpperCase().split( vals[k] ).length > 1 ){
												score += 1;
											}
											if( comments.toUpperCase().split( vals[k] ).length > 1 ){
												score += 1;
											}
										}

										if( !score ){
											return;
										}

										if( score >= 100 ){
											title_matches++;
										}

										matches.push( { func: $( this ), score: score } );
									} );

									matches.sort( function( a, b ){
										return b.score - a.score;
									} );

									matches = matches.slice( 0, 30 );

									for( k in matches ){
										if( !title_matches || ( title_matches && matches[k].score >= 100 ) ){
											matches[k].func.show().css( 'order', k + 1 );
										}
									}

									if( title_matches ){
										$( '.matches' ).text( title_matches );
									} else {
										$( '.matches' ).text( matches.length );
									}

									if( title_matches == 1 || ( !title_matches && matches.length == 1 ) ){
										$( '.function:visible:first .collapse' ).show();
									}
									
								} else {
									$( '.function' ).slice( 0, 30 ).show();
									$( '.matches' ).text( 0 );
								}
							}, 1 );
						} );

						$( '.search' ).trigger( 'keyup' );

						$( '.card-title' ).click( function(){ 
							$( this ).next().toggle();
							if ( $( this ).next().is(':visible')) {
                                formatVisibleCode();
                            }
						} );


						$( 'body' ).on( 'click', 'code .token.function', function(){

							var func = $( this ).text();
							var c = '';

							if( $( this ).prev().text() == '>' ){
								func = '->' + func;
								c = $( this ).prev().prev().prev().text();
							}

							if( $( this ).prev().text() == ':' ){
								func = '::' + func;
								var after_node = $( this ).prev().prev()[0];
								var nodes = $( this ).parent()[0].childNodes;
								var last_node = false;
								for( k in nodes ){
									if( nodes[k] === after_node ){
										c = last_node.nodeValue;
										break;
									}
									last_node = nodes[k];
								}
							}

							if( c == '$this' || c == 'self' ){
								c = $( this ).parents( '.card-body' ).first().find( '.card-title' ).text().split( '->' )[0].split( '::' )[0];
							}

							c = c.replace( '$', '' );

							if( c ){
								func = c + func;
							}

							$( '.search' ).val( func ).trigger( 'keyup' );

						} );

					} );
				</script>
			</head>
			<body>
				<div class="container">

					<nav class="navbar navbar-expand-lg navbar-dark">
					  <span class="navbar-brand" href="#">Functions: <?php echo count( $functions ) ?></span>
					  <span class="navbar-brand" href="#">with comments: <?php echo count( array_filter( $functions, function( $a ){ return $a['comments'] ? 1 : 0; } ) ) ?></span>
					  <span class="navbar-brand" href="#">matches: <span class="matches">0</span></span>
					</nav>

					<div class="form-group">
						<input type="text" class="search form-control form-control-lg" placeholder="Search">
					</div>

					<div class="functions">
						<?php 
						$x = 0;
						foreach( $functions as $name => $data ){
							?>
							<div class="function card" style="display: none">
								<div class="card-body">
									<?php if( $data['comments'] ){ ?>
										<div class="comments card-text">
											<pre><code2 class="language-php"><?php echo htmlentities( $data['comments'] ) ?></code2></pre>
										</div>
									<?php } ?>
									<h5 class="card-title"><span class="name"><?php echo $name ?></span>( <small><code class="language-php"><?php echo htmlentities( $data['params'] ) ?></code></small> )</h5>
									<div id="function<?php echo $x ?>" class="collapse">
										<pre><code2 class="language-php"><?php echo htmlentities( '<?php ' . $data['contents'] . '?>' ) ?></code2></pre>
									</div>
									<small><?php echo $data['file'] ?></small>
								</div>
							</div>
						<?php $x++;
						} ?>
					</div>

				</div>
			</body>
		</html>
		<?php 
	}

}