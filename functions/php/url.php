<?php

function fr_url_is_base64( $url ){
	return substr( $url, 0, 5 ) == 'data:' && strstr( $url, ';base64,' );
}




/**
 * v2.1
 * - Added pauses between requests to the same domain
 * - Optional gzip compression for external urls
 */
function fr_url_external_exists( $url, $options = [] ){

	$url = str_replace( ' ', '%20', $url );
	$parts_url = parse_url( $url );

	if( empty( $parts_url['scheme'] ) || !in_array( $parts_url['scheme'], [ 'http', 'https' ] ) ){
		return true;
	}

	$domain = fr_get_domain( $url );
	$cache_key = 'fr_url_external_exists_' . $url . '_' . md5( json_encode( $options ) );
	$code = fr_cache( $cache_key );
	$last_download = (array)fr_global( 'fr_url_external_exists_last_download' );
	$from_cache = true;
	$result_data = fr_global( 'fr_url_external_exists_result' );

	if( $code === false ){

		$from_cache = false;


	    if( isset( $last_download[$domain] ) ){
	    
		    $time_from_last_call = fr_micro_time() - $last_download[$domain];

		    // Allow at least 2 seconds between two consective calls to the same domain
		    if( isset( $last_download[$domain] ) && $time_from_last_call < 2 ){
		    	$time_to_sleep = (int)( ( 2 - $time_from_last_call ) * 1000000 );
		    	usleep( $time_to_sleep );
		    }
		}


		$ch = curl_init( $url );

		$options = $options + [
			CURLOPT_TIMEOUT => 10,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36',
			CURLOPT_BUFFERSIZE => 1000 * 512,
			CURLOPT_NOPROGRESS => false,
			CURLOPT_PROGRESSFUNCTION => function( $download_size, $downloaded ){
				return ( $downloaded >= ( 1000 * 1024 ) ) ? 1 : 0; // Download no more than 1MB
			},
			CURLOPT_HTTPHEADER => []
		];

		if( !empty( $parts_url['user'] ) && !empty( $parts_url['pass'] ) ){
			$options[CURLOPT_USERPWD] = $parts_url['user'] . ":" . $parts_url['pass'];  
		}
	   	
	    foreach( $options as $k => $v ){
	    	curl_setopt( $ch, $k, $v );  
		}

		ob_start();
	    curl_exec( $ch );
	    $c = ob_get_clean();
	    $code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );


	    // If code is 406 Not Acceptable add a gzip option
	    // Gzip is not enabled by default because it takes a long time to process and locally it may overload the server with many unclosed connections
	    if( $code == 406 && !isset( $options[CURLOPT_HTTPHEADER]['encoding'] ) ){
	    	sleep( 2 );
	    	$options[CURLOPT_HTTPHEADER]['encoding'] = 'accept-encoding: gzip, deflate, br';
			return fr_url_external_exists( $url, $options );
	    }

	    $result_data = [
	    	'code' => $code,
	    	'info' => curl_getinfo( $ch )
	    ];

	    curl_close( $ch );
	    fr_cache( $cache_key, $code );

	    $last_download[$domain] = fr_micro_time();
	    fr_global( 'fr_url_external_exists_last_download', $last_download );
	}

	$result_data['from_cache'] = $from_cache;
	fr_global( 'fr_url_external_exists_result', $result_data );

    $res = substr( $code, 0, 1 ) == 2 && strlen( $code ) == 3;

    // Error code for too many requests
    if( $code == 429 ){
    	$res = true;
    }

    return $res;
}