<?php 
class FR_DB {

    var $db;
    var $last_query = '';
    var $time = 0;
    var $error = '';
    var $error_callback = false;

    function __construct( $db_host, $db_user = '', $db_pass = '', $db_name = '', $error_callback = false ){

        if( is_array( $db_host ) ){
            list( $db_host, $db_user, $db_pass, $db_name ) = array_values( $db_host );
        }

        $this->db = $this->connect( $db_host, $db_user, $db_pass, $db_name );
        $this->db->set_charset( "utf8" );
        $this->error_callback = $error_callback;
    }

    function find_replace( $old, $new, $tables = array(), $replace_guids = true ){
        if( !$tables ){
            $tables = $this->get_tables();
        }

        foreach( $tables as $table ){
            $fields = $this->table_field_names( $table );
            $primary_field = $this->primary_field( $table );
            if( !$primary_field ){
                $primary_field = reset( $fields );
            }

            $q = 'WHERE ( ' . $this->to_string( array_fill_keys( $fields, '%' . $old . '%' ), ' OR ', 'LIKE' ) . ' )';

            $rows = $this->query( 'SELECT * FROM `' . $table . '` ' . $q );

            foreach( $rows as $row_index => $row ){
                $update = array();

                foreach( $row as $col => $value ){
                    if( $col == 'guid' && !$replace_guids ){
                        continue;
                    }
                    if( strstr( $value, $old ) ){
                        if( fr_is_serialized( $value ) ){
                            if( !unserialize( $value ) == 'object' ){
                                $value = unserialize( $value );
                                $value = fr_replace_recursive( $old, $new, $value );
                                $value = serialize( $value );
                            }
                        }else{
                            $value = fr_replace_recursive( $old, $new, $value );
                        }
                        $update[$col] = $value;
                    }
                }

                if( $update ){
                    $this->update( $table, $update, array( $primary_field => $row[$primary_field] ) );
                }
            }
        }
        return true;
    }

    function primary_field( $table ){
        return $this->query_var( 'Column_name', 'SHOW KEYS FROM `' . $this->esc( $table ) . '` WHERE Key_name = "PRIMARY"');
    }

    function table_field_names( $table ){
        $fields = $this->table_fields( $table );
        $fields = fr_array_pick( $fields, 'Field' );
        return $fields;
    }

    function table_fields( $table ){
        return $this->query( 'DESCRIBE `' . $this->esc( $table ) . '`');
    }

    function insert( $table, $fields ){

        $keys = array_keys( $fields );

        foreach( $fields as $k => $v ){
            $fields[$k] = $this->esc( $v );
        }

        $result = $this->query( 'INSERT INTO `' . $this->esc( $table ) . '` (`' . implode( '`,`', $keys ) . '`) VALUES ("' . implode( '","', $fields ) . '")' );
        
        if( fr_is_error( $result ) ){
            return $result;
        }

        return $this->db->insert_id;

    }



    function update( $table, $update, $where = array() ){

        $update = $this->to_string( $update, ',' );

        if( is_array( $where ) ){
            $where = $this->to_string( $where, ' AND ' );
        }

        if( !$where ){
            $where = '1 = 1';
        }

        $result = $this->query( 'UPDATE `' . $this->esc( $table ) . '` SET ' . $update . ' WHERE ' . $where );
        
        if( fr_is_error( $result ) ){
            return $result;
        }

        return $this->db->affected_rows;
    }

    function delete( $table, $where = [] ){

        if( is_array( $where ) ){
            $where = $this->to_string( $where, ' AND ' );
            if( !$where ){
                $where = '1 = 1';
            }
        }
        $result = $this->query( 'DELETE FROM `' . $this->esc( $table ) . '` WHERE ' . $where );

        if( fr_is_error( $result ) ){
            return $result;
        }

        return $this->db->affected_rows;

    }

    function to_string( $a, $separator = ', ', $operator = '=' ){

        foreach( $a as $k => $v ){
            $a[$k] = '`' . $this->esc( $k ) . '` ' . $operator . ' "' . $this->esc( $v ) . '"';
        }

        return implode( $separator, $a );
        
    }


    function query( $q, $col_to_return = false ){
        
        $rows = [];

        fr_start_timer( 'FR_DB' );
        $this->error = '';
        
        try{
            $result = $this->db->query( $q );
        }catch( Exception $e ){
            $this->error = $e->getMessage();
            $result = false;
        }

        $this->time = fr_get_timer( 'FR_DB' );
        $this->last_query = $q;

        if( !$this->error ){
            if( $result && is_object( $result ) ){
                while( $row = $result->fetch_assoc() ){
                    $rows[] = $row;
                }
            }

            if( $col_to_return ){
                $rows = array_column( $rows, $col_to_return );
            }
            
        } else{

            $rows = new fr_error( $this->error );

            if( function_exists( 'do_action' ) ){
                do_action( 'fr_db_error', $rows, $q, $col_to_return );
            }

            if( $this->error_callback ){
                ( $this->error_callback )( $this->error, $q );
            }

        }

        return $rows;
    }


    function query_row( $q ){
        $rows = $this->query( $q );

        if( fr_is_error( $rows ) ){
            return $rows;
        }

        return reset( $rows );
    }



    function query_col( $q ){
        return $this->query( $q, 0 );
    }



    function query_var( $var, $q ){
        $rows = $this->query( $q );

        if( fr_is_error( $rows ) ){
            return $rows;
        }

        $row = reset( $rows );
        if( isset( $row[$var] ) ){
            return $row[$var];
        } else {
            return false;
        }
    }

    function get_tables(){
        $db_name = $this->selected_db();
        $tables = $this->query( "SHOW TABLES FROM `" . $db_name . "`" );
        $tables = fr_array_pick( $tables, 'Tables_in_' . $db_name );
        return $tables;
    }

    function rename_table( $from, $to ){
        $this->query( "RENAME TABLE `" . $from . "` TO `" . $to . "`" );
    }

    function selected_db(){
        return $this->query_var( 'DATABASE()', 'SELECT DATABASE();' );
    }

    function count( $table, $q = '' ){
        return $this->query_var( 'COUNT(*)', 'SELECT COUNT(*) FROM `' . $this->esc( $table ) . '` ' . $q );
    }

    function prepare(){
        $args = func_get_args();
        foreach( $args as $k => $arg ){
            if( $k ){
                $args[$k] = $this->esc( $arg );
            }
        }
        return call_user_func_array( 'sprintf', $args );
    }

    function connect( $db_host, $db_user, $db_pass, $db_name = '' ){
        return new mysqli( $db_host, $db_user, $db_pass, $db_name );
    }

    function create_db( $db_name ){
        return $this->db->query("CREATE DATABASE IF NOT EXISTS `" . $this->esc( $db_name ) . "`");
    }

    function esc( $s ){
        return $this->db->real_escape_string( $s );
    }

    function import_db( $file ){
        $templine = '';
        $handle = fopen($file, "r");
        global $Migrate;
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if (substr($line, 0, 2) == '--' || $line == '')
                    continue;

                $templine .= $line;
                if (substr(trim($line), -1, 1) == ';'){

                    // Fix older database export
                    $templine = str_replace( 'TYPE=InnoDB', 'ENGINE=InnoDB',$templine );
                    $templine = str_replace( 'TYPE=MyISAM', 'ENGINE=MyISAM',$templine );

                    $this->db->query($templine);
                    if( $this->db->error ){
                        return $this->db->error;
                    }
                    $templine = '';
                }
            }
            return true;
        }
        fclose( $handle );
    }

    function clear_database( $table_prefix = '' ){
        $db_name = $this->selected_db();
        if( $db_name ){
            $tables = $this->get_tables(); 
            foreach($tables as $table ) {

                // Remove only tables that match this prefix
                if( $table_prefix && substr( $table, 0, strlen( $table_prefix ) ) != $table_prefix ){
                    continue;
                }

                $this->db->query( "DROP TABLE `" . $db_name . "`.`$table`" );
                if ( $this->db->errno ){
                    return;
                }
            }
            return true;
        }
    }

    function get_post_meta( $table, $id_key, $id, $key ){
        return $this->query_var( 'meta_value', 'SELECT meta_value FROM ' . $table . ' WHERE ' . $id_key . ' = "' . (int)$this->esc( $id ) . '" AND meta_key = "' . $this->esc( $key ) . '"' );
    }

    function update_post_meta( $table, $id_key, $id, $key, $val ){
        if( $this->get_post_meta( $table, $id_key, $id, $key ) !== false ){
           return $this->update( $table, [ 'meta_value' => $val ], [ $id_key => (int)$id, 'meta_key' => $key ] );
        } else {
            return  $this->insert_post_meta( $table, $id_key, $id, $key, $val );
        }
    }

    function insert_post_meta( $table, $id_key, $id, $key, $val ){
        return $this->insert( $table, [ $id_key => (int)$id, 'meta_key' => $key, 'meta_value' => $val ] );
    }
}




// This class has no dependencies on the framework

class FR_DB_Standalone {
    function __construct( $table_prefix, $host = '', $user = '', $pass = '', $db = '' ){
        if( is_array( $table_prefix ) ){
            list( $table_prefix, $host, $user, $pass, $db ) = array_values( $table_prefix );
        }

        $this->prefix = $table_prefix;
        $this->db = mysqli_connect( $host, $user, $pass, $db );
    }

    function query( $q ){
        $res = $this->db->query( $q );
        if( !is_bool( $res ) ){
            $rows = [];
            while( $row = $res->fetch_assoc() ){
                $rows[] = $row;
            }
            $res = $rows;
        }

        return $res;
    }

    function get_var( $q, $var ){
        $res = $this->query( $q );
        $res = reset( $res );
        if( isset( $res[$var] ) ){
            return $res[$var];
        } else {
            return false;
        }
    }

    function get_post_meta( $pid, $key ){
        return $this->get_var( 'SELECT meta_value FROM ' . $this->prefix . 'postmeta WHERE post_id = "' . $this->esc( $pid ) . '" AND meta_key = "' . $key . '"', 'meta_value' );
    }

    function update_post_meta( $pid, $key, $val ){
        return $this->query( 'UPDATE ' . $this->prefix . 'postmeta SET meta_value = "' . $this->esc( $val ) . '" WHERE post_id = "' . $this->esc( $pid ) . '" AND meta_key = "' . $key . '"' );
    }

    function esc( $s ){
        return $this->db->real_escape_string( $s );
    }
}




function fr_wp_get_settings_from_file( $db_file ){

    if( !file_exists( $db_file ) ){
        return [];
    }

    $contents = file_get_contents( $db_file );

    $settings = [
        'host' => fr_extract( $contents, "define(|'DB_HOST', '", "'" ),
        'user' => fr_extract( $contents, "define(|'DB_USER', '", "'" ),
        'pass' => fr_extract( $contents, "define(|'DB_PASSWORD', '", "'" ),
        'db' => fr_extract( $contents, "define(|'DB_NAME', '", "'" ),
        'prefix' => fr_extract( $contents, "\$table_prefix|'", "'" ),

        'AUTH_KEY' => fr_extract( $contents, "define(|'AUTH_KEY'|'", "'" ),
        'SECURE_AUTH_KEY' => fr_extract( $contents, "define(|'SECURE_AUTH_KEY'|'", "'" ),
        'LOGGED_IN_KEY' => fr_extract( $contents, "define(|'LOGGED_IN_KEY'|'", "'" ),
        'NONCE_KEY' => fr_extract( $contents, "define(|'NONCE_KEY'|'", "'" ),
        'AUTH_SALT' => fr_extract( $contents, "define(|'AUTH_SALT'|'", "'" ),
        'SECURE_AUTH_SALT' => fr_extract( $contents, "define(|'SECURE_AUTH_SALT'|'", "'" ),
        'LOGGED_IN_SALT' => fr_extract( $contents, "define(|'LOGGED_IN_SALT'|'", "'" ),
        'NONCE_SALT' => fr_extract( $contents, "define(|'NONCE_SALT'|'", "'" ),
    ];

    return $settings;
}





/**
 * Retrives column names from any table in the database
 * @param  string $table The table name
 * @return array
 */
function fr_get_table_columns( $table ){

    global $wpdb;

    $columns = fr_cache( 'fr_get_table_columns_' . $table );
    if( $columns === false ){
        $columns = $wpdb->get_results( 'SHOW COLUMNS FROM ' . $table );
        $columns = array_column( $columns, 'Field' );
        fr_cache( 'fr_get_table_columns_' . $table, $columns );
    }

    return $columns;

}