<?php 


function fr_youtube_video( $vid, $width = '100%', $height = '100%', $amp = false ) {

    list( $vid, $params ) = fr_youtube_id_from_url( $vid );

    if( $vid ) {

        ob_start();

        if( !$amp ) { ?>
            <iframe width="<?php echo $width ?>" height="<?php echo $height ?>" src="<?php echo fr_youtube_embed_url( $vid, $params ) ?>" frameborder="0" allowfullscreen></iframe>
        <?php } else {

            $new_params = [];

            foreach( $params as $k => $param ) {
                $new_params['data-param-' . $k] = $param;
            } ?>

            <amp-youtube data-videoid="<?php echo $vid ?>" layout="responsive" width="<?php echo $width ?>" height="<?php echo $height ?>" <?php echo fr_array_to_tag_attr( $new_params ) ?>></amp-youtube>
        <?php }

        return ob_get_clean();
    }
}



function fr_youtube_embed_url( $vid, $params = [] ) {

    if( filter_var( $vid, FILTER_VALIDATE_URL ) ) {
        list( $vid, $old_params ) = fr_youtube_id_from_url( $vid );
        $params = array_merge( $old_params, $params );
    }

    if( $vid ) {
        return fr_build_link( 'https://www.youtube.com/embed/' . $vid, $params );
    }
}




function fr_youtube_id_from_url( $url ) {

    $url = trim( $url );
    
    if( !filter_var( $url, FILTER_VALIDATE_URL ) ) {
        return [ '', [] ];
    }

    $parsed_url = parse_url( $url );
    $host = $parsed_url['host'];
    $path = isset( $parsed_url['path'] ) ? $parsed_url['path'] : '';
    $query = isset( $parsed_url['query'] ) ? $parsed_url['query'] : '';

    $args = [];
    parse_str( $query, $args );

    // Handle youtu.be short links
    if( $host === 'youtu.be' ) {
        return [trim( $path, '/' ), $args];
    }

    // Handle youtube.com and www.youtube.com
    if( in_array( $host, ['youtube.com', 'www.youtube.com', 'm.youtube.com'] ) ) {
        // Handle /embed/ URLs
        if( strpos( $path, '/embed/' ) === 0 ) {
            $path_parts = explode( '/', trim( $path, '/' ) );
            return [end( $path_parts ), $args];
        }

        // Handle /v/ URLs
        if( strpos( $path, '/v/' ) === 0 ) {
            $path_parts = explode( '/', trim( $path, '/' ) );
            return [end( $path_parts ), $args];
        }

        // Handle /watch URLs
        if( isset( $args['v'] ) ) {
            $id = $args['v'];
            unset( $args['v'] );
            return [$id, $args];
        }

        // Handle /shorts/ URLs
        if( strpos( $path, '/shorts/' ) === 0 ) {
            $path_parts = explode( '/', trim( $path, '/' ) );
            return [end( $path_parts ), $args];
        }
    }

    // If no valid YouTube URL format is found
    return ['', []];
}



function fr_youtube_short_link( $id ){

	return 'https://youtu.be/' . $id;

}








