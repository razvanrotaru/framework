<?php

// https://developers.mailerlite.com/reference

class FR_Mailerlite {

	function __construct( $api_key ){

		$this->api_key = $api_key;
		$this->init( $api_key );

	}



	function init( $api_key ){
		$this->client = new \MailerLiteApi\MailerLite( $api_key );
	}


	function get_subscriber_groups( $subscriber ){
		$subscribersApi = $this->client->subscribers();
		return $subscribersApi->getGroups( $subscriber );
	}

	function add_subscriber_to_group( $group_id, $subscriber ){
		$subscribersApi = $this->client->groups();
		return $subscribersApi->addSubscriber( $group_id, $subscriber );
	}

	function remove_subscriber_from_group( $group_id, $subscriber ){
		$subscribersApi = $this->client->groups();
		return $subscribersApi->removeSubscriber( $group_id, $subscriber );
	}

	function subscribe( $data ){
		$subscribersApi = $this->client->subscribers();
		return $subscribersApi->create( $data );
	}

	function delete_subscriber( $email ){
		$subscribersApi = $this->client->subscribers();
		return $subscribersApi->delete( $email );
	}



	function get_subscriber( $email ){
		$subscribersApi = $this->client->subscribers();
		$res = (array)$subscribersApi->find( $email );

		if( empty( $res['error'] ) ){
			return $res;
		}
	}

	function update_subscriber( $email, $data ){
		$subscribersApi = $this->client->subscribers();
		return $subscribersApi->update( $email, $data );
	}


	function get_webhooks(){
		return $this->custom_call( 'webhooks' );
	}

	function create_webhook( $url, $event ){
		return $this->custom_call( 'webhooks', [
			'url' => $url,
			'event' => $event
		] );
	}


	function custom_call( $endpoint, $post = [], $headers = [], $options = [] ){

		$headers[] = 'X-MailerLite-ApiKey: ' . $this->api_key;

		$res = fr_curl( 'https://api.mailerlite.com/api/v2/' . $endpoint, $post, $headers, $options );
		$res = json_decode( $res['body'], true );
		
		return $res;
	}

}