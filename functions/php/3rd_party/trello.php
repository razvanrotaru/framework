<?php

class Fr_Trello {

	// https://trello.com/app-key
	private $api_key;
	private $token;
	private $api_url = 'https://api.trello.com/1';


	function __construct( $api_key, $token ) {

		$this->api_key = $api_key;
		$this->token = $token;

	}


	function get( $endpoint, $params = [] ) {

		$url = $this->api_url . $endpoint;
		$params['key'] = $this->api_key;
		$params['token'] = $this->token;

		$response = fr_curl( fr_build_link( $url, $params ) );

		return @json_decode( $response['body'], true );

	}
    
    function get_boards(): array {

        $endpoint = '/members/me/boards';
        $params = [
            'fields' => 'name,id',
        ];

        return $this->get($endpoint, $params);

    }
    
    function get_lists($board_id): array {

        $endpoint = "/boards/$board_id/lists";
        $params = [
            'fields' => 'name,id',
        ];

        return $this->get($endpoint, $params);

    }


	function get_cards( $board_id, $list_id ): array {

		$endpoint = "/lists/$list_id/cards";
		$params = [
			'board_id' => $board_id,
		];

		$cards = $this->get( $endpoint, $params );

		return $cards;

	}


}