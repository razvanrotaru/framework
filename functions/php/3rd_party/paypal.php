<?php
// Docs: https://api-docs.transferwise.com/

class PayPal_Payments {

	var $key;
	var $secret;
	var $token;
	var $is_sandbox = false;
	var $debug = false;

	function __construct( $key, $secret, $is_sandbox = false, $debug = false ){
		$this->key = $key;
		$this->secret = $secret;
		$this->is_sandbox = $is_sandbox;
		$this->debug = $debug;
	}

	function make_payment( $recipients, $subject, $message, $batch_id ){

		// If only one recipient then make it an array
		if( !isset( $recipients[0] ) ){
			$recipients = [
				$recipients
			];
		}

		$query = [
			'sender_batch_header' => [
				'sender_batch_id' => $batch_id,
				'email_subject' => $subject,
				'email_message' => $message
			],
			'items' => []
		];

		foreach( $recipients as $recipient ){
			$item = [
				'recipient_type' => 'EMAIL',
				'amount' => [
					'value' => $recipient['amount'],
					'currency' => $recipient['currency']
				],
				'sender_item_id' => $recipient['transfer_id'],
				'receiver' => $recipient['email']
			];
			if( !empty( $recipient['note'] ) ){
				$item['note'] = $recipient['note'];
			}
			$query['items'][] = $item;
		}

		$response = $this->query( 'payments/payouts', $query );

		if( !fr_is_error( $response ) ){
			if( isset( $response['links'][0]['href'] ) ){
				return $response['links'][0]['href'];
			} else {
				if( isset( $response['message'] ) ){
					return new fr_error( $response['message'], $response );
				} else {
					return new fr_error( __( 'Failed to make PayPal payment', 'vc' ), $response );
				}
			}
		} else {
			return $response;
		}
	}

	function get_endpoint( $target ){
		$endpoint = 'https://api.paypal.com/v1/' . $target;
		if( $this->is_sandbox ){
			$endpoint = 'https://api.sandbox.paypal.com/v1/' . $target;
		}
		return $endpoint;
	}

	function query( $target, $query ){

		$endpoint = $this->get_endpoint( $target );

		$this->set_token();

		$res = fr_curl( $endpoint, $query, [
			'Authorization' => 'Bearer ' . $this->token,
			'Content-Type' => 'application/json'
		]  );

		if( !empty( $res['body'] ) ){

			$body = json_decode( $res['body'], 1 );

			if( isset( $body['errors'] ) ){
				$body = fr_error( $body['errors'][0]['message'], $body['errors'] );
			}

			if( $this->debug ){
				fr_p( [
					'endpoint' => $endpoint,
					'request_data' => $query,
					'response_data' => $body
				] );
			}

			return $body;
		} else {
			return new fr_error( 'PayPal connection failed' );
		}
	}

	function set_token(){
		$oauth_endpoint = 'https://api.paypal.com/v1/oauth2/token';
		if( $this->is_sandbox ){
			$oauth_endpoint = 'https://api.sandbox.paypal.com/v1/oauth2/token';
		}

		$res = fr_curl( $oauth_endpoint, 'grant_type=client_credentials', [
			'Content-Type' => 'application/x-www-form-urlencoded',
			'Accept-Language' => 'en_US'
		], [
			CURLOPT_USERPWD => $this->key . ':' . $this->secret
		]  );

		$res = json_decode( $res['body'], 1 );

		if( $res && !empty( $res['access_token'] ) ){
			$this->token = $res['access_token'];
		} else {
			return fr_error( __( 'Failed to retrive PayPal access token', 'vc' ), $res );
		}
	}

	function get_transfer_data( $url ){

		$endpoint = $url;

		$this->set_token();

		$res = fr_curl( $endpoint, [], [
			'Authorization' => 'Bearer ' . $this->token,
			'Content-Type' => 'application/json'
		]  );

		if( !empty( $res['body'] ) ){

			$body = json_decode( $res['body'], 1 );

			if( isset( $body['errors'] ) ){
				$body = fr_error( $body['errors'][0]['message'], $body['errors'] );
			}

			if( $this->debug ){
				fr_p( [
					'endpoint' => $endpoint,
					'request_data' => $query,
					'response_data' => $body
				] );
			}

			return $body;
		} else {
			return new fr_error( 'PayPal connection failed' );
		}
	}

	// Full list of errors: https://developer.paypal.com/docs/api/payments.payouts-batch/v1/#errors 
	function get_status_label( $status ){
		$status = strtolower( $status );
		$status = str_replace( '_', ' ', $status );
		$status = ucwords( $status );
		return $status;
	}
}