<?php
class fr_rapid_api {

	var $data = [];
	var $is_error = false;
	var $curl_data = [];

	function __construct( $url, $host, $key, $headers = [] ){
		$this->data = [
			'url' => $url,
			'host' => $host,
			'key' => $key,
			'headers' => $headers
		];
	}

	function post( $post ){

		$this->is_error = false;

		$headers = array_merge( [
			'content-type' => 'application/json',
			'x-rapidapi-host' => $this->data['host'],
			'x-rapidapi-key' => $this->data['key'],
			'useQueryString' => true
		], $this->data['headers'] );

		$post = json_encode( $post );

		$res = fr_curl( 'https://rapidapi.p.rapidapi.com/' . $this->data['url'], $post, $headers );

		$this->curl_data = $res;

		if( $res['info']['http_code'] != 200 ){
			$this->is_error = true;
		}
		
		$data = @json_decode( $res['body'], true );

		if( !is_array( $data ) ){
			$data = [];
			$this->is_error = true;
		}

		if( empty( $data['data'] ) || !strstr( $data['data'], 'Success' ) ){
			$this->is_error = true;
		}

		return $data;
	}


	function is_error(){
		return $this->is_error;
	}

	function get_curl_data(){
		return $this->curl_data;
	}

}