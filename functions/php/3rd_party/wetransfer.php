<?php

class FR_Wetransfer {

	static function get_direct_download_link( $url, $return_all_data = false ) {

	    // 1. Fetch the HTML content
	    $response = wp_remote_get( $url );
	    $html_content = wp_remote_retrieve_body( $response );

	    // 2. Extract the required parameters
	    $domain_user_id = fr_extract( $html_content, '"trackingId":"', '"' );
	    $security_hash = fr_extract( $html_content, '"recipientId":"', '"' );
	    $transfer_id = fr_extract( $html_content, '"transferId":"', '"' );

	    if ( ! $domain_user_id || ! $security_hash || ! $transfer_id ) {
	        return; // Couldn't extract all required data
	    }

	    // 3. Construct the API request with extracted parameters
	    $api_url = "https://wetransfer.com/api/v4/transfers/{$transfer_id}/download";

	    $body = [
	        'domain_user_id' => $domain_user_id,
	        'intent' => 'entire_transfer',
	        'security_hash' => $security_hash
	    ];

	    $api_response = wp_remote_post( $api_url, [
	        'method' => 'POST',
	        'headers' => [ 'Content-Type' => 'application/json' ],
	        'body' => json_encode( $body ),
	    ] );

	    // 4. Fetch the direct link from the API's JSON response
	    $json_data = json_decode( wp_remote_retrieve_body( $api_response ), true );
	    if ( isset( $json_data['direct_link'] ) ) {

	    	if( $return_all_data ){
	    		return [
	    			'download_url' => $json_data['direct_link'],
	    			'tracking_id' => $domain_user_id,
			        'user_id' => $security_hash,
			        'transfer_id' => $transfer_id
	    		];
	    	}

	        return $json_data['direct_link'];
	    }

	    return false; // No direct link found

	}

}
