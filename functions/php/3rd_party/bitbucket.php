<?php 
class FR_BITBUCKET{

  var $version;
  var $enpoint_root_url;
  var $user;
  var $pass;

  function __construct( $user, $pass ){
    $this->user = $user;
    $this->pass = $pass;
    $this->version = '2.0';
    $this->enpoint_root_url = 'https://' . $user . ':' . $pass . '@api.bitbucket.org';
  }

  function create_repo( $repo_name, $options = array() ){
    $defaults = array(
      'scm' => 'git',
      'is_private' => true
    );

    $options = array_merge( $defaults, $options );

    $url = $this->enpoint_root_url . '/' . $this->version . '/repositories/' . $this->user . '/' . $repo_name;
    $res = fr_curl( $url, $options );

    return json_decode( $res['body'], 1 );
  }

  function get_repo( $repo_name ){

    $url = $this->enpoint_root_url . '/' . $this->version . '/repositories/' . $this->user . '/' . $repo_name;
    $res = fr_curl( $url );

    return json_decode( $res['body'], 1 );
  }

  function is_error( $response ){
    if( $response['type'] == 'error' ){
      return $response['error']['message'];
    }
  }
}