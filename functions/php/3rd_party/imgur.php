<?php


function fr_imgur_extra_code_from_url( $url ){

	if( !strstr( $url, '//imgur.com/' ) ){
		return false;
	}

	$url = fr_strstrb( $url, '#' );
	$url = fr_strstrb( $url, '?' );
	$url = explode( '/', $url );
	$url = fr_array_remove_empty( $url );
	$code = end( $url );

	return $code;
}