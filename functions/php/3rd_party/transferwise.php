<?php
// Docs: https://docs.wise.com/api-docs/api-reference

class TransferWise {

	var $key;
	var $is_sandbox = false;
	var $profile_id;
	var $debug = false;


	// https://transferwise.com/help/articles/2571907/what-currencies-can-i-send-to-and-from
	// https://transferwise.com/help/articles/2974947/what-countries-can-i-send-usd-to

	function __construct( $key, $private_key, $is_sandbox = false, $debug = false ){
		$this->key = $key;
		$this->private_key = $private_key;
		$this->is_sandbox = $is_sandbox;
		$this->debug = $debug;
	}


	function get_balances(){
		return $this->query( 'v3/profiles/' . $this->profile_id . '/balances?types=STANDARD' );
	}


	function get_currency_balance( string $currency ): array {

		$balances = $this->get_balances();

		if( !$balances || !is_array( $balances ) ){
			return [];
		}

		foreach( $balances as $balance ){

			if( $balance['currency'] != $currency ){
				continue;
			}

			return $balance;

		}

		return [];

	}


	function get_statements( string $currency ){

		$balance = $this->get_currency_balance( $currency );

		if( !$balance ){
			return [];
		}

		return $this->query( fr_build_link( 'v1/profiles/' . $this->profile_id . '/balance-statements/' . $balance['id'] . '/statement.json', [
			'currency' => 'USD',
			'intervalStart' => date( "Y-m-d\TH:i:s", strtotime( '-30 days' ) ) . ".000Z",
			'intervalEnd' => date( "Y-m-d\TH:i:s", strtotime( 'now' ) ) . ".000Z",
			'type' => 'COMPACT'
		] ) );

	}

	function get_profiles(){
		return $this->query( 'v1/profiles' );
	}

	function set_profile_id( $profile_id ){
		$this->profile_id = $profile_id;
	}

	function get_quote( $query ){
		return $this->query( 'v1/quotes', $query );
	}

	function create_transfer( $query ){
		return $this->query( 'v1/transfers', $query );
	}

	function get_transfer_requirements( $query ){
		return $this->query( 'v1/transfer-requirements', $query );
	}

	function fund_transfer( $transfer_id ){
		return $this->query( 'v3/profiles/' . $this->profile_id . '/transfers/' . $transfer_id . '/payments', [
			'type' => 'BALANCE'
		] );
	}

	function get_allowed_countries(){
		$res = $this->query( 'v1/countries' );
		if( !fr_is_error( $res ) ){
			return $res;
		} else {
			return array_column( $res['values'], 'name', 'code' );
		}
	}

	function get_address_requirements( $country_code ){
		return $this->query( 'v1/address-requirements', [
			'details' => [
				'country' => strtoupper( $country_code )
			]
		] );
	}

	function get_endpoint( $target ){
		$endpoint = 'https://api.transferwise.com/' . $target;
		if( $this->is_sandbox ){
			$endpoint = 'https://api.sandbox.transferwise.tech/' . $target;
		}
		return $endpoint;
	}

	function query( $target, $query = [] ){

		$endpoint = $this->get_endpoint( $target );

		$options = [];
		if( $this->is_sandbox ){
			$options[CURLOPT_SSL_VERIFYHOST] = false;
			$options[CURLOPT_SSL_VERIFYPEER] = false;
		}

		$res = fr_curl( $endpoint, $query, [
			'Authorization' => 'Bearer ' . $this->key,
			'Content-Type' => 'application/json'
		], $options );

		// If TransferWise requires additional security check
		if( $res['info']['http_code'] == 403 ){
			$response_headers = fr_http_headers_to_array( $res['header'] );
			$approval = $response_headers['x-2fa-approval'] ?? '';
			$signed = openssl_sign( $approval , $signature , $this->private_key, OPENSSL_ALGO_SHA256 );
			if( $signed ){

				// Perform the request again with the signed approval key
				$res = fr_curl( $endpoint, $query, [
					'Authorization' => 'Bearer ' . $this->key,
					'Content-Type' => 'application/json',
					'x-2fa-approval' => $approval,
					'X-Signature' => base64_encode( $signature )
				], $options );
		
			} else {
				$res['body'] = fr_error( 'Failed to sign TransfeWise approval request', [ $target, $query, $res ] );
			}
		}
		


		if( !empty( $res['body'] ) ){
			$body = json_decode( $res['body'], 1 );
			
			if( !$body ){
				$body = $res['body'];
			}

			if( !$body ){
				$body = fr_error( 'Empty response from TransferWise', [ $target, $query ] );
			} else if( isset( $body['errors'] ) ){
				$body = fr_error( $body['errors'][0]['message'], [ $target, $query, $body['errors'] ] );
			} else if( isset( $body['error'] ) ){
				$body = fr_error( $body['error'], [ $target, $query, $body ] );
			}

			if( $this->debug ){
				fr_p( [
					'endpoint' => $endpoint,
					'request_data' => $query,
					'response_data' => $body
				] );
			}

			return $body;
		} else {
			return new fr_error( 'TransferWise connection failed', [ $target, $query, $res ] );
		}
	}

	function send_money_via_email( $amount, $full_name, $source_currency, $target_currency, $transaction_id, $reference, $source_of_funds, $transfer_purpose, $account_details, $guaranteedTargetAmount = true ){
		if( !$this->profile_id ){
			return new fr_error( 'No profile id set' );
		}

		// Get quote
		$quote = $this->get_quote( [
			'profile' => $this->profile_id,
			'source' => $source_currency,
			'target' => $target_currency,
			'rateType' => 'FIXED',
			'sourceAmount' => $amount,
			'type' => 'BALANCE_PAYOUT'
		] );

		if( fr_is_error( $quote ) ){
			$quote->error_type = 'quote from source amount';
			return $quote;
		}

		// This option allows the sender to support the transaction fees
		if( $guaranteedTargetAmount ){
			$quote = $this->get_quote( [
				'profile' => $this->profile_id,
				'source' => $source_currency,
				'target' => $target_currency,
				'rateType' => 'FIXED',
				'targetAmount' => fr_price( $amount * $quote['rate'], false ),
				'type' => 'BALANCE_PAYOUT'
			] );

			if( fr_is_error( $quote ) ){
				$quote->error_type = 'quote from target amount';
				return $quote;
			}
		}


		// Create account from email address

		$data = [
			'profile' => $this->profile_id,
			'accountHolderName' => $full_name,
			'currency' => $target_currency,
			'type' => 'email',
			'details' => $account_details
		];
		
		$account = $this->query( 'v1/accounts', $data );

		if( fr_is_error( $account ) ){
			$account->error_type = 'account creation';
			return $account;
		}


		// Create a transfer

		$transfer = $this->create_transfer( $s = [
			'targetAccount' => $account['id'],
			'quote' => $quote['id'],
			'customerTransactionId' => fr_uuid( $transaction_id ),
			'details' => [
				'reference' => $reference,
				'sourceOfFunds' => $source_of_funds,
				'transferPurpose' => $transfer_purpose
			]
		] );

		if( fr_is_error( $transfer ) ){
			$transfer->error_type = 'create transfer';
			return $transfer;
		}

		// Fund transfer
		$fund_transfer = $this->fund_transfer( $transfer['id'] );

		if( fr_is_error( $fund_transfer ) ){
			$fund_transfer->error_type = 'fund transfer';
			return $fund_transfer;
		}

		if( isset( $fund_transfer['status'] ) && $fund_transfer['status'] == 'COMPLETED' ){
			return $transfer['id'];
		} else {
			return new fr_error( $fund_transfer );
		}
	}


	function get_transfer_data( $transfer ){
		return $this->query( 'v1/transfers/' . $transfer );
	}



	// https://docs.wise.com/api-docs/api-reference/transfer#list
	function get_transfers( $data = [] ){

		$params = [ 'profile' => $this->profile_id ];
		$params = array_merge( $params, $data );
		$url = fr_build_link( 'v1/transfers', $params );

		return $this->query( $url );

	}

	

	function get_status_label( $status ){
		$statuses = [
			'incoming_payment_waiting' => 'Awaiting Funding',
			'waiting_recipient_input_to_proceed' => 'Waiting for Recipient',
			'processing' => 'Processing',
			'funds_converted' => 'Funds Converted',
			'outgoing_payment_sent' => 'Payment Sent',
			'cancelled' => 'Canceled',
			'funds_refunded' => 'Fund Refunded',
			'bounced_back' => 'Bounced Back'
		];

		if( isset( $statuses[$status] ) ){
			return $statuses[$status];
		}
	}



	function get_accepted_currencies( $currency ){

		$currencies = $this->query( 'v1/currency-pairs' );

		if( !$currencies || empty( $currencies['sourceCurrencies'] ) ){
			return;
		}

		$currencies = array_column( $currencies['sourceCurrencies'], 'targetCurrencies', 'currencyCode' );

		if( !isset( $currencies[$currency] ) ){
			return;
		}

		$targets = array_column( $currencies[$currency], 'currencyCode' );

		return $targets;
	}


}

