<?php 
function fr_vimeo_video( $vid, $width = '100%', $height = '100%', $amp = false ){
	if(filter_var($vid, FILTER_VALIDATE_URL)){
		list( $vid, $hash )=fr_vimeo_id_from_url($vid);
	}
	if($vid){
		ob_start();
			if( !$amp ){
				?><iframe width="<?php echo $width ?>" height="<?php echo $height ?>" src="<?php echo fr_vimeo_embed_url($vid) ?>" frameborder="0" allowfullscreen></iframe><?php
			} else {
				?><amp-vimeo data-videoid="<?php echo $vid ?>" layout="responsive" width="<?php echo $width ?>" height="<?php echo $height ?>"></amp-vimeo><?php 
			}
		return ob_get_clean();
	}
}

function fr_vimeo_embed_url( $id, $hash ){
	if(filter_var($id, FILTER_VALIDATE_URL)){
		list( $id, $hash ) =fr_vimeo_id_from_url( $id );
	}
	if($id){
		$url = "https://player.vimeo.com/video/" . $id;
		if( $hash ){
			$url .= "?h=" . $hash;
		}
		return $url;
	}
}

function fr_vimeo_id_from_url($url){
	if( strstr( $url, '?' ) ){
		$url = strstr( $url, '?', 1 );
	}
	$url = trim( $url, '/' );
	$url = explode( '/', $url );

	if( is_numeric( $url[count( $url ) - 2] ) ){
		return [
			'id' => $url[count( $url ) - 2],
			'hash' => $url[count( $url ) - 1]
		];
	} else if( (int)$url[count( $url ) - 1] == $url[count( $url ) - 1] ){
		return [
			'id' => $url[count( $url ) - 1],
			'hash' => ''
		];
	} else {
		return [
			'id' => '',
			'hash' => ''
		];
	}
}