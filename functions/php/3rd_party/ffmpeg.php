<?php

// REQUIRES FFMPEG INSTALLED ON THE SERVER
// Spectrogram
function fr_ffmpeg_audio_to_image( $file, $w, $h, $color = [ 0, 0, 0, 0 ], $half = false ) {

    if( is_numeric( $file ) ) {
        $file = wp_get_attachment_url( $file );
    }

    if( !$file ) {
        return;
    }

    $file = str_replace( get_bloginfo( 'url' ), ABSPATH, $file );
    $file = str_replace( '//', '/', $file );

    $image = explode( '.', $file );
    $image[ count( $image ) - 1 ] = 'png';
    $image = implode( '.', $image );
    $image = str_replace( '.png', "-{$w}x{$h}.png", $image );

    if( !file_exists( $image ) ) {

        if( file_exists( $image ) ) {
            unlink( $image );
        }

        $cmd = fr_ffmpeg_get_path( 'ffmpeg' ) . ' -i ' . escapeshellarg( $file ) . ' -filter_complex "aformat=channel_layouts=mono,showwavespic=s=' . $w . 'x' . $h . '" -frames:v 1 -vsync 2 ' . escapeshellarg( $image );

        $output = fr_exec( $cmd, true );

        if( !file_exists( $image ) ) {
            return;
        }

        chmod( $image, 0755 );

        $img = imagecreatefrompng( $image );

        for( $y = 0; $y < $h; $y++ ) {
            $ok = 0;

            for( $x = 0; $x < $w; $x++ ) {
                $colors = imagecolorsforindex( $img, imagecolorat( $img, $x, $y ) );

                if( $colors['red'] == 0 && $colors['green'] == 0 && $colors['blue'] == 0 && $colors['alpha'] == 127 ) {
                } else {
                    $ok++;
                }
            }

            if( $ok > 3 && !isset( $crop['top'] ) ) {
                $crop['top'] = $y;
            }

            if( $ok <= 3 && isset( $crop['top'] ) && !isset( $crop['bottom'] ) ) {
                $crop['bottom'] = $y - 1;
                break;
            }
        }

        if( !isset( $crop['bottom'] ) ) {
            $crop['bottom'] = $h;
        }

        if( $crop['bottom'] - $crop['top'] == 0 ) {
            $crop['top'] = 0;
            $crop['bottom'] = $h;
        }

        $h = $crop['bottom'] - $crop['top'];

        if( $half ) {
            $h = ceil( $h / 2 );
        }

        $new_img = fr_image_crop( $img, $w, $h, 0, $crop['top'] );

        imagefilter( $new_img, IMG_FILTER_NEGATE );
        imagefilter( $new_img, IMG_FILTER_COLORIZE, $color[0], $color[1], $color[2], $color[3] );
        imagepng( $new_img, $image );

    }

    $uploads = wp_upload_dir();

    if( strstr( $image, $uploads['basedir'] ) ) {
        $image = str_replace( $uploads['basedir'], $uploads['baseurl'], $image );
    } else {
        $image = explode( '/wp-content/', $image );
        $image = site_url() . '/wp-content/' . $image[1];
    }

    return $image;

}

	
function fr_ffmpeg_audio_to_mp3( $file, $output_file = '' ) {

	if( !$output_file ) {
		$output_file = fr_file_change_extension( $file, 'mp3' );
	}

	$cmd = fr_ffmpeg_get_path( 'ffmpeg' ) . ' -y -i ' . escapeshellarg( $file ) . ' -acodec libmp3lame ' . escapeshellarg( $output_file ) . ' 2>&1';
	$res = fr_exec( $cmd, 1 );

	if( file_exists( $output_file ) ) {
		return $output_file;
	} else {
		return new fr_error( __( 'Failed to convert audio file to mp3', 'framework' ), [
			'vars' => func_get_args(),
			'cmd_output' => $res
		] );
	}

}



function fr_ffmpeg_audio_watermark( $file, $watermark, $output_file ){
	$cmd = fr_ffmpeg_get_path( 'ffmpeg' ) . ' -y -i ' . escapeshellarg( $file ) . ' -filter_complex "amovie=' . escapeshellarg( $watermark ) . ':loop=0,asetpts=N/SR/TB[beep]; [0][beep]amix=duration=shortest,volume=2"  ' . escapeshellarg( $output_file );
	$res = fr_exec( $cmd, 1 );

	if( file_exists( $output_file ) ){
    	return $output_file;
	} else {
    	return new fr_error( __( 'Failed to add watermark to audio file', 'framework' ), [ 
    		'vars' => func_get_args(),
    		'cmd_output' => $res
    	] );
    }
	return $output_file;
}

function fr_ffmpeg_set_path( $path_ffmpeg, $path_ffprobe = '', $path_ffplay = '' ){
	if( empty( $path_ffprobe ) ){
		$path_ffprobe = str_replace( 'ffmpeg', 'ffprobe', $path_ffmpeg );
	}
	if( empty( $path_ffplay ) ){
		$path_ffplay = str_replace( 'ffmpeg', 'ffplay', $path_ffmpeg );
	}
	fr_global( 'fr_ffmpeg_path', [
		'ffmpeg' => $path_ffmpeg,
		'ffprobe' => $path_ffprobe,
		'ffplay' => $path_ffplay
	] );
}

function fr_ffmpeg_get_path( $key ){
	$path = fr_global( 'fr_ffmpeg_path' );
	if( !$path ){
		$path = 'ffmpeg';
		$path = [
			'ffmpeg' => 'ffmpeg',
			'ffprobe' => 'ffprobe',
			'ffplay' => 'ffplay',
		];
	}
	return $path[$key];
}

function fr_ffmpeg_audio_info( $file, $key = '' ){

	$cache_key = [ 'fr_audio_info', $file ];
	$new = fr_cache( $cache_key );

	if( $new === false ){
		$path = fr_ffmpeg_get_path( 'ffprobe' );
		$cmd = $path . ' -i ' . escapeshellarg( $file ) . ' -show_streams -select_streams a:0';
		$info = fr_exec( $cmd, true );
		$info = trim( $info );
		$info = explode( "\n", $info );
		$info = array_slice( $info, 1, -1 );

		$new = [];
		foreach( $info as $v ){
			$v = explode( '=', $v );
			if( !isset( $v[1] ) ){
				continue;
			}
			$v[0] = trim( $v[0] );
			$v[1] = trim( $v[1] );
			$new[$v[0]] = $v[1];
		}

		fr_cache( $cache_key, $new );
	}

	if( $key && isset( $new[$key] ) ){
		return $new[$key];
	}

	return $new;
}


function fr_ffmpeg_audio_duration( $file_path ){

	$command = fr_ffmpeg_get_path( 'ffprobe' ) . ' -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 ' . escapeshellarg( $file_path );
    $output = shell_exec( $command );
    $file_duration = floatval( $output );

    return $file_duration;

}



function fr_ffmpeg_audio_crop( $file_path, $start_time, $duration ){

	$extension = pathinfo( $file_path, PATHINFO_EXTENSION );
	$output_file = tempnam( sys_get_temp_dir(), 'vc' );
	$output_file = fr_file_change_extension( $output_file, $extension );
	$command = fr_ffmpeg_get_path( 'ffmpeg' ) . ' -y -i ' . escapeshellarg( $file_path ) . ' -ss ' . escapeshellarg( $start_time ) . ' -t ' . escapeshellarg( $duration ) . ' -c copy ' . escapeshellarg( $output_file );
	
	exec($command . ' 2>&1', $output, $returnCode);

	if ( $returnCode === 0 ) {
	    return $output_file;
	} else {
		return new fr_error( __( 'Failed to crop audio file', 'framework' ), [ 
			'vars' => func_get_args(),
			'cmd_output' => $output,
			'return_code' => $returnCode
		] );
	}

}



function fr_ffmpeg_audio_split( $file_path, $duration, $duration_overlap = 0, $start_index = false, $end_index = fals ){

	$file_parts = [];
	$total_duration = fr_ffmpeg_audio_duration( $file_path );
	$window = $duration - $duration_overlap;
	$current_window = 0;
	$index = -1;

	while( $current_window + $duration_overlap < $total_duration ){
		
		$index++;

		if( $start_index !== false && $index < $start_index ){
			$current_window += $window;
			continue;
		}

		if( $end_index !== false && $index > $end_index ){
			break;
		}


		$start_time = $current_window;
		$part_duration = $window + $duration_overlap;

		$file_part = fr_ffmpeg_audio_crop( $file_path, $start_time, $part_duration );

		if( fr_is_error( $file_part ) ){
			return $file_part;
		}

		$file_parts[$index] = $file_part;
		$current_window += $window;

	}

	return $file_parts;

}



function fr_ffmpeg_audio_split_by_size( $file_path, $max_file_size, $duration_overlap = 0, $start_index = false, $end_index = false ){

	$total_duration = fr_ffmpeg_audio_duration( $file_path );
	$size = filesize( $file_path );

	if( $size <= $max_file_size ){
		return [ [ $file_path ], $total_duration, 1 ];
	}

	$parts = ceil( $size / $max_file_size );
	$duration = ceil( $total_duration / $parts );
	$file_parts = fr_ffmpeg_audio_split( $file_path, $duration, $duration_overlap, $start_index, $end_index );

	return [ $file_parts, $duration, $parts ];

}




function fr_ffmpeg_side_chain_compress( $file1, $file2, $output_file ){

	// Remove output file if already exists
	if( file_exists( $output_file ) ){
    	unlink( $output_file );
    }

	$cmd = fr_ffmpeg_get_path( 'ffmpeg' ) . ' -i ' . escapeshellarg( $file1 ) . ' -i ' . escapeshellarg( $file2 ) . ' -filter_complex "[1:a]asplit=2[sc][mix];[0:a][sc]sidechaincompress=' . fr_acf_get_field( 'side-chain_compression_settings', 'options' ) . '[bg];[bg][mix]amerge[final]" -map [final] ' . escapeshellarg( $output_file );
    $res = fr_exec( $cmd, 1 );

    if( file_exists( $output_file ) ){
    	return $output_file;
    } else {
    	return new fr_error( __( 'Failed to chain compress audio file', 'framework' ), [ 
    		'vars' => func_get_args(),
    		'cmd_output' => $res
    	] );
    }
}

function fr_ffmpeg_create_silent_mp3( $output_file, $duration ){

	// Remove output file if already exists
	if( file_exists( $output_file ) ){
    	unlink( $output_file );
    }

	$cmd = fr_ffmpeg_get_path( 'ffmpeg' ) . ' -f lavfi -i anullsrc -t ' . (float)$duration . ' -c:a libmp3lame ' . escapeshellarg( $output_file );
	$res = fr_exec( $cmd, 1 );

	if( file_exists( $output_file ) ){
    	return $output_file;
    } else {
    	return new fr_error( __( 'Failed to create silent mp3', 'framework' ), [ 
    		'vars' => func_get_args(),
    		'cmd_output' => $res 
    	] ); 
    }
}


function fr_ffmpeg_file_convert( $file_path, $to_mime, $settings = [] ){

	$default_settings = [
		'quality' => 100,
	];

	$settings = array_merge( $default_settings, $settings );

	$extension = fr_data_mime_to_extension( $to_mime );
	$output_file = fr_file_change_extension( $file_path, $extension );
	$cmd = fr_ffmpeg_get_path( 'ffmpeg' ) . ' -y -i ' . escapeshellarg( $file_path );

	// Add settings parameters if provided
	if( !empty( $settings ) ){

		if( $to_mime === 'image/jpeg' ){
			if( isset( $settings['quality'] ) ){
				$cmd .= ' -q:v ' . intval($settings['quality']);
			}
		}

		if ($to_mime === 'image/jpeg') {
			if (isset($settings['quality'])) {
				$quality = intval($settings['quality']);
				$quality = max(0, min(100, $quality));
				$ffmpeg_quality = 31 - ($quality * 0.29);
				$ffmpeg_quality = round($ffmpeg_quality);
				$cmd .= ' -q:v ' . $ffmpeg_quality;
			} else {
				$cmd .= ' -q:v 31';
			}
			
			$cmd .= ' -f mjpeg';
		}

		if( $to_mime === 'image/png' ){
			if( isset( $settings['compression_level'] ) ){
				$cmd .= ' -compression_level ' . intval($settings['compression_level']);
			}
		}

		if( $to_mime === 'image/webp' ){
			if( isset( $settings['quality'] ) ){
				$cmd .= ' -quality ' . intval($settings['quality']);
			}
		}

		if( $to_mime === 'audio/mp3' ){
			if( isset( $settings['bitrate'] ) ){
				$cmd .= ' -b:a ' . escapeshellarg($settings['bitrate']);
			}

			if( isset( $settings['quality'] ) ){
				// VBR quality: 0 (best) to 9 (worst)
				$vbr_quality = max(0, min(9, 9 - round($settings['quality'] * 0.09)));
				$cmd .= ' -q:a ' . $vbr_quality;
			}
		} 
 
		if( $to_mime === 'video/mp4' ){
			if( isset( $settings['quality'] ) ){
				$quality = intval($settings['quality']);
				$crf = 51 - ($quality * 0.51); // Convert quality (0-100) to CRF (51-0)
				$cmd .= ' -crf ' . round($crf);
			} elseif( isset( $settings['crf'] ) ){
				$cmd .= ' -crf ' . intval($settings['crf']);
			}
			if( isset( $settings['preset'] ) ){
				$cmd .= ' -preset ' . escapeshellarg($settings['preset']);
			}
		}

		if( $to_mime === 'video/webm' ){
			if( isset( $settings['crf'] ) ){
				$cmd .= ' -crf ' . intval($settings['crf']);
			}
			if( isset( $settings['bitrate'] ) ){
				$cmd .= ' -b:v ' . escapeshellarg($settings['bitrate']); 
			}
		}
	}

	$cmd .= ' ' . escapeshellarg( $output_file );

	$res = fr_exec( $cmd, 1 );

	if( file_exists( $output_file ) ){
		return $output_file;
	} else {
		return new fr_error( __( 'Failed to convert file', 'framework' ), [ 
			'vars' => func_get_args(),
			'cmd_output' => $res
		] );
	}

}





