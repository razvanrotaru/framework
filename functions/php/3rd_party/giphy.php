<?php
function fr_giphy_embed( $link, $width = '100%', $height = '100%', $amp = false ){
	$id = 0;
	if(filter_var($link, FILTER_VALIDATE_URL)){
		$id=fr_url_get_last_part( $link );
		$id = explode( '-', $id );
		$id = end( $id );
	}
	if($id){
		ob_start();
			if( !$amp ){
				
			} else {
				?><amp-anim src="https://media.giphy.com/media/<?php echo $id ?>/giphy.gif" width="<?php echo $width ?>" height="<?php echo $height ?>"></amp-anim><?php 
			}
		return ob_get_clean();
	}
}