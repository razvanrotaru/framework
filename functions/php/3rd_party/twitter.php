<?php
function fr_twitter_status( $link, $width = '100%', $height = '100%', $amp = false ){
	$id = 0;
	if(filter_var($link, FILTER_VALIDATE_URL)){
		$id=fr_url_get_last_part( $link );
	}
	if($id){
		ob_start();
			if( !$amp ){
				// To be added
			} else {
				?><amp-twitter data-tweetid="<?php echo $id ?>" layout="responsive" width="<?php echo $width ?>" height="<?php echo $height ?>"></amp-twitter><?php 
			}
		return ob_get_clean();
	}
}

function fr_twitter_link( $url, $text, $hashtags = '' ){
	$text = substr( $text, 0, 1000 );
	return fr_build_link( 'http://twitter.com/share', [
		'text' => $text,
		'url' => $url,
		'hashtags' => $hashtags
	] );
}