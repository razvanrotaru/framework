<?php
class FR_Mailchimp {

	var $mailchimp;

	function __construct( $key ){

		include_once( FRAMEWORK_DIR . '/resources/mailchimp/MailChimp.php'); 
		include_once( FRAMEWORK_DIR . '/resources/mailchimp/Batch.php'); 

		if( !class_exists( 'MailChimp' ) ){
			return new fr_error( 'Class MailChimp does not exist. https://github.com/drewm/mailchimp-api' );
		}
		$this->mailchimp = new MailChimp( $key );
	}

	function get_tags( $list_id ){
		$res = $this->mailchimp->get( 'lists/' . $list_id . '/segments' );
		$tags = [];
		if( $res && isset( $res['segments'] ) ){
			$tags = array_column( $res['segments'], 'name', 'id' );
		}
		return $tags;
	}

	function get_tag_ids( $list_id, $tags ){
		$existing_tags = $this->get_tags( $list_id );
		$new_tags = array_diff( $tags, $existing_tags );

		foreach( $new_tags as $tag ){
			$res = $this->mailchimp->post( "lists/$list_id/segments", [
				'name' => $tag,
				'static_segment' => []
			]);
		}

		return $this->get_tags( $list_id );
	}

	function get_batch_status( $id ){
		$batch = $this->mailchimp->new_batch( $id );
		return $batch->check_status();
	}

	function subscribe( $list_id, $users ){
		$users = fr_make_multidim_array( $users );
		$batch = $this->mailchimp->new_batch();

		foreach( $users as $k => $user ){
			//return $this->mailchimp->put( "lists/$list_id/members/" . md5( strtolower( $user['email_address'] ) ), $user );
			$batch->put( 'op_update_' . $k, "lists/$list_id/members/" . md5( strtolower( $user['email_address'] ) ), $user );
		}

		return $batch->execute();
	}

	function get_user( $list_id, $email ){
		return $this->mailchimp->get( "lists/$list_id/members/" . md5( strtolower( $email ) ) );
	}

}