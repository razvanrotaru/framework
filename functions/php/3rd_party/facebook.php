<?php
function fr_facebook_embed( $link, $width = '100%', $height = '100%', $amp = false ){
	if($link){
		ob_start();
			if( !$amp ){
				// To be added
			} else {
				?><amp-facebook layout="responsive" data-href="<?php echo $link ?>" width="<?php echo $width ?>" height="<?php echo $height ?>"></amp-facebook><?php 
			}
		return ob_get_clean();
	}
}