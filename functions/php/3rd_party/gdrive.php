<?php
class fr_gdrive {

	var $work_folder;
	var $project_id;
	var $client_id;
	var $client_secret;
	var $top_folder;
	var $redirect_url;
	var $auth_code;
	var $client;
	var $service;
	var $scope;

	function __construct( $work_folder, $scope, $project_id, $client_id, $client_secret, $top_folder, $redirect_url, $auth_code = '' ){

		if( $work_folder ){
			fr_required_dir( $work_folder );
		}

		$this->work_folder = $work_folder;
		$this->scope = $scope;
		$this->project_id = $project_id;
		$this->client_id = $client_id;
		$this->client_secret = $client_secret;
		$this->top_folder = $top_folder;
		$this->redirect_url = $redirect_url;

		$this->auth_code = $auth_code;
		$this->client = $this->getClient();
		$this->service = new Google_Service_Drive( $this->client );
	}



	function upload( $file_path, $folder = '', $file_name = '' ) {

		$chunkSizeBytes = 1 * 1024 * 1024;
		$file_name = $file_name ?: basename( $file_path );

		$this->client->setDefer( true );
		$mimeType = fr_file_mime_type( $file_path );

		$file = new Google_Service_Drive_DriveFile();
		$file->setName( $file_name );
		$file->setMimeType( $mimeType );

		if( !$folder ) {
			$folder = $this->top_folder;
		}

		$file->setParents( [ $folder ] );

		$request = $this->service->files->create( $file );

		$media = new Google_Http_MediaFileUpload(
			$this->client,
			$request,
			$mimeType,
			null,
			true,
			$chunkSizeBytes
		);

		$media->setFileSize( filesize( $file_path ) );

		$status = false;
		$handle = fopen( $file_path, 'rb' );

		while( !$status && !feof( $handle ) ) {
			$chunk = fread( $handle, $chunkSizeBytes );
			$status = $media->nextChunk( $chunk );
		}

		fclose( $handle );
		
		return [$status, $status[0]->id];
		
	}



	function create_file( string $file_name, string $folder_id, string $data, string $mimeType = 'application/octet-stream', $resumable = false, $resumable_file_id = '' ){

		$file_name = basename( $file_name );
		$directory_path = dirname( $file_name );
		$file = new Google_Service_Drive_DriveFile();
		$file->setName( basename( $file_name ) );
		$file->setMimeType( $mimeType );

		// Set the parent folder.
		if ( !$folder_id ) {
			$folder_id = $this->top_folder;
		}

		$file->setParents( [ $folder_id ] );

		$options = [
	        'data' => $data,
	        'uploadType' => $resumable ? 'resumable' : 'media',
	        'mimeType' => $mimeType,
	    ];

	    if( $resumable_file_id ){
	       $options['upload_id'] = $resumable_file_id;
	    }

		$createdFile = $this->service->files->create( $file, $options );

	    return $createdFile;

	}



	function create_folder( $folder_name, $parent_folder = '' ){

		$file = new Google_Service_Drive_DriveFile();
		$file->setName( $folder_name );
		$file->setMimeType( 'application/vnd.google-apps.folder' );

		// Set the parent folder.
		if ( !$parent_folder ) {
			$parent_folder = $this->top_folder;
		}
		$file->setParents( [ $parent_folder ] );

		$response = $this->service->files->create( $file );
		$createdFile = false;

		if( is_array( $response ) ){
			$createdFile = reset( $response );
		}

		if( $createdFile && !empty( $createdFile->id ) ){
			return $createdFile->id;
		} else {
			return new fr_error( 'Failed creating Google Drive folder', $createdFile );
		}
		
	}



	function make_path( $path ){

		$path = trim( $path, '/' );
		$path = explode( '/', $path );
		$folder_id = '';

		foreach( $path as $folder ){

			$id = $this->file_exists( $folder, $folder_id );

			if( !$id ){
				$id = $this->create_folder( $folder, $folder_id );
			}

			if( fr_is_error( $id ) ){
				return $id;
			}

			if( !$id ){
				return false;
			}

			$folder_id = $id;

		}
		
		return $folder_id;
	}

	function file_exists( $file_name, $folder = '' ){
		$files = $this->scan_dir( $folder, [ 'q' => "(title = '" . $file_name . "' and trashed = false)" ] );
		if( !empty( $files['items'][0]['id'] ) ){
			return $files['items'][0]['id'];
		}
	}

	function file_info( $file = '' ){
		return $this->get( 'files/' . $file );
	}

	function get_auth_url(){
		return $this->client->createAuthUrl();
	}

	function getClient(){

	    $client = new Google_Client();

	    $client->setAuthConfig( [
	    	'installed' => [
	    		'client_id' => $this->client_id,
	    		'client_secret' => $this->client_secret,
	    		'project_id' => $this->project_id,
	    		'auth_uri' => 'https://accounts.google.com/o/oauth2/auth',
	    		'auth_provider_x509_cert_url' => 'https://www.googleapis.com/oauth2/v1/certs',
	    		'redirect_uris' => [
	    			$this->redirect_url
	    		]
	    	]
	    ] );

	    $client->setApplicationName( 'theName' );
	    $client->setScopes( $this->scope );
	    $client->setAccessType('offline');


	    // Load previously authorized token from a file, if it exists.
	    // The file token.json stores the user's access and refresh tokens, and is
	    // created automatically when the authorization flow completes for the first
	    // time.
	    $tokenPath = $this->work_folder . '/token.json';

	    if (file_exists($tokenPath)) {
	        $accessToken = json_decode(file_get_contents($tokenPath), true);
	        $client->setAccessToken($accessToken);
	    }

	    // If there is no previous token or it's expired.
	    if ($client->isAccessTokenExpired()) {
	        // Refresh the token if possible, else fetch a new one.
	        if ( $client->getRefreshToken()) {
	            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
	        } else if( $this->auth_code ){
	            // Exchange authorization code for an access token.
	            $accessToken = $client->fetchAccessTokenWithAuthCode( $this->auth_code );

	            if( !array_key_exists('error', $accessToken) ){
		            $client->setAccessToken($accessToken);
		        }
	        }
	        // Save the token to a file.
	        if (!file_exists(dirname($tokenPath))) {
	            mkdir(dirname($tokenPath), 0700, true);
	        }
	        if( $client->getAccessToken() ){
		        file_put_contents($tokenPath, json_encode($client->getAccessToken()));
		    }
	    }
	    return $client;
	}

	function scan_dir( $folder = '', $q = [] ) {
		if( !$folder ){
			$folder = $this->top_folder;
		}
		return $this->get( 'files/' . $folder . '/children/', $q );
	}

	function download( $file, $path = '' ) {
		$contents = $this->get( 'files/' . $file, [ 'alt' => 'media' ] );
		if( $path ){
			return file_put_contents( $path, $contents );
		} else {
			return $contents;
		}
	}

	function get_view_url( $file_id ){

	    $this->make_file_public( $file_id );

	    $service = new Google_Service_Drive( $this->client );
	    $file = $service->files->get( $file_id, array('fields' => 'webViewLink') );

	    if( !$file ){
	    	return '';
	    }

		return $file[0]->webViewLink;

	}


	function get_download_url( $file_id ){

	    $this->make_file_public( $file_id );
        return 'https://drive.google.com/uc?export=download&id=' . $file_id;

	}


	function make_file_public( $file_id ){

		$service = new Google_Service_Drive( $this->client );

	    $newPermission = new Google_Service_Drive_Permission(array(
	        'type' => 'anyone',
	        'role' => 'reader',
	        'allowFileDiscovery' => false,
	    ));

	    $permissionResponse = $service->permissions->create( $file_id, $newPermission );

	    if (is_array($permissionResponse) && isset($permissionResponse[0]) && $permissionResponse[0] instanceof Google_Service_Drive_Permission) {
            return true;
        } else {
            return false;
        }

	}
	

	function delete_file( $file ) {
		$res = $this->delete( 'files/' . $file );
		return $res;
	}

	function get( $path, $q = [] ){
		$token = $this->client->getAccessToken();
		$res = fr_curl( 'https://www.googleapis.com/drive/v2/' . $path . '?' . http_build_query( $q ), [], [ 'Authorization' => $token['token_type'] . ' ' . $token['access_token'] ] );
		return $this->parse_response( $res );
	}

	function delete( $path ){
		$token = $this->client->getAccessToken();
		$res = fr_curl( 'https://www.googleapis.com/drive/v2/' . $path, [], [ 'Authorization' => $token['token_type'] . ' ' . $token['access_token'] ], [ CURLOPT_CUSTOMREQUEST => 'DELETE' ] );
		return $this->parse_response( $res );
	}

	function parse_response( $res ){
		$res = $res['body'];
		$json = json_decode( $res, 1 );
		if( $json !== NULL ){
			$res = $json;
		}
		return $res;
	}


	function list_files_in_directory( $directory_id ) {

		$optParams = [
			'q'      => "'" . $directory_id . "' in parents and trashed = false",
			'fields' => 'files(id, name, mimeType, size, createdTime)',
		];

		$results = $this->service->files->listFiles( $optParams )[0];

		$files = [];

		foreach( $results->getFiles() as $file ) {
			$files[] = [
				'id'       => $file->getId(),
				'name'     => $file->getName(),
				'mimeType' => $file->getMimeType(),
				'size'     => $file->getSize(),
				'created'  => $file->getCreatedTime()
			];
		}

		return $files;
		
	}

}