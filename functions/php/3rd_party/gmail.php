<?php
function fr_gmail_get_main_email( $email ){

    if( !strstr( $email, '@gmail.com' ) ){
    	return $email;
    }

	if( strstr( $email, '+' ) ){
		$extra = fr_extract( $email, '+', '@' );
		$email = str_replace( '+' . $extra . '@', '@', $email );
	}
	$email = explode( '@', $email );
	$email[0] = str_replace( '.', '', $email[0] );
	$email = implode( '@', $email );

     return $email;
}