<?php
function fr_validation_filter( $data, $rules, $errors = [] ){	

	// Format rules
	foreach( $rules as $name => $field ){

		if( !is_array( $field ) ){
			$rules[$name] = $field = [
				'type' => $field
			];
		}

		foreach( $field as $k => $rule ){
			if( !is_array( $rule ) ){
				if( $k == 'required' ){
					$rules[$name][$k] = [ 
						'required' => 1,
						'error' => $rule
					];
				} else {
					$rules[$name][$k] = [ $k => $rule ];
				}
			}
		}
	}

	// Order the data like the rules and set default values if they don't exist
	$new_data = [];
	$new_data['loop_keys'] = $data['loop_keys'] ?? [];

	foreach( $rules as $k => $rule ){
		$new_data[$k] = '';

		// This is the default value that is set before the form is submitted
        if( isset( $rule['not_set'] ) ){
            $new_data[$k] = reset( $rule['not_set'] );
        }

		if( isset( $data[$k] ) ){
			$new_data[$k] = $data[$k];
		}
	}
	$data = $new_data;


	// Typecast the data
	foreach( $rules as $k => $group ){
		if( !isset( $data[$k] ) ){
			$v = '';
		} else {
			$v = $data[$k];
		}
		foreach( $group as $k2 => $rule ){
			if( !isset( $rule['type'] ) ){
				continue;
			}
			if( $rule['type'] == 'array' || $rule['type'] == 'loop' ){
				if( !is_array( $v ) ){
					$v = [];
				}
			}
			if( in_array( $rule['type'], [ 'int', 'number' ] ) ){
				if( !is_numeric( $v ) || is_array( $v ) || is_object( $v ) ){
					$v = 0;
				} else {
					$v = (int)$v;
				}
			}
			if( $rule['type'] == 'number' ){
				if( $v < 0 ){ $v = 0; }
			}
			if( $rule['type'] == 'float' ){
				if( !is_numeric( $v ) || is_array( $v ) || is_object( $v ) ){
					$v = 0;
				} else {
					$v = (float)$v;
				}
			}
			if( $rule['type'] == 'price' ){
				if( !is_numeric( $v ) || is_array( $v ) || is_object( $v ) ){
					$v = 0;
				} else {
					$v = str_replace( ',', '', $v );
					$v = floor( (float)$v * 100 ) / 100;
					if( $v < 0 ){ $v = 0; } // Keep the price positive. Don't accept negative numbers.
				}
			}
			if( $rule['type'] == 'date' ){
				if( !$v || is_array( $v ) || is_object( $v ) ){
					$v = false;
				} else {
					$v = @strtotime( $v );
					$v = date( $group['date_format']['date_format'] ?? DB_DATE_FORMAT, $v );
				}
			}
			if( $rule['type'] == 'datetime' ){
				if( !$v || is_array( $v ) || is_object( $v ) ){
					$v = false;
				} else {
					$v = @strtotime( $v );
					$v = date( DB_DATE_TIME_FORMAT, $v );
				}
			}
			if( $rule['type'] == 'boolean' ){
				if( $v === 'false' ){
					$v = 0;
				} else if( !is_array( $v ) && !is_object( $v ) ){
					$v = (bool)$v;
				} else {
					$v = 0;
				}
			}
			if( in_array( $rule['type'], [ 'string', 'email', 'phone', 'text', 'file', 'date', 'html', 'url' ] ) ){
				if( is_array( $v ) || is_object( $v ) ){
					$v = '';
				} else {
					$v = (string)$v;
				}
			}
		}
		$data[$k] = $v;
	}



	// Validate data

	foreach( $data as $k => $v ){

		// This is the default value in case the validation fails
		if( isset( $rules[$k]['default'] ) ){
			if( !$v ){
				$v = $rules[$k]['default']['default'] instanceof Closure ? $rules[$k]['default']['default']( $v, $data, $k ) : $rules[$k]['default']['default'];
			}
			unset( $rules[$k]['default'] );
		}

		if( !isset( $rules[$k] ) ){
			continue;
		}

		// Process the rules
		foreach( $rules[$k] as $rule_key => $rule ){


			// Abort early to not execute callbacks
			
			if( isset( $rule['empty_if'] ) ){
				if( !empty( $data[$rule['empty_if']] ) ){
					$data[$k] = '';
					continue 2;
				}
			}

			if( isset( $rule['empty_if_not'] ) ){
				if( empty( $data[$rule['empty_if_not']] ) ){
					$data[$k] = '';
					continue 2;
				}
			}


			// Generate user defined rules based on data
			foreach( $rule as $k2 => $subrule ){
				if( $k2 == 'format' ){
					continue;
				}
				$rule[$k2] = $rules[$k][$k2] = $subrule instanceof Closure ? $subrule( $v, $data, $k ) : $subrule;
			}

			$error = false;

			// Issue errors

			if( isset( $rule['max_length'] ) ){
				if( is_array( $v ) ){
					if( count( $v ) > $rule['max_length'] ){
						$error = true;
						$v = array_slice( $v, 0, $rule['max_length'] );
					}
				} else {
					if( strlen( $v ) > $rule['max_length'] ){
						$error = true;
						$v = substr( $v, 0, $rule['max_length'] );
					}
				}
			}
			if( isset( $rule['min_length'] )){
				if( is_array( $v ) ){
					if( $v && count( $v ) < $rule['min_length'] ){
						$error = true;
					}
				} else {
					if( strlen( $v ) && strlen( $v ) < $rule['min_length'] ){
						$error = true;
					}
				}
			}
			if( isset( $rule['max'] ) ){
				if( $v > $rule['max'] ){
					$error = true;
					$v = $rule['max'];
				}
			}
			if( isset( $rule['min'] ) && strlen( $v ) ){
				if( $v < $rule['min'] ){
					$error = true;
					$v = $rule['min'];
				}
			}
			if( isset( $rule['max_date'] ) && $v ){
				if( strtotime( $v ) > strtotime( $rule['max_date'] ) ){
					$error = true;
					$v = $rule['max_date'];
				}
			}
			if( isset( $rule['min_date'] ) && $v ){
				if( strtotime( $v ) < strtotime( $rule['min_date'] ) ){
					$error = true;
					$v = $rule['min_date'];
				}
			}
			if( isset( $rule['required'] ) ){
				$required = fr_call_if_func( $rule['required'], [ $data ] );
				if( $required ){
					if( !$v ){
						$error = true;
					}
				} else if( !empty( $rule['remove_if_not_required'] ) ) {
					$v = '';
				}
			}
			if( isset( $rule['format'] ) ){
				$v = call_user_func( $rule['format'], $v, $data );
			}


			// Validate
			
			if( isset( $rule['extensions'] ) && $v ){
				$v2 = explode( '.', $v );
				$ext = end( $v2 );
				$ext = strtolower( $ext );
				$rule['extensions'] = array_map( 'strtolower', $rule['extensions'] );
				if( count( $v2 ) == 1 || !in_array( $ext, $rule['extensions'] ) ){
					$error = true;
				}
			}

			if( isset( $rule['validate'] ) ){
				if( $rule['validate'] === false ){
					$error = true;
				}
			}

			if( !empty( $rule['type'] ) ){
				if( $rule['type'] == 'email' && $v ){
					if ( !filter_var( $v, FILTER_VALIDATE_EMAIL ) ) {
						$error = true;
					}
				}
				if( $rule['type'] == 'phone' && $v ){
					if ( array_diff( str_split( $v ), [ 0,1,2,3,4,5,6,7,8,9,' ','+','-','(',')','.' ] ) ) {
						$error = true;
					}
				}
				if( $rule['type'] == 'url' && $v ){
					if ( !filter_var( $v, FILTER_VALIDATE_URL ) ) {
						$error = true;
					}
				}
				if( $rule['type'] == 'date' && $v ){
					$v = date( $rules[$k]['date_format']['date_format'] ?? 'Y-m-d', strtotime( $v ) );
				}
				if( $rule['type'] == 'datetime' && $v ){
					$v = date( 'Y-m-d H:i:s', strtotime( $v ) );
				}
				if( $rule['type'] == 'loop' && $v ){
					foreach( $v as $k2 => $v2 ){
						$v2 = array_merge( $data, $v2 );
						$v2['loop_keys'][] = $k2;
						list( $v[$k2], $errors ) = fr_validation_filter( $v2, $rules[$k]['fields'], $errors );
					}
				}
			}


			// Parse value

			if( !empty( $rule['type'] ) ){
				if( $rule['type'] == 'text' ){
					$v = fr_strip_html_tags_and_content( $v );
				}
				if( $rule['type'] == 'html' ){
					$v = htmlspecialchars( $v );
				}
			}
			// Keep only the value/s that match a predefined list
			if( isset( $rule['options'] ) ){
				$rule['options'] = $rule['options'] instanceof Closure ? $rule['options']( $v, $data, $k ) : $rule['options'];

				if( is_array( $v ) ){
					$v = array_intersect( $rule['options'], $v );
				} else {
					if( !in_array( $v, $rule['options'] ) ){
						$v = '';
					}
				}
			}

			// Filters the data using a user defined function
			if( isset( $rule['filter'] ) ){
				$v = $rule['filter'];
			}

			// If value is empty set the default value
			if( isset( $rule['default'] ) && !$v ){ 
				$rule['default'] = $rule['default'] instanceof Closure ? $rule['default']( $v, $data, $k ) : $rule['default'];
				$v = $rules[$k]['default'];
			}

			if( !empty( $rule['trim'] ) ){
				$v = trim( $v );
			}


			if( isset( $rule['exclude_characters'] ) ){
				$groups = fr_make_array( $rule['exclude_characters'] );
				foreach( $groups as $group_k => $group ){
					if( $group == 'punctuation' ){
						$groups[$group_k] = '!"#$%&\'()*+,./:;<=>?@[\\]^_`{¦}|~';
					}
					if( $group == 'numbers' ){
						$groups[$group_k] = '0123456789';
					}
				}
				$exclude_chars = implode( '', $groups );
				$exclude_chars = str_split( $exclude_chars );
				$v_chars = str_split( $v );
				$found = array_intersect( $v_chars, $exclude_chars );
				$clean = array_diff( $v_chars, $exclude_chars );

				if( $found ){
					$v = implode( '', $clean );
					$error = true;
					$rule['error'] .= ': ' . implode( '', $found );
				}
			}



			$rules[$k][$rule_key] = $rule;



			// On error stop and process next data
			if( $error && isset( $rule['error'] ) ){
				$errors[$k] = $rule['error'];
			}
			
            if( ( isset( $rule['on_error_abort'] ) || isset( $rules[$k]['on_error_abort'] ) ) && $errors ){
                break 2;
            }

            if( !empty( $errors[$k] ) ){
				continue 2;
            }

			
			$data[$k] = $v;
		}
	}

	unset( $data['loop_keys'] );

	return [ $data, $errors, $rules ];
}










function fr_validation_filter_merge_common( $add_filters, $rules ){

	$new_filters = [];
	foreach( $add_filters as $name => $filter ){
		$new_filters[$name] = [];

		if( !is_array( $filter ) ){
			$filter = [
				'type' => $filter
			];
		}

		foreach( $filter as $rule => $options ){
			if( !is_numeric( $rule ) && isset( $rules[$rule] ) ){
				if( !is_array( $options ) ){
					$options = [
						'error' => $options
					];
				}
				$new_filters[$name][$rule] = array_merge( $rules[$rule], $options );
			} else {
				$new_filters[$name][$rule] = $options;
			}
		}
	}

	return $new_filters;
}





/**
 * Value comparison and log as error if wrong
 * @param $data Mixed Data to be checked
 * @param $rules Mixed Rules to check the data against. If value is not an array it will assumed $data should be equal to it.
 * @param $error_data Mixed Optional data to be logged that helps identify the issue
 * @param $custom_error_message String Optional message to be logged on error
 * @param $urgent Bool If set to true notify developer via email
 * @param $lvl Int Internal value to be used recursively. Do not set
 * @return Bool
 */

function fr_valid( $data, $rules = [], $error_data = [], string $custom_error_message = '', bool $urgent = false, int $lvl = 0 ): bool {
	
	$orig_rules = $rules;

	// Format rules before validation
	
	$formated = [];
	if( !is_array( $rules ) ){
		$formated = [
			[ 'value', $rules ]
		];
	} else {
		foreach( $rules as $k => $rule ){
			if( !is_numeric( $k ) ){
				$rule = [ $k, $rule ];
			}
			if( $rule == 'OR' ){

			} else if( !is_array( $rule ) ){
				$rule = [ $rule ];
			}
			$formated[] = $rule;
		}
	}
	$rules = $formated;


	


	$valid = true;
	$errors = [];
	$relation = 'AND';
	if( in_array( 'OR', $rules ) ){
		$relation = 'OR';
		$rules = fr_array_remove_item( $rules, 'OR' );
	}

	foreach( $rules as $rule ){

		if( $rule[0] == 'OR' || is_array( $rule[0] ) ){
			$rule = [
				'value',
				fr_valid( $data, $rule, $error_data, $custom_error_message, $urgent = false, $lvl + 1 ) ? $data : null
			];
		}


		$type = $rule[0];
		
		if( $type == 'int' ){
			if( !is_numeric( $data ) ){
				$errors[] = 'Non-numeric value';
			} else if( strlen( (int)$data ) != strlen( $data ) ){
				$errors[] = 'Non-integral value';
			}
		}

		// Only numbers starting from: 1, 2, 3
		elseif( $type == 'natural' ){
			if( !is_numeric( $data ) ){
				$errors[] = 'Non-numeric value';
			} else if( strlen( (int)$data ) != strlen( $data ) || $data <= 0 ){
				$errors[] = 'Non-natural number value';
			}
		}

		// Only numbers starting from: 0, 1, 2, 3
		elseif( $type == 'whole' ){
			if( !is_numeric( $data ) ){
				$errors[] = 'Non-numeric value';
			} else if( strlen( (int)$data ) != strlen( $data ) || $data < 0 ){
				$errors[] = 'Non-whole number value';
			}
		}

		elseif( $type == 'float' ){
			if( !is_float( $data ) ){
				$errors[] = 'Non-float value';
			}
		}

		elseif( $type == 'false' ){
			if( $data !== false ){
				$errors[] = 'Non-false value';
			}
		}

		elseif( $type == 'true' ){
			if( $data !== true ){
				$errors[] = 'Non-true value';
			}
		}

		elseif( $type == 'bool' ){
			if( !is_bool( $data ) ){
				$errors[] = 'Non-boolean value';
			}
		}

		else if( $type == 'array' ){
			if( !is_array( $data ) ){
				$errors[] = 'Non-array value';
			}
		} 

		else if( $type == 'object' ){
			if( !is_object( $data ) ){
				$errors[] = 'Non-object value';
			}
		}

		else if( $type == 'range' ){
			if( !is_numeric( $data ) ){
				$errors[] = 'Non-numeric value';
			} else if( $rule[1][0] > $data || $rule[1][1] < $data ){
				$errors[] = 'Value outside range';
			}
		} 

		else if( $type == 'options' ){
			if( is_array( $data ) || is_object( $data ) ){
				$errors[] = 'Invalid value format';
			} else if( !in_array( $data, $rule[1] ) ){
				$errors[] = 'Invalid value';
			}
		}

		else if( $type == 'keys' ){
			if( !is_array( $data ) ){
				$errors[] = 'Non-array value';
			} else if( $error = array_diff_key( array_flip( $rule[1] ), $data ) ){
				$errors[] = 'Missing array keys: ' . implode( ', ', array_flip( $error ) );
			}
		}

		else if( $type == 'value' ){
			if( $rule[1] !== $data ){
				$errors[] = 'Invalid value';
			}
		}

		else if( $type == 'exists' ){
			if( !$data ){
				$errors[] = 'Value doesn\'t exist';
			}
		}

		else if( $type == 'not_exists' ){
			if( $data ){
				$errors[] = 'Value exists';
			}
		}

		else if( $type == 'file_exists' ){
			if( !$data || !file_exists( $data )  ){
				$errors[] = 'File doesn\'t exist: ' . $data;
			}
		}

		else if( $type == 'not_empty' ){
			if( !$data ){
				$errors[] = 'Value is empty';
			}
		}

		else if( $type == 'string' ){
			if( !is_string( $data ) ){
				$errors[] = 'Value is not string';
			}
		}

		else {
			trigger_error( 'Invalid validation rule: ' . $rule[0] );
		}

		
		if( $errors && $relation == 'AND' ){
			$valid = false;
			break;
		}
	}

	if( !$rules && !$data ){
		$errors[] = 'Value is empty';
		$valid = false;
	}

	if( $relation == 'OR' && count( $errors ) == count( $rules ) ){
		$valid = false;
	}

	if( !$valid && !$lvl ){

		$data = [
			'test_value' => $data,
			'tested_rules' => $orig_rules,
		];

		if( $error_data ){
			$data['data'] = $error_data;
		}

		$errors = implode( ', ', $errors );

		if( $custom_error_message ){
			$errors  = $custom_error_message;
		}

		fr_validation_failed( $errors, $data, $urgent, 1 );
	}

	return $valid;
}



/**
 * Gets an array of files called using the debug_backgrace function
 * @param  integer $start        Starting point in debug_backgrace
 * @param  integer $end          Length of max function to display
 * @param  boolean $add_function Optional add function called
 * @return array
 */
function fr_validation_failed( $error_message, $data = [], $urgent = false, $msg_lvl = 1 ){

	$error_message = 'Failed validation. ' . $error_message;

	if( !is_array( $data ) ){
		$data = [
			'data' => $data
		];
	}

	$data['location'] = fr_get_debug_backtrace( $msg_lvl + 1, 100 );
	$data['url'] = fr_get_current_url();
	$data['referer'] = $_SERVER['HTTP_REFERER'] ?? '';
	$data['POST'] = $_POST;

	if( function_exists( 'get_current_user_id' ) ){
		$data['user_id'] = get_current_user_id();
	}

	fr_log( $error_message, $data, true, false, 2 );

	if( $urgent ){
	    fr_dev_email( $error_message, $data );
	}

	if( !ini_get( 'display_errors' ) ){
		return false;
	}

	return true;
}












