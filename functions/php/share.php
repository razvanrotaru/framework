<?php
function fr_share_links( $include, $title, $link, $name ){
	$sn = array(
		'facebook' => array( 
			'icon' => 'facebook-f',
			'icon_type' => 'fab fa-',
			'title' => __( 'Facebook' ),
			'color' => '#325a97',
			'url' => 'https://www.facebook.com/sharer/sharer.php?text=%1$s&u=%2$s'
		),
		'twitter' => array( 
			'icon' => 'twitter',
			'icon_type' => 'fab fa-',
			'title' => __( 'Twitter' ),
			'color' => '#00aced',
			'url' => 'https://twitter.com/intent/tweet?counturl=%2$s&text=%1$s&url=%2$s&via=%3$s'
		),
		'linkedin' => array( 
			'icon' => 'linkedin',
			'icon_type' => 'fab fa-',
			'title' => __( 'LinkedIn' ),
			'color' => '#0077b5',
			'url' => 'http://www.linkedin.com/shareArticle?mini=true&source=%3$s&summary=&title=%1$s&url=%2$s'
		),
		'reddit' => array( 
			'icon' => 'reddit',
			'icon_type' => 'fab fa-',
			'title' => __( 'Reddit' ),
			'color' => '#ff4500',
			'url' => 'https://reddit.com/submit?title=%1$s&url=%2$s'
		),
		'pocket' => array( 
			'icon' => 'get-pocket',
			'icon_type' => 'fab fa-',
			'title' => __( 'Pocket' ),
			'color' => '#dd4f58',
			'url' => 'https://getpocket.com/save?url=%2$s'
		),
		'flipboard' => array( 
			'icon' => 'flipboard',
			'icon_type' => 'fab fa-',
			'title' => __( 'Flipboard' ),
			'color' => '#cf3c33',
			'url' => 'https://share.flipboard.com/bookmarklet/popout?title=%1$s&url=%2$s&v=2'
		),
		'email' => array( 
			'icon' => 'envelope',
			'icon_type' => 'fas fa-',
			'title' => __( 'Email' ),
			'color' => '#7d7d7d',
			'url' => 'mailto:?subject=%1$s&body=%%0A%%0A%2$s'
		)
	);

	foreach( $sn as $k => $v ){
		if( $include && !in_array( $k, $include ) ){
			unset( $sn[$k] );
			continue;
		}
		$sn[$k]['url'] = sprintf( $sn[$k]['url'], urlencode( $title ), urlencode( $link ), urlencode( $name ) );
	}

	return $sn;
}