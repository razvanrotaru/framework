<?php
// Compensate for the WP string translation function
if( !function_exists( '__' ) ){
	function __( $s ){
		return $s;
	}
	function _e( $s ){
		echo $s;
	}
}