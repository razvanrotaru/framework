<?php

//HELPER FUNCTION USED FOR DEBUGGING
function fr_c( $s=array(), $name="", $compact = true ){
  fr_p( $s, $name, 0, true, $compact );
}

function fr_pdump( $s=array(),$name="" ){
  ob_start();
  var_dump( $s );
  $s = ob_get_clean();
  $s = str_replace( '=>' . PHP_EOL . ' ', ' =>', $s );
  fr_p( $s, $name );
}

function fr_pjson( $s=array(),$name="" ){
  fr_p( json_encode( $s ), $name );
}


/** 
 * Quickly display data when developing
 */

if( !function_exists( 'fr_p' ) ){
  function fr_p($s=array(),$name="",$print=0,$console = false, $compact = true, $backtrace_offset = 0 ){

    if( !headers_sent() ){
      header( 'Content-Type: text/html; charset=utf-8' );
    }

    $exit=0;
    if($name==1){
      $exit=1;
      $name="";
    }
    $color="";
    $colors=array("black","blue","DarkGreen","DarkOliveGreen","green","brown","darkred","DarkViolet","indigo","IndianRed","LightSeaGreen","DarkOrange");
    if(!isset($_ENV["info_display"])){$_ENV["info_display"]=array();}
    if($name){
      if(!isset($_ENV["info_display"][$name])){
        $e=array_diff($colors,$_ENV["info_display"]);
        $color=reset($e);
        if($name=="error"){$color="red";}
        if($name=="ok" || $name=="success"){$color="DarkGreen";}
        if($name=="warning"){$color="DarkOrange";}
        $_ENV["info_display"][$name]=$color;
      }else{
        $color=$_ENV["info_display"][$name];
      }
    }
    if(!$color){
      $color=reset( $colors );
    }
    if( in_array( $name, $colors ) ){
      $color = $name;
    }

    $d = debug_backtrace( false, $backtrace_offset + 1 );
    $d = array_slice( $d, $backtrace_offset, 1 );

    if($print){
      print_r(array($s,$d[0]["line"]." ".basename($d[0]["file"])));//ok
    }

    $path = basename( dirname( $d[0]['file'] ) ) . '/' . basename($d[0]["file"]) . ':' . $d[0]["line"];

    if( $console && !$compact ){
      ?>
      <script>console.log( '%c <?php echo $name ." ". str_replace( "\n", "\\n", print_r( $s, 1 ) )." ". $path ?>', 'background: <?php echo $color ?>; color: white;' )</script>
      <?php 
    } else if( $console ){
      ?>
      <script>
        console.log( '<?php echo $name ?>', <?php echo json_encode( $s ) ? json_encode( $s ) : '"json_encoding_error"' ?>, '<?php echo $path ?>' )</script>
      <?php 
    } else {

      $os=$s;
      $s=print_r($s,1);//ok
      $s=htmlentities(html_entity_decode($s));
      if(!$s){
        $s=print_r($os,1);//ok
      }

      if( !strlen( $s ) ){
        $s = '&nbsp;';
      }

      echo "<div style='" . ( $exit && !fr_global( 'fr_p' ) ? 'position:fixed; top: 0; left: 0; width: 100%; overflow: auto; height: 100%; z-index: 100000000000; background-color: black' : '' ) . "'><table style='width:100%;direction:ltr;font-family:monospace;' class='fr_p_message'>
        <tr>
          <td style='width:100px;vertical-align:top'><strong>".$name."</strong></td>
          <td>
            <div style='width:100%;clear:both;background-color:".$color."; color:white;margin-bottom:0.5px'>
              <span style='float:right; font-size:12px;opacity:0.5;user-select: none;'>".$path."</span>
              <pre style='white-space: pre-wrap;background-color:transparent;margin: 0;'><code style='background-color:".$color."; color:white;direction:ltr;font-family:monospace; font-size: 14px;line-height: 1.4em'>" . $s . "</code></pre>
            </div>
          </td>
        </tr>
      </table></div>";
      }

      fr_global( 'fr_p', 1 );
    if($exit){fr_exit();}
  }
}


if( !function_exists( 'fr_p_date' ) ){
  function fr_p_date( $date, $key = '' ){

    if( is_array( $date ) ){
      fr_p( fr_date_array( $date ), $key, 0, false, false, 2 );
    } else {
      fr_p( fr_date( $date ), $key, 0, false, false, 2 );
    }

  }
}




function fr_error_log_get( $max_mb_size = 1, $log = false ){
  $log = $log ? $log : ini_get( 'error_log' );
  $max_mb_size *= 1000000;
  clearstatcache();
  $max_mb_size = min( filesize( $log ), $max_mb_size );
  $contents = file_get_contents( $log, FALSE, NULL, -$max_mb_size, $max_mb_size );

  return $contents;
}

function fr_error_log( $m, $data = NULL, $urgent = false, $exit_on_urgent = true ){

  if( fr_is_error( $m ) ){
    if( $data ){
      $data = [ $data, $m->data ];
    } else {
      $data = $m->data;
    }
    $m = $m->message;
  }

  $log_message = $m;
  if( !empty( $data ) ){
    $log_message .= "\n" . print_r( $data, 1 ) . "\n\n";
  }

  if( fr_is_development() && ( defined( 'WP_DEBUG' ) && WP_DEBUG && ( !defined( 'WP_DEBUG_DISPLAY' ) || WP_DEBUG_DISPLAY ) ) ){
    fr_p( [ $m, $data ], $exit_on_urgent );
  }

  if( $urgent ){
    fr_dev_email( 'Urgent Bug: ' . $m, $data );
  }

  error_log( $log_message );

}




function fr_delay_error_log( $delay_key, $delay_count, $message, $data = NULL, $urgent = false, $exit_on_urgent = true, $reset_after_sec = 0 ){

  if( fr_delay( $delay_key, $delay_count, $reset_after_sec ) ){
    unset( $contents[$delay_key] );
    fr_error_log( $message, $data, $urgent, $exit_on_urgent );
    return true;
  }

  return false;

}




function fr_delay( $delay_key, $delay_count, $reset_after_sec = 0 ){

  $file_path = str_replace( '.log', '-delay.log', ini_get( 'error_log' ) );
  $contents = file_exists( $file_path ) ? file_get_contents( $file_path ) : '';

  $contents = @json_decode( $contents, true );

  if( !$contents ){
    $contents = [];
  }

  $contents[$delay_key] ??= [];
  $contents[$delay_key][] = time();
  $error = false;

  if( $reset_after_sec ){
    foreach( $contents[$delay_key] as $k => $v ){
      if( time() - $v > $reset_after_sec ){
        unset( $contents[$delay_key][$k] );
      }
    }
  }

  if( count( $contents[$delay_key] ) > $delay_count ){
    unset( $contents[$delay_key] );
    $error = true;
  }

  file_put_contents( $file_path, json_encode( $contents, JSON_PRETTY_PRINT ), LOCK_EX );

  return $error;

}



function fr_delay_reset( $delay_key ){

  $file_path = str_replace( '.log', '-delay.log', ini_get( 'error_log' ) );
  $contents = file_exists( $file_path ) ? file_get_contents( $file_path ) : '';

  $contents = @json_decode( $contents, true );

  if( !$contents ){
    $contents = [];
  }

  if( !isset( $contents[$delay_key] ) ){
    return false;
  }

  unset( $contents[$delay_key] );
  file_put_contents( $file_path, json_encode( $contents, JSON_PRETTY_PRINT ), LOCK_EX );

  return true;

}





/**
 * Will send an email to the developer. Useful on urgent issues. The email is capped at once per 1 hour
 * @param $subject String The email subject
 * @param $message mixed The email body
 * @return Bool
 */

function fr_dev_email( string $subject, $message ): bool {

  if( !defined( 'DEV_EMAIL' ) ){
    return false;
  }

  if( fr_is_development() ){
    return false;
  }

  $last_time = 0;
  $log_data = fr_error_log_get();
  if( strstr( $log_data, 'dev_email_sent_at: ' ) ){
    $log_data = explode( 'dev_email_sent_at: ', $log_data );
    $last_time = end( $log_data );
    $last_time = strstr( $last_time, "\n", true );
  }

  // One email per hour
  if( time() - $last_time < 3600 ){
    return false;
  }

  if( function_exists( 'wp_mail' ) ){
    wp_mail( DEV_EMAIL, $subject, '<pre><code>' . print_r( $message, 1 ) . '</code></pre>' );
  } else {
    mail( DEV_EMAIL, $subject, '<pre><code>' . print_r( $message, 1 ) . '</code></pre>' );
  }
  
  $log_message = "\n dev_email_sent_at: " . time() . "\n";
  error_log( $log_message );

  return true;

}


function fr_log( $m, $data = false, $once = false, $destination = false, $debug_lvl = 0 ){

  $om = $m;

  if( !is_string( $m ) ){
    $m = print_r( $m, 1 );
  }

  if( !empty( $data ) ){
    $m .= "\n" . print_r( $data, 1 ) . "\n";
  }

  $debug = fr_get_debug_backtrace( 1 + $debug_lvl, 1, false );
  $m = $debug[0] . ' ' . $m;

  $m = '[' . date( 'd-M-Y H:i:s' ) . ' UTC] ' . $m . "\n";

  if( !$destination ){
    $destination = ini_get( 'error_log' );
  }

  if( $once ){

    $current = fr_error_log_get();

    if( str_contains( $current, $om ) ){
      return false;
    }
  }

  return file_put_contents( $destination, $m, FILE_APPEND );
}


function fr_log_debug( $m = 'debug_log', $data = [], $once = true, $destination = false, $debug_lvl = 0 ): void {
  $data = fr_make_array( $data );
  $data['debug'] = fr_get_debug_backtrace( 2 + $debug_lvl );
  fr_log( $m, $data, $once, $destination, 2 + $debug_lvl );
}





/**
 * V2.0
 * - Added support for group log
 */
function fr_log_view( $group_log = false, $log_file = false ){

  $content = fr_error_log_get( $group_log ? 10 : 1, $log_file );
  $content = trim( $content );
  
  if( !$group_log ){
    fr_p( $content );
    exit;
  }




  $content = explode( "\n", $content );
  $current_key = false;
  $current_index = 0;
  $groups = [];
  $index = 0;

  foreach( $content as $line ){
    if( strstr( $line, ' UTC] ' ) ){
      $current_key = fr_strstra( $line, ' UTC] ' );
      $line = $current_key;
      $index++;
    }

    if( !$current_key ){
      continue;
    }

    if( !isset( $groups[$current_key] ) ){
      $groups[$current_key] = [];
    }

    if( !isset( $groups[$current_key][$index] ) ){
      $groups[$current_key][$index] = '';
    }

    $groups[$current_key][$index] .= $line . "\n";
  }


  foreach( $groups as $k => $group ){
    $new = [];
    foreach( $group as $v ){
      $key = md5( $v );

      if( !isset( $new[$key] ) ){
        $new[$key] = [
          'count' => 0,
          'text' => $v
        ];
      }

      $new[$key]['count']++;
    }

    $groups[$k] = array_values( $new );
  }

  uasort( $groups, function( $a, $b ){
    return $a[0]['count'] < $b[0]['count'];
  } );


  foreach( $groups as $k => $group ){
    fr_p( [ $k, $group ] );
  }


  exit;
}

function fr_log_clear( $log = false ){
  $log = $log ? $log : ini_get( 'error_log' );
  if( file_exists( $log ) ){
    file_put_contents( $log, '' );
  }
}


function fr_error( $message, $data = [], $type = '', $info = [], $lvl = 2 ){
  return new fr_error( $message, $data, $type, $info, $lvl );
}

class fr_error{
  var $message;
  var $data;
  var $type;
  var $info;
  function __construct( $message, $data = [], $type = '', $info = [], $lvl = 1 ){

    $data = fr_make_array( $data );
    $data['debug_backtrace'] = fr_get_debug_backtrace( $lvl );

    $this->message = $message;
    $this->data = $data;
    $this->type = $type;
    $this->info = $info;

    return $this;
    
  }
}

function fr_is_error( $ob ){
  if( is_object( $ob ) && get_class( $ob ) == 'fr_error' ){
    return true;
  }
}


function fr_message($message,$type="success"){
  fr_session( 'fr_message', [
    'text' => $message,
    'type' => $type
  ] );
}

function fr_is_message(){
  return fr_session( 'fr_message' );
}

function fr_get_message(){
  return fr_session( 'fr_message' ) ? fr_session( 'fr_message' ) : [];
}

function fr_display_message($message="",$type="success",$display=""){
  if($message){
    fr_message($message,$type);
  }
  if( !fr_is_message() ){
    return;
  }
  $message = fr_get_message();
  fr_clear_message();
  
  ob_start();
  if($display=="woocommerce"){
    if($message["type"]=="success"){
      $message["type"]="message";
    }
    ?>
    <div class="fr_message woocommerce-<?php echo $message["type"] ?>" style="margin-bottom: 30px;clear:both">
      <span class="edd_error"><?php echo $message["text"] ?></span>
    </div>
    <?php 
  }else{
    ?>
    <div class="fr_message woocommerce-<?php echo $message["type"] ?> edd_errors edd-alert edd-alert-<?php echo $message["type"] ?> fes-form-field-<?php echo $message["type"] ?>" style="margin-bottom: 30px;clear:both">
      <span class="edd_error"><?php echo $message["text"] ?></span>
    </div>
    <?php 
  }
  return ob_get_clean();
}

function fr_clear_message(){
  fr_session( 'fr_message', [] );
}





//FIELD MESSAGES FOR CUSTOM FORMS

function fr_field_message($k,$v="",$wrapper="validation_message"){
  if(empty($_ENV["field_message"])){
    $_ENV["field_message"]=array();
  }
  if(empty($_ENV["field_message"][$k])){
    $_ENV["field_message"][$k]=array();
  }
  if($v!==""){
    $_ENV["field_message"][$k]=$v;
  }else if($_ENV["field_message"][$k]){
    if($wrapper){
      return '<div class="'.$wrapper.'">'.$_ENV["field_message"][$k].'</div>';
    }else{
      return $_ENV["field_message"][$k];
    }
  }
}

function fr_clear_field_messages(){
  $_ENV["field_message"]=array();
}


/**
 * Function that sends an email to everything it can about the current state of the script
 */
function fr_debug_email( $email, $subject, $data ){
  $data = [
    'data' => $data,
    'post' => $_POST,
    'get' => $_GET,
    'files' => $_FILES,
    'backtrace' => fr_get_debug_backtrace( 1, 20 )
  ];

  ob_start();
  fr_p( $data );
  $message = ob_get_clean();

  wp_mail( $email, $subject, $message );
}



function fr_start_query_log(){

  global $wpdb;

  if ( !defined( 'SAVEQUERIES' ) ){
    define( 'SAVEQUERIES', true );
  }

  if( !$wpdb->queries ){
    return;
  }

  $keys = array_keys( $wpdb->queries );

  $_ENV['fr_start_query_log'] = end( $keys );
}






function fr_end_query_log(){

  global $wpdb;

  $queries = $wpdb->queries;
  $queries = array_slice( $queries, $_ENV['fr_start_query_log'] + 1 );

  $times = array_column( $queries, 1 );

  $slim_queries = [];
  foreach( $queries as $k => $query ){

    $query[0] = str_replace( "\n", ' ', $query[0] );
    $query[0] = str_replace( "\r", ' ', $query[0] );
    $query[0] = str_replace( "\t", ' ', $query[0] );
    $query[0] = fr_replace_loop( '  ', ' ', $query[0] );

    $backtrace = explode( ', ', $query[2] );
    $backtrace = array_diff( $backtrace, explode( ', ', "require('wp-blog-header.php'), require_once('wp-load.php'), require_once('wp-config.php'), require_once('wp-settings.php'), do_action('init'), WP_Hook->do_action, WP_Hook->apply_filters, {closure}" ) );
    $backtrace = implode( ', ', $backtrace );

    $slim_queries[$k] = [
      'time' => $query[1],
      'query' => $query[0],
      'backtrace' => $backtrace
    ];
  }

  uasort( $slim_queries, function( $a, $b ){
    return $a['time'] < $b['time'];
  } );

  $data = [
    'count' => count( $queries ),
    'time' => array_sum( $times ),
    'max' => max( $times ),
    'queries' => $slim_queries
  ];

  fr_p( $data );
}




function fr_log_has_errors(){

  $log_file = ini_get( 'error_log' );

  if( !file_exists( $log_file ) ){
    return;
  }

  $contents = filesize( $log_file );

  if( !$contents ){
    return;
  }

  return true;

}




function fr_check_log_for_errors(){

  if( wp_doing_ajax() ){
    return;
  }

  if( !ini_get( 'display_errors' ) && !fr_constant( 'SCRIPT_DEBUG' ) ){
    return;
  }

  fr_add_actions( 'wp_footer admin_footer', function(){

    if( !fr_log_has_errors() ){
      return;
    }

    ?>
    <style>
      #fr_log_errors {
        position: fixed; 
        bottom: 0; 
        right: 0; 
        background-color: #DE350B; 
        color: white; 
        padding: 10px 20px; 
        text-align: center; 
        z-index: 10000000
      }
      #fr_log_errors a {
        color: inherit!important;
        text-decoration: underline;
      }
    </style>
    <div id="fr_log_errors">
      <?php 
      echo 
      fr_html_a( fr_build_link( fr_get_current_url(), [ 'fr_view_log' => 1 ] ), __( 'Errors', 'technadu' ), [ 'target' => '_blank' ] ) . ' | ' . 
      fr_html_a( fr_build_link( fr_get_current_url(), [ 'fr_clear_log' => 1 ] ), __( 'Clear Log', 'technadu' ) )
      ?>
    </div><?php 
  } );
}



function fr_enable_view_clear_log(){

  fr_GET_trigger( 'fr_clear_log', function(){

    $log_file = false;

    if( !empty( $_GET['log_file'] ) ){
      $log_file = fr_file_change_name( ini_get( 'error_log' ), $_GET['log_file'] );
    }

    fr_log_clear( $log_file );
    fr_redirect_without_param( 'fr_clear_log' );
  } );



  fr_GET_trigger( 'fr_view_log', function(){

    $log_file = ini_get( 'error_log' );

    if( !empty( $_GET['log_file'] ) ){
      $log_file = fr_file_change_name( ini_get( 'error_log' ), $_GET['log_file'] );
    }

    ?>
    <div style="float: right">
      <?php echo $log_file ?>
    </div>

    <a href="<?php echo fr_build_link( fr_get_current_url(), [ 'fr_clear_log' => 1 ] ) ?>">Clear Log</a> |
    <?php if( empty( $_GET['fr_group_log'] ) ){ ?>
      <a href="<?php echo fr_build_link( fr_get_current_url(), [ 'fr_group_log' => 1 ] ) ?>">Group View</a>
    <?php } else { ?>
      <a href="<?php echo fr_modify_url_params( [], [ 'fr_group_log' ] ) ?>">Normal View</a>
    <?php } ?>
    <?php 

    fr_log_view( !empty( $_GET['fr_group_log'] ), $log_file );
  } );

}




function fr_custom_error_reporting( $custom_data = [] ){

  set_error_handler( function( $errno, $errstr, $errfile, $errline ) use ( $custom_data ){

    // Check if the suppress error character is present at that line

    $cache_key = 'fewo_custom_error_reporting_' . $errfile . '_' . $errline . '_' . $errstr;
    $suppressed = fr_cache( $cache_key );

    if( $suppressed === false ){

      if( file_exists( $errfile ) ){

        $contents = file_get_contents( $errfile );
        $contents = explode( "\n", $contents );
        $suppressed = isset( $contents[$errline - 1] ) && strstr( $contents[$errline - 1], '@' ) ? 1 : 0;

        fr_cache( $cache_key, $suppressed );
        
      } else {
        $suppressed = true;
      }

    }
    
    if( $suppressed ){
      return;
    }

    if( defined( 'ABSPATH' ) ){
      $errfile = str_replace( ABSPATH, '', $errfile );
    }

    $url = fr_get_current_url();
    $uid = function_exists( 'fr_user' ) ? fr_user() : '';

    $data = [
      'date' => date( DB_DATE_TIME_FORMAT ),
      'file' => $errfile . ':' . $errline,
      'url' => $url,
      'uid' => $uid,
      'backtrace' => fr_get_debug_backtrace( 1 )
    ];

    $data = array_merge( $data, $custom_data );

    fr_log( $errstr, $data, false, false, false );

    return true;

  } );

}




function fr_get_debug_backtrace( $start = 0, $end = 100, $add_function = true ){

  $debug_backtrace = debug_backtrace( DEBUG_BACKTRACE_PROVIDE_OBJECT, $start + $end );
  $debug_backtrace = array_slice( $debug_backtrace, $start );

  $logs = [];

  foreach( $debug_backtrace as $trace ){

    $trace = array_merge( [
      'file' => '',
      'args' => [],
      'function' => '',
      'line' => 0
    ], $trace );

    if( defined( 'ABSPATH' ) ){
      $trace['file'] = str_replace( ABSPATH, '', $trace['file'] );
    }

    if( !empty( $trace['class'] ) ){
      $trace['function'] = $trace['class'] . $trace['type'] . $trace['function'];
    }

    $args = [];

    foreach( $trace['args'] as $arg ){
      if( is_array( $arg ) ){
        $json = json_encode( $arg );
        if( strlen( $json ) < 1000 ){
          $args[] = $json;
        } else {
          $args[] = 'array';
        }
      } else if( is_object( $arg ) ){
        $args[] = get_class( $arg );
      } else if( is_numeric( $arg ) ) {
        $args[] = $arg;
      } else if( is_bool( $arg ) ) {
        $args[] = $arg ? 'true' : 'false';
      } else if( is_null( $arg ) ) {
        $args[] = 'null';
      } else if( is_string( $arg ) || is_numeric( $arg ) ) {
        if( strlen( $arg ) < 1000 ) {
          $args[] = "'" . $arg . "'";
        } else {
          $args[] = '...';
        }
      }
    }

    $args = implode( ', ', $args );

    if( $args ){
      $args = ' ' . $args . ' ';
    }

    $log = $trace['file'] . ':' . $trace['line'];

    if( $add_function ){
      $log .= ' ' . $trace['function'] . '(' . $args . ')';
    }

    $logs[] = $log;

  }

  return $logs;
}


// fr_debug( get_defined_vars() );
function fr_debug( $defined_vars = [], $format_dates = false ){

  $vars = array_slice( $defined_vars, -50 );

  foreach( $vars as $k => $v ){

    if( is_object( $v ) ){
      $vars[$k] = get_class( $v );
    }

    if( is_array( $v ) && strlen( json_encode( $v ) ) > 1000 ){
      $vars[$k] = '[large array]';
    }

    if( is_string( $v ) && strlen( $v ) > 1000 ){
      $vars[$k] = '[large string]';
    }

  }

  $data = [
    'backtrace' => fr_get_debug_backtrace( 2 ),
    'vars' => $vars
  ];

  $data = array_filter( $data );

  if( $format_dates ){
    fr_p_date( $data, 1 );
  } else {
    fr_p( $data, 1 );
  }

}

