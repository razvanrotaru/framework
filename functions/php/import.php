<?php 
function fr_extract_email( $c, $only_valid = true ){
    $emails = fr_extract_emails( $c, $only_valid );
    return reset( $emails );
}


/*
correct emails:
l@email.co.uk
e.mail@asfa.com
emaAil @ asfasf.com
email [AT] asfa.com
email(AT)asfa.com 

incorrect emails:
neg @ atives.  asfa
@asfsagatasfas.com
test me.com
*/

function fr_extract_emails( $c, $only_valid = true ){

    $matches = [];
    $c = ' ' . $c . ' ';

    while( 1 ){

        preg_match( '/[^a-z0-9_\-\+.]([a-z0-9_\-\+.]+[ ]?(?:@|\(at\)|\[at\])[ ]?[a-z0-9\-]+[ ]?(?:\.|\(dot\)|\[dot\])[ ]?[\(\[]?\(?(?:[a-z]{2,3})(?:\.[a-z]{2})?[\)\]]?)[^a-z0-9]/mi', $c, $res );
        
        if( !empty( $res[1] ) ){
            if( !$only_valid || filter_var( $res[1], FILTER_VALIDATE_EMAIL ) ){
                $matches[] = $res[1];
            }
            $c = str_replace( $res[1], '', $c );
            continue;
        }

        break;
    }

    return $matches;
}




/*
correct phone numbers:
s+40746363392 
+40 746 363 392 
+40 746363392 
040746363392 
040 746 363 392 
04 0746 363 392 
(040) 0746-363-392 
040 0746-363-392 
1-869 746 363 392 

incorrect phone numbers:
4253523
322 5235 2356 25 23 562362
,325235,235625,23562362
33253 235432 242 324 242 2424
*/

function fr_extract_phones( $c ){

    $matches = [];
    $c = ' ' . $c . ' ';

    while( 1 ){

        preg_match( '/[^0-9]((1-)?(([+][0-9]{10,11})|([+][0-9]{1,2}( |-)?[0-9]{1,3}( |-)?[0-9]{1,3}( |-)?[0-9]{1,3})|([0-9]{12})|([0-9]{3}( |-)[0-9]{3}( |-)[0-9]{3}( |-)[0-9]{3})|([0-9]{2,3}( |-)[0-9]{4}( |-)[0-9]{3}( |-)[0-9]{3})|(\([0-9]{3}\)( |-)[0-9]{4}( |-)[0-9]{3}( |-)[0-9]{3})))[^0-9]/m', $c, $res );

        if( !empty( $res[1] ) ){
            $matches[] = $res[1];
            $c = str_replace( $res[1], '', $c );
            continue;
        }

        break;
    }

    return $matches;
}

function fr_extract_url($c){
    $matches = fr_extract_urls( $c );
    return reset( $matches );
}

function fr_extract_urls($c) {
    $c = str_replace(['"', "'"], ' ', $c ?? '');
    preg_match_all('!https?://[^<>\s]+!', $c, $matches);
    return $matches[0];
}




/**
 * It extracts strings using multiple delimiters
 * @param  $content string String from which to extract the text
 * @param  $start
 * @param  $end
 * @param  $info array An array of delimiters like for fewo_extract_multiple
 * @return array
 */
function fr_loop_extract( string $content = '', string $start = '', string $end = '', array $info = [] ): array {

    if( $content === null ){
        return [];
    }
    
    if( $start && $end ){
        $content = fr_extract( $content,$start,$end );
    }

    $x=0;
    $data = [];
    foreach( $info as $i ){

        $i2 = explode( '|', $i[0] );
        $i2_len = array_map( 'strlen', $i2 );
        $end_len = strlen( $i[1] );

        $offset = 0;
        $y = 0;
        while( 1 ){
            foreach( $i2 as $k => $v ){
                $pos = strpos( $content, $v, $offset );
                if( $pos === false ){
                    break 2;
                }
                $offset = $pos + $i2_len[$k];
            }

            $pos2 = strpos( $content, $i[1], $offset );

            if( $pos2 === false ){
                break;
            }

            $data[$y][$x] = substr( $content, $offset, $pos2 - $offset );
            $offset = $pos2 + $end_len;

            $y++;
        }

        $x++;
    }

    return $data;
}


function fr_single_loop_extract( $c, $start, $end ){
    $res = fr_loop_extract($c,"","",[ [ $start, $end ] ]);
    $res = array_column( $res, 0 );
    return $res ?: [];
}


function fr_single_loop_extract_reverse( $c, $start, $end ){
    $c = strrev( $c );
    $start = strrev( $start );
    $end = strrev( $end );
    $res = fr_single_loop_extract( $c, $end, $start );
    $res = array_map( 'strrev', $res );
    $res = array_reverse( $res );
    return $res;
}

function fr_single_loop_extract_remove( $c, $start, $end, $remove = '', $include_wrapper = true ){
    $found = fr_single_loop_extract( $c, $start, $end );
    
    foreach( $found as $v ){
        if( $include_wrapper ){
            $c = str_replace( $start . $v . $end, $remove, $c );
        } else {
            $c = str_replace( $v, $remove, $c );
        }
    }
    return $c;
}

function fr_extract( $c, $b, $a = '' ){

    $b = str_replace( '\\|', '?*?*?', $b );
    $b = explode( '|', $b );

    $offset = 0;
    foreach( $b as $bb ){
        $bb = str_replace( '?*?*?', '|', $bb );
        $offset = strpos( $c, $bb, $offset );

        if( $offset === false ){
            return false;
        }

        $offset += strlen( $bb );
    }

    if( $a == '' ){
        return substr( $c, $offset );
    }

    if( is_array( $a ) ){
        $c = substr( $c, $offset );
        $location = 0;
        $match = '';

        foreach( $a as $k => $v ){
            $pos = strpos( $c, $v );

            if( !$location || $pos !== false && $pos < $location ){
                $location = $pos;
                $match = $v;
            }
        }


        if( $location ){
            $c = [ substr( $c, 0, $location ), $match ];
        } else {
            $c = [];
        }
    } else {
        $pos = strpos( $c, $a, $offset );
        $c = substr( $c, $offset, $pos - $offset );
    }

    return $c;

}



function fr_extract_with_wrap( $c, $b, $a = '' ){
    $match = fr_extract( $c, $b, $a );
    if( strlen( $match ) ){
        $res = $b . $match . $a;
    } else {
        $res = '';
    }
    return $res;
}


function fr_extract_replace( $c, $b, $a, $replace_with = '' ){
    $match = fr_extract( $c, $b, $a );
    if( strlen( $match ) ){
        $c = fr_str_replace_once( $match, $replace_with, $c );
    }
    return $c;
}

function fr_extract_replace_with_wrap( $c, $b, $a, $replace_with = '' ){
    $match = fr_extract( $c, $b, $a );
    if( strlen( $match ) ){
        $c = fr_str_replace_once( $b . $match . $a, $replace_with, $c );
    }
    return $c;
}