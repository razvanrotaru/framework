<?php
function fr_csv_to_array($file){
	return array_map('str_getcsv', file($file));
}

function fr_csv_string_to_array( $data ){
  $lines = explode(PHP_EOL, $data);
  $array = array();
  foreach ($lines as $line) {
      $array[] = str_getcsv($line);
  }
  return $array;
}

function fr_array_to_csv($data, $delimiter = ',', $enclosure = '"') {
	$handle = fopen('php://temp', 'r+');
	foreach ($data as $line) {
		fputcsv($handle, $line, $delimiter, $enclosure);
	}
	rewind($handle);
	$contents = '';
	while (!feof($handle)) {
		$contents .= fread($handle, 8192);
	}
	fclose($handle);
	return $contents;
}



function fr_required_dirs($dirs){
	foreach($dirs as $dir){
		fr_required_dir( $dir );
	}
}

function fr_required_dir( $dir, $perms = 0755 ){
	if(!file_exists($dir)){
		mkdir($dir,$perms,1);
	}
}



function fr_get_mp3_file_data($mp3,$name=''){
	if(is_numeric($mp3)){
		$mp3=get_attached_file($mp3);
	}
	require_once(FRAMEWORK_DIR.'/resources/getid3/getid3.php');
	
	$getID3 = new getID3;
	$mixinfo = $getID3->analyze($mp3);
	if( empty($mixinfo["fileformat"]) && $name ){
		$p=pathinfo($name);
		$mixinfo["fileformat"]=$p["extension"];
	}
	return $mixinfo;
}


function fr_remove_mp3_file_data($mp3){
	$TextEncoding = 'UTF-8';
	require_once(FRAMEWORK_DIR.'/resources/getid3/getid3.php');
	
	// Initialize getID3 engine
	$getID3 = new getID3;
	$getID3->setOption(array('encoding'=>$TextEncoding));

	require_once(FRAMEWORK_DIR.'/resources/getid3/write.php');
	// Initialize getID3 tag-writing module
	$tagwriter = new getid3_writetags;
	$tagwriter->filename = $mp3;

	$tagwriter->tagformats = array('id3v2.3');

	// set various options (optional)
	$tagwriter->overwrite_tags    = true;
	$tagwriter->remove_other_tags = true;
	$tagwriter->tag_encoding      = $TextEncoding;

	// populate data array
	$TagData = array();
	$tagwriter->tag_data = $TagData;
	
	// write tags
	if ($tagwriter->WriteTags()) {
	    return 1;
	} else {
	    return false;
	}
}



function fr_image_crop( $img, $w, $h, $x = 0, $y = 0 ){
	$new_img = imagecreatetruecolor( $w, $h );
	imagesavealpha( $new_img, true );
	imagealphablending( $new_img, false );
	imagefill( $new_img, 0, 0, imagecolorallocatealpha ( $new_img, 255, 255, 255, 127 ) );
	imagecopyresampled ($new_img, $img ,0, 0, $x, $y, $w, $h, $w, $h );
	return $new_img;
}



//archive an array of files
//the files can have custom names [ file_path => file_name ]
function fr_zip_files($files,$archive_name="",$password=""){
	set_time_limit(600);


	if(strstr($archive_name,DIRECTORY_SEPARATOR)){
		$file_archive=$archive_name;
	}else{
		if(!$archive_name){
			$archive_name=md5(fr_micro_time());
		}
		$file_archive=fr_uploads_dir()."/".$archive_name.".zip";
	}

	if(file_exists($file_archive)){
		unlink($file_archive);
	}


	if($password){
		if(!is_numeric(key($files))){
			$all_files=array_keys($files);
		}else{
			$all_files=$files;
		}

		// remove unexistent files
		foreach( $all_files as $k => $file ){
			if( !file_exists( $file ) ){
				unset( $all_files[$k] );
			}
		}

		// return if no files
		if( !$all_files ){
			return;
		}


		// rename file path unique
		$unique_files = array();
		$unique_file_names = array();
		foreach( $all_files as $file ){
			$info = pathinfo( $file );
			$unique_file_names[] = $name = fr_unique_file_path( basename( $file ) , $unique_file_names );
			$unique_files[$file] = $info["dirname"] . "/" . $name;
			rename( $file, $unique_files[$file] );
		}
		$all_files = $unique_files;


		// escape file paths
		foreach($all_files as $k=>$file){
			$all_files[$k]=escapeshellarg($file);
		}

		//hopefully the current system allows shell_exec
		shell_exec('zip -jP '.escapeshellarg($password).' '.escapeshellarg($file_archive).' '.implode(" ",$unique_files));
		

		// rename back the file names
		foreach( $unique_files as $original => $file ){
			rename( $file, $original );
		}

		if( file_exists( $file_archive ) && filesize( $file_archive ) ){
			return $file_archive;
		}
	}



	$unique_file_names = [];
	foreach ( $files as $file => $name ) {

		if( is_numeric( $file ) ){
			$file = $name;
			$name = '';
		}

		if( !$name ){
			$name = basename( $file );
		}

		$file = fr_url_to_path( $file );
		$name = fr_unique_file_path( $name, $unique_file_names );
		$unique_file_names[$file] = $name;
	}

	$unique_file_names_escaped = [];
	foreach( $unique_file_names as $file => $name ){
		$unique_file_names_escaped[] = escapeshellarg( $file );
	}


    
	// PHP ZIP

	$zip = new ZipArchive();
	
	if ( $zip->open( $file_archive, ZIPARCHIVE::CREATE ) === true ) {
		
		$index = 0;
		foreach ( $unique_file_names as $file => $name ) {

			if( !file_exists( $file ) ){
				continue;
			}

			$zip->addFile( $file, $name );
	    	$zip->setCompressionIndex( $index, ZipArchive::CM_STORE );
	    	$index++;
		}

		$zip->close();

		if( file_exists( $file_archive ) && filesize( $file_archive ) ){
			return $file_archive;
		}
	}


	return false;
}

function fr_command_exists( $cmd ) {

    $return = shell_exec( sprintf( 'which %s', escapeshellarg( $cmd ) ) );

    return !empty( $return );

}

function fr_unzip( $file, $path = "" ){
	$zip = new ZipArchive;
	if ($zip->open( $file ) === TRUE) {
	    $zip->extractTo( $path );
	    $zip->close();
	    return true;
	} else {
	    return false;
	}
}

function fr_pclzip_unzip( $file, $path = "" ){
	include_once(FRAMEWORK_DIR . '/resources/pclzip.lib.php' );
	$archive = new PclZip( $file );
	if( !$path ){
		$path = dirname( $file );
	}
	if ( $archive->extract( PCLZIP_OPT_PATH, $path ) != 0 ) {
		return true;
	}
}

function fr_array_to_xls($array,$file){
	include_once(FRAMEWORK_DIR."/resources/excelwriter.inc.php"); 
    
    $excel=new ExcelWriter($file);//.xls
    
    if($excel==false)
        return;
        
    foreach($array as $row){
    	$excel->writeLine($row);
    }
    $excel->close(); 
    return 1;
}


/**
 * info @ https://github.com/mk-j/PHP_XLSXWriter
 */

function fr_array_to_xlsx($sheets,$file=""){
	include_once(FRAMEWORK_DIR."/resources/xlsxwriter.class.php"); 
    
    $excel=new XLSXWriter();//.xls
    
    if($excel==false)
        return;
    
    foreach($sheets as $name=>$sheet){
	    if(!empty($sheet["header"])){
	    	$excel->writeSheetHeader($name, $sheet["header"] );
	    }
	    if(!empty($sheet["data"])){
		    foreach($sheet["data"] as $row){
				$excel->writeSheetRow($name, $row );
		    }
		}
	}

    if($file){
    	$excel->writeToFile($file);
    	return 1;
    }
    return $excel->writeToString();
}



/**
 * Replace shortcodes in a word document
 * @param  string $file       File path
 * @param  array $shortcodes 
 * @param  string $start Shortcode start character
 * @param  string $end Shortcode end character
 * @return boolean
 */
function fr_docx_replace_shortcodes($file, $shortcodes, $start="[", $end="]", $wraper_start="", $wraper_end=""){
	$zip = new ZipArchive;
	if ($zip->open($file) === TRUE) {
		$newContents = array();
		for ($i = 0; $i < $zip->numFiles; $i++) {
			$file_to_change = $zip->getNameIndex($i);
			if(substr($file_to_change,-4,4)!=".xml"){
				continue;
			}
		    $oldContents = $zip->getFromName($file_to_change);
		    $newContents[$file_to_change] = fr_do_shortcodes_array($oldContents, $shortcodes, $start, $end, $wraper_start, $wraper_end);
		}

		foreach($newContents as $file_to_change=>$c){
		    $zip->deleteName($file_to_change);
		    $zip->addFromString($file_to_change, $c);
		}
	    return $zip->close();
	}
}


function fr_docx_get_shortcodes($file, $start="[", $end="]", $wraper_start="", $wraper_end=""){
	$zip = new ZipArchive;
	$labels = [];
	if ($zip->open($file) === TRUE) {
		$newContents = array();
		for ($i = 0; $i < $zip->numFiles; $i++) {
			$file_to_change = $zip->getNameIndex($i);
			if(substr($file_to_change,-4,4)!=".xml"){
				continue;
			}
		    $contents = $zip->getFromName($file_to_change);
		    $labels = array_merge( $labels, fr_single_loop_extract( $contents, $start, $end ) );
		}
	    $zip->close();
	}
	$labels = array_unique( $labels );

	return $labels;
}



function fr_view_document_in_google($file,$embed="false"){
	return "https://docs.google.com/gview?embedded=".$embed."&url=".fr_path_to_url($file);
}
function fr_view_document_in_office($file){
	return "https://view.officeapps.live.com/op/view.aspx?src=".urlencode(fr_path_to_url($file)."?time=".strtotime("now"));
}



function fr_unique_file_path( $file, $files = false ){
	$info = pathinfo( $file );

	if( $files === false ){
		$files = fr_scan_dir( $info["dirname"], array(), array(), 1, 1 );
		$files = array_column( $files, 'path' );
	}

	$orig_name = basename( $file );

	$x = 1;
	while( in_array( $file, $files ) ){
		$info = pathinfo( $file );

		$file = substr( $orig_name, 0, - ( strlen( $info["extension"] ) + 1 ) ) . $x . "." . $info["extension"];
		if( $info["dirname"] != "." ){
			$file = $info["dirname"] . "/" . $file;
		}
		$x++;
	}
	return $file;
}

/**
 * Rename existing attachment
 * @param  int $fid  Attachement ID
 * @param  string $name New name with extension
 * @return string	The path to the new file
 */
function fr_rename_attachment( $fid, $name ){

	$file = get_attached_file( $fid );
	$info = pathinfo( $file );
	$file_new = $info['dirname'] . '/' . $name;

	if( !$file ){
		return false;
	}

	if( $file == $file_new ){
		return $file;
	}
	

	if( !rename( $file, $file_new ) ){
		return false;
	}

	if( !file_exists( $file_new ) ){
		return false;
	}

	// Make path relative
	$file2 = fr_uploads_relative_path( $file_new );

	update_post_meta( $fid, '_wp_attached_file', $file2 );

	$meta = fr_m( '_wp_attachment_metadata', $fid );

	if( !$meta ){
		return $file_new;
	}

	if( isset( $meta['file'] ) ){
		$meta['file'] = fr_file_change_name( $meta['file'], $name );
	}

	$old_new = [];
	if( isset( $meta['sizes'] ) ){
		foreach( $meta['sizes'] as $k => $v ){
			$interm_file_new_name = fr_file_name( $name ) . '-' . $v['width'] . 'x' . $v['height'];
			$interm_file_new_name = fr_file_change_name( $v['file'], $interm_file_new_name );
			$interm_file = $info['dirname'] . '/' . $v['file'];
			$interm_file_new = $info['dirname'] . '/' . $interm_file_new_name;

			if( !file_exists( $interm_file ) ){
				if( file_exists( $interm_file_new ) ){
					$old_new[$v['file']] = $interm_file_new_name;
					$meta['sizes'][$k]['file'] = $interm_file_new_name;
				}
				continue;
			}

			if( rename( $interm_file, $interm_file_new ) ){
				$old_new[$v['file']] = $interm_file_new_name;
				$meta['sizes'][$k]['file'] = $interm_file_new_name;
			}
		}
	}


	if( isset( $meta['ShortPixel'] ) && isset( $meta['ShortPixel']['thumbsOptList'] ) ){
		foreach( $meta['ShortPixel']['thumbsOptList'] as $k => $v ){
			if( isset( $old_new[$v] ) ){
				$meta['ShortPixel']['thumbsOptList'][$k] = $old_new[$v];
			}
		}
	}


	fr_m( '_wp_attachment_metadata', $meta, $fid );

	return $file_new;
}


function fr_dir_remove( $dir ){
	if( file_exists( $dir ) ){
		$files = fr_scan_dir( $dir, array(), array(), 0, 1 );
		foreach( $files as $file ){
			if( is_file( $file["path"] ) ){
				unlink( $file["path"] );
			}
			if( is_dir( $file["path"] ) ){
				rmdir( $file["path"] );
			}
		}
		rmdir( $dir );
	}
}


function fr_delete_dir_contents($dir,$rmv_par_dir=0,$max_files=0){
	$count = 0;
	if( file_exists( $dir ) ){
		$files = fr_scan_dir( $dir, array(), array(), 0, 0, $max_files );

		foreach( $files as $k=>$v ){
			unlink($v["path"]);
			$count++;
		}
		$files = fr_scan_dir( $dir, array(), array(), 0, 2, $max_files );
		$files = array_reverse( $files );
		foreach( $files as $k=>$v ){
			rmdir($v["path"]);
			$count++;
		}
		if($rmv_par_dir){
			rmdir($dir);
			$count++;
		}
	}
	return $count;
}



function fr_move_dir_contents( $from, $to ){
	if( $from && $to ){
		$res=fr_scan_dir( $from, array(), array(), 0, 1 );
		$res = array_reverse( $res );

		foreach($res as $k=>$v){
			$new_path = str_replace( $from, $to, $v["path"] );

			if( is_dir( $v["path"] ) && !file_exists( $new_path ) ){
				$perms = fileperms( $v["path"] );
				mkdir( $new_path, $perms, 1 );
			}

			if( is_file( $v["path"] ) ){
				rename( $v["path"], $new_path );
			}
		}
	}
}

function fr_unlink( $file ){
	if( file_exists( $file ) ){
		unlink( $file );
	}
}

function fr_file_clear( $file ){
	return file_put_contents( $file, '' );
}

function fr_file_is( $file, $ext ){
	return $ext == fr_file_ext( $file );
}

function fr_file_ext( $file ){
	$file_parts = pathinfo( $file );
	if( !empty( $file_parts['extension'] ) ){
		return $file_parts['extension'];
	}
}

function fr_file_has_ext( $file, $ext ){
	return fr_file_ext( $file ) == $ext;
}


function fr_file_safe_get_contents( $file, $max = 10 ){
	$x = 0;
	if( !file_exists( $file ) ){
		return false;
	}

	while( !is_writable( $file ) || !is_readable( $file ) ){
		if( $x >= $max ){
			return false;
		}
		usleep( 100000 );
		$x++;
	}
	return file_get_contents( $file );
}

function fr_file_safe_put_contents( $file, $data, $max = 10 ){
	$x = 0;
	while( file_exists( $file ) && ( !is_writable( $file ) || !is_readable( $file ) ) ){
		if( $x >= $max ){
			return false;
		}
		usleep( 100000 );
		$x++;
	}
	return file_put_contents( $file, $data );
}

function fr_file_url( $pid ){
	return wp_get_attachment_url( $pid );
}

function fr_file_path( $pid, $location = false ){
	$path = '';
	if( $location !== false ){
		$path = fr_uploads_dir() . '/' . $location;
	} else {
		$path = get_attached_file( $pid );
	}
	return $path;
}
function fr_file_name( $file ){
	$info = pathinfo( $file );
	return $info['filename'];
}

function fr_file_append_to_name( $file, $append ){
	if( $file ){
		$info = pathinfo( $file );
		return  $info['dirname'] . '/' . $info['filename'] . $append . '.' . $info['extension'];
	}
}

function fr_file_change_extension( $file, $ext ){

	if( $file ){

		$info = pathinfo( $file );
		$dir = $info['dirname'];

		if( $dir == '.' ){
			$dir = '';
		} else {
			$dir .= DIRECTORY_SEPARATOR;
		}

		return $dir . $info['filename'] . '.' . $ext;
		
	}
}

function fr_file_change_name( $file, $name, $ext = false ){
	if( $file ){
		$info = pathinfo( $file );
		$info2 = pathinfo( $name );

		if( empty( $info2['extension'] ) ){
			if( $ext == false ){
				$ext = $info['extension'];
			}
			$file = $name . '.' . $ext;
		} else {
			$file = $name;
		}

		if( $info['dirname'] != '.' ){
			$file = $info['dirname'] . '/' . $file;
		}
	}
	return $file;
}

function fr_file_append_name( $file, $append, $ext = false ){
	if( strlen( $append ) ){
		$file = fr_file_append_to_name( $file, $append );
	}
	if( strlen( $ext ) ){
		$file = fr_file_change_extension( $file, $ext );
	}
	return $file;
}

/**
 * Check if the given files are different from each other
 * Return only the different ones
 * @param  array $files File paths
 * @return array
 */
function fr_files_unique( $files ){
    // Hash the files and return only the unique ones
    $new_files = array();
    foreach( $files as $file ){
        $new_files[sha1_file( $file )] = $file;
    }

    return $new_files;
}




function fr_file_base64_encode( $file ){
	if( file_exists( $file ) ){
		$mime = fr_file_mime_type( $file );
		if( $mime == 'image/svg' ){
			$mime .= '+xml';
		}
		return 'data:' . $mime . ';base64,' . base64_encode( file_get_contents( $file ) );
	}
}

function fr_file_mime_type( $file ){
	$mime = mime_content_type( $file );

	// Fix for PHP bug: https://bugs.php.net/bug.php?id=77784
	if( strstr( $mime, 'application/' ) ){
		$mime = explode( 'application/', $mime );
		$mime = 'application/' . $mime[1];
	}

	return $mime;
}


function fr_file_base64_decode( $data, $allowed_extensions = [], $file = false ){
	if( substr( $data, 0, 5 ) != 'data:' ){
		return;
	}

	$data = explode( ',', $data );
	if( count( $data ) != 2 ){
		return;
	}

	$mime = fr_extract( $data[0], ':', ';' );
	if( !$mime ){
		return;
	}

	$extension = fr_data_mime_to_extension( $mime );
	if( !$extension || !in_array( $extension, $allowed_extensions ) ){
		return;
	}

	$img = base64_decode( $data[1] );

	if( !$img ){
		return;
	}

	if( $file ){
		//$file = fr_file_change_extension( $file, $extension );
		file_put_contents( $file, $img );
	}

	return [ 'image' => $img, 'mime' => $mime, 'extension' => $extension ];
}



function fr_watermark_audio( $orig, $watermark, $output_file ){

	// Create the file names
	$silent_file = fr_file_append_name( $orig, '-36326747819-silent', 'mp3' );
	$silent_file_with_watermark = fr_file_append_to_name( $silent_file, '-watermark' );

	// Extract the duration of the original file
	$duration = fr_ffmpeg_audio_info( $orig, 'duration' );

	// Abort if failed to extract duration
	if( !$duration ){
		fr_error_log( 'Failed to extract duration of audio file', $orig, 1 );
		return;
	}

	// Create a silent file as long as the original
	$silent_file = fr_ffmpeg_create_silent_mp3( $silent_file, $duration );

	// Abort if failed to generate silent file
	if( fr_is_error( $silent_file ) ){
		fr_error_log( $silent_file, 0, 1 );
		return;
	}

	// Repeat the audio watermark over the silent file 
	$silent_file_with_watermark = fr_ffmpeg_audio_watermark( $silent_file, $watermark, $silent_file_with_watermark );

	// Delete helper file
	unlink( $silent_file );

	// Abort if failed to generate silent file with watermark
	if( fr_is_error( $silent_file_with_watermark ) ){
		fr_error_log( $silent_file_with_watermark, $orig, 1 );
		return;
	}

	// Side-chain compress the original and silent file with watermark
	$output_file = fr_ffmpeg_side_chain_compress( $orig, $silent_file_with_watermark, $output_file );

	// Delete helper file
	unlink( $silent_file_with_watermark );

	// Abort if filed to side-chain compress the audio files
	if( fr_is_error( $output_file ) ){
		fr_error_log( $output_file, 0, 1 );
		return;
	}


	// Return file on success
	return $output_file;
}


function fr_delete_dirs_in_dir( $dir, $depth = 1 ){
	$res = fr_scan_dir( $dir, [], [], $depth, 1 );
	$total = 0;
	foreach( $res as $v ){
		if( is_dir( $v['path'] ) ){
			rmdir( $v['path'] );
			$total++;
		}
	}
	return $total;
}

function fr_file_info( $file, $k ){
	$info = pathinfo( $file );
	return $info[$k];
}

function fr_ext_to_accept( $ext ){
	foreach( $ext as $k => $v ){
		$ext[$k] = '.' . $v;
	}
	return implode( ',', $ext );
}


function fr_file_create_fake_upload( $field_name, $file_name, $mime, $contents ){

   	$file = tmpfile();
    $file_path = tempnam( sys_get_temp_dir(), 'fr' );
    file_put_contents( $file_path, $contents );

    $_FILES[$field_name] = [
      'name' => $file_name,
      'type' => $mime,
      'error' => 0,
      'tmp_name' => $file_path,
      'size' => filesize( $file_path )
    ];

    return $file_path;
}
















function fr_svg_icon( $name, $attr = [], $url = false, $reuse = true ){

	$file = false;
	$files = fr_scan_dir( fr_global( 'fr_global_svg_icons_directory' ), [], [ 'svg' ] );
	
	foreach( $files as $f ){
		if( $f['file'] == $name . '.svg' ){
			$file = $f['path'];
			break;
		}
	}

	if( !$file ){
		return false;
	}

	if( $url ){
		return fr_img_bas64_svg( $file, [], true );
	}

	$title = '';
	$c = file_get_contents( $file );
	fr_global( 'icon', strlen( $c ), true );
	if( !isset( $attr['class'] ) ){
		$attr['class'] = '';
	}
	$attr['class'] .= ' fr-svg-icon ' . fr_sanitize_title( 'fr-svg-icon-' . $name );
	$attr['role'] = 'img';
	if( !empty( $attr['title'] ) ){
		$title = '<title>' . esc_html( $attr['title'] ) . '</title>';
		unset( $attr['title'] );
		$attr['aria-labelledby'] = 'title';
	}


	// If the SVG contains a mask, don't reuse, because the browser duplicates SVG in DOM and the SVG fails to find the mask targeted by and id

	if( $reuse && strstr( $c, '<mask' ) ){
		$reuse = false;
	}


	// Make internal ids unique if not reusable

	if( !$reuse ){
		$unique = 'id' . fr_unique_id( 'fr_svg_icon' );
		$c = str_replace( 'id="', 'id="' . $unique . '-', $c );
		$c = str_replace( "id='", "id='" . $unique . '-', $c );
		$c = str_replace( 'url(#', 'url(#' . $unique . '-', $c );
		$c = str_replace( 'xlink:href="#', 'xlink:href="#' . $unique . '-', $c );
		$c = str_replace( "xlink:href='#", "xlink:href='#" . $unique . '-', $c );
	}

	$c = str_replace( '<svg ', '<svg ' . fr_array_to_tag_attr( $attr ) . ' ', $c );

	if( !$reuse ){
		return $c;
	}


	// Reuse icons 
	$id = 'fr-svg-icon-' . $name;
	$svg = strstr( $c, '>', true ) . '>' . $title . '<use xlink:href="#' . $id . '"/></svg>';

	$c = fr_str_replace_once( '>', '><defs><g id="' . $id . '">', $c );
	$c = str_replace( '</svg>', '</g></defs></svg>', $c );
	$c = fr_extract_replace_with_wrap( $c, 'viewBox="', '"' );
	$c = fr_extract_replace_with_wrap( $c, 'width="', '"' );
	$c = fr_extract_replace_with_wrap( $c, 'height="', '"' );
	fr_global( 'fr_svg_icons', [ $id => $c ], ARRAY_A, true );

	return $svg;
}


function fr_svg_icons_get(){
	$icons = fr_global( 'fr_svg_icons' );
	ob_start();
		?>
		<div class="fr_reference_svg_icons" style="display: none">
			<?php echo implode( '', $icons ); ?>
		</div>
		<?php 
	$c = ob_get_clean();
	return $c;
}




function fr_download_chunked_file( $url, $file_path = false, $chunkSize = 1024 * 1024, $max_file_size = 1024 * 1024 * 1024 ){

	if( !$file_path ){
		$file_path = tempnam(sys_get_temp_dir(), 'prefix');
	}

    $remoteFile = fopen($url, 'rb');

    if (!$remoteFile) {
        return new fr_error( "Cannot open remote file: $url" );
    }

    $localFile = fopen($file_path, 'wb');
    if (!$localFile) {
        fclose($remoteFile);
        return new fr_error("Cannot open local file for writing: $file_path");
    }

    $total_read = 0;

    while (!feof($remoteFile)) {
        $chunk = fread($remoteFile, $chunkSize);
        fwrite($localFile, $chunk);

        $total_read += strlen( $chunk );

        if( $total_read > $max_file_size ){
			fclose($remoteFile);
			fclose($localFile);
			return new fr_error( "File too large: $url" );
		}

    }

    fclose($remoteFile);
    fclose($localFile);

    return $file_path;

}






/**
 * Get the duration of an audio file to the iso format
 */
function fr_seconds_to_iso8601( $seconds ) {

	$total_seconds = round( (float)$seconds );
	$hours = floor( $total_seconds / 3600 );
	$minutes = floor( ( $total_seconds % 3600 ) / 60 );
	$seconds = $total_seconds % 60;

	$duration = 'PT';

	if( $hours > 0 ) {
		$duration .= $hours . 'H';
	}

	if( $minutes > 0 ) {
		$duration .= $minutes . 'M';
	}

	if( $seconds > 0 || $minutes == 0 ) {
		$duration .= $seconds . 'S';
	}

	return $duration;

}






function fr_save_contents_to_tmp_file( $contents, $ext = 'tmp' ){

	$system_tmp_file = sys_get_temp_dir() . '/' . md5( $contents );
	file_put_contents( $system_tmp_file, $contents );

	return file_exists( $system_tmp_file ) ? $system_tmp_file : false;

}



function fr_save_url_to_tmp_file( $url ){

	$system_tmp_file = sys_get_temp_dir() . '/' . md5( $url );
	
	$context = stream_context_create([
		'http' => [
			'timeout' => 50,  // timeout in seconds
			'follow_location' => 1
		],
		'ssl' => [
			'verify_peer' => false,
			'verify_peer_name' => false
		]
	]);

	$source = fopen($url, 'rb', false, $context);
	if ($source === false) {
		return false;
	}

	$destination = fopen($system_tmp_file, 'wb');
	if ($destination === false) {
		fclose($source);
		return false;
	}

	$success = stream_copy_to_stream($source, $destination);
	
	fclose($source);
	fclose($destination);

	if ($success === false) {
		unlink($system_tmp_file);
		return false;
	}

	return file_exists($system_tmp_file) ? $system_tmp_file : false;

}



function fr_file_name_encoded_for_header( $filename ) {

	$fallback = preg_replace( '/[\r\n\t\s]+/', ' ', strip_tags( $filename ) );
	$fallback = preg_replace( '/[^\x20-\x7e]/', '', $fallback );
	$fallback = str_replace( '"', "'", $fallback );
	$encoded = rawurlencode( $filename );

	return "filename=\"$fallback\"; filename*=UTF-8''$encoded";

}





function fr_delete_dir_recursively( string $dir, array $allowed_paths ){

	if( !$dir ){
		trigger_error( 'Missing directory in fr_delete_dir_recursively', E_USER_WARNING );
		return false;
	}

	// Normalize and sanitize the input path
	$dir = realpath( $dir );

	// Check if the directory exists and is within allowed paths
	if( !$dir || !is_dir( $dir ) ){
		trigger_error( 'Missing directory in fr_delete_dir_recursively', E_USER_WARNING );
		return false;
	}

	$is_allowed = false;
	foreach( $allowed_paths as $allowed_path ){
		if( strpos( $dir, $allowed_path ) === 0 ){
			$is_allowed = true;
			break;
		}
	}

	if( !$is_allowed ){
		trigger_error( 'Directory not allowed in fr_delete_dir_recursively', E_USER_WARNING );
		return false;
	}

	// Prevent deletion of critical directories
	$critical_dirs = [ '/', '', '/home', '/etc', '/var', '/usr', '/bin', '/sbin' ];
	if( in_array( $dir, $critical_dirs ) ){
		trigger_error( 'Critical directory in fr_delete_dir_recursively', E_USER_WARNING );
		return false;
	}

	// Use a safe iterator to avoid potential issues with symlinks
	$iterator = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator( $dir, RecursiveDirectoryIterator::SKIP_DOTS ),
		RecursiveIteratorIterator::CHILD_FIRST
	);

	foreach( $iterator as $file ){

		if( $file->isDir() ){
			if( !rmdir( $file->getRealPath() ) ){
				return false;
			}
		} else {
			if( !unlink( $file->getRealPath() ) ){
				return false;
			}
		}

	}

	return rmdir( $dir );

}



function fr_generate_next_nested_directory_path( $dir, $depth = 3, $lvl = 0 ){

	$dirs = [];

	if( file_exists( $dir ) ){
		$files = scandir( $dir );

		foreach( $files as $file ){

			if( $file == '.' || $file == '..' ){
				continue;
			}

			if( !is_dir( $dir . $file ) ){
				continue;
			}

			$dirs[$file] = $dir . $file . '/';

		}
	}


	if( !$dirs ){
		$dirs[] = $dir . '0/';
	}

	ksort( $dirs, SORT_NUMERIC );
	$last = end( $dirs );


	if( file_exists( $last ) ){
		$files = scandir( $last );
		$file_nr = count( $files ) - 2;
		if( $file_nr >= 1000 ){
			$dirs[] = $dir . count( $dirs ) . '/';
			$last = end( $dirs );
		}
	}

	if( $lvl < $depth ){
		$last = fr_generate_next_nested_directory_path( $last, $depth, $lvl + 1 );
	}

	return $last;

}