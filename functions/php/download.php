<?php 
if( !defined( 'DOWNLOAD_PASS' ) ){
	if( defined( 'AUTH_KEY' ) ){
		define( "DOWNLOAD_PASS", AUTH_KEY );
	} else {
		define( "DOWNLOAD_PASS", "sdg326g27" );
	}
}


function fr_force_download($file, $exit = 1, $name = false) {
    ini_set('memory_limit', '10000M');
    set_time_limit(6000);
    if ($file && file_exists($file)) {
        if ($name === false) {
            $name = strtolower(basename($file));
        }
        // Stop buffering
        while (ob_get_level()) ob_end_clean();
        if (headers_sent($filename, $line)) {
            fr_log_debug('Force download - Headers already sent', [$filename, $line], true);
        }
        $name = str_replace(".zip", ".ZIP", $name); //Android 2.1 fix
        header('Content-Disposition: attachment; filename="' . $name . '"');
        header('Content-Type: application/octet-stream');

        if (IS_WP) {
            $file = str_replace(get_bloginfo("url") . "/", ABSPATH, $file);
        }

        // Read file in chunks
        $handle = fopen($file, 'rb');
        if ($handle !== false) {
            while (!feof($handle)) {
                echo fread($handle, 8192);
                flush();
            }
            fclose($handle);
        }

        if ($exit) {
            fr_exit();
        }
        return 1;
    }
}



function fr_force_download_content( $filename, $content, $exit = true ){
	ini_set('memory_limit','1000M');
	
	// Stop buffering
	while( ob_get_level() ) ob_end_clean();

	header('Content-Disposition: attachment; filename="'.$filename.'"');
	header('Content-Type: application/octet-stream');
	echo $content;
	if( $exit ){
		fr_exit();
	}
}

function fr_stream_audio( $file_url, $content_type = 'audio/mpeg' ){

	ini_set('memory_limit','1000M');
	
	// Stop buffering
	while( ob_get_level() ) ob_end_clean();

	$contents = file_get_contents( $file_url );
	$file_name = 'audio.mp3';

	header('Content-Disposition: inline; filename="' . $file_name . '"');
	header('Content-Type:' . $content_type );
	header('Content-Length: ' . strlen( $contents ) );
	echo $contents;
	
	fr_exit();
}

function fr_multiple_downloads($files){
	ini_set('memory_limit','1000M');
	set_time_limit(6000);

	// Stop buffering
	while( ob_get_level() ) ob_end_clean();

	if( $file_archive = fr_zip_files($files,"all_downloads")){
		header("Content-type: application/zip");
		header("Content-Disposition: attachment; filename=downloads.zip");
		header("Content-length: " . filesize($file_archive));
		header("Pragma: no-cache");
		header("Expires: 0");
		readfile($file_archive);
		unlink($file_archive);
		fr_exit();
	}
}


function fr_download_file( $file_path, $file_name ){

	if( !file_exists( $file_path ) ){
		return false;
	}

	$size = filesize( $file_path );
	$fp = fopen( $file_path, "r" );

	header("Content-Type: application/octet-stream");
	header("Content-Length: " . $size);
	header("Content-Disposition: attachment; filename=" . $file_name);

	fpassthru( $fp );
	fclose( $fp );
	exit;

}


function fr_download_content( $file_content, $file_name ) {

	$size = strlen( $file_content );

	header( 'Content-Type: application/octet-stream' );
	header( 'Content-Length: ' . $size );
	header( 'Content-Disposition: attachment; filename="' . rawurlencode( $file_name ) . '"' );

	echo $file_content;

	exit;
	
}


