<?php 

/**
 * Automatically inclused all php files found in a directory
 */

function fr_include_files($dir, $exclude_files = [] ){
	$dir = fr_dir_change_separator( $dir );
	if( file_exists( $dir ) ){

		$files = fr_scan_dir( $dir, [], [ 'php' ] );

		$include_at_end = [];
		
		foreach( $files as $file ){

			if( in_array( $file['file'], $exclude_files ) ){
				continue;
			}

			if( $file['file'] == 'general.php' ){
				$include_at_end[] = $file;
				continue;
			}

			function_exists( 'do_action' ) ? do_action( 'fr_include_files_before', $file["path"] ) : false;
			include_once( $file["path"] );
			function_exists( 'do_action' ) ? do_action( 'fr_include_files_after', $file["path"] ) : false;
		}

		foreach( $include_at_end as $file ){

			function_exists( 'do_action' ) ? do_action( 'fr_include_files_before', $file["path"] ) : false;
			include_once( $file["path"] );
			function_exists( 'do_action' ) ? do_action( 'fr_include_files_after', $file["path"] ) : false;
			
		}
	}
}


/**
 * Recursivly scan a folder to retrive all files
 */

function fr_scan_dir( $dir, $exclude = [], $ext = [], $max_lvl = 0, $inc_dirs = 0, $max_files = 0, $lvl = 0 ) {
	
	if( !file_exists( $dir ) || ( $max_lvl && $max_lvl <= $lvl ) ) {
		return [];
	}

	if( $lvl == 0 ) {
		$dir = fr_dir_change_separator( $dir );
		$dir = rtrim( $dir, '/' );
		$inc_dirs = (int) $inc_dirs;
	}

	if( in_array( $dir, $exclude ) ) {
		return [];
	}

	if( in_array( $dir . DIRECTORY_SEPARATOR, $exclude ) ) {
		return [];
	}

	foreach( $exclude as $exclude_dir ) {
		if( substr( $exclude_dir, -1, 1 ) == '*' && substr( $exclude_dir, 0, -1 ) == substr( $dir, 0, strlen( $exclude_dir ) - 1 ) ) {
			return [];
		}
	}

	$r = scandir( $dir );

	$a = [];

	if( $inc_dirs < 2 ) {
		foreach( $r as $k => $v ) {

			if( $v == '.' || $v == '..' ) {
				continue;
			}

			if( in_array( $v, $exclude ) ) {
				continue;
			}

			$file = fr_dir_change_separator( $dir . DIRECTORY_SEPARATOR . $v );

			if( in_array( $file, $exclude ) ) {
				continue;
			}

			if( is_dir( $file ) ) {

				if( in_array( $v, $exclude ) ) {
					continue;
				}

				$a = array_merge( $a, fr_scan_dir( $file, $exclude, $ext, $max_lvl, 0, $max_files, $lvl + 1 ) );

			} else {

				$e = explode( '.', $v );
				$e = strtolower( end( $e ) );

				if( $ext && !in_array( $e, $ext ) ) {
					continue;
				}

				if( in_array( '.' . $e, $exclude ) ) {
					continue;
				}

				$a[] = [
					'file' => $v,
					'path' => $file,
					'dir' => $dir
				];
			}

			if( $max_files && count( $a ) >= $max_files ) {
				break;
			}

		}
	}

	if( $inc_dirs ) {

		$a2 = [];

		foreach( $r as $k => $v ) {

			if( $v == '.' || $v == '..' ) {
				continue;
			}

			if( in_array( $v, $exclude ) ) {
				continue;
			}

			$file = fr_dir_change_separator( $dir . DIRECTORY_SEPARATOR . $v );

			if( in_array( $file, $exclude ) ) {
				continue;
			}

			if( is_dir( $file ) ) {

				if( in_array( $v, $exclude ) ) {
					continue;
				}

				$a[] = [
					'file' => $v,
					'path' => $file,
					'dir' => $file
				];

				$a = array_merge( $a, fr_scan_dir( $file, $exclude, $ext, $max_lvl, 2, $max_files, $lvl + 1 ) );
			}

			if( $max_files && count( $a ) >= $max_files ) {
				break;
			}
		}

		$a2 = array_reverse( $a2 );
		$a = array_merge( $a2, $a );
	}

	return $a;
}



function fr_show_full_nr( $nr ){
	return rtrim( number_format( $nr, 10, '.', '' ), '0' );
}


//Functions useful for php speed optimization

function fr_start_timer( $key = '', $add = 0 ){

    if( !isset( $_ENV['start_timer'][$key] ) ){
        $_ENV['start_timer'][$key] = ['start' => 0, 'total' => 0];
    }

    $_ENV['start_timer'][$key]['start'] = fr_micro_time();
    
    if( ! $add ){
        $_ENV['start_timer'][$key]['total'] = 0;
    }

}

function fr_show_timer( $key = '', $console = false ) {

    fr_p( 'This page was created in ' . fr_get_timer( $key ) . ' seconds.', $key, 0, $console, true, 1 );

}

function fr_get_timer( $key = '', $decimals = 0 ) {

    if( !isset( $_ENV['start_timer'][ $key ]['total'] ) ){
        return 0;
    }

    $end_time = fr_micro_time();
    $_ENV['start_timer'][ $key ]['total'] += $end_time - $_ENV['start_timer'][ $key ]['start'];
    $_ENV['start_timer'][ $key ]['start'] = $end_time;

    if( $decimals ){
        return round( $_ENV['start_timer'][ $key ]['total'], $decimals );
    }

    return fr_show_full_nr( $_ENV['start_timer'][ $key ]['total'] );

}

function fr_micro_time(){
	$mtime = microtime();
	$mtime = explode( ' ', $mtime );
	$mtime = $mtime[1] + $mtime[0];
	return $mtime;
}

function fr_start_memory_usage( $k = '' ){
	$_ENV['fr_start_memory'][$k] = memory_get_peak_usage( true );
}

function fr_show_memory_usage( $k = '' ){
	$endtime = memory_get_peak_usage( true );
	$totaltime = ( $endtime - $_ENV['fr_start_memory'][$k] );
	fr_p( 'Used ' . fr_bytes_to_mb( $totaltime ) . ' MB.' );
}

function fr_get_memory_usage( $k = '' ){
	$endtime = memory_get_peak_usage( true );
	return ( $endtime - $_ENV['fr_start_memory'][$k] );
}

function fr_set_memory_limit( $limit ){
	ini_set( 'memory_limit', $limit );
}



function fr_get_server_memory_info(): array {

	$data = explode("\n", file_get_contents('/proc/meminfo'));
	$memory_info = [];
	foreach ($data as $line) {
		$parts = explode(":", $line);
		if (count($parts) == 2) {
			list($key, $val) = $parts;

			if( str_contains( $val, 'kB' ) ){
				$val = str_replace( 'kB', '', $val );
				$val = (int)$val * 1024;
			} else {
				$val = (int)$val;
			}

			$memory_info[$key] = $val;
		}
	}

	return $memory_info;

}



function fr_get_server_memory_usage() {

    $info = fr_get_server_memory_info();

    $total_memory = $info['MemTotal'];
    $free_memory = $info['MemFree'] + $info['Buffers'] + $info['Cached'];
    $used_memory = $total_memory - $free_memory;

    return [ 'total' => $total_memory, 'used' => $used_memory, 'percent' => round( $used_memory / $total_memory * 100, 2 ) ];

}



/*
* Remove empty items from array
* @param array $array
*/

function fr_array_remove_empty( $array, $recursive = false ){
	foreach( $array as $k => $v ){
		if( empty( $v ) ){
			unset( $array[$k] );
		} else {
			if( $recursive && is_array( $v ) ){
				$array[$k] = fr_array_remove_empty( $v, $recursive );
			}
		}
	}
	return $array;
}



function fr_str_to_array( $s, $separator = ',' ){
	$a = explode( $separator, $s );
	$a = array_map( 'trim', $a );
	return $a;
}


function fr_multiline_to_array( $s ){

	$s = explode( PHP_EOL, $s );
	$s = array_map( 'trim', $s );
	$s = array_filter( $s );

	return $s;

}



function fr_array_map_keys( $callback, $a ){
	$a = array_combine( array_map( $callback, array_keys( $a ) ), $a );
	return $a;
}

/**
 * Allows you to map multiple functions
 */
function fr_array_map( $functions, $a ){
	$functions = explode( ' ', $functions );
	foreach( $functions as $function ){
		$a = array_map( $function, $a );
	}
	return $a;
}


function fr_array_map_recursive( $callback, $a, $keys = [] ){

	if( is_array( $a ) ){

		foreach( $a as $k => $v ){
			$a[$k] = fr_array_map_recursive( $callback, $v, array_merge( [ $k ], $keys ) );
		}

	} else {
		
		$a = $callback( $a, $keys );

	}

	return $a;
}

function fr_array_return_recursive( $a, $callback, $keys = [], $res = [] ){

	if( is_array( $a ) ){

		foreach( $a as $k => $v ){
			$res = fr_array_return_recursive( $v, $callback, array_merge( [ $k ], $keys ), $res );
		}

	} else {
		
		$res = $callback( $a, $keys, $res );

	}

	return $res;

}


function fr_array_default_keys( $a, $default ){
	foreach( $default as $v ){
		if( !isset( $a[$v] ) ){
			$a[$v] = '';
		}
	}
	return $a;
}


function fr_array_key_numeric( $a ){
	return is_array( $a ) && is_numeric( key( $a ) );
}


function fr_validate_time_format($at){
	return date("H:i:s",strtotime(date("Y-m-d")." ".$at));
}


/**
 * Returns the current url
 * @param  integer $no_get
 */
function fr_get_current_url( $no_get = 0, $ajax_referer = false ){

	if( !isset( $_SERVER['HTTP_HOST'] ) ){
		return false;
	}

    $r = fr_get_protocol() . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    if( $ajax_referer && function_exists( 'fr_is_ajax' ) && fr_is_ajax() && isset( $_SERVER['HTTP_REFERER'] ) ){
    	$r = $_SERVER['HTTP_REFERER'];
    }

    if( $no_get ){
        $r = explode( "?" ,$r );
        $r = reset( $r );
        $r = explode( "#" ,$r );
        $r = reset( $r );
    }
    return $r;
}


function fr_get_url_without_domain( $url ){

	$domain = fr_get_domain( $url );
	$url = explode( $domain, $url );
	$url = end( $url );

	return $url;

}


/**
 * Get current protocol
 * @return string
 */
function fr_get_protocol(){
    return (isset($_SERVER['HTTPS']) ? "https" : "http");
}


function fr_build_link($link,$args=array(), $options = array() ){

	if( !is_array( $link ) ){
		$link = fr_get_permalink( $link );
		$parts = parse_url($link);
	} else {
		$parts = $link;
	}

	$parts = array_merge( $parts, $options );

	$link = '';

	// Add scheme
	if( !empty( $parts["scheme"] ) ){
		$link = $parts["scheme"]."://";
	}

	// Add user & pass
	if( !empty( $parts['user'] ) && !empty( $parts['pass'] ) ){
		$link .= $parts['user'] . ':' . $parts['pass'] . '@';
	}

	// Add host
	if( !empty( $parts["host"] ) ){
		$link .= $parts["host"];
	}

	// Add port
	if( !empty( $parts['port'] ) ){
		$link = $link . ':' . $parts["port"];
	}

	// Add path
	if( !empty( $parts["path"] ) ){
		$link .= $parts["path"];
	}

	// Add query params
	if( empty( $parts["query"] ) ){
		$parts["query"] = '';
	}
	parse_str( $parts["query"], $query );
	$query=array_merge( $query, $args );

	$unique_id = 5657382885;

	foreach( $query as $k => $v ){
		if( $v === '' || $v === false ){
			$query[$k] = $unique_id;
		}
	}

	$query=http_build_query( $query, '', '&' );
	$query = str_replace( '=' . $unique_id, '', $query );

	if( $query ){
		$link .= "?" . $query;
	}

	// Add fragment
	if( !empty( $parts["fragment"] ) ){
		$link .= "#" . $parts["fragment"];
	}

	return $link;
}


function fr_url_without_params( $url ){
	$url = explode( '?', $url );
	$url = reset( $url );
	$url = explode( '#', $url );
	$url = reset( $url );
	return $url;
}


//Check if link is external

function fr_is_link_external($l){ 
	return !strstr( $l, $_SERVER['SERVER_NAME'] );
}


//Converts 120 in 2:00

function fr_minutes_to_time($nr){
	$nr=(int)$nr;
	$minutes=floor($nr/60);
	$sec=$nr-$minutes*60;
	if(strlen($sec)<=1){
		$sec="0".$sec;
	}
	return $minutes.":".$sec;
}


//Converts 2:00 in 120

function fr_time_to_minutes($time){
	$time=explode(":",$time);
	$nr=(int)$time[0]*60;
	if(isset($time[1])){
		$nr+=(int)$time[1];
	}
	return $nr;
}


/**
 * Build Query. It keeps existing get variables
 * @param  array  $add    A key=>value pair of paramters to be added
 * @param  array  $remove An array of parameter keys to be removed
 * @return string         Returns a string of parameters
 */
function fr_modify_url_params( $add = [], $remove = [], $url = false ){

	if( $url === false ){
		$url = fr_get_current_url();
	}

	$parts = parse_url( $url );
	if( isset( $parts["query"] ) ){
		parse_str( $parts["query"], $g );
	} else {
		$g = [];
	}

    $g = array_merge( $g, $add );
    $g = array_diff_key( $g, array_flip( $remove ) );

    if( isset( $parts["query"] ) ){
    	unset( $parts["query"] );
    }

    $g = fr_build_link( '', $g, $parts );
    
    return $g;
}



/**
 * Get the parameters from an url
 */
function fr_url_get_params( $url ){
	$parts = parse_url( $url );
	$g = [];
	if( isset( $parts["query"] ) ){
		parse_str( $parts["query"], $g );
	}
	return $g;

}



/**
 * Helper function to get the result from a function only if is callable
 *
 * @param mixed $var
 * @param array $vars Array of variables to be sent to the function
 */
function fr_call_if_func($var, $vars=array(), $clean_buffer = false ){
	if( fr_is_anonymous_func( $var ) ){
		if( $clean_buffer ){
			ob_start();
		}
		if( $vars ){
        	$var = call_user_func_array( $var, $vars );
        } else {
        	$var = call_user_func( $var );
        }
        if( $clean_buffer ){
			$var .= ob_get_clean();
		}
    }
    return $var;
}


function fr_call_if_callable( $var, $vars=array(), $clean_buffer = false ){

	if( is_array( $var ) ){
		return $var;
	}

	if( fr_is_anonymous_func( $var ) || function_exists( $var ) ){
		if( $clean_buffer ){
			ob_start();
		}
		if( $vars ){
        	$var = call_user_func_array( $var, $vars );
        } else {
        	$var = call_user_func( $var );
        }
        if( $clean_buffer ){
			$var .= ob_get_clean();
		}
    }



    return $var;
}


function fr_is_anonymous_func( $var ){
	return !is_string( $var ) && !is_array( $var ) && is_a( $var, 'Closure' ) && is_callable( $var );
}


/**
 * Get domain from url
 * @param  string $url Optional url to parse
 * @return string Domain from  the url
 */
function fr_get_domain( $url="" ){
	if( !$url ){
		$url = fr_get_current_url();
	}
	$info = parse_url( $url );
	if( isset( $info["host"] ) ){
		return $info["host"];
	}
}



function fr_get_url_root( $url="" ){
	if( !$url ){
		$url = fr_get_current_url();
	}
	$info = parse_url( $url );

	$root = '';
	if( !empty( $info['scheme'] ) ){
		$root .= $info['scheme'];
	}
	if( !empty( $info['host'] ) ){
		$root .= '://' . $info['host'];
	}
	if( !empty( $info['port'] ) ){
		$root .= ':' . $info['port'];
	}

	return $root;
}


/**
 * Simple filter system, like the one WP uses
 * @return mixed
 */
function fr_apply_filters(){
	global $fr_add_filter;

	$args = func_get_args();
	$filter = array_shift( $args );
	$var = array_shift( $args );

	if( !empty( $fr_add_filter[$filter] ) ){
		ksort( $fr_add_filter[$filter], SORT_NUMERIC );

		foreach( $fr_add_filter[$filter] as $lvl => $callbacks ){
			foreach( $callbacks as $callback ){
				if( is_callable( $callback ) ){
					$var = call_user_func_array( $callback, array_merge( array( $var ), $args ) );
				}
			}
		}
	}
	return $var;
}

/**
 * Add function to filter
 * @param string $filter Filter name
 * @param callback $callback The function to callback
 * */
function fr_add_filter( $filter, $callback, $lvl = 0 ){
	global $fr_add_filter;
	if( !isset( $fr_add_filter ) ){
		$fr_add_filter = [];
	}

	$lvl = (string)$lvl;
	if( !isset( $fr_add_filter[ $filter ] ) ){
		$fr_add_filter[ $filter ] = [];
	}
	if( !isset( $fr_add_filter[ $filter ][$lvl] ) ){
		$fr_add_filter[ $filter ][$lvl] = [];
	}
	$fr_add_filter[ $filter ][$lvl][] = $callback;
}


function fr_make_acronym( $words ){
	$words = str_replace( array( '-', '_' ), ' ', $words );
	$words = explode( " ", $words );
	$acronym = '';
	foreach ( $words as $w ) {
	  $acronym .= $w[0];
	}
	return strtoupper( $acronym );
}


function fr_shell_exec( $command ){
	if( fr_is_shell_exec_enabled() ){
		return shell_exec( $command );
	}
}

function fr_is_shell_exec_enabled(){
	return is_callable('shell_exec') && false === stripos(ini_get('disable_functions'), 'shell_exec');
}

/**
 * Changes the directory separator in a path based on operating system
 */

function fr_dir_change_separator( $path ){
	return str_replace( '/', DIRECTORY_SEPARATOR, $path );
}

function fr_bytes_to_mb( $nr ){
	return round( $nr / 1048576, 2 );
}

function fr_mb_to_bytes( $nr ){
	return (float)$nr * 1048576;
}



function fr_convert_to_bytes( $size ) {

	$size = trim( $size );
	$last = strtolower( $size[ strlen( $size ) - 1 ] );
	$size = (float) substr( $size, 0, -1 );


	switch( $last ) {
		case 't':
			$size *= 1024;
		case 'g':
			$size *= 1024;
		case 'm':
			$size *= 1024;
		case 'k':
			$size *= 1024;
	}


	return (int) $size;

}


function fr_to_bytes( $nr ) {

	$nr = strtoupper( $nr );

    $suffixes = ['T' => 1099511627776, 'G' => 1073741824, 'M' => 1048576, 'K' => 1024];
    $suffix = substr( $nr, -1 );

    if ( isset( $suffixes[$suffix] ) ) {
        $nr = substr( $nr, 0, -1 ) * $suffixes[$suffix];
    } else {

	    $suffixes = ['TB' => 1099511627776, 'GB' => 1073741824, 'MB' => 1048576, 'KB' => 1024];
	    $suffix = substr( $nr, -2 );

	    if ( isset( $suffixes[$suffix] ) ) {
	        $nr = substr( $nr, 0, -2 ) * $suffixes[$suffix];
	    }

    }

    return $nr;

}


function fr_file_is_locked( $file ){
	$file = fopen( $file, 'w' );
	$lock = !flock( $file, LOCK_EX|LOCK_NB );
	fclose( $file );
	return $lock;
}


/**
 * Make sure that any variable passed will be returned as an array
 * If a string is passed it will create an array and add the string as an item
 * If an array is passed it will return it exactly the same
 * @param  mixed $s The variable to check
 * @return array
 */
function fr_make_array( $s ){
    return is_array( $s ) ? $s : ( $s ? [ $s ] : [] );
}



function fr_object_to_array( $object ){
	return json_decode( json_encode( $object ), true );
}
function fr_array_to_object( $array ){
	return json_decode( json_encode( $array ), false );
}

function fr_array_reduce_dim( $a, $count = 1, $keep_keys = false ){
	for( $x = 0; $x < $count; $x++ ){
		if( !$keep_keys ){
			$a = array_map( 'array_values', $a );
		}
		$new = [];
		foreach( $a as $v ){
			$new = array_merge( $new, $v );
		}
		$a = $new;
	}
	return $a;
}


function fr_array_multiply( $array, $times ){

	$new = [];
	for( $x = 0; $x < $times; $x++ ){
		$new = array_merge( $new, $array );
	}

	return $new;
}


function fr_array_group( $a, $key, $value_key ){
	$new = [];
	foreach( $a as $row ){

		if( !isset( $row[$key] ) || !isset( $row[$value_key] ) ){
			continue;
		}

		$object = $row[$key];

		if( !isset( $new[$object] ) ){
			$new[$object] = [];
		}
		
		$new[$object][] = $row[$value_key];
	}

	return $new;
}


function fr_array_group_on( $a, $key ){

	$new = [];

	foreach( $a as $row ){

		if( !isset( $row[$key] ) ){
			continue;
		}

		$object = $row[$key];
		unset( $row[$key] );

		if( !isset( $new[$object] ) ){
			$new[$object] = [];
		}
		
		$new[$object][] = $row;
	}

	return $new;
}




function fr_array_occurrence_count( $array ) {

	$result = [];

	foreach( $array as $value ) {
		if( !isset( $result[$value] ) ) {
			$result[$value] = 0;
		}

		$result[$value]++;
	}

	return $result;

}





/**
 * Recursive check an array if there are any string to unserialize
 * @param  array $a Multidimension array
 * @return array    The unserialized array
 */
function fr_array_unserialize( $a ){
	foreach( $a as $k => $v ){
		if( is_array( $v ) ){
			$v = fr_array_unserialize( $v );
		} else {
			$v = fr_unserialize( $v );
		}
		$a[$k] = $v;
	}
	return $a;
}

function fr_unserialize( $v ){
	if( $v && !is_array( $v ) && !is_object( $v ) ){
		$new = @unserialize( $v );
		if( $new !== false ){
			$v = $new;
		}
	}
	return $v;
}

function fr_serialize( $v ){
	if( is_array( $v ) || is_object( $v ) ){
		$v = serialize( $v );
	}
	return $v;
}

function fr_array_pick( $array, $key ){
	return array_map( function( $a ) use ( $key ) { if( isset( $a[$key] ) ) return $a[$key]; }, $array );
}

function fr_array_remove_key( $a, $key ){
	if( is_array( $a ) && isset( $a[$key] ) ){
		unset( $a[$key] );
	}
	return $a;
}

function fr_array_relative_insert( $old, $new, $key, $difference = 1 ){
	$index = array_search( $key, array_keys( $old ) ) + $difference;
	$old = array_merge( array_slice( $old, 0, $index ), $new, array_slice( $old, $index ) );
	return $old;
}

function fr_array_sort( $a, $key, $dir = 'asc', $type = SORT_REGULAR ){
	uasort( $a, function( $a, $b ) use ( $key, $dir, $type ){ 
		$a = (array)$a;
		$b = (array)$b;
		if( $type == SORT_REGULAR ){
			if( $dir == 'asc' ){
				return strcmp( $a[$key], $b[$key] );
			} else {
				return strcmp( $b[$key], $a[$key] );
			}
		} else {
			if( $dir == 'asc' ){
				return $a[$key] > $b[$key];
			} else {
				return $a[$key] < $b[$key];
			}
		}
	} );
	return $a;
}


/**
 * Remove one or multiple values from an array
 * @param @a Array
 * @param @items Mixed / Array
 * @return Array
 */

function fr_array_remove_item( $a, $items ){
	if( !is_array( $items ) ){
		$items = [ $items ];
	}

	foreach( $a as $k => $v ){
		foreach( $items as $k2 => $item ){
			if( $v === $item ){
				unset( $a[$k] );
				break;
			}
		}
	}

	return $a;
}

function fr_array_is_last( $a, $key ){
	$keys = array_keys( $a );
	$pos = array_search( $key, $keys );
	return !isset( $keys[$pos+1] );
}


// Function from WP
function fr_is_serialized( $data, $strict = true ) {
	// if it isn't a string, it isn't serialized.
	if ( ! is_string( $data ) ) {
		return false;
	}
	$data = trim( $data );
 	if ( 'N;' == $data ) {
		return true;
	}
	if ( strlen( $data ) < 4 ) {
		return false;
	}
	if ( ':' !== $data[1] ) {
		return false;
	}
	if ( $strict ) {
		$lastc = substr( $data, -1 );
		if ( ';' !== $lastc && '}' !== $lastc ) {
			return false;
		}
	} else {
		$semicolon = strpos( $data, ';' );
		$brace     = strpos( $data, '}' );
		// Either ; or } must exist.
		if ( false === $semicolon && false === $brace )
			return false;
		// But neither must be in the first X characters.
		if ( false !== $semicolon && $semicolon < 3 )
			return false;
		if ( false !== $brace && $brace < 4 )
			return false;
	}
	$token = $data[0];
	switch ( $token ) {
		case 's' :
			if ( $strict ) {
				if ( '"' !== substr( $data, -2, 1 ) ) {
					return false;
				}
			} elseif ( false === strpos( $data, '"' ) ) {
				return false;
			}
			// or else fall through
		case 'a' :
		case 'O' :
			return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
		case 'b' :
		case 'i' :
		case 'd' :
			$end = $strict ? '$' : '';
			return (bool) preg_match( "/^{$token}:[0-9.E-]+;$end/", $data );
	}
	return false;
}


function fr_replace_recursive( $old, $new, $a ){
	if( is_object( $a ) ){
		foreach( $a as $k => $v ){
			$a->$k = fr_replace_recursive( $old, $new, $v );
		}
	}else if( gettype( $a ) == 'object' ){
	}else if( is_array( $a ) ){
		foreach( $a as $k => $v ){
			$a[$k] = fr_replace_recursive( $old, $new, $v );
		}
	}else{
		$a = str_replace( $old, $new, $a );
	}
	return $a;
}


function fr_replace_in_serialized_string( $string, $old, $new ){

	$string = explode( $old, $string );
	$diff = strlen( $new ) - strlen( $old );
	foreach( $string as $k => $v ){
		if( $k ){
			$last_part = $string[$k - 1];
			$last_part = explode( ':', $last_part );
			if( !isset( $last_part[count( $last_part )-2] ) ){
				echo htmlentities( print_r($last_part,1) );
				echo htmlentities( print_r($string,1) );
				fr_exit();
			}
			$len = $last_part[count( $last_part )-2];
			$last_part[count( $last_part )-2] = $len + $diff;
			$last_part = implode( ':', $last_part );
			$string[$k - 1] = $last_part;
		}
	}

	$string = implode( $new, $string );

	return $string;
}


function fr_increment( $number, $chars, $len_chars = '' ){
	if( $len_chars === '' ){
		$len_chars = strlen( $chars );
	}
	if( $number === '' ){
		return substr( $chars, 0, 1 );
	}

	$char = substr( $number, -1, 1 );
	$index = strpos( $chars, $char );

	if( $len_chars > $index + 1 ){
		$number = substr( $number, 0, -1 ) . substr( $chars, $index + 1, 1 );
	}else{
		$left = substr( $number, 0, -1 );
		if( $left === '' ){
			$char = substr( $chars, 0, 1 );
		}else{
			$char = fr_increment( $left, $chars, $len_chars );
		}
		$number =  $char . substr( $chars, 0, 1 );
	}
	return $number;
}




function fr_combinator( $current, $rules ){

	if( !$current ){
		$current = array_map( 'key', $rules );
		return $current;
	}

	$keys = array_keys( $current );
	$end_key = end( $keys );
	$index = end( $current );
	$index++;

	if( isset( $rules[$end_key][$index] ) ){
		$current[$end_key] = $index;
		return $current;
	}

	$number_part = array_slice( $current, 0, -1 );

	if( !$number_part ){
		return false;
	}

	$new = fr_combinator( $number_part, $rules );

	if( $new === false ){
		return false;
	}

	$current = array_merge( $new, [ $end_key => 0 ] );
	return $current;
}




function fr_string_replace_middle( $string, $replace, $pos, $len ){
	return substr( $string, 0, $pos ) . $replace . substr( $string, $pos + $len );
}

function fr_array_merge_recursive( $a, $b, $callback = false, $lvl = 0 ){

	$e = [];
	foreach( $b as $k => $v ){
		if( is_int( $k ) && isset( $a[$k] ) ){
			$e[$k] = $v;
			unset( $b[$k] );
		}
	}
	
	$c = array_merge( array_keys( $a ), array_keys( $b ) );

	$d= [];
	foreach( $c as $v ){
		if( isset( $a[$v] ) && !isset( $b[$v] ) ){
			$d[$v]=$a[$v];
		}else if( !isset( $a[$v] ) && isset( $b[$v] ) ){
			$d[$v]=$b[$v];
		}else {
			if( is_array( $a[$v] ) ){
				if( !is_array( $b[$v] ) ){
					$d[$v] = $b[$v];
				} else {
					$d[$v] = fr_array_merge_recursive( $a[$v], $b[$v], $callback, $lvl + 1 );
				}
			}else{
				if( $callback ){
					$d[$v] = call_user_func_array( $callback, array( $a[$v], $b[$v] ) );
				} else {
					$d[$v] = $b[$v];
				}
			}
		}
	}



	$is_sequential = isset( $e[0] );

	foreach( $e as $k => $v ){
		if( $is_sequential ){
			$d[] = $v;
		} else {
			$d[$k] = $v;
		}
	}

	return $d;
}




function fr_array_merge_recursive_key( $a, $b, $lvl = 0 ){

	if( isset( $a[0] ) ){
		return $b;
	}

	if( !is_array( $b ) ){
		return $a;
	}

	$d = $a;

	foreach( $b as $k => $v ){

		if( isset( $d[$k] ) && is_array( $d[$k] ) && is_array( $v ) ){
			$d[$k] = fr_array_merge_recursive_key( $d[$k], $v, $lvl + 1 );
		} else {
			$d[$k] = $v;
		}

	}

	return $d;

}




function fr_css_nr( $nr, $default = 'px' ){
	$metrics = array(
		'px',
		'em',
		'vw',
		'%'
	);
	if( (int)$nr ){
		$found = false;
		foreach( $metrics as $metric ){
			if( strstr( $nr, $metric ) ){
				$found = true;
				break;
			}
		}
		if( !$found ){
			$nr .= $default;
		}
	}
	return $nr;
}


function fr_args_from_link( $url, $key = ''){
	$url = explode( '?', $url );
	parse_str( end( $url ), $args );

	if( $key && !empty( $args[$key] ) ){
		return $args[$key];
	}
	return $args;
}


function fr_is_divisible( $x, $y ){
	return $x && $x%$y==0;
}

function fr_seconds_to_time( $seconds, $return_array = false ){
	$seconds = (float)$seconds;
	$hours = floor( $seconds / 3600 );
	$seconds -= $hours * 3600;
	$minutes = floor( $seconds / 60 );
	$seconds -= $minutes * 60;
	$show = array();
	if( $hours ){
		$show['hours'] = $hours;
	}
	$show['minutes'] = str_pad( $minutes, 2, '0', STR_PAD_LEFT );
	$show['seconds'] = str_pad( $seconds, 2, '0', STR_PAD_LEFT );

	return $return_array ? $show : implode( ':', $show );
}


// Has test from parent
function fr_date_difference( $date1, $date2 = null ){

	$date1 = new DateTime( $date1 );
	$date2 = $date2 ? new DateTime( $date2 ) : new DateTime();
	$interval = $date2->diff( $date1 );

	return [
		'days' => $interval->d,
		'hours' => $interval->h,
		'minutes' => $interval->i,
		'seconds' => $interval->s
	];

}


// Has test
function fr_time_ago( $date1, $date2 = null, $strings = [], $use_short_names = false ) {

    $default_strings = [
        'ago' => '%s ago',
        'now' => 'just now',
        'yesterday' => 'yesterday',
        'second' => ['s' => 'sec', 'l' => ['singular' => 'second', 'plural' => 'seconds']],
        'minute' => ['s' => 'min', 'l' => ['singular' => 'minute', 'plural' => 'minutes']],
        'hour' => ['s' => 'h', 'l' => ['singular' => 'hour', 'plural' => 'hours']],
        'day' => ['s' => 'd', 'l' => ['singular' => 'day', 'plural' => 'days']]
    ];

    // Use array_merge instead of array_merge_recursive
    $strings = array_merge( $default_strings, $strings );
    $diff = fr_date_difference( $date1, $date2 );

    if ($diff['days'] == 1) {
        return $strings['yesterday'];
    } elseif( $diff['days'] > 0 ) {
        $unit = 'day';
        $value = $diff['days'];
    } elseif( $diff['hours'] > 0 ) {
        $unit = 'hour';
        $value = $diff['hours'];
    } elseif( $diff['minutes'] > 0 ) {
        $unit = 'minute';
        $value = $diff['minutes'];
    } elseif( $diff['seconds'] > 0 ) {
        $unit = 'second';
        $value = $diff['seconds'];
    } else {
        return $strings['now'];
    }

    $unit_string = $use_short_names ? $strings[$unit]['s'] : ( $value === 1 ? $strings[$unit]['l']['singular'] : $strings[$unit]['l']['plural'] );
    $time_string = $value . ( $use_short_names ? '' : ' ' ) . $unit_string;

    return sprintf( $strings['ago'], $time_string );
}



function fr_is_email_valid( $email ){
	return filter_var( $email, FILTER_VALIDATE_EMAIL );
}

function fr_is_email_domain_registered( $email ){
	if( !fr_is_email_valid( $email ) ){
		return;
	}
	$domain = explode( '@', $email );
	if( count( $domain ) == 2 ){
		$domain = end( $domain );
		if( $domain ){
			if ( checkdnsrr( idn_to_ascii( $domain . '.', 0, INTL_IDNA_VARIANT_UTS46 ), 'MX' ) ) {
				return 1;
			}
		}
	}
}

function fr_array_chunk( $terms, $column_count ){
	$chunk_size = ceil( count( $terms ) / $column_count );
	return array_chunk( $terms, $chunk_size );
}


// Returns the string after the match
function fr_strstra( $s, $k ){
	return substr( $s, strpos( $s, $k ) + strlen( $k ) );
}

// Returns the string before the match or the original string
function fr_strstrb( $s, $k ){

	if( strpos( $s, $k ) === false ){
		return $s;
	}

	return strstr( $s, $k, true );
}


function fr_array_key_search( $k, $a ){
	$keys = array_keys( $a );
	return array_search( $k, $keys );
}

/** 
 * Searches a string within a string from the end first
*/
function fr_strstr_from_end( $s, $needle ){
	$s_rev = strrev( $s );
	$needle_rev = strrev( $needle );
	$before = strstr( $s_rev, $needle_rev, 1 );
	if( $before === false ){
		return false;
	}
	return substr( $s, - strlen( $before ) );
}



/**
 * Function to include an array item relative to another
 * @param  array  $array
 * @param  string  $key              Target key
 * @param  array  $array_to_include
 * @param  integer $pos              Offset position relative to target key. It can be in front(-1) or after (1)
 * @return array
 */
function fr_instert_relative_to_key( $array, $key, $array_to_include, $pos = 1 ){
	$key_pos = array_search( $key, array_keys( $array ) ) + $pos;
	$array = array_merge( array_slice( $array, 0, $key_pos, true ) , $array_to_include, array_slice( $array, $key_pos, count( $array ), true ) );
	return $array;
}



function fr_auto_index( $key ){
	fr_global( 'fr_auto_index_' . $key, 1, 1 );
	return fr_global( 'fr_auto_index_' . $key );
}


/**
 * Manages global variables
 */

function fr_global( $key, $val = NULL, $increment = false, $merge = false ){
	global $fr_global;
	if( empty( $fr_global ) ){
		$fr_global = [];
	}
	if( $val === NULL ){
		if( isset( $fr_global[$key] ) ){
			return $fr_global[$key];
		} else {
			return false;
		}
	}else{
		if( $increment ){
			if( $increment === ARRAY_A ){
				if( !isset( $fr_global[$key] ) ){
					$fr_global[$key] = [];
				}
				if( is_array( $val ) && $merge ){
					$fr_global[$key] = array_merge( $fr_global[$key], $val );
				} else {
					$fr_global[$key][] = $val;
				}
				return true;
			} else {
				if( !isset( $fr_global[$key] ) ){
					$fr_global[$key] = 0;
				}
				return $fr_global[$key] += $val;
			}
		}else{
			$fr_global[$key] = $val;
			return true;
		}
	}
}



/**
 * Group multidimensional array by specific values
 * @param  array $a   The multidimensional array
 * @param  array  $ks The keys to group the data on
 */
function fr_array_unique_on_values( $a, $ks = [], $sep = '|' ){
	if( !$a ){
		return $a;
	}
	$ks = fr_make_array( $ks );
	if( !$ks ){
		$ks = array_keys( fr_array_flaten( $a, true ) );
	}
	$ks = array_flip( $ks );
	$new = array();
	foreach( $a as $k => $v ){
		$keys = array_intersect_key( $v, $ks );
		$keys = implode( $sep, $keys );
		$new[$keys] = $v;
	}
	return $new;
}

function fr_array_is_multidim( $a ){
	if( !is_array( $a ) ){
		return false;
	}
	$a = reset( $a );
	if( !is_array( $a ) ){
		return false;
	}
	return true;
}

function fr_make_multidim_array( $a ){
	if( !$a ){
		return [];
	}

	if( !is_array( $a ) ){
		$a = [
			[
				$a
			]
		];
	}

	if( !fr_array_is_multidim( $a ) ){
		$a = [ $a ];
	}
	return $a;
}

function fr_date( $date, $format = 'Y-m-d H:i:s' ){
	$date = round( $date );
	if( !is_numeric( $date ) ){
		$date = strtotime( $date );
	}
	return date( $format, $date );
}


function fr_date_array( $dates, $format = 'Y-m-d H:i:s' ){

	$new = [];

	foreach( $dates as $k => $date ){

		if( is_int( $k ) && strlen( $k ) >= 10 ){
			$k = fr_date( $k, $format );
		}

		if( is_array( $date ) ){
			$new[$k] = fr_date_array( $date, $format );
		} else if( (int)$date && strlen( (int)$date ) >= 10 ){
			$new[$k] = '[' . fr_date( $date, $format ) . ']';
		} else {
			$new[$k] = $date;
		}

	}

	return $new;

}


function fr_date_add_gmt_offset( $date, $offset, $format = false ){
	$format = $format ? $format : DB_DATE_TIME_FORMAT;
	$date = strtotime( $date );
	return date( $format, strtotime( ( $offset >= 0 ? '+' . $offset : $offset ) . ' hours', $date ) );
}


function fr_maybe_create_dir( $dir, $permissions = 0755 ){
	if( !file_exists( $dir ) ){
		mkdir( $dir, $permissions, 1 );
	}
	return $dir;
}

function fr_str_starts_with( $s, $k ){
	return substr( $s, 0, strlen( $k ) ) === $k;
}

function fr_str_ends_with( $s, $k ){
	return substr( $s, -strlen( $k ), strlen( $k ) ) === $k;
}

function fr_replace_loop( $old, $new, $s ){

	while( 1 ){
		$new_s = str_replace( $old, $new, $s );

		if( $new_s === $s ){
			break;
		}

		$s = $new_s;
	}

	return $s;
}


function fr_str_replace_once( $find, $replace, $s ){
	$pos = strpos( $s, (string)$find );
	if ( $pos !== false ) {
	    $s = substr_replace( $s, $replace, $pos, strlen( $find ) );
	}
	return $s;
}

function fr_str_maybe_remove( $s, $to_remove, $location = 'anywhere' ){

	if( $location == 'anywhere' ){
		$s = fr_str_replace_once( $to_remove, '', $s );
	} else if( $location == 'start' ){
		if( substr( $s, 0, strlen( $to_remove ) ) == $to_remove ){
			$s = substr( $s , strlen( $to_remove ) );
		}
	}else if( $location == 'end' ) {
		if( substr( $s, -strlen( $to_remove ), strlen( $to_remove ) ) == $to_remove ){
			$s = substr( $s , 0, -strlen( $to_remove ) );
		}
	}
	return $s;
}


function fr_average( $a ){
	if( !$a ){
		return 0;
	}
	return array_sum( $a ) / count( $a );
}

function fr_ltrim( $s, $s2 ){
	$sl = strlen( $s2 );
	if( substr( $s, 0, $sl ) == $s2 ){
		$s = substr( $s, $sl );
	}
	return $s;
}

function fr_rtrim( $s, $s2 ){
	$sl = strlen( $s2 );
	if( substr( $s, -$sl, $sl ) == $s2 ){
		$s = substr( $s,  0, -$sl );
	}
	return $s;
}

function fr_trim( $s, $s2 ){
	$s = fr_ltrim( $s, $s2 );
	$s = fr_rtrim( $s, $s2 );
	return $s;
}


function fr_number_ordinal( $nr, $locale = 'en_US' ){
	$nf = new NumberFormatter( $locale, NumberFormatter::ORDINAL );
	return $nf->format( $nr );
}

function fr_bites_to_mb( $nr ){
	return round( $nr / 1048576, 2 );
}

function fr_string_to_ord( $s ){
	$s = str_split( $s, 1 );
	foreach( $s as $k => $v ){
		$s[$k] = [$v, ord( $v )];
	}
	return $s;
}

function fr_is_localhost(){
	return ( defined( 'IS_LOCALHOST' ) && IS_LOCALHOST ) || fr_get_domain() == "localhost";
}

function fr_is_development(){
	return ( defined( 'IS_DEVELOPMENT' ) && IS_DEVELOPMENT ) || ( defined( 'IS_DEV' ) && IS_DEV ) || fr_is_localhost();
}

function fr_replace_vars( $s, $vars ){
	return fr_do_shortcodes_array( $s, $vars, '%', '%' );
}

function fr_unset( &$a, $k ){
	if( isset( $a[$k] ) ){
		unset( $a[$k] );
	}
	return $a;
}

function fr_json_decode( $s ){
	return json_decode( stripslashes( $s ), true );
}

function fr_json_exit( $s ){
	echo json_encode( $s );
	exit();
}

function fr_json_script( $data, $id = '' ){
	$attr = [];
	$attr['type'] = 'application/json"';
	if( $id ){
		$attr['id'] = $id;
	}

	ob_start();
	?>
	<script <?php echo fr_array_to_tag_attr( $attr ) ?>>
		<?php echo json_encode( $data ) ?>
	</script>
	<?php 
	return ob_get_clean();
}




function fr_session( $k, $val = null ){
	if( $val !== null ){
		if( !isset( $_SESSION['fr'] ) ){
			$_SESSION['fr'] = [];
		}
		$_SESSION['fr'][$k] = $val;
	} else {

		if( isset( $_SESSION['fr'][$k] ) ){
			return $_SESSION['fr'][$k];
		}
	}
}


/**
 * Using this function will read and write the data to the session file immediately 
 */

function fr_session_old( $k, $val = null ){

	global $fr_session_handler;

	if( !$fr_session_handler ){
		return;
	}

	if( $val !== null ){
		if ( session_status() == PHP_SESSION_NONE ) {
		    session_start();
		}

		// Reload the data from the session file
		$fr_session_handler->reload();

		if( !isset( $_SESSION['fr'] ) ){
			$_SESSION['fr'] = [];
		}
		$_SESSION['fr'][$k] = $val;
		
		// Save the data to the session file
		$fr_session_handler->save();
	} else {

		// Reload the data from the session file
		$fr_session_handler->reload();

		if( isset( $_SESSION['fr'][$k] ) ){
			return $_SESSION['fr'][$k];
		}
	}
}

function fr_session_clear( $k ){
	if( isset( $_SESSION['fr'][$k] ) ){
		unset( $_SESSION['fr'][$k] );
	}
}


function fr_session_handler(){

	if ( session_status() != PHP_SESSION_NONE || headers_sent() ) {
	    return;
	}

	global $fr_session_handler;
	$fr_session_handler = new fr_session_handler();

	session_set_save_handler(
	    array($fr_session_handler, 'open'),
	    array($fr_session_handler, 'close'),
	    array($fr_session_handler, 'read'),
	    array($fr_session_handler, 'end_of_execution_write'),
	    array($fr_session_handler, 'destroy'),
	    array($fr_session_handler, 'gc')
	);

	// the following prevents unexpected effects when using objects as save handlers
	register_shutdown_function('session_write_close');

}



class fr_session_handler {
    private $savePath;

    function open($savePath, $sessionName){
        $this->savePath = $savePath;
        if (!is_dir($this->savePath)) {
            mkdir($this->savePath, 0777);
        }
        return true;
    }

    function close(){
        return true;
    }

    function read($id){
    	if( !file_exists( "$this->savePath/sess_$id" ) ){
    		$this->write( $id, '' );
        }
        return (string)@file_get_contents("$this->savePath/sess_$id");
    }

    // On execution end write the data but update "fr" from the session file before that
    function end_of_execution_write($id, $data){
        $session = $_SESSION;
        $this->reload( $id );
        if( isset( $_SESSION['fr'] ) ){
        	$session['fr'] = $_SESSION['fr'];
        } else {
        	$session['fr'] = [];
        }
        $_SESSION = $session;
        $this->save();
        return true;
    }

    function write($id, $data){
        return file_put_contents("$this->savePath/sess_$id", $data ) === false ? false : true;
    }

    function destroy($id){
        $file = "$this->savePath/sess_$id";
        if (file_exists($file)) {
            unlink($file);
        }

        return true;
    }

    function gc($maxlifetime){
        foreach ( glob("$this->savePath/sess_*") as $file) {
            if ( file_exists( $file ) && filemtime( $file ) + $maxlifetime < time() ) {
                unlink($file);
            }
        }

        return true;
    }

    function reload( $id = false ){
    	if( $id === false ){
    		$id = session_id();
    	}
    	session_decode( $this->read( $id ) );
    }

    function save(){
    	$data = session_encode();
    	$id = session_id();
    	if( $id && $this->read( $id ) !== $data ){
    		$this->write( $id, $data );
    	}
    }
}


function fr_redirect($l="",$e="302"){
	$l = fr_get_permalink( $l );
	if(!$l){
		$l=explode("?",fr_get_current_url());
		$l=reset($l);
	}
	$l = str_replace( ' ', '%20', $l );
	if( function_exists( 'wp_redirect' ) ){
		$l = apply_filters( 'fr_redirect', $l, $e );
		wp_redirect($l,$e);
	} else {
		header( 'Location: ' . $l, true, $e );
	}
	fr_exit();
}



function fr_get_permalink( $id ){
	if( ( is_numeric( $id ) || is_object( $id ) ) && function_exists( 'get_permalink' ) ){
		$id = get_permalink( $id );
	}
	
	return $id;
}

/**
 * Redirect to current url but without a paramter
 * @param  array/string $param One parameter key or an array of paramter keys
 */
function fr_redirect_without_param( $remove_param, $add_params = [] ){
    $remove_param = fr_make_array( $remove_param );
    $url = fr_modify_url_params( $add_params, $remove_param );
    fr_redirect( $url, 302 );
}

function fr_clean_post_redirect(){
	fr_redirect(fr_get_current_url());
}

function fr_clean_get_redirect(){
	fr_redirect( fr_get_current_url( 1 ), 302 );
}

/**
 * Array column that keeps the keys
 */
function fr_array_column( $a, $key ){
	$new = [];
	foreach( $a as $k => $v ){
		$new[$k] = $v[$key] ?? '';
	}
	return $new;
}


function fr_object_column( $a, $key, $on_key = false ){
	$new = [];
	foreach( $a as $k => $v ){
		if( $on_key === false ){
			$new[$k] = $v->$key;
		} else {
			$new[$v->$on_key] = $v->$key;
		}
	}
	return $new;
}


function fr_array_flaten( $a, $unique = false ){
	$b = [];
	foreach( $a as $v ){
		if( is_array( $v ) ){
			foreach( $v as $v2 ){
				$b[] = $v2;
			}
		} else {
			$b[] = $v;
		}
	}
	if( $unique ){
		$b = array_unique( $b );
	}
	return $b;
}

/**
 * Array column that concatinates multiple values
 */
function fr_array_columns( $a, $keys, $sep = '|' ){
	$b = [];
	$keys = array_flip( $keys );
	foreach( $a as $k => $v ){
		$key = array_intersect_key( $v, $keys );
		$key = implode( $sep, $key );
		$b[$key] = $k;
	}
	return $b;
}




/**
 * Conditional shortcodes
 * v1.0
 * Does not support nesting or value comparison
 */
function fr_do_conditional_shortcodes_array( $text, $shortcodes, $start = '[', $end = ']' ){

	$text = explode( $start . 'if ', $text );
	$new_text = [];
	foreach( $text as $k => $v ){
		if( !$k ){
			$new_text[] = $v;
			continue;
		}

		$tag = strstr( $v, $end, true );
		$end_tag = $start . '/if ' . $tag . $end;
		$content = strstr( $v, $end_tag, true );
		$content_after_end_tag = substr( strstr( $v, $end_tag ), strlen( $end_tag ) );
	
		if( !empty( $shortcodes[$tag] ) ){
			$new_text[] = fr_extract( $v, $end, $start . '/' ) . $content_after_end_tag;
		} else {
			$new_text[] = $content_after_end_tag;
		}
	}
	$text = implode( '', $new_text );

	return $text;
}




// given a key value array it replaces all shortcodes found in the given text
function fr_do_shortcodes_array($text,$shortcodes,$start="[",$end="]",$wraper_start="",$wraper_end=""){
	foreach( $shortcodes as $shortcode => $value ){
		$text=str_replace($wraper_start.$start.$shortcode.$end.$wraper_end,$wraper_start.$value.$wraper_end,$text);
	}
	return $text;
}


/**
 * Helper function to output the class name 'active' in case two values are equal
 */
function fr_active( $value1, $value2, $echo = true ){
	$return = $value1 == $value2 ? 'active' : '';
	if( $echo ){
		echo $return;
	} else {
		return $return;
	}
}


function fr_exit(){
	$is_exit = fr_global( 'fr_exit' );
	fr_global( 'fr_exit', true );
	if( $is_exit != true ){
		fr_apply_filters( 'fr_exit' );
	}
	exit();
}


// Convert to a real number
function fr_real_number( $nr ){
	$nr = (int)$nr;
	if( $nr < 0 ){
		$nr = 0;
	}
	return $nr;
}



// function used for the sole purpose of manually initiating certain functions
// with the intent of testing them
function fr_testing_ground( $callback, $exit = true ){
	fr_GET_trigger( 'testing_ground', function() use ( $callback, $exit ){

		if( !current_user_can( 'manage_options' ) && !fr_constant( 'IS_DEVELOPMENT' ) ){
			return;
		}

		fr_add_actions( 'admin_init template_redirect', function() use ( $callback, $exit ){

			set_time_limit( 600 );
			fr_call_if_func( $callback );

			if( $exit ){
				fr_exit();
			}
		}, 100 );
	} );
}


function fr_testing( $s = false ){
	if( $s == false ){
		return isset( $_GET['testing_ground'] );
	} else {
		if( isset( $_GET['testing_ground'] ) ){
			fr_p( $s );
		}
	}
}

// Generates a unique identified based on the given value
function fr_uuid( $data ){
	$data = substr( md5( $data, true ), 0, 16 );

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}


function fr_enable_debug( $settings = [] ){
	ini_set( 'display_errors', 1 );
	ini_set( 'display_startup_errors', 1 );
	error_reporting( E_ALL );

	if( isset( $settings['log_file'] ) ){
		ini_set( 'log_errors', 1 );
		ini_set( 'error_log', $settings['log_file'] );
	}
}


function fr_ceil( $nr, $dec = 0 ){
	$t = 1;
	for( $x = 0; $x < $dec; $x++ ){
		$t *= 10;
	}
	$nr = ceil( $nr * $t ) / $t;
	return $nr;
}

function fr_remove_double_http( $s ){
	$protocol = fr_get_protocol();
	$s = str_replace( 'http://http://', $protocol . '://', $s );
	$s = str_replace( 'https://https://', $protocol . '://', $s );
	$s = str_replace( 'https://http://', $protocol . '://', $s );
	$s = str_replace( 'http://https://', $protocol . '://', $s );
	$s = str_replace( 'http://#', '#', $s );
	$s = str_replace( 'https://#', '#', $s );
	return $s;
}

function fr_get_paragraphs( $c, $min_paragraphs = 1, $min_chars = false ){
	$c = str_replace( [ '<p>', '</p>' ], "\n", $c );
	$c = trim( $c );
	$c = explode( "\n\n", $c );
	$c = fr_array_remove_empty( $c );

	$new = [];
	foreach( $c as $k => $v ){
		if( $k >= $min_paragraphs ){
			if( $min_chars === false || $min_chars <= strlen( implode( ' ', $new ) ) ){
				break;
			}
		}
		$new[] = $v;
	}

	$new = implode( "\n\n", $new );

	return $new;
}


function fr_default( $a, $keys ){
	$keys = fr_make_array( $keys );
	$a = array_merge( $keys, $a );
	return $a;
}

function fr_is_text_in_lang( $s, $lang ){
	if( $lang == 'he' ){
		return preg_match( "/\p{Hebrew}/u", $s );
	}
	return false;
}

function fr_implode( $a ){
	return implode( ', ', $a );
}
function fr_explode( $s ){
	return explode( ',', $s );
}

function fr_referrer_policy( $type = 'origin' ){
	if( !headers_sent() ){
		header( 'Referrer-Policy: ' . $type );
	}
}

function fr_date_iso( $date, $offset = '00:00' ){
	if( !is_numeric( $date ) ){
		$date = strtotime( $date );
	}
	return date( 'Y-m-dTH:i:s+' . $offset, $date );
}

/**
 * Returns the time in ISO 8601 format
 * Documentation https://en.wikipedia.org/wiki/ISO_8601#Durations
 * @param  int $time   	   Time in seconds
 * @return string
 */
function fr_time_iso( $time ){
	$time = fr_seconds_to_time( $time, true );
	$s = 'PT';

	if( isset( $time['hours'] ) ){
		$s .= 'H' . $time['hours'];
	}
	if( isset( $time['minutes'] ) ){
		$s .= 'M' . $time['minutes'];
	}
	if( isset( $time['seconds'] ) ){
		$s .= 'S' . $time['seconds'];
	}

	return $s;
}




function fr_utc_to_local_time( $string ){

    $local_date = new DateTime( $string, new DateTimeZone( 'UTC' ) );
    $local_date->setTimezone( new DateTimeZone( wp_timezone_string() ) );

    return $local_date->getTimestamp();

}




function fr_local_time_to_utc( $string, string $timezone ): int {

    if( is_int( $string ) ){
        $string = date( 'Y-m-d H:i:s', $string );
    }

    $local_date = new DateTime( $string, new DateTimeZone( $timezone ) );
    $local_date->setTimezone( new DateTimeZone( 'UTC' ) );

    return $local_date->getTimestamp();

}




function fr_remove_smart_quotes( $s ) {

	$s = str_replace(
	array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"),
	array("'", "'", '"', '"', '-', '--', '...'), $s );
	
	$s = str_replace(
	array( '″', chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(133) ),
	array( '"', "'", "'", '"', '"', '-', '--', '...' ), $s );
	
	return $s;
}



/**
 * It transforms a relative url to an absolute url
 * It replicates how the browser does it
 * Unit tests are available
 * @param  string $relative_url  Ex: ../page
 * @param  string $reference_url Ex: https://site.com/
 * @return string                Ex: https://site.com/page
 */
function fr_relative_url_to_absolute( $relative_url, $reference_url ){

	$parts = parse_url( $relative_url );
	$parts_url = parse_url( $reference_url );

	if( !empty( $parts['scheme'] ) && !in_array( $parts['scheme'], [ 'http', 'https' ] ) ){
		return $relative_url;
	}

	if( isset( $parts_url['fragment'] ) ){
		unset( $parts_url['fragment'] );
	}
	if( !isset( $parts['path'] ) ){
		$parts['path'] = '';
	}
	if( !isset( $parts_url['path'] ) ){
		$parts_url['path'] = '';
	}
	
	if( strlen( $parts['path'] ) && !in_array( substr( $parts['path'], 0, 1 ), [ '?', '#' ] ) ){
		if( isset( $parts_url['query'] ) ){
			unset( $parts_url['query'] );
		}
	}

	$parts_new = $parts;
	if( empty( $parts_new['host'] ) ){
		$parts_new['host'] = $parts_url['host'];
		if( empty( $parts_new['query'] ) && !empty( $parts_url['query'] ) ){
			$parts_new['query'] = $parts_url['query'];
		}
	}
	if( empty( $parts_new['scheme'] ) ){
		$parts_new['scheme'] = $parts_url['scheme'];
	}

	$parts_new['host'] = strtolower( $parts_new['host'] );
	$parts_new['scheme'] = strtolower( $parts_new['scheme'] );


	if( $parts_new['path'] == '.' ){
		$parts_new['path'] = './';
	}
	if( substr( $parts_new['path'], -2, 2 ) == '/.' ){
		$parts_new['path'] .= '/';
	}

	if( $parts_new['path'] == '..' ){
		$parts_new['path'] = '../';
	}
	if( substr( $parts_new['path'], -3, 3 ) == '/..' ){
		$parts_new['path'] .= '/';
	}

	if( !empty( $parts['host'] ) ){
		// If host is set then do nothing
	} else if( !strlen( $parts_new['path'] ) ){
		$parts_new['path'] = $parts_url['path'];
	} else if( substr( $parts_new['path'], 0, 1 ) != '/' ){
		$rel_path = $parts_url['path'];
		if( substr( $rel_path, -1, 1 ) != '/' && $rel_path ){
			$rel_path = explode( '/', $rel_path );
			array_pop( $rel_path );
			$rel_path = implode( '/', $rel_path );
			if( substr( $rel_path, -1, 1 ) != '/' && $rel_path ){
				$rel_path .= '/';
			}
		}
		$parts_url['path'];
		$parts_new['path'] = $rel_path . $parts_new['path'];
	}


	$parts = explode( '/', $parts_new['path'] );

	while( 1 ){
		$parts = array_values( $parts );
		for( $x = 0; $x < count( $parts ); $x++ ){
			if( $parts[$x] == '.' ){
				unset( $parts[$x] );
				continue 2;
			}
			if( $parts[$x] == '..' ){
				unset( $parts[$x] );
				if( isset( $parts[$x-1] ) ){
					unset( $parts[$x-1] );
				}
				continue 2;
			}
		}
		break;
	}
	
	$parts_new['path'] = implode( '/', $parts );

	if( $parts_new['path'] && substr( $parts_new['path'], 0, 1 ) != '/' ){
		$parts_new['path'] = '/' . $parts_new['path'];
	}

	$absolute_url = fr_build_link( '', [], $parts_new );

	return $absolute_url;
}



function fr_css_relative_urls_to_absolute( $css, $reference_url ){

	$links = fr_single_loop_extract( $css, 'url(', ')' );


	foreach( $links as $link ){

		$first_char = fr_closest_string( $link, [ '"', "'" ] );
		if( $first_char !== false ){
			$url = fr_extract( $link, $first_char, $first_char );
		} else {
			$url = $link;
		}
		$url = trim( $url );

		// If its base64 then abort
		if( substr( $url, 0, 5 ) == 'data:' ){
			continue;
		}

		$new_url = fr_relative_url_to_absolute( $url, $reference_url );
		if( $url && $new_url != $url ){
			$new_link = str_replace( $url, $new_url, $link );
			$css = str_replace( $link, $new_link, $css );
			// fr_p( $new_link );
			// fr_p( $css, 1 );
		}
	}

	return $css;
}




/**
 * Finds the closest matching string in another string from a set of strings
 * @param  string $string String in which to search for
 * @param  array $parts  An array of strings to find
 * @return string
 */		
function fr_closest_string( $string, $parts, $offset = 0 ){

	$found = [];
	foreach( $parts as $part ){
		if( strstr( $string, $part ) ){
			$found[$part] = strpos( $string, $part, $offset );
		}
	}

	if( !$found ){
		return false;
	}

	asort( $found, SORT_NUMERIC );

	return key( $found );
}



function fr_maybe_base64_deocode( $s ){
	if( substr( $s, 0, 7 ) == 'base64:' ){
		$s = substr( $s, 7 );
		$s = base64_decode( $s );
	}
	$s = str_replace( "\\", "", $s );
	return $s;
}



function fr_day_name( $day ){
	return date( 'D', strtotime( "Sunday +{$day} days" ) );
}
function fr_month_name( $month ){
	return date('F', mktime( 0, 0, 0, $month, 10 ) );
}

function fr_days_in_month( $month, $year ){
	if( !$year ){
		$year = date( 'Y' );
	}
	return date( "t", strtotime( "{$year}-{$month}-01" ) );
}

function fr_chars( $types ){
	$chars = [];
	$types = explode( ',', $types );
	foreach( $types as $type ){
		if( $type == 'numeric' ){
			$chars = array_merge( $chars, range( 0, 9 ) );
		}
		if( $type == 'alpha' ){
			$chars = array_merge( $chars, range( 'a', 'z' ) );
		}
	}
	return $chars;
}


function fr_url_get_last_part( $url ){
	$parts = parse_url( $url );
	if( !isset( $parts['path'] ) ){
		return;
	}
	$path = $parts['path'];
	$path = trim( $path, '/' );
	$path = explode( '/', $path );
	return end( $path );
}

function fr_pad_number( $nr, $length = 2 ){
	return str_pad( $nr, $length, '0', STR_PAD_LEFT );
}

function fr_url_change_protocol( $url, $protocol ){
	return fr_build_link( $url, [], [ 'scheme' => $protocol ] );
}


function fr_unique_id( $key = '' ){
	$id = fr_global( 'fr_unique_id_' . $key );
	if( !$id ){
		$id = 0;
	}
	$id++;
	fr_global( 'fr_unique_id_' . $key, $id );
	return $id;
}

function fr_unique_id_clear( $key = '' ){
	fr_global( 'fr_unique_id_' . $key, 0 );
}

function fr_array_value_on_key( $a, $key, $value ){
	$new = [];
	foreach( $a as $k => $v ){
		if( isset( $v[$key] ) && isset( $v[$value] ) ){
			if( !isset( $new[$v[$key]] ) ){
				$new[$v[$key]] = [];
			}
			$new[$v[$key]][$k] = $v[$value];
		}
	}
	return $new;
}

function fr_text_single_paragraph( $s ){
	$s = str_replace( [ "\n", "\t", "\r" ], ' ', $s );
	$s = fr_replace_loop( '  ', ' ', $s );
	return $s;
}

function fr_strip_tags( $s, $single_paragraph = false ){
	$s = strip_tags( $s );
	$s = str_replace( '&nbsp;', ' ', $s );

	if( $single_paragraph ){
		$s = fr_text_single_paragraph( $s );
	}

	return $s;
}


/**
 * This is to fix the PHP error: Only variables should be passed by reference 
 * Use this instead of reset()
 */
function fr_reset( $array ){
	if( is_array( $array ) ){
		return reset( $array );
	} else {
		return $array;
	}
}

/**
 * Checks if the string is a number. If it is then it formats it in a standard way
 * @param  string $nr
 * @return string
 */
function fr_format_if_number( $nr ){
	$nr = trim( $nr );
	if( strlen( (float)$nr ) == strlen( $nr )  ){
		$decimals = 0;
		if( strstr( $nr, '.' ) ){
			$decimals = strlen( fr_strstra( $nr, '.' ) );
		}

		$nr = number_format( $nr, $decimals );
	}
	return $nr;
}


function fr_url_dirname( $url ){
	return pathinfo( $url, PATHINFO_DIRNAME );
}

function fr_get_location_of_function( $function ){
	$reflFunc = new ReflectionFunction( $function );
	return $reflFunc->getFileName() . ':' . $reflFunc->getStartLine();
}

function fr_attachment_url( $pid ){
	return wp_get_attachment_url( $pid );
}

function fr_string_to_isbn( $str ){	

	$str = md5( $str );
    $arr = str_split( $str, 4 );
    foreach ( $arr as $grp ) {
        $dec[] = str_pad( hexdec( $grp ), 5, '0', STR_PAD_LEFT);
    }
    $str = implode( '', $dec );
    $str = substr( $str, 0, 16 );
    $str = str_split( $str, 4 );
    $str = implode( '-', $str );
    
    return $str;
}



function fr_name_split( $name ){
	$name = trim( $name );
	$name = explode( ' ', $name );
	$last_name = array_pop( $name );
	$first_name = implode( ' ', $name );
	return [ $first_name, $last_name ];
}



function fr_autop( $s ){
	$s = explode( "\n\n", $s );
	$s = '<p>' . implode( "</p><p>", $s ) . '</p>';
	return $s;
}




function fr_add_js_setting( $k, $v ){
	global $fr_js_setting;
	if( !isset( $fr_js_setting ) ){
		$fr_js_setting = array();
	}
	$fr_js_setting[$k] = $v;
}


function fr_show_js_settings(){
	global $fr_js_setting;
	if( !empty( $fr_js_setting ) ){
		?>
		<script type="text/javascript">
			var fr_settings = <?php echo json_encode( $fr_js_setting ) ?>;
		</script>
		<?php 
	}
}


function fr_array_last( $a ){
	return array_pop( $a );
}


function fr_word_counter( $s ){
    $s = preg_replace('/\s+/', ' ', trim( $s ) );
    $words = explode( " ", $s );
    return count( $words );
}


function fr_line_counter( $s ){
	$s = str_replace( "\r\n", "\n", $s );
	$s = str_replace( "\r", "\n", $s );
	$s = explode( "\n", $s );
	return count( $s );
}

/**
 * Strips backslahes from url paramaters
 */

function fr_rgpost( $data ){
	return is_array( $data ) ? $data : str_replace("\\","",$data);
}

function fr_is_md5( $s ){
	return preg_match( '/^[a-f0-9]{32}$/', $s );
}




function fr_get_value_recursively( $data, $keys, $separator = '.' ){

	if( !is_array( $keys ) ){
		$keys = explode( $separator, $keys );
	}

	$key = array_shift( $keys );
	if( isset( $data[$key] ) ){
		if( $keys ){
			if( is_callable( $data[$key] ) ){
				return call_user_func_array( $data[$key], [ $keys ] );
			} else {
				return fr_get_value_recursively( $data[$key], $keys );
			}
		} else {
			return $data[$key];
		}
	}
}





/**
 * Array Walk
 * v1.0
 */
function fr_array_walk( $a, $callback, $values = [], $lvl = 0 ){

	$stop = false;

	foreach( $a as $k => $v ){

		list( $values, $stop ) = $callback( $k, $v, $values, $lvl );

		// Don't go deeper
		if( $stop == 1 ){
			$stop = false;
			continue;
		}

		// Stop entirely
		if( $stop == 2 ){
			break;
		}

		if( is_array( $v ) ){
			list( $values, $stop ) = fr_array_walk( $v, $callback, $values, $lvl + 1 );
			if( $stop == 2 ){
				break;
			}
		}
	}

	if( $lvl ){
		return [ $values, $stop ];
	} else {
		return $values;
	}
}






function fr_set_value_recursively( $data, $keys, $value, $separator = '.' ){

	if( !is_array( $keys ) ){
		$keys = explode( $separator, $keys );
	}

	$key = array_shift( $keys );
	if( isset( $data[$key] ) ){
		if( $keys ){
			$data[$key] = fr_set_value_recursively( $data[$key], $keys, $value );
		} else {
			$data[$key] = $value;
		}
	}

	return $data;
}



function fr_random_password( $reference, $len ){

	$reference = md5( $reference );
	$reference = str_split( $reference );
	$reference = array_map( 'ord', $reference );
	$reference = implode( '', $reference );
	$reference = str_split( $reference, 15 );
	$reference = array_map( 'intval', $reference );

	$chars_alpha = implode( '', fr_chars( 'alpha' ) );
	$chars = [
		implode( '', fr_chars( 'numeric' ) ),
		$chars_alpha,
		strtoupper( $chars_alpha ),
		'-+=$#@!?&^*{}[]()/\'"`~,;:.<>'
	];

	foreach( $reference as $k => $v ){
		foreach( $chars as $k2 => $char ){
			srand( $v );
			$chars[$k2] = str_shuffle( $chars[$k2] );
		}
	}

	$part = max( 1, ceil( $len / count( $chars ) ) );
	$pass = '';
	foreach( $chars as $set ){
		$pass .= substr( $set, 0, $part );
	}
	$pass = substr( $pass, 0, $len );


	foreach( $reference as $k => $v ){
		srand( $v );
		$pass = str_shuffle( $pass );
	}

	return $pass;

}


function fr_array_is_sequential( $a ){
	$keys = array_keys( $a );
	$x = 0;
	foreach( $keys as $k ){
		if( $k != $x ){
			return false;
		}
		$x++;
	}
	return true;
}


function fr_array_to_php_string( $a, $lvl = 0 ){
	if( !is_array( $a ) ){
		return false;
	}

	$sequential = fr_array_is_sequential( $a );
	$count = count( $a );

	$x = 1;
	$s = "[";
	foreach( $a as $k => $v ){

		if( is_bool( $v ) ){
			if( $v ){
				$v = 'true';
			} else {
				$v = 'false';
			}
		} else if( is_numeric( $v ) ){
			$v = $v;
		} else if( is_array( $v ) || is_object( $v ) ){
			$v = fr_array_to_php_string( $v, $lvl + 1 );
		} else {
			$v = '"' . str_replace( '"', '\\"', $v ) . '"';
		}

		if( $x != $count ){
			$v .= ',';
		}

		if( !$sequential ){
			$k = '"' . str_replace( '"', '\\"', $k ) . '"';
			$v = $k . ' => ' . $v;
		}

		$s .= "\n" . str_repeat( "\t", $lvl + 1 ) . $v;

		$x++;
	}
	$s .= "\n" . str_repeat( "\t", $lvl ) .  "]";

	return $s;
}




function fr_html_extract_text( $t ){

	$t = fr_strip_html_tags_and_content( $t, [ 'head', 'script', 'style', 'noscript' ] );
	$t = strip_tags( $t );
	$t = fr_normalize_new_line_char( $t );
	$t = explode( "\n", $t );
	$t = array_map( 'trim', $t );
	$t = implode( "\n", $t );
	$t = fr_replace_loop( "\n\n\n", "\n\n", $t );
	$t = trim( $t );

	return $t;
}


/**
 * Removes empty lines.
 *
 * @param  string $t Text
 * @return string
 */
function fr_remove_empty_lines( $t ){
	$t = fr_normalize_new_line_char( $t );
	$t = explode( "\n", $t );
	$t = array_map( 'trim', $t );
	$t = implode( "\n", $t );
	$t = fr_replace_loop( "\n\n", "\n", $t );
	$t = trim( $t );
	return $t;
}



function fr_normalize_new_line_char( $t ){
	$t = str_replace( "\r\n", "\n", $t );
	$t = str_replace( "\r", "\n", $t );
	return $t;
}



function fr_string_hide( $string, $string_to_hide, $character = '*' ){

	$found_name = mb_stristr( $string, $string_to_hide );
	
	if( $found_name ){
		$found_name = mb_substr( $found_name, 0, mb_strlen( $string_to_hide ) );
		$string = str_replace( $found_name, str_repeat( '*', mb_strlen( $found_name ) ), $string );
		return $string;
	}

	$string_urf8 = iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE', $string );
	$string_to_hide_utf8 = iconv( 'UTF-8', 'ASCII//TRANSLIT//IGNORE', $string_to_hide );

	$found_name = mb_stristr( $string_urf8, $string_to_hide_utf8 );
	
	if( $found_name ){
		$found_name = mb_substr( $found_name, 0, mb_strlen( $string_to_hide_utf8 ) );
		$string = str_replace( $found_name, str_repeat( '*', mb_strlen( $found_name ) ), $string_urf8 );
		return $string;
	}

	return $string;
}


function fr_fix_file( $file, $replace, $replace_with ){
	
	$error = false;
	
	if( !file_exists( $file ) ){
		$error = 'File doesn\'t exist: ' . $file;
	}

	if( @filesize( $file ) > 1000000 ){
		$error = 'File larger than 1MB, possible bug: ' . $file;
	}

	if( !$error ){
		$content = file_get_contents( $file );

		if( !strstr( $content, $replace ) && !strstr( $content, $replace_with ) ){
			$error = 'String doesn\'t exist: "' . $replace . '" in file ' . $file;
		} else {
			if( !strstr( $content, $replace ) || strstr( $content, $replace_with ) ){
				return;
			}
			$new_content = str_replace( $replace, $replace_with, $content );
			file_put_contents( $file, $new_content );

			// Check
			$written_content = file_get_contents( $file );
			if( $new_content != $written_content ){
				file_put_contents( $file, $new_content );
				$written_content = file_get_contents( $file );
				if( $new_content != $written_content ){
					$error = 'Failed writing to file the fix: ' . $file;
				}
			}
		}
	}

	if( $error ){
		fr_log( 'fr_fix_file - ' . $error, false, true, false, 1 );
	}
}




function fr_constant( $k ){
	if( defined( $k ) ){
		return constant( $k );
	}
}



function fr_is_me( $s = null, $exit = false ){

	if( !isset( $_GET['me'] ) ){
		return false;
	}

	if( $s !== null ){
		fr_p( $s, $exit );
	}

	return true;
	
}



function fr_last_name_hide( $name ){

	$name = trim( $name );

	if( fr_str_ends_with( $name, '.' ) ){
		return $name;
	}

	$parts = explode( ' ', $name );

	if( count( $parts ) == 1 ){
		return $name;
	}

	$last_part = array_pop( $parts );
	$last_part = substr( $last_part, 0, 1 ) . '.';
	$parts[] = $last_part;

	$name = implode( ' ', $parts );

	return $name;
}



function fr_str_replace_keep_case( $from, $to, $string ){

	$to = str_replace( '|', "\$0", $to );
	$from = preg_quote( $from, '/' );
	return preg_replace( '/\b' . $from . '\b/i', $to, $string );

}




function fr_json_html_attr( $a ){
	return fr_html_esc_attr( json_encode( $a ) );
}






function fr_url_change_path( $url, $new_path = [] ){

	if( is_array( $new_path ) ){
		$new_path = '/' . implode( '/', $new_path ) . '/';
	}

	if( is_string( $url ) ){
		$url = parse_url( $url );
	}
	$url['path'] = $new_path;

	return fr_build_link( $url );

}


/**
 * Helper function to execute a callback if the GET variable is set
 * @param  string  $key            The GET key to be expected
 * @param  callback  $callback     Function to be calledback if GET key exists
 * @param  boolean $only_for_admin Trigger this only for administrators
 */
function fr_GET_trigger( $keys, $callback, $only_for_admin = false ){


	if( $only_for_admin && function_exists( 'fr_user' ) && fr_user( 'role' ) != 'administrator' ) {
		return false;
	}

	$keys = fr_make_array( $keys );
	if( !array_diff_key( array_flip( $keys ), $_GET ) ){
		
		$function = function() use ( $callback, $keys ) {
			
			// Return one value if only one key is passed
			if( count( $keys ) == 1 ){
				$values = $_GET[$keys[0]];
			} else {// Return an array of values if multiple keys are passed
				$values = $_GET;
			}
			$callback( $values, $_GET );
		};

		if( defined( 'ABSPATH' ) ){
			fr_wp_init( $function, 1000 );
		} else {
			$function();
		}
	}
}






/**
 * Helper function to execute a callback if the POST variable is set
 * @param  string  $key            The POST key to be expected
 * @param  callback  $callback     Function to be calledback if POST key exists
 * @param  boolean $only_for_admin Trigger this only for administrators
 */
function fr_POST_trigger( $keys, $callback, $only_for_admin = false ){


	if( $only_for_admin && function_exists( 'fr_user' ) && fr_user( 'role' ) != 'administrator' ) {
		return false;
	}

	$keys = fr_make_array( $keys );
	if( !array_diff_key( array_flip( $keys ), $_POST ) ){
		
		$function = function() use ( $callback, $keys ) {
			
			// Return one value if only one key is passed
			if( count( $keys ) == 1 ){
				$values = $_POST[$keys[0]];
			} else {// Return an array of values if multiple keys are passed
				$values = $_POST;
			}
			$callback( $values, $_POST );
		};

		if( defined( 'ABSPATH' ) ){
			fr_wp_init( $function, 1000 );
		} else {
			$function();
		}
	}
}





function fr_compare( $a, $operator, $b ){

	$res = false;
	$negation = false;

	if( substr( $operator, 0, 1 ) == '!' ){
		$negation = true;
		$operator = substr( $operator, 1 );
	}

	if( $operator == '=' ){
		$res = $a == $b;
	}
	if( $operator == '!=' ){
		$res = $a != $b;
	}
	if( $operator == '>' ){
		$res = $a > $b;
	}
	if( $operator == '<' ){
		$res = $a < $b;
	}
	if( $operator == '>=' ){
		$res = $a >= $b;
	}
	if( $operator == '<=' ){
		$res = $a <= $b;
	}

	if( $negation ){
		$res = !$res;
	}

	return $res;
}



function fr_calc( $a, $operator, $b ){

	$res = false;

	if( $operator == '+' ){
		$res = $a + $b;
	}

	if( $operator == '-' ){
		$res = $a - $b;
	}

	if( $operator == '*' ){
		$res = $a * $b;
	}

	if( $operator == '/' ){
		$res = $a / $b;
	}

	if( $operator == '.' ){
		$res = $a . $b;
	}

	if( $operator == '|' ){
		$res = (int)$a | (int)$b;
	}

	if( $operator == '&' ){
		$res = (int)$a & (int)$b;
	}

	if( $operator == '||' ){
		$res = $a || $b;
	}

	if( $operator == '&&' ){
		$res = $a && $b;
	}

	return $res;
}




function fr_sanitize_title( $s ){

	$s = strtolower( $s );
	$s = str_replace( [ ' ', '+' ], '-', $s );
	$s = fr_replace_loop( '--', '-', $s );

	return $s;
}




function fr_terms_to_string( $terms, $taxonomy = false, $start_string = '', $end_string = '', $sep = ', ' ){

	if( !$terms ){
		return '';
	}

	$terms = is_array( $terms ) ? array_filter( $terms ) : $terms;

	if( is_numeric( $terms ) ){
		$terms = wp_get_post_terms( $terms, $taxonomy );
	}

	$terms = fr_make_array( $terms );
	$term = reset( $terms );
	$names = [];

	if( is_object( $term ) ){
		foreach( $terms as $term ){
			$names[] = $term->name;
		}
	} else {
		$names = get_terms( [
			'taxonomy' => $taxonomy,
			'include' => $terms,
			'hide_empty' => false,
			'fields' => 'id=>name'
		] );

		$names = array_replace( array_flip( $terms ), $names );
	}

	return $start_string . implode( $end_string . $sep . $start_string, $names ) . $end_string;
}






function fr_age( $dob ){
	$dob = strtotime( $dob );
	$years = date( 'Y' ) - date( 'Y', $dob );
	$dob = strtotime( '+' . $years . ' years', $dob );
	$years += ( time() - $dob ) / 86400 / 365;
	return $years;
}


function fr_contains( $s1, $s2 ) {
    return strpos($s1, $s2) !== false;
}


function fr_array_items_to_string( $a ){
	return array_map( 'strval', $a );
}



function fr_dir_size( $dir ){
    $size = 0;
    foreach ( glob( rtrim( $dir, '/' ) . '/*', GLOB_NOSORT ) as $each ) {
        $size += is_file( $each ) ? filesize( $each ) : folderSize( $each );
    }
    return $size;
}




function fr_floor( $amount, $precision = 0 ){
    $precise = pow( 10, $precision );
    return floor( $amount * $precise ) / $precise;
}



function fr_string_transliterate( $txt ) {
    $transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE', 'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');
    return str_replace(array_keys($transliterationTable), array_values($transliterationTable), $txt);
}





function fr_str_remove_emoji( $string ){

    return preg_replace( '/[^[:alnum:][:blank:][:punct:]]/u', '', $string );
    
}


function fr_strstr_first_match( $str, $find = [], $offset = 0 ){

	$matches = [];
	foreach( $find as $item ){

		$pos = mb_strpos( $str, $item, $offset );

		if( $pos === false ){
			continue;
		}

		$matches[$item] = $pos;
	}

	asort( $matches, SORT_NUMERIC );
	$matches = array_keys( $matches );

	return reset( $matches );

}


function fr_strstr_last_match( $str, $find = [], $is_reversed = false ){

	$str = $is_reversed ? $str : strrev( $str );

	$matches = [];
	foreach( $find as $item ){

		$rev_item = strrev( $item );
		$pos = strpos( $str, $rev_item );

		if( $pos === false ){
			continue;
		}

		$matches[$item] = $pos;
	}

	asort( $matches, SORT_NUMERIC );
	$match = key( $matches );

	return $match;

}





function fr_string_has_text( $s, $include_numbers = false ){ 
	// Remove string variables
	$s = html_entity_decode( $s );
	$s = str_replace( [ '%s', '%1$s', '%2$s', '%3$s', '%d' ], '', $s );
	
	if( $include_numbers ){
		if( preg_match( '/[a-zA-Z0-9]/', $s ) ){
			return true; 
		}
	} else if ( preg_match( '/[a-zA-Z]/', $s ) ){ 
		return true; 
	}

	return false; 
} 





function fr_array_filter( $a, $functions ){

	return array_filter( $a, function( $s ) use ( $functions ) {

		foreach( $functions as $function ){
			$s = $function( $s );
		}

		return $s;

	} );

}






function fr_keep_character_range( $s, $min_ord, $max_ord, $other_chars_to_keep = [] ){

	$s = str_split( $s );
	$other_chars_to_keep = array_flip( $other_chars_to_keep );

	foreach( $s as $k => $v ){

		$ord = ord( $v );

		if( $ord >= $min_ord && $ord <= $max_ord ){
			continue;
		}

		if( isset( $other_chars_to_keep[$ord] ) ){
			continue;
		}

		$s[$k] = '';

	}

	$s = implode( '', $s );

	return $s;

}




function fr_get_max_number_of_repeats( $string, $character ){
	$max_num_of_repeats = 0;
	$num_of_repeats = 0;
	for( $i = 0; $i < strlen( $string ); $i++ ){
		if( $string[$i] == $character ){
			$num_of_repeats++;
		}
		else{
			$num_of_repeats = 0;
		}
		if( $num_of_repeats > $max_num_of_repeats ){
			$max_num_of_repeats = $num_of_repeats;
		}
	}
	return $max_num_of_repeats;
}




function fr_str_replace_from( $from, $to, $string, $pos ){

	$pos = is_numeric( $pos ) ? $pos : strpos( $string, $pos );
	$pos = $pos ?: 0;
	$string = substr( $string, 0, $pos ) . str_replace( $from, $to, substr( $string, $pos ) );

	return $string;
	
}



function fr_str_replace_last( $search, $replace, $string ){

	$pos = strrpos( $string, $search );

	if( $pos === false ){
		return $string;
	}

	return substr_replace( $string, $replace, $pos, strlen( $search ) );

}




function fr_median( $a ){

	if( !$a ){
		return 0;
	}

	$a = array_values( $a );
	sort( $a, SORT_NUMERIC );
	$index = floor( count( $a ) / 2 );
	return $a[$index];
}



function fr_percent_diff( $nr, $nr2, $decimals = false ){
	
	$diff = 100 / $nr * $nr2 - 100;

	if( $decimals !== false ){
		$diff = round( $diff, $decimals );
	}

	return $diff;

}


function fr_str_to_ord( $s ){
	$s = str_split( $s );
	$s = array_combine( $s, $s );
	$s = array_map( 'ord', $s );
	return $s;
}


function fr_str_includes( $string, $substr_array ){

	if( is_string( $substr_array ) ){
		return strpos( $string, $substr_array ) !== false;
	}

	foreach( $substr_array as $substr ){
		if( strpos( $string, $substr ) !== false ){
			return true;
		}
	}

	return false;
}








function fr_php_file_has_code( $file ){
    $c = file_get_contents( $file );
    $c = fr_php_strip_comments( $c );
    $c = str_replace( [ '<?php', '?>' ], '', $c );
    $c = trim( $c );
    return $c;
}


function fr_php_strip_comments( $c ){

    foreach ( token_get_all( $c ) as $token ) {
        if ( !in_array( $token[0], [ T_COMMENT, T_DOC_COMMENT ] ) ) {
            continue;
        }
        $c = str_replace( $token[1], '', $c );
    }

    return $c;
}




function fr_virus_scanner( $dir ){
    $files = fr_scan_dir( $dir, [ 
        '.git', 
        'LICENSE', 
        'NOTICE', 
        'THIRD-PARTY-LICENSES', 
        'COPYING', 
        'AUTHORS',
        'VERSION',
        'CHANGELOG',
        'CHANGES',
        'README',
        'LICENCE',
        'Dockerfile',
        'Makefile',
        'minifycss',
        'minifyjs',
        'bin',
        'wpo',
        'mailpoet',
        'tcpdf_fonts'
    ] );
    $static_files_dirs = [
        'wp-content/uploads/',
        'wp-content/cache/',
    ];

    $suspect = [];

    foreach( $files as $file ){

        $ext = pathinfo( $file['file'], PATHINFO_EXTENSION );

        if( $ext == 'pid' ){
            $suspect[] = $file['path'];
            continue;
        }

        if( !in_array( $ext, [ 'php', 'yml', 'json', 'txt', 'md', 'js', 'gz', 'transient', 'css', 'png', 'mp3' ] ) && is_executable( $file['path'] ) ){
            $suspect[] = $file['path'];
            continue;
        }

        foreach( $static_files_dirs as $dir ){
            if( !strstr( $file['path'], $dir ) ){
                continue;
            }

            if( $ext == 'php' && fr_php_file_has_code( $file['path'] ) ){
                $suspect[] = $file['path'];
                continue 2;
            }
        }

    }

    fr_p( $suspect );

}




/**
 * Split array into columns for display in table rows
 * @param  array  $a
 * @return array
 */
function fr_split_array_into_cols( array $a, int $col_nr ): array {
    
    $groups = [];
    $row_count = ceil( count( $a ) / $col_nr );

    for( $x = 0; $x < $row_count; $x++ ){
        for( $y = 0; isset( $a[$x + $y] ); $y += $row_count ){
            $groups[$x][] = $a[$x + $y];
        }
    }

    return $groups;
    
}





/**
 * It converts any arbitrary date or timestamp into locale specific format
 * @param  int|string $date   Timestamp or date string
 * @param  string $format 	  Format of the outputted string
 * @param  string $locale     Locale of the output
 * @return string 			  Date string in local language
 */
function fr_get_date_in_locale( $date, $format = 'Y-m-d', $locale = 'en_US' ){

	$timestamp = is_int( $date ) ? $date : strtotime( $date );
	$format = str_replace( [ 'D', 'l', 'M', 'F' ], [ '|1|', '|2|', '|3|', '|4|' ], $format );
	$new_date = date( $format, $timestamp );
	
	$cache_key = 'fr_get_date_in_locale_' . $date . $new_date . $locale;

	$res = fr_cache( $cache_key );
	if( $res !== false ){
		return $res;
	}
	$formatter = fr_cache( 'fr_get_date_in_locale_int_' . $locale ) ?: fr_cache( 'fr_get_date_in_locale_int_' . $locale, new IntlDateFormatter( $locale, IntlDateFormatter::FULL, IntlDateFormatter::NONE, 'GMT+00:00', IntlDateFormatter::GREGORIAN, 'EEE EEEE MMM MMMM' ) );

	$date_time = new DateTime( date( DB_DATE_TIME_FORMAT, $timestamp ) );

	$translated_dates = $formatter->format( $date_time );
	$translated_dates = ucwords( $translated_dates );
	$translated_dates = explode( ' ', $translated_dates );

	$new_date = str_replace( [ '|1|', '|2|', '|3|', '|4|' ], $translated_dates, $new_date );

	return $new_date;

}




/**
 * Gets an array of month names in a specific locale
 * @param  string $locale
 * @return array
 */
function fr_get_month_names_in_locale( string $locale ): array {

	$month_names = [];

	for( $i = 1; $i <= 12; $i++ ){
		$month = str_pad( $i, 2, '0', STR_PAD_LEFT );
		$month_names[] = fr_get_date_in_locale( '2022-' . $month . '-01 00:00:00', 'F', $locale ); 
	}

	return $month_names;

}



function fr_get_utc_offset( $timezone ){

	$timezone = new DateTimeZone( $timezone );
	$date = new DateTime( 'now', $timezone );
	$offset = $timezone->getOffset($date);

	return $offset;

}






/**
 * Function similar to sprintf but string variables can have custom names. Example %where% %how% with paid advertising
 */
function fr_sprintf( string $string, array $params ): string {

	preg_match_all( '/%(.*?)%/', $string, $matches );

	$vars = $matches[1];

	foreach( $vars as $var ){
		$string = str_replace( '%'.$var.'%', $params[$var] ?? '', $string );
	}

	return $string;

}



function fr_printf( string $string, array $params ): void {

	echo fr_sprintf( $string, $params );

}




/**
 * It allows you to control the curvature via $exponent value
 */
function fr_exponential_value_from_range( float $value, float $min_value, float $max_value, float $exponent ): float {

    $value = min( $value, $max_value );
    $value = pow( log( $value - $min_value + 1 ), $exponent ) / pow( log( $max_value - $min_value + 1 ), $exponent );

    return $value;

}



function fr_logarithmic_value_from_range( float $value, float $min_value = 0, float $max_value = 10, float $base = 2 ): float {

    $value = max( $min_value, min( $value, $max_value ) );
    $value = log( $value - $min_value + 1, $base ) / log( $max_value - $min_value + 1, $base );
    $value = $value * 10;
    
    return $value;
}




/**
 * Round integers or floats
 * Example: fr_round( 123.45, 2 ) = 120
 */

function fr_round( float $nr, int $length = 1 ): float {

	// get number of decimals from a number
	$nr_str = (string)$nr;
	$decimal_nr = str_contains( $nr, '.' ) ? strlen( $nr_str ) - strpos( $nr_str, '.' ) - 1 : 0;

	// make nr integer
	$nr *= 10 ** $decimal_nr;

	// add $length decimals to number
	$nr_str = (string)$nr;
	$nr /= 10 ** ( strlen( $nr_str ) - $length );

	// round number
	$nr = round( $nr );
	
	// remove $length decimals to number
	$nr *= 10 ** ( strlen( $nr_str ) - $length );

	// add back the decimals
	$nr /= 10 ** $decimal_nr;

	return $nr;

}








function fr_int_to_time_string(int $time): string {
    // Determine if the time is negative
    $sign = $time < 0 ? '-' : '';

    // Convert the time to a positive value
    $time = abs($time);

    $hours = floor($time / 3600);
    $minutes = floor(($time - $hours * 3600) / 60);
    $seconds = $time - $hours * 3600 - $minutes * 60;
    $string = $sign . $hours . ':' . str_pad($minutes, 2, '0', STR_PAD_LEFT) . ':' . str_pad($seconds, 2, '0', STR_PAD_LEFT);

    return $string;
}





function fr_estimate_reading_time( string $text ): int {

	$text = strip_tags( $text );
    $words_per_minute = 200;
    $word_count = str_word_count( $text );
    $estimated_time = ceil( $word_count / $words_per_minute );

    return $estimated_time;

}






/**
 * Used for malware detection, fixes, etc
 */

function fr_find_string_in_files( $directory, $search_strings, $ignored_file_names = [], $included_extensions = [], $excluded_extensions = [ 'jpg', 'jpeg', 'png', 'gif', 'mp4', 'avi', 'mkv', 'zip', 'rar', 'tar', 'gz' ] ) {

    $iterator = new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $directory ) );
    $search_strings = fr_make_array( $search_strings );

    $matching_files = [];

    foreach( $iterator as $file ) {
     
        if( !$file->isFile() ) {
            continue;
        }
        $extension = pathinfo( $file->getFilename(), PATHINFO_EXTENSION );
        $file_name = $file->getFilename();

        // If the file has one of the excluded extensions, is larger than 1MB,
        // is in the ignored names list, or is not in the included extensions list (if provided),
        // then skip the current iteration.
        if( in_array( $extension, $excluded_extensions ) || 
            $file->getSize() > 1048576 || 
            in_array( $file_name, $ignored_file_names ) ||
            (!empty($included_extensions) && !in_array( $extension, $included_extensions )) ) {
            continue;
        }

        // Check for each string in the file
        $contents = file_get_contents( $file->getRealPath() );
        foreach($search_strings as $search_string) {
            $position = strpos( $contents, $search_string );
            if( $position !== false ) {
                // Capture 50 characters before the match and cover a total length of 100 characters
                $start_pos = max(0, $position - 50); // Ensure we don't go below 0
                $snippet = substr($contents, $start_pos, 100);
                $matching_files[] = [
                    'path' => $file->getRealPath(), 
                    'matched_string' => $search_string, 
                    'snippet' => $snippet
                ];
                break; // Move to the next file after the first match
            }
        }
    }

    return $matching_files;
}





function fr_extract_links( $text ){

	$links = [];

   	preg_match_all( '/http:\/\/[\w\-\.\/?=&#%]+/', $text, $http_matches );
    $links = array_merge($links, $http_matches[0]);

    preg_match_all( '/https:\/\/[\w\-\.\/?=&#%]+/', $text, $https_matches );
    $links = array_merge($links, $https_matches[0]);

    preg_match_all( '/href\s*=\s*(["\'])(.*?)\1/i', $text, $matches );
    $links = array_merge($links, $matches[2]);

	foreach( $links as $k => $link ){
		$links[$k] = rtrim( $link, '.' );
	}

	$links = array_unique( $links );

	return $links;

}






function fr_remove_extra_indentation( $string ){

	$string = rtrim( $string );
	$lines = explode( PHP_EOL, $string );
	$min_indentation = 9999;
	$found_text = false;

	foreach( $lines as $k => $line ){

		if( empty( trim( $line ) ) ){

			if( !$found_text ){
				unset( $lines[$k] );
			}

			continue;
		}

		$found_text = true;
		$indentation = strlen( $line ) - strlen( ltrim( $line ) );
		$min_indentation = min( $min_indentation, $indentation );

	}

	foreach( $lines as $k => $line ){

		if( empty( $line ) ){
			continue;
		}

		$lines[$k] = substr( $line, $min_indentation );

	}

	$string = implode( PHP_EOL, $lines );

	return $string;


}




function fr_array_reduce( $array ){

	return array_reduce( $array, 'array_merge', [] );

}






function fr_process_uploaded_files() {

    $allProcessedFiles = [];

    foreach ($_FILES as $inputName => $fileArray) {
        // Check if 'name' is an array, indicating multiple files or file attributes
        if (is_array($fileArray['name'])) {
            foreach ($fileArray['name'] as $fieldName => $fileName) {
                // Check if the file name is not empty
                if (!empty($fileName)) {
                    $allProcessedFiles[$inputName][$fieldName] = [
                        'name'     => $fileArray['name'][$fieldName],
                        'type'     => $fileArray['type'][$fieldName],
                        'tmp_name' => $fileArray['tmp_name'][$fieldName],
                        'error'    => $fileArray['error'][$fieldName],
                        'size'     => $fileArray['size'][$fieldName]
                    ];
                }
            }
        } else {
            // Single file case
            if (!empty($fileArray['name'])) {
                $allProcessedFiles[$inputName] = [
                    'name'     => $fileArray['name'],
                    'type'     => $fileArray['type'],
                    'tmp_name' => $fileArray['tmp_name'],
                    'error'    => $fileArray['error'],
                    'size'     => $fileArray['size']
                ];
            }
        }
    }

    return $allProcessedFiles;
    
}








function fr_trim_words($text, $num_words = 55, $more = '…') {

    $text = wp_strip_all_tags($text);

    $words = mb_split('\s+', $text);
    $trimmed = array_slice($words, 0, $num_words);

    $text = implode(' ', $trimmed);

    if (count($words) > $num_words) {
        $text .= $more;
    }

    return $text;
    
}



function fr_esc_attr( $string ){

	return htmlspecialchars( $string, ENT_QUOTES, 'UTF-8' );

}


function fr_esc_html( $string ){

	return htmlspecialchars( $string, ENT_QUOTES, 'UTF-8' );

}



/**
 * Transforms slugs in titles
 */
function fr_unsanitize_title( $title ){
	$title = str_replace( '-', ' ', $title );
	$title = str_replace( '_', ' ', $title );
	$title = ucwords( $title );
	return $title;
}



/**
 * Similar to the native function but it doesn't fail in some circumstances
 */
function fr_parse_str( $string ){

	$string = trim( $string );

	if( !$string ){
		return [];
	}

	$parts = explode('&', $string);
	$parsed = [];

	foreach ($parts as $part) {
	    if (strpos($part, '=') !== false) {
	        list($key, $value) = explode('=', $part, 2);
	        $parsed[$key] = urldecode($value);
	    } else {
	        $key = $part;
	        $parsed[$key] = null;
	    }
	}

	return $parsed;

}




function fr_trim_lines( $string ){

	$lines = explode( PHP_EOL, $string );
	$lines = array_map( 'trim', $lines );
	$string = implode( PHP_EOL, $lines );

	return $string;

}




function fr_string_to_array( $string, $separator = ',' ){

	$string = trim( $string );
	$items = explode( $separator, $string );
	$items = array_map( 'trim', $items );
	$items = array_filter( $items );

	return $items;

}



function fr_str_str_intersection( $str1, $str2 ) {

	$len1 = strlen( $str1 );
	$len2 = strlen( $str2 );
	$min_len = min( $len1, $len2 );
	$matches = [];


	for( $i = 0; $i < $min_len; $i++ ) {

		for( $j = $i + 1; $j <= $min_len; $j++ ) {

			$substring = substr( $str1, $i, $j - $i );

			if( strpos( $str2, $substring ) !== false ) {
				$matches[$substring] = strlen( $substring );
			}

		}

	}


	asort( $matches, SORT_NUMERIC );
	$matches = array_keys( $matches );
	$best_match = end( $matches );



	return $best_match;
	
}




function fr_str_split_once( $string, $delimiter ) {

	$pos = strpos( $string, $delimiter );

	if( $pos === false ){
		return [ $string, '' ];
	}

	$part1 = substr( $string, 0, $pos );
	$part2 = substr( $string, $pos + strlen( $delimiter ) );

	return [ $part1, $part2 ];

}







function fr_ip_in_range( $ip, $range ) {

	if( strpos( $range, '/' ) === false ) {
		$range .= filter_var( $range, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 ) ? '/128' : '/32';
	}

	list( $range_ip, $netmask ) = explode( '/', $range, 2 );
	$netmask = intval( $netmask );

	if( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) && filter_var( $range_ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {

		$ip_binary = ip2long( $ip );
		$range_binary = ip2long( $range_ip );

		if( $ip_binary === false || $range_binary === false || $netmask < 0 || $netmask > 32 ) {
			return false;
		}

		$netmask_binary = ~( ( 1 << ( 32 - $netmask ) ) - 1 );

		return ( $ip_binary & $netmask_binary ) === ( $range_binary & $netmask_binary );

	}

	if( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 ) && filter_var( $range_ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 ) ) {

		$ip_binary = inet_pton( $ip );
		$range_binary = inet_pton( $range_ip );

		if( $ip_binary === false || $range_binary === false || $netmask < 0 || $netmask > 128 ) {
			return false;
		}

		$netmask_binary = pack( 'H*', str_repeat( 'f', $netmask / 4 ) . str_repeat( '0', ( 128 - $netmask ) / 4 ) );

		return ( $ip_binary & $netmask_binary ) === ( $range_binary & $netmask_binary );

	}

	return false;

}







function fr_url_has_redirect( $url, $credentials = '' ) {

    $ch = curl_init($url);
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);

    if( $credentials ){
    	curl_setopt($ch, CURLOPT_USERPWD, $credentials );
    }

    curl_exec($ch);
    
    $final_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    $redirect_count = curl_getinfo($ch, CURLINFO_REDIRECT_COUNT);

    curl_close($ch);

    if( $redirect_count >= 10 ) {
        return null;
    }

    if( $url === $final_url ){
		return false;
	}

	$final_url = str_contains( $final_url, '@' ) ? fr_single_loop_extract_remove( $final_url, '//', '@', '//' ) : $final_url;
    
    return $final_url;

}






function fr_url_get_status_code( $url ) {

	$ch = curl_init( $url );
	
	curl_setopt( $ch, CURLOPT_NOBODY, true );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, false );
	
	curl_exec( $ch );
	
	$httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
	
	curl_close( $ch );

	return $httpCode;
	
}



function fr_enumerate_lines( $string ) {

	$lines = explode( "\n", $string );

	foreach( $lines as $key => &$line ) {
		$line = ( $key + 1 ) . ".\t" . $line;
	}

	return implode( "\n", $lines );

}



function fr_get_ip(){

	$ip = $_SERVER['REMOTE_ADDR'];

	if( isset( $_SERVER['HTTP_CF_CONNECTING_IP'] ) ){
		$ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
	}

	if( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ){
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}

	return $ip;
	
}




function fr_time_string_to_seconds( $string ){

	$seconds = 0;

	if( str_contains( $string, 'd' ) ){
		$parts = explode( 'd', $string );
		$seconds += (float)$parts[0] * 86400;
		$string = $parts[1];
	}

	if( str_contains( $string, 'h' ) ){
		$parts = explode( 'h', $string );
		$seconds += (float)$parts[0] * 3600;
		$string = $parts[1];
	}

	if( str_contains( $string, 'm' ) ){
		$parts = explode( 'm', $string );
		$seconds += (float)$parts[0] * 60;
		$string = $parts[1];
	}

	if( str_contains( $string, 's' ) ){
		$parts = explode( 's', $string );
		$seconds += (float)$parts[0];
	}

	return $seconds;


}






class fr_system_resource_info {

	static function get() {

		return array_merge(
			self::get_memory_info(),
			[
				'php_memory_limit' => self::get_php_memory_limit(),
				'php_memory_used' => round( 100 / self::get_php_memory_limit() * memory_get_usage( true ), 2 )
			],
			self::get_cpu_info(),
			self::get_disk_info()
		);

	}

	static function get_memory_info() {

		$default = [
			'server_memory_total' => null,
			'server_memory_used' => null,
		];

		$mem_info = self::execute_shell_command( 'free -b' );

		if( !$mem_info || count( $mem_info ) < 2 ) {
			return $default;
		}

		$parts = preg_split( '/\s+/', $mem_info[1] );

		if( count( $parts ) < 7 ){
			return $default;
		}

		return [
			'server_memory_total' => (int)$parts[1],
			'server_memory_used' => round( 100 / (int)$parts[1] * (int)$parts[2], 2 ),
		];

	}

	static function get_php_memory_limit() {

		$limit = ini_get( 'memory_limit' );

		return $limit === '-1' ? -1 : fr_to_bytes( $limit );

	}

	static function get_cpu_info() {

		$cores = (int)( self::execute_shell_command( 'nproc' )[0] ?? '0' );
		$load = sys_getloadavg()[0] ?? 0;

		return [
			'cpu_cores' => $cores,
			'cpu_used' => $cores > 0 ? round( ( $load / $cores ) * 100, 2 ) : $load * 100,
		];

	}

	static function get_disk_info() {

		$total = disk_total_space( '/' );
		$free = disk_free_space( '/' );
		$used = $total - $free;

		return [
			'disk_space_total' => $total,
			'disk_space_used' => round( 100 / $total * $used, 2 ),
		];

	}

	static function execute_shell_command( $command ) {

		if( !function_exists( 'shell_exec' ) || in_array( 'shell_exec', explode( ',', ini_get( 'disable_functions' ) ) ) ) {
			return null;
		}

		return array_filter( explode( "\n", shell_exec( "$command 2>/dev/null" ) ?: '' ) );

	}

}






function fr_diff_compare( $str1, $str2 ) {

	$str1 = is_array( $str1 ) ? json_encode( $str1, JSON_PRETTY_PRINT ) : json_encode( $str1, JSON_PRETTY_PRINT );
	$str2 = is_array( $str2 ) ? json_encode( $str2, JSON_PRETTY_PRINT ) : json_encode( $str2, JSON_PRETTY_PRINT );

	$lines1 = explode( "\n", $str1 );
	$lines2 = explode( "\n", $str2 );
	$max_lines = max( count( $lines1 ), count( $lines2 ) );

	$rows = '';

	for( $i = 0; $i < $max_lines; $i++ ) {
		$line1 = $lines1[$i] ?? '';
		$line2 = $lines2[$i] ?? '';
		$style = ( $line1 !== $line2 ) ? 'background-color: #ffcccc;' : '';
		$rows .= '<tr><td style="border: 0"><pre>' . htmlspecialchars( $line1 ) . '</pre></td><td  style="border: 0;' . $style . ';"><pre>' . htmlspecialchars( $line2 ) . '</pre></td></tr>';
	}

	return '<table border=\'1\' style=\'border-collapse: collapse; width: 100%;\'><tr><th>Original</th><th>Comparison</th></tr>' . $rows . '</table>';

}



function fr_hirerchical_array_to_string( $array, $parent_key, $parent_value, $label_callback = null, $depth = 0 ) {

	$string = '';
	$tab_string = str_repeat( "\t", $depth );

	foreach( $array as $key => $value ) {

		if( !isset( $value[$parent_key] ) ){
			continue;
		}

		if( $value[$parent_key] !== $parent_value ){
			continue;
		}

		$string .= $tab_string . ( $label_callback ? $label_callback( $key, $value ) : $key ) . PHP_EOL;
		$result = fr_hirerchical_array_to_string( $array, $parent_key, $value[$parent_key], $label_callback, $depth + 1 );

		if( $result ){
			$string .= $tab_string . $result . PHP_EOL;
		}

	}

	return $string;

}	