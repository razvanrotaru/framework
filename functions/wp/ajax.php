<?php 
function fr_is_ajax( $action = false ){
	$res = wp_doing_ajax() || isset( $_POST['gform_ajax'] ) || defined( 'REST_REQUEST' );
  
  if( $action !== false ){
    $res = $res && isset( $_POST['action'] ) && $_POST['action'] == $action;
  }

  return $res;
}


/**
 * Checks if the ajax call was made from the administration
 */
function fr_is_admin_ajax(){
  return fr_is_ajax() && strstr( $_SERVER['HTTP_REFERER'], '/wp-admin/' );
}


//Function that handles multiple ajax requests at one time to be more eficient. 
//It only requires the funtion name to start with "ajax_"
function fr_multi_ajax(){
	$data = isset( $_POST["data"] ) ? fr_decode_ajax_data( $_POST["data"] ) : array();

  if( !is_array( $data ) ){
    return;
  }
  
	foreach($data as $k=>$v){
    if( !is_array( $v ) || count( $v ) != 2 ){
      fr_log( 'Bad multi ajax data', $data );
      continue;
    }
    $_ENV['fr_current_ajax'] = $v[0];
		if(function_exists("ajax_".$v[0])){
			if($v[1] && is_string( $v[1] ) ){
				parse_str($v[1],$v[1]);
			}
			call_user_func( 'ajax_' . $v[0], $v[1] );
		}
    do_action( 'fr_ajax_' . $v[0], $v[1] );
    do_action( 'fr_ajax', $v[0], $v[1] );
	}
	fr_ajax_send_response();
}

add_action('wp_ajax_multi_ajax', 'fr_multi_ajax');
add_action("wp_ajax_nopriv_multi_ajax", "fr_multi_ajax");



/**
 * Decodes the data received via ajax
 * @param  string $data
 * @return string
 */
function fr_decode_ajax_data( $data ){
  return json_decode( urldecode( base64_decode( $_POST["data"] ) ), 1 );
}


//Helper function that sends commands to the ajax javascript helper
//$selector = css type selector
//$type = addClass, removeClass, html
//$val = a value corresponding to the $type.
//Example: ajax_response(".my_btn","addClass","active");
function fr_ajax_add_response($type,$selector = false,$val = false ){
  global $ajax_response;
  $current_ajax = isset( $_ENV['fr_current_ajax'] ) ? $_ENV['fr_current_ajax'] : '';
  if(!isset($ajax_response) || !is_array($ajax_response)){
    $ajax_response=array();
  }
  if( $selector === false ){
  	$val = $type;
  	$type = 'data';
  	$selector = '';
  }else if( $val === false ){
  	$val = $selector;
  	$selector = '';
  }
  if( !isset( $ajax_response[$current_ajax] ) ){
  	$ajax_response[$current_ajax] = array();
  }
  $ajax_response[$current_ajax][]=array("selector"=>$selector,"type"=>$type,"val"=>$val);
}


//Function to compile the ajax response and end the php execution
function fr_ajax_send_response( $response = false ){
  global $ajax_response;
  if(!isset($ajax_response) || !is_array($ajax_response)){
    $ajax_response=array();
  }

  if( $response === false ){
    $response = $ajax_response;
  } else if( !empty( $ajax_response[''] ) ){
    $response['js_rules'] = $ajax_response[''];
  }

 
  echo json_encode( $response );
  fr_exit();
}

// Auto-Call ajax functions
add_action( 'init', function(){
	if( fr_is_ajax() && !empty( $_POST['action'] ) && $_POST['action'] != 'multi_ajax' ){
    if( function_exists( "ajax_" . $_POST['action'] ) ){
  		call_user_func_array( "ajax_" . $_POST['action'], [ $_POST ] );
    }
    do_action( 'fr_ajax_' . $_POST['action'], $_POST );
    do_action( 'fr_ajax', $_POST['action'], $_POST );
	}
} );

add_action( 'wp_loaded', function(){
  if( fr_is_ajax() && !empty( $_POST['action'] ) && $_POST['action'] != 'multi_ajax' ){
    if( function_exists( "ajax_wp_loaded_" . $_POST['action'] ) ){
      call_user_func_array( "ajax_wp_loaded_" . $_POST['action'], [ $_POST ] );
    }
  }
} );


// Automatically call ajax functions from an object
function fr_ajax_action_in_object( $object ){
  fr_wp_init( function() use ( $object ){
    if( fr_is_ajax() && isset( $_POST['action'] ) && method_exists( $object, 'ajax_' . $_POST['action'] ) ){
      call_user_func( [ $object, 'ajax_' . $_POST['action'] ], $_POST );
      exit;
    }
  });


  add_action( 'fr_ajax', function( $action, $data ) use ( $object ){
    if( method_exists( $object, 'ajax_' . $action ) ){
      call_user_func( [ $object, 'ajax_' . $action ], $data );
    }
  }, 10, 2 );
}



/**
 * Ajax
 * Executes a callback on ajax action
 * v1.0
 */
function fr_ajax( $action, $callback ){
  add_action( 'fr_ajax_' . $action, function( $data ) use( $callback ){
    call_user_func( $callback, $data );
  } );
}



function fr_ajax_get_original_url(){
  if( fr_is_ajax() ){
    $url = $_SERVER['HTTP_REFERER'];
  } else {
    $url = fr_get_current_url();
  }
  return $url;
}