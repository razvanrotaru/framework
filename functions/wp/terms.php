<?php 
// Allow terms to be searched by like name without all wildcards 

add_filter( 'terms_clauses', function( $pieces, $taxonomies, $args ){
    global $wpdb;
    $wildcard = $wpdb->prepare( "%s", '%');

    if ( !empty( $args['name__like'] ) && ( strstr( $args['name__like'], '%' ) || strstr( $args['name__like'], $wildcard ) ) ) {
        $to_replace = $wpdb->prepare( "t.name LIKE %s", '%' . $wpdb->esc_like( $args['name__like'] ) . '%' );
        $name = str_replace( '%', '', $args['name__like'] );
        if( substr( $args['name__like'], 0, 1 ) == '%' ){
            $with = $wpdb->prepare( "t.name LIKE %s", '%' . $wpdb->esc_like( $name ) );
        }else if( substr( $args['name__like'], -1, 1 ) == '%' ){
            $with = $wpdb->prepare( "t.name LIKE %s", $wpdb->esc_like( $name ) . '%' );
        }else {
            $with = $wpdb->prepare( "t.name LIKE %s", $wpdb->esc_like( $name ) );
        }
        $pieces['where'] = str_replace( $to_replace , $with, $pieces['where'] );
    }
    return $pieces;
}, 10, 3 );


//Retrives the term that is on the lowest level.

function fr_get_child_term($id,$tax,$return_all=0){
    $terms=wp_get_post_terms($id,$tax);
    usort($terms,function($a,$b){
    	return $a->term_id<=>$b->term_id;
    });
    if($return_all){
        return $terms;
    }
    return reset($terms);
}

function fr_get_term($id,$tax,$key=""){

    if( !$id ){
        return false;
    }

	$term=get_term_by("id",$id,$tax);
	if($key){
        if( $term ){
		    return $term->$key;
        }
	}else{
		return $term;
	}
}

function fr_get_term_by_parent( $field, $label, $taxonomy, $parent ){
    $parent_terms = get_terms( array(
        'taxonomy' => $taxonomy,
            $field => $label,
            'parent' => $parent,
            'hide_empty' => false
    ) );
    return reset( $parent_terms );
}


function fr_tax( $tax, $key = '' ){
    $taxonomy = get_taxonomy( $tax );
    if( $key ){
        if( $key == 'label' ){
            return $taxonomy->$key;
        }else if( isset( $taxonomy->labels->$key ) ){
            return $taxonomy->labels->$key;
        }
    }else{
        return $taxonomy;
    }
}

function fr_current_term(){
    $object=get_queried_object();
    if( !empty( $object->taxonomy ) ){
        return $object;
    }
}



function fr_taxonomy_options( $taxonomy, $hierarchical = false, $parent = 0, $lvl = 0 ){

    if( !$hierarchical ){
        $parent = '';
    }

    $options = array();
    $terms = get_terms( array(
            "taxonomy"=>$taxonomy,
            "hide_empty"=>false,
            'parent' => $parent
    ));
    foreach($terms as $term){
            $options[$term->term_id] = str_repeat( '•', $lvl ) . ' ' . $term->name;
            if( $hierarchical ){
                    $suboptions = fr_taxonomy_options( $taxonomy, $hierarchical, $term->term_id, $lvl + 1 );
                    $options = $options + $suboptions;
            }
    }
    return $options;
}

function fr_get_taxonomy_by_slug( $slug ){
    $res = get_taxonomies( [ 'public' => 1 ], 'objects' );
    foreach( $res as $v ){
        if( $v->rewrite['slug'] == $slug ){
            return $v->name;
        }
    }
}


function fr_tm( $k, $v = false, $tid = false ){
    if( $tid == false ){
        $tid = $v;
        $v = false;
    }

    if( is_object( $tid ) ){
        $tid = $tid->term_id;
    }

    if( $v === false ){
        return get_term_meta( $tid, $k, 1 );
    } else {
        return update_term_meta( $tid, $k, $v );
    }
}


/**
 * An efficient way of updating the terms only if they are different from the current ones
 * @param    int $pid 
 * @param    array $terms
 * @param    string $taxonomy
 */
function fr_set_post_terms( $pid, $terms, $taxonomy ){

    $terms = fr_make_array( $terms );
    $terms = array_unique( $terms );
    $terms = fr_array_remove_empty( $terms );
    sort( $terms, SORT_NUMERIC );
    $old_terms = wp_get_post_terms( $pid, $taxonomy, [ 'fields' => 'ids' ] );
    sort( $old_terms, SORT_NUMERIC );
    if( $old_terms != $terms ){
        wp_set_post_terms( $pid, $terms, $taxonomy );
        return true;
    }
    return false;

}


function fr_get_tax_label( $tax, $singular = false ){
    $tax = get_taxonomy( $tax );
    if( !$tax ){
        return;
    }

    if( $singular ){
        return $tax->labels->singular_name;
    }

    return $tax->label;
}




function fr_tax_is_public( $tax ){
    $settings = (array)get_taxonomy( $tax );
    return !empty( $settings['public'] ) && !empty( $settings['publicly_queryable'] );
}


function fr_tax_slug( $tax ){
    $tax_settings = (array)get_taxonomy( $tax );
    if( !$tax_settings ){
        return false;
    }
    return $tax_settings['rewrite']['slug'];
}



function fr_tax_url( $tax ){
    return get_bloginfo( 'url' ) . '/' . fr_tax_slug( $tax ) . '/';
}


function fr_get_term_links( $term_slugs, $taxonomy = '', $no_url_root = '' ){

    if( !$term_slugs ){
        return [];
    }

    $first = reset( $term_slugs );

    if( is_object( $first ) ){
        $new = [];
        foreach( $term_slugs as $object ){
            $new[$object->term_id] = $object->slug;
        }
        $term_slugs = $new;
    }

    $res = get_taxonomy( $taxonomy );
    $tax_slug = $res->rewrite['slug'];

    if( $no_url_root ){
        $root = '';
    } else {
        $root = get_bloginfo( 'url' );
    }

    $links = [];

    foreach( $term_slugs as $id => $slug ){
        $links[$slug] = $root . '/' . $tax_slug . '/' . $slug . '/';
    }

    return $links;
}




function fr_set_existing_post_terms( $post_id, $term_names, $taxonomy ) {

    $existing_terms = get_terms( [
        'taxonomy' => $taxonomy,
        'hide_empty' => false,
        'fields' => 'names'
    ] );

    $existing_terms = array_combine( array_map( 'strtolower', $existing_terms ), $existing_terms );
    $valid_terms = [];

    foreach( $term_names as $term_name ){

        $term_name = trim( strtolower( $term_name ) );
       
        if( !isset( $existing_terms[$term_name] ) ){
            continue;
        }

        $valid_terms[] = $existing_terms[$term_name];

    }

    wp_set_post_terms( $post_id, $valid_terms, $taxonomy, false );

}





