<?php
function fr_import_terms( $taxonomy, $json ){
	$terms = json_decode( $json, 1 );

	$lvls = array();
	foreach( $terms as $term ){
		$label = trim( $term );
		$lvl = substr_count( $term, "\t" );
		$lvls[$lvl] = $label;
		$parent = 0;
		if( $lvl > 0 ){
			$parent = fr_get_term_by_parent( 'name', $lvls[$lvl-1], $taxonomy, $lvl-1 );
			if( $parent ){
				$parent = $parent->term_id;
			}
		}
		$term_ob = fr_get_term_by_parent( 'name', $label, $taxonomy, $parent );
		if( !$term_ob ){
			$res = wp_insert_term( $label, $taxonomy, array( 'parent' => $parent ) );
		}
	}
}


function fr_export_tax_meta( $tax, $meta_keys ){
    $terms = get_terms( [
        'taxonomy' => $tax,
        'hide_empty' => false
    ] );
    $data = [];
    foreach( $terms as $term ){
        $data[$term->term_id] = [
            'data' => [
                'id' => $term->term_id,
                'slug' => $term->slug,
                'name' => $term->name,
            ],
            'meta' => []
        ];
        foreach( $meta_keys as $key ){
            $data[$term->term_id]['meta'][$key] = get_term_meta( $term->term_id, $key, 1 );
        }
    }
    return json_encode( $data );
}

function fr_import_tax_meta( $tax, $on_data, $s ){

    $terms = json_decode( $s, 1 );

    if( !$terms ){
        return;
    }
    
    $not_found = [];
    foreach( $terms as $term ){
        $term_ob = get_term_by( $on_data, $term['data'][$on_data], $tax );

        if( !$term_ob ){
            $not_found[] = $term;
            continue;
        }

        foreach( $term['meta'] as $k => $v ){
            fr_p( [$term_ob->term_id, $k, $v], 'imported' );
            update_term_meta( $term_ob->term_id, $k, $v );
        }
    }

    return $not_found = [];
}


/**
 * Allow admin to export/import term meta info
 */
function fr_export_import_enable_action(){

    fr_GET_trigger( 'fr_export_tax_meta', function( $values, $data ){

        if( !current_user_can( 'manage_options' ) && ( empty( $data['start'] ) || $data['start'] != SECURE_AUTH_KEY ) ){
            return false;
        }
        
        $values = explode( ',', $values );
        $s = fr_export_tax_meta( $data['tax'], $values );

        // If the result is larger than 1500 characters then create a special link that the remote script can get the data from
        if( strlen( $s ) > 1500 && empty( $data['start'] ) ){
            $url = fr_get_current_url();
            $url = fr_build_link( $url, [ 'start' => SECURE_AUTH_KEY ] );
            $url = base64_encode( $url );
            $s = $url;
        }

        echo $s;
        exit;
    } );

    fr_GET_trigger( 'fr_import_tax_meta', function( $json, $data ){

        $json_url = base64_decode( $json );
        if( $json_url ){
            $json = file_get_contents( $json_url );
        }

        $r = fr_import_tax_meta( $data['tax'], 'slug', $json );
        if( $r ){
            fr_p( $r, 'not_imported' );
        }
        exit;
    }, true );

}