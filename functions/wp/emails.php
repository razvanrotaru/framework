<?php 

class FR_SMTP{
	var $settings;

	function __construct($settings){
		$defaults=array(
			"sender"=>get_option("admin_email"),
			"sender_name"=>get_bloginfo("name"),
			"encryption"=>"ssl"
			);
		$this->settings=array_merge($defaults,$settings);

		add_action( 'phpmailer_init', array($this,'init') );
	}

	function init($phpmailer){
		$phpmailer->isSMTP();     
	    $phpmailer->Host = $this->settings["host"];
	    if( !empty( $this->settings["username"] ) ){
	    	$phpmailer->SMTPAuth = true; // Force it to use Username and Password to authenticate
		    $phpmailer->Username = $this->settings["username"];
		    $phpmailer->Password = $this->settings["password"];
	    }
	    $phpmailer->Port = $this->settings["port"];
	    $phpmailer->SMTPSecure = $this->settings["encryption"];
	    $phpmailer->From = $this->settings["sender"];
	    $phpmailer->Sender = $this->settings["sender"];
		$phpmailer->FromName = $this->settings["sender_name"];
		if( !empty( $this->settings["not_secure"] ) ){
			$phpmailer->SMTPSecure = false;
			$phpmailer->SMTPAutoTLS = false;
		}
	}
}



/**
 * Converts a string to array
 *
 * @param string $to Example: John <john@email.com>
 * @return array
 */

function fr_email_str_to_array( $to ){

	if( is_array( $to ) ){
		return $to;
	}

	$to = explode(",", $to);
	foreach($to as $k=>$email){
		$email = explode("<", $email);
		if(count($email)==1){
			$email[1] = $email[0];
			$email[0] = "";
		}
		$to[$k] = array(
				trim($email[0]),
				trim(str_replace(">","",$email[1]))
				);
	}
	return $to;
}


function fr_email_apply_css( $css_file ){
	add_filter( 'wp_mail', function( $data ) use ( $css_file ) {
		$css = file_get_contents( $css_file );
		$data['message'] = fr_html_apply_css( $data['message'], $css );
		return $data;
	} );
}


function fr_email_enable_html(){
	add_filter( 'wp_mail_content_type', function(){
		return 'text/html';
	}, 11 );
}

function fr_email_enable_multipart( $boundary ){
	add_filter( 'wp_mail_content_type', function() use ( $boundary ){
		return 'multipart/alternative; boundary=' . $boundary;
	}, 11 );
}

function fr_email_set_sender( $name = '', $email = '' ){
	if( !$name ){
		$name = get_bloginfo();
	}
	if( !$email ){
		$email = get_option( 'admin_email' );
	}
	add_filter( 'wp_mail_from', function() use ( $email ) {
	    return $email;
	} );
	add_filter( 'wp_mail_from_name', function() use ( $name ) {
	    return $name;
	} );
}



function fr_email_redirect_emails( $email ){
	add_filter( 'wp_mail', function( $data ) use ( $email ){ 

		if( $email == $data['to'] ){
			return $data;
		}

		$data['subject'] .= ':  ' . $data['to'];
		$data['to'] = $email;
		return $data;
	} );
}





function fr_email_html_to_text( $text ){

	$links = fr_single_loop_extract( $text, '<a ', '</a>' );
	foreach( $links as $link ){
		$href = fr_extract( $link, 'href|"', '"' );
		$label = substr( strstr( $link, '>' ), 1 );

		if( strstr( $label, '<img' ) ){
			continue;
		}

		$label = strip_tags( $label );
		$label = trim( $label );
		if( $label ){
			$replace_with = $label . ' ( ' . $href . ' )';
		} else {
			$replace_with = $href;
		}
		$text = str_replace( '<a ' . $link . '</a>', $replace_with, $text );
	}

	$block_tags = fr_data_html_tags();
	foreach( $block_tags as $tag ){
		$text = str_replace( '</' . $tag . '>', '</' . $tag . '>' . "\n", $text );
	} 

	$text = strip_tags( $text );
	$text = html_entity_decode( $text );
	$text = trim( $text );
	$text = str_replace( "\t", ' ', $text );
	$text = str_replace( "\r", '', $text );

	while( 1 ){
		$new = fr_replace_loop( "\n \n", "\n", $text );
		$new = fr_replace_loop( "\n\n\n", "\n\n", $new );
		$new = fr_replace_loop( '  ', ' ', $new );
		$new = trim( $new );

		if( $new === $text ){
			break;
		}

		$text = $new;
	}

	return $text;
}









