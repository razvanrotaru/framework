<?php 
// AMP documentation: https://www.ampproject.org/docs/fundamentals/spec
// Validator: https://validator.ampproject.org/
// Rules: 
// 		no !important in style
// 		style inline and max 50kb


function fr_amp(){
	if( fr_is_amp() ){
		ob_start( function( $c ){

			// Move styles in to header
			$c = explode( '<body', $c );
			$parts = explode( ' style="', $c[1] );
			$styles = array();
			foreach( $parts as $k => $part ){
			    if ( $k ){
			    	if( strlen( strstr( $part, '"', 1 ) ) ){
				        $styles[] = "#dinamic-style-" . $k . '{ ' . strstr( $part, '"', 1 ) . '; }';
				    }
				    $parts[$k] =  ' id="dinamic-style-' . $k . substr( $part, strlen( strstr( $part, '"', 1 ) ) );
			    }
			}
			$styles = fr_css_minify( implode( "\n", $styles ) );
			$c = str_replace( '<style amp-custom>', "<style amp-custom>\n" . $styles, $c[0] ) . '<body' . implode( '', $parts );
			
			$c = fr_amp_convert_content( $c, 320 );

			$c = str_replace( '</head>', '<link rel="canonical" href="' . fr_get_current_url( 1 ) . '"></head>', $c );

			return $c;
		});
	}else{
		add_action( 'wp_head', function(){
			?><link rel="amphtml" href="<?php echo fr_get_current_url( 1 ) ?>?amp"><?php 
		} );
	}
}




/**
 * Converts HTML to valid AMP HTML
 * v1.1
 * @param  string  $c                  The HTML string
 * @param  integer $layout_width       The default width of the layout is needed to know which images to make responsive
 * @param  array   $map_domains        Changes the src domain to something else (may need to be separated from this in the future)
 * @param  array   $remote_image_sizes Allows to specify the dimensions for remote images. (may need to be separated from this in the future)
 * @return string  					   Valid AMP HTML
 */
function fr_amp_convert_content( $c, $layout_width = false, $map_domains = [], $remote_image_sizes = [] ){
	if( !is_array( $remote_image_sizes ) ){
		$remote_image_sizes = [];
	}
	$root_url = get_bloginfo( 'url' );

	// These will replace html strings at the end
	$replacements = [ 
		[ '!important', '' ],
		[ '<video', '<amp-video' ],
		[ '<audio', '<amp-audio' ],
		[ ' action=', ' action-xhr=' ],// Form actions
		[ '<iframe ', '<amp-iframe ' ],
		[ '</iframe>', '</amp-iframe>' ]
	];

	$keep_atts_func = function( $ob, $allowed ){
		$atts = $ob->attr( '*' );
		$atts = array_keys( $atts );
		$allowed = explode( ' ', $allowed );
		$atts_to_remove = array_diff( $atts, $allowed );

		foreach( $atts_to_remove as $att ){
			if( substr( $att, 0, 5 ) == 'data-' ){
				continue;
			}
			$ob->removeAttr( $att );
		}
	};



	$convert_attr_values = function( $ob, $atts ){
		foreach( $atts as $attr_name => $attr_values ){
			$value = $ob->attr( $attr_name );
			foreach( $attr_values as $from => $to ){
				if( $value == $from ){
					$ob->attr( $attr_name, $to );
					break;
				}
			}
		}
	};


	// Replace images
	$c =explode( '<img', $c );
	foreach( $c as $k => $v ){
		if( $k ){
			$c[$k] = fr_str_replace_once( '>', '></amp-img>', $v );
		}
	}
	$c = implode( '<amp-img', $c );


	// Iframes

	$dom = new fr_dom( $c );
	$iframes = $dom->find( 'iframe' );
	foreach( $iframes as $iframe ){
		$ob = fr_dom( $iframe );
		$keep_atts_func( $ob, 'src srcdoc frameborder allowfullscreen allowpaymentrequest allowtransparency referrerpolicy sandbox fallback heights layout media noloading on placeholder sizes width height class id' );
		
		// https://amp.dev/documentation/guides-and-tutorials/develop/style_and_layout/control_layout/?format=websites
		if( !$ob->attr( 'layout' ) ){
			$ob->attr( 'layout', 'responsive' );
		}

		// Convert invalid values to valid ones
		$convert_attr_values( $ob, [
			'frameborder' => [
				'yes' => 1,
				'no' => 0,
				'' => 0
			],
			'allowtransparency' => [
				'true' => 'allowtransparency',
				'' => 'allowtransparency'
			],
			'allowfullscreen' => [
				'true' => 'allowfullscreen',
				'' => 'allowfullscreen'
			],
			'allowpaymentrequest' => [
				'true' => 'allowpaymentrequest',
				'' => 'allowpaymentrequest'
			]
		] );

		if( $ob->attr( 'width' ) == '100%' ){
			$ob->attr( 'width', 800 );
		}

		if( $ob->attr( 'height' ) == '100%' ){
			$ob->attr( 'height', 530 );
		}

		// Add sandbox attributes to bypass security restrictions
		// https://github.com/ampproject/amphtml/blob/master/spec/amp-iframe-origin-policy.md
		// Since v1.1
		$sandbox = $ob->attr( 'sandbox' );
		$sandbox .= ' allow-scripts allow-same-origin';
		$sandobx = trim( $sandbox );
		$sandbox = explode( ' ', $sandbox );
		$sandbox = array_unique( $sandbox );
		$sandbox = implode( ' ', $sandbox );
		$sandobx = $ob->attr( 'sandbox', $sandbox );
	}


	$images = $dom->find( 'amp-img' );
	
	foreach( $images as $image ){
		$ob = fr_dom( $image );
		$keep_atts_func( $ob, 'src srcset sizes alt attribution layout media noloading on placeholder sizes width height class id tabindex role' );


		// Determine width and height of the image if not set
		
		$width = $ob->attr( 'width' );
		$width = str_replace( 'px', '', $width );
		$height = $ob->attr( 'height' );
		$height = str_replace( 'px', '', $height );
		
		if( !$width || !$height ){
			$src = $ob->attr( 'src' );
			$domain = fr_get_domain( $src );
			$path = '';

			foreach( $map_domains as $url => $p ){
				if( strstr( $src, $url ) ){
					$path = str_replace( $url . '/', $p, $src );
					break;
				}
			}

			if( !$path || !file_exists( $path ) ){
				if( !empty( $remote_image_sizes[$src] ) ){
					$size = $remote_image_sizes[$src];
				} else {
					$size = fr_img_remote_size( $src );
					if( $size ){
						$size = [
							'width' => $size[0],
							'height' => $size[1],
						];
						$remote_image_sizes[$src] = $size;
					}
				}
			} else if( file_exists( $path ) ) {
				$size = fr_img_get_size( $path );
			} else {
				$size = [];
			}

			if( $size ){
				if( !$width ){
					$ob->attr( 'width', $size['width'] );
					$ob->attr( 'height', $size['height'] );
				} else if ( !$height ){
					$height = round( $width / ( $size['width'] / $size['height'] ) );
					$ob->attr( 'height', $height );
				}
			}
		}

		// Only if the image is larger than the layout then make it responsive
		if( $layout_width ){
			$width = fr_dom( $image )->attr( 'width' );
			if( $width > $layout_width ){
				fr_dom( $image )->attr( 'layout', 'responsive' );
			}
		}
	}




	$images = $dom->find( 'a' );
	
	foreach( $images as $image ){

		$ob = fr_dom( $image );
		$keep_atts_func( $ob, 'download href hreflang media ping referrerpolicy rel target type class id on title dir accesskey contenteditable draggable hidden spellcheck tabindex translate' );

	}



	$videos = $dom->find( 'video' );
	
	foreach( $videos as $video ){

		$ob = fr_dom( $video );
		$width = $ob->attr( 'width' );
		if( !$width ){
			$ob->attr( 'width', '800' );
			$ob->attr( 'height', '530' );
		}
		$ob->attr( 'layout', 'responsive' );


	}

	
	$dom->find( '[style]' )->removeAttr( 'style' );
	$dom->find( 'script:not([type="application/json"]):not([type="text/plain"])' )->remove();
	$dom->find( 'style' )->remove();
	$dom->find( 'body meta' )->remove();

	$c = $dom->output();

	foreach( $replacements as $replace ){
		$c = str_replace(  $replace[0], $replace[1], $c );
	}

	return [ $c, $remote_image_sizes ];
}

function fr_amp_enqueue_style( $selector, $style ){
	if( !isset( $_ENV['amp_styles'] ) ){
		$_ENV['amp_styles'] = array();
	}
	$_ENV['amp_styles'][] = $selector . '{' . $style . '}';
}

function fr_amp_add_styles( $html = false ){
	if( !empty( $_ENV['amp_styles'] ) ){
		$styles = fr_css_minify( implode( "\n", $_ENV['amp_styles'] ) );
		$html = str_replace( '<style amp-custom>', "<style amp-custom>" . $styles, $html );
	}

	return $html;
}

function fr_is_amp(){
	return isset( $_GET['amp'] );
}