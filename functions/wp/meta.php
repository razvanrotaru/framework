<?php 
/**
 * Helper function for get_post_meta() and update_post_meta()
 * @param  string  $k  Meta Key
 * @param  integer $id Post ID
 * @param mixed $v Meta value to save
 * @return mixed
 */
function fr_m($k,$id=0,$v=false){

	if( is_object( $id ) ){
		$id = $id->ID;
	}

	if( $v !== false ){
		$id2 = $id;
		$id = $v;
		$v = $id2;
	}
	
	if( !$id ){
		$id = get_the_ID();
	}
	if( $v === false ){
		return get_post_meta( $id, $k, 1 );
	}else{
		return update_post_meta( $id, $k, $v );
	}
}



function fr_m_increment( $k, $id = 0, $val = 1 ){
	$old = (float)fr_m( $k, $id );
	$new = $old + $val;
	fr_m( $k, $new, $id );
}


/**
 * Helper function for get_user_meta() and update_user_meta()
 * @param  string  $k  Meta Key
 * @param  integer $id Post ID
 * @param mixed $v Meta value to save
 * @return mixed
 */
function fr_um($k,$id=0,$v=null){

	if( is_object( $id ) ){
		$id = $id->ID;
	}

	if( $v !== null ){
		$id2 = $id;
		$id = $v;
		$v = $id2;
	}

	if( !$id ){
		$id = fr_user();
	}
	if( $v === null ){
		return get_user_meta( $id, $k, 1 );
	}else{
		return update_user_meta( $id, $k, $v );
	}
}

function fr_meta_updated( $keys, $callback, $remove_action = false, $meta_type = 'post' ){
	if( !is_array( $keys ) ){
		$keys = array( $keys );
	}

	$answer = function( $meta_id, $object_id, $meta_key, $_meta_value ) use ( $callback, $keys, $remove_action, $meta_type ) {
		if( in_array( $meta_key, $keys ) && !fr_global( 'fr_meta_updated_disabled' ) ){
			fr_global( 'fr_meta_updated_disabled', true );
			fr_call_if_func( $callback, array( $object_id, $meta_key, $_meta_value ) );
			fr_global( 'fr_meta_updated_disabled', false );
		}
	};
	add_action( "added_{$meta_type}_meta", $answer, 10, 4 );
	add_action( "updated_{$meta_type}_meta", $answer, 10, 4 );
	add_action( "deleted_{$meta_type}_meta", $answer, 10, 4 );
}

function fr_get_post_meta( $pid ){
	$values = get_post_meta( $pid );
	$values = array_map( function( $a ){
		return reset( $a );
	}, $values );
	return $values;
}