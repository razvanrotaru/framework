<?php
/**
 * After login save favorites from session
 */
add_action( 'init', function(){
	$favs = fr_session( 'fr_add_to_favorites' );
	if( $favs && fr_user() ){
		foreach( $favs as $pid ){
			fr_add_to_favorites( $pid, get_post_type( $pid ) );
		}
		fr_session( 'fr_add_to_favorites', [] );
	}
} );



/**
 * Save favorite to user's account
 * @param  string $acf_key   ACF Key to where to save the info
 * @param  int $id           ID of the post to save
 * @param  string $post_type Post type accepted
 * @return boolean
 */
function fr_add_to_favorites( $id, $post_type, $acf_key='favorites' ){
	if( get_post_type( $id ) == $post_type ){
		$favorites = fr_get_favorites( $acf_key );
		$favorites[] = $id;
		$favorites = array_unique( $favorites );
		if( fr_user() ){
			update_field( $acf_key, $favorites, 'user_' . fr_user() );
		}else{
			fr_session( 'fr_add_to_favorites', $favorites );
		}
		return 1;
	}
}


/**
 * Remove from favorites
 * @param  string $acf_key   ACF Key to where to save the info
 * @param  int $id           ID of the post to save
 * @return boolean
 */
function fr_remove_from_favorites( $id, $acf_key='favorites' ){
	$favorites = fr_get_favorites( $acf_key );
	unset( $favorites[ array_search( $id, $favorites) ] );
	if( fr_user() ){
		update_field( $acf_key, $favorites, 'user_' . fr_user() );
	}else{
		fr_session( 'fr_add_to_favorites', $favorites );
	}
	return 1;
}


/**
 * Return favorites
 * @param  string $acf_key   ACF Key to where to save the info
 * @return  array
 */
function fr_get_favorites( $acf_key = 'favorites' ){
	$favorites = array();
	if( fr_user() ){
		$favorites = get_field( $acf_key, 'user_' . fr_user() );
	}else if( $favs = fr_session( 'fr_add_to_favorites' ) ){
		$favorites = $favs;
	}
	if( empty( $favorites ) || !is_array( $favorites ) ){
		$favorites = array();
	}
	return $favorites;
}


/**
 * Return favorites
 * @param  string $acf_key   ACF Key to where to save the info
 * @param  int $id           ID of the post to save
 * @return boolean
 */
function fr_in_favorites( $id, $acf_key = 'favorites' ){
	$favorites = fr_get_favorites( $acf_key );
	return in_array( $id, $favorites );
}