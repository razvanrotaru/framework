<?php
function fr_url_lost_password( $url ){
	add_filter( 'lostpassword_url',  function() use ( $url ) {
	    return $url;
	}, 10, 0 );
}