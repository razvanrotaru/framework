<?php
// copy the current filters
global $fr_tests;

$fr_tests = [
	'started_at' => DB_DATE_TIME,
	'memory_start' => memory_get_peak_usage( 1 ),
	'all' => [],
	'failed' => []
];


// Initiate tests
add_action('template_redirect', function(){

	global $fr_tests;

	if( !empty( $_GET['fr_doing_test'] ) ){
		ini_set( 'error_log', WP_CONTENT_DIR . '/fr_tests.log' );
	}


	// Only administrators with full privileges can do tests
	if( !current_user_can( 'manage_options' ) && !fr_is_development() ){
		return;
	}

	if( !empty( $_GET["fr_tests_stop"] ) ){
		fr_tests_stop();
		return;
	}


	if( !empty( $_GET["fr_test"] ) ){
		$file_name = $_GET["fr_test"];
		$file_name = trim( $file_name, '/' );
		$test_locations = [];
		$test_directories = [
			THEME_DIR . '/tests',
			FRAMEWORK_DIR . '/tests'
		];

		if( $file_name == 1 ){
			$test_locations = $test_directories;
		} else {

			if( $file_name == 'unit_tests' ){

				$test_locations[] = THEME_DIR . '/tests/unit-tests-v2';
				$test_locations[] = THEME_DIR . '/tests/unit-tests';
				$test_locations[] = FRAMEWORK_DIR . '/tests';

			} else {

				foreach( $test_directories as $dir ){
					$relative_dir = str_replace( THEME_DIR . '/', '', $dir );

					if( substr( $file_name, 0, strlen( $relative_dir ) ) == $relative_dir ){
						$target = $dir . substr( $file_name, strlen( $relative_dir ) );
				
						if( file_exists( $target ) ){
							$test_locations[] = $target;
							break;
						}
					}
					
					$files = fr_scan_dir( $dir, [], [ 'php' ] );

					foreach( $files as $file ){
						if( $file['file'] == $file_name . '.php' || $file['path'] == THEME_DIR . '/' . $file_name . '.php' || $file['file'] == 'test_' . $file_name . '.php' || $file['file'] == 'test-' . $file_name . '.php' ){
							$test_locations[] = $file['path'];
							break;
						}
					}
					if( $test_locations ){
						break;
					}
				}

			}
		}


		if( $test_locations ){
			fr_test_execute( $test_locations );
			$log = file_get_contents( fr_test_log_file_path() );
			$log = str_replace( 'Running tests...', '', $log );
			if( $fr_tests['failed'] ){
				fr_p( $fr_tests['failed'], 'error' );
			}
			fr_p( 'Tests done' );
			echo $log;
		} else {
			fr_p( __( 'Test not found', 'framework' ) );
		}
		fr_exit();
	}
});


function fr_test_execute( $test_folders = [] ){

	global $fr_test_filters, $wp_filter, $fr_tests;

	fr_test_log_clear();
	
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting( E_ALL & ~E_DEPRECATED );

	$fr_test_filters = $wp_filter;
	fr_start_timer( 'fr_tests' );

	foreach( $fr_test_filters as $tag => $filter ){
		$fr_test_filters [ $tag ] = clone $filter;
	}

	if( file_exists( THEME_DIR . "/tests/helpers/" ) ){
		fr_include_files( THEME_DIR . '/tests/helpers/' );
	}

	// Include tests
	foreach( $test_folders as $test_folder ){
		$res = true;
		if( strstr( $test_folder, THEME_DIR ) || strstr( $test_folder, FRAMEWORK_DIR ) ){
			$res = fr_do_tests( $test_folder );
		} else if( file_exists( THEME_DIR . "/tests/" . $test_folder ) ){
			$res = fr_do_tests( THEME_DIR . "/tests/" . $test_folder );
		}
		if( !$res ){
			break;
		}
	}

	do_action( 'init_fr_tests', 1 );

	$memory_usage = memory_get_peak_usage( 1 ) - $fr_tests['memory_start'];

	$stats = [ 
		'test_nr' => count( $fr_tests['all'] ), 
		'failed_test_nr' => count( $fr_tests['failed'] ), 
		'started_at' => $fr_tests['started_at'], 
		'time_taken' => fr_get_timer( 'fr_tests' ), 
		'memory_usage' => fr_bites_to_mb( $memory_usage ) . 'MB'
	];

	fr_test_log( $stats, '', 'before' );

	fr_tests_clean_cache();

	$stats['failed'] = $fr_tests['failed'];

	return $stats;
}


function fr_test( $test_name, $result_func, $time_limit=10, $memory_limit=500 ){

	global $fr_tests, $wp_filter, $fr_test_filters;

	fr_tests_can_continue();

	// Set a time limit to stop infinite loops
	set_time_limit($time_limit);

	// Set memory limit to stop infinite loops
	ini_set( 'memory_limit', $memory_limit . 'M' );
	
	fr_start_timer( 'fr_test' );
	$memory_start = memory_get_peak_usage( 1 );

	// do the test
	list( $expected, $operator, $result ) = $results = fr_call_if_func( $result_func );
	if( !empty( $results[3] ) ){
		$data = $results[3];
	}else{
		$data = array();
	}

	if( !empty( $results[4] ) ){
		$url = $results[4];
	}else{
		$url = "";
	}

	$file_lvl = !empty( $results[5] ) ? $results[5] : 1; 

	$failed = false;
	$duration = fr_get_timer( 'fr_test' );
	$memory_usage = memory_get_peak_usage( 1 ) - $memory_start;
	$memory = memory_get_usage( 1 );
	$json_result = $result;
	$json_expected = $expected;

	if( is_array( $json_result ) || is_object( $json_result ) ){
		$json_result = json_encode( $json_result );
	}
	if( is_array( $json_expected ) || is_object( $json_expected ) ){
		$json_expected = json_encode( $json_expected );
	}

	// check if result is valid
	if(
		( $operator === '=' && $json_expected != $json_result ) ||
		( $operator === '!=' && $json_expected == $json_result ) ||
		( $operator === '>' && $json_result <= $json_expected ) ||
		( $operator === '<' && $json_result >= $json_expected ) ||
		( $operator === '>=' && $json_result < $json_expected ) ||
		( $operator === '<=' && $json_result > $json_expected ) ||
		( $operator === 'strstr' && !strstr( $json_result, $json_expected ) ) ||
		( $operator === '!strstr' && strstr( $json_result, $json_expected ) ) ||
		( $operator === 'stristr' && !stristr( $json_result, $json_expected ) ) ||
		( $operator === '!stristr' && stristr( $json_result, $json_expected ) )
	){
		$failed = true;
	}

	if( $log = file_get_contents( ABSPATH . '/wp-content/fr_tests.log' ) ){
		$fr_test_failed = 1;
		$result = 'Logged error detected';
		$data = $log;
	}

	// store the result
	$res = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT,$file_lvl + 1);
	$info = pathinfo($res[$file_lvl]["file"]);
	$filename = $info['filename'];
	if( strstr( $info['dirname'], '/tests/' ) ){
		$dirname = explode( '/tests/', $info['dirname'] );
		$filename = $dirname[1] . '/' . $filename;
	}

	$info_data = array( 'url' => $url, 'filename' => $filename, "test" => $test_name, "expected" => $expected, "operator" => $operator, "result" => $result, "data" => $data, "duration" => round( $duration / 1000, 3 ) . "s", "memory_usage" => round( $memory_usage / 1000 / 1000, 2 ) . 'MB', 'memory' => round( $memory / 1000 / 1000, 2 ) . 'MB' );
	
	// log results
	if( $failed ){

		$fr_tests['failed'][] = $info_data;

	} else {
		$info_data = array_intersect_key( $info_data, array_flip( [ 'url', 'test' ] ) );
		fr_test_log( $info_data, 'success', 'after' );
	}

	$fr_tests['all'][] = $info_data;

	// reset filters after each test
	$wp_filter = $fr_test_filters;

	return !$failed;
}


function fr_test_log_file_url(){
	return site_url() . '/wp-content/uploads/tests_finished.html';
}

function fr_test_log_file_path(){
	return ABSPATH . 'wp-content/uploads/tests_finished.html';
}

function fr_test_set_site_pass( $user, $pass ){
	fr_global( 'fr_test_set_site_pass', $user . ':' . $pass );
}

function fr_btest( $test_name, $url, $result_func, $time_limit=3, $memory_limit=100 ){

	global $fr_tests;

	if( $fr_tests['failed'] ){
		return false;
	}

	$url = fr_get_permalink( $url );

	$result = fr_tests_get_url( $url, [] );
	if( is_array( $result ) ){
		list( $html, $data ) = $result;
	} else {
		fr_test( $test_name, function() use ( $result ) { 
			return [ false, '=', true, [ __( 'WP error when trying to get url', 'framework' ), $result ] ];
		} );
		return;
	}

	if( is_wp_error( $html ) ){
		fr_test( $test_name, function() use ( $result_func, $html, $url ){
			return array( true, '=', false, array( $html, $url ) );
		}, $time_limit, $memory_limit );
	}else{

		$results = $result_func( $html, $data );
		if( !$results ){
			$results = [ [ true, "=", true ] ];
		} else if( !is_array( $results[0] ) || count( $results[0] ) < 3 ){
			$results = [ $results ];
		}

		foreach( $results as $result ){
			$sub_test = '';
			if( isset( $result[3] ) ){
				if( is_string( $result[3] ) ){
					$sub_test = ' > ' . $result[3];
					$result[3] = $html;
				} else {
					$result[3] = array( $result[3], $html );
				}
			}else{
				$result[3] = $html;
			}
			$result[4] = $url;

			$success = fr_test( $test_name . $sub_test, function() use ( $result ){
				return $result;
			}, $time_limit, $memory_limit );

			if( !$success ){
				return false;
			}
		}
	}

	return true;
};

function fr_tests_can_continue(){
	$file = FRAMEWORK_DIR . '/fr_cancel_test.txt';
	if( file_exists( $file ) ){
		unlink( $file );
		
		fr_p( 'Tests stopped' );

		exit;
	}

	return true;
}

function fr_tests_stop(){

	set_time_limit( 150 );

	$file = FRAMEWORK_DIR . '/fr_cancel_test.txt';
	file_put_contents( $file, 1 );

	for( $x = 0; $x < 120; $x++ ){

		if( !file_exists( $file ) ){
			return true;
		}

		sleep( 1 );
	}

	return false;
}


function fr_tests_get_doc( $html ){
	include_once( FRAMEWORK_DIR . '/resources/phpQuery-onefile.php' );
	return phpQuery::newDocument( $html );
}

function fr_tests_get_url( $url, $args = array(), $is_post = 0 ){
	$timelimit = ini_get( 'max_execution_time' );
	set_time_limit( 30 );

	$url = fr_get_permalink( $url );

	$cache_key = "fr_tests_get_url_" . $url . md5( json_encode( $args ) );
	$response = fr_tests_cache( $cache_key );

	if( $response === false || $response === null ){
		$default_args = array();
		$default_args['timeout'] = 30;
		if( $is_post ){
			$default_args['method'] = 'POST';
		}

		if( fr_global( 'fr_test_set_site_pass' ) ){
			$default_args['headers'] = [
				'Authorization' => 'Basic ' . base64_encode( fr_global( 'fr_test_set_site_pass' ) )
			];
		}

		$args = array_merge( $default_args, $args );
		$url = fr_build_link( $url, [ 'fr_doing_test' => true ] );
	
		if( !empty( $args['method'] ) && $args['method'] == 'POST' ){
			$response = wp_remote_post( $url, $args );
		}else{
			$response = wp_remote_get( $url, $args );
		}

		if( !$response ){
			fr_log_debug( 'WP failed to get remote response for test', [ $url, $args, $response ] );
		}

		if( !is_wp_error($response) ){
			fr_tests_cache( $cache_key, $response );
		}else{
			set_time_limit( $timelimit );
			return new WP_Error( 'URL couldn\'t be fetched: ' . $url, array( $response ) );
		}
	}

	set_time_limit( $timelimit );

	return [ $response['body'], $response ];
}

function fr_tests_cache( $name, $data = false ){
	$name = md5( $name );
	$folder = ABSPATH . '/wp-content/cache/fr_tests/';
	fr_required_dir( $folder );

	$file = $folder . '/' . $name . '.txt';

	if( $data !== false ){
		file_put_contents( $file, json_encode( $data ) );
	}else {
		if( file_exists( $file ) ){
			return json_decode( file_get_contents( $file ), ARRAY_A );
		}else{
			return false;
		}
	}
}

function fr_tests_clean_cache(){
	$folder = ABSPATH . '/wp-content/cache/fr_tests/';
	$files = fr_scan_dir( $folder );
	foreach( $files as $file ){
		unlink( $file['path'] );
	}
}

function fr_do_tests( $tests_folder ){

	if( file_exists( $tests_folder ) ){
		if( is_dir( $tests_folder ) ){
			$tests = fr_scan_dir( $tests_folder, array( 'manual-tests', 'helpers.php' ), array( 'php' ) );
			foreach( $tests as $test ){
				include_once( $test['path'] );
			}
		}else{
			include_once( $tests_folder );
		}
	}

	return true;
}


function fr_test_mockup_postmeta( $posts ){
	add_filter('get_post_metadata',function( $metadata, $object_id, $meta_key, $single ) use ( $posts ) {
		foreach( $posts as $pid => $post ){
			if( $object_id == $pid ){
				foreach( $post as $key => $value ){
					if( $meta_key == $key ){
						$metadata = $value;
						break;
					}
				}
			}
		}

		return $metadata;
	},1000000,4);

	fr_test_mockup_acf( $posts );
}


function fr_test_mockup_acf( $posts ){
	add_filter('acf/load_value',function(  $value, $post_id, $field ) use ( $posts ) {

		foreach( $posts as $pid => $post ){
			if( $post_id == $pid ){
				foreach( $post as $k => $v ){
					if( $field['name'] == $k ){
						$value = $v;
						break;
					}
				}
			}
		}

		return $value;
	},1000000,4);
}


function fr_test_mockup_usermeta( $users ){
	add_filter('get_user_metadata',function( $metadata, $object_id, $meta_key, $single ) use ( $users ) {
		global $wpdb;
		foreach( $users as $uid => $user ){
			if( $object_id == $uid ){
				foreach( $user as $key => $value ){
					if( $key == 'roles' ){
						$key = $wpdb->get_blog_prefix() . 'capabilities';
					}
					if( $meta_key == $key ){
						$metadata = $value;
						break;
					}
				}
			}
		}

		return $metadata;
	},1000000,4);

	add_filter( 'fr_user', function( $d, $uid ) use ( $users ) {
		foreach( $users as $uid2 => $user ){
			if( $uid2 == $uid ){
				foreach( $user as $key => $value ){
					if( is_object( $d ) ){
						$d->$key = $value;
					}else{
						$d[$key] = $value;
					}
				}
			}
		}
		return (object)$d;
	}, 1000, 2 );
}


function fr_test_mockup_post( $posts ){
	add_filter('the_title', function( $title, $id ) use ( $posts ) {
		foreach( $posts as $pid => $post ){
			if( $id == $pid ){
				if( isset( $post['post_title'] ) ){
					$title = $post['post_title'];
				}
			}
		}

		return $title;
	},1000000,2);
}


function fr_test_mockup_current_post( $post_data ){
	global $post;
	$post = (object)$post_data;
}

function fr_test_get_next_user(){
	global $fr_test_get_next_user;
	if( empty( $fr_test_get_next_user ) ){
		$fr_test_get_next_user = 0;
	}
	$users = new WP_User_Query(array( 'number'=>1, 'fields'=>'ids','offset'=>$fr_test_get_next_user));

	$fr_test_get_next_user++;
	$results = $users->results;
	return reset( $results );
}

function fr_test_get_next_post(){
	global $fr_test_get_next_post;
	if( empty( $fr_test_get_next_post ) ){
		$fr_test_get_next_post = 0;
	}
	$posts = new WP_Query(array( 'posts_per_page'=>1,'post_type'=>'any', 'fields'=>'ids','offset'=>$fr_test_get_next_post));

	$fr_test_get_next_post++;
	$posts = $posts->posts;
	return reset( $posts );
}

function fr_test_log_clear(){
	file_put_contents( fr_test_log_file_path(), 'Running tests...' );
	file_put_contents( ABSPATH . '/wp-content/fr_tests.log', '' );
}

function fr_test_log( $contents, $type = 'success', $position = 'after' ){

	$log_file = fr_test_log_file_path();

	if( empty( $_ENV['test_index'] ) ){
		$_ENV['test_index'] = 0;
	}
	$_ENV['test_index']++;

	$contents = array( 'date' => date( 'Y-m-d H:i:s' ) . ' ' . $_ENV['test_index'] ) + $contents;
	if( $type == 'success' ){
		$contents = implode( ' ', $contents );
	}

	ob_start();
		fr_p( $contents, $type );
	$contents = ob_get_clean();

	if( $position == 'after' ){
		file_put_contents( $log_file, $contents, FILE_APPEND );
	} else {
		$c = file_get_contents( $log_file );
		file_put_contents( $log_file, $contents . $c );
	}
}

function fr_test_get_log(){
	return file_get_contents( ABSPATH . '/wp-content/tests.log' );
}

/*
Function to do tests while developing code
 */
function fr_dev_test( $func, $vars, $operator, $expected ){
	fr_test( 'dev_test_' . fr_global( 'fr_dev_test_index', 1, 1 ), function() use ( $func, $vars, $operator, $expected ){ 
		return [ $expected, $operator, fr_call_if_func( $func, [ $vars ] ), $vars ];
	} );
}

function fr_test_file( $name ){
	$files = fr_scan_dir( THEME_DIR . '/tests', [], [ 'php' ] );

	foreach( $files as $file ){
		if( $file['file'] == $name . '.php' ){
			fr_test_execute( $file['path'] );
		}
	}

}


/*
Must add dependecy support for Class::method(), Class:var, Class()->method(), Class()->var
Must add support for class unit tests
 */
function fr_unit_test( $function, $options ){

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	$returns = [];
	$default = [
		'args' => [],
		'tests' => [],
		'dependencies' => [],
		'class_dependecies' => []
	];
	fr_global( 'fr_unit_test_result', [] );

	$option_groups = func_get_args();
	array_shift( $option_groups  );

	foreach( $option_groups as $options ){

		$options = array_merge( $default, $options );

		if( isset( $options['test'] ) ){
			if( !is_array( $options['test'] ) ){
				$options['test'] = [ $options['test'] ];
			}
			$options['tests'][] = $options['test'];
		}

		extract( $options );

		fr_global( 'fr_unit_test_dependancies', $dependencies );
		fr_global( 'fr_unit_test_class_dependancies', $class_dependecies );

		$id = fr_global( 'fr_unit_test_id_' . $function, 1, 1 );
		if( !is_array( $args ) ){
			$args = [ $args ];
		}

		$reflection = new ReflectionFunction( $function );
		if( !method_exists( $reflection, 'getFileName' ) ){
			fr_p( __( 'Can\'t find function: ' . $function, 'ab' ) );
			return;
		}

		$code = file_get_contents( $reflection->getFileName() );
		$code = explode( "\n", $code );
		$function_code = array_slice( $code, $reflection->getStartLine() - 1, $reflection->getEndLine() - $reflection->getStartLine() + 1 );
		$function_code = implode( "\n", $function_code );

		// Adjust the function code
		// Normalize
		$function_code = str_replace( "\t", " ", $function_code );
		// Change the function name to be unique
		$new_function_name = $function . '_UNIT_TEST_' . $id;
		$function_code = str_replace( ' ' . $function . '(', ' ' . $new_function_name . '(', $function_code );
		// Insert dependencies
		foreach( $dependencies as $name => $func ){
			if( strstr( $name, '|' ) ){
				$name = strstr( $name, '|', 1 );
			}
			$function_code = str_replace( ' ' . $name . '(', ' fr_unit_test_dependancy( "' . $name . '",', $function_code );
			$function_code = str_replace( '( )', '()', $function_code );
			$function_code = str_replace( 'fr_unit_test_dependancy( "' . $name . '",)', 'fr_unit_test_dependancy( "' . $name . '" )', $function_code );
		}
		foreach( $class_dependecies as $name => $func ){
			if( strstr( $name, '|' ) ){
				$name = strstr( $name, '|', 1 );
			}
			$function_code = str_replace( ' new ' . $name . '(', ' new fr_unit_test_dependancy( "' . $name . '",', $function_code );
			$function_code = str_replace( '( )', '()', $function_code );
			$function_code = str_replace( 'new fr_unit_test_dependancy( "' . $name . '",)', 'new fr_unit_test_dependancy( "' . $name . '" )', $function_code );
		}

		$test_folder = ABSPATH . '/wp-content/cache/fr_tests/';
		fr_required_dir( $test_folder );
		$test_file = $test_folder . '/unit_test.php';

		$file_code = "<?php\n" . $function_code;
		file_put_contents( $test_file, $file_code );

		include( $test_file );

		$get = $_GET;
		$post = $_POST;
		if( isset( $options['dependencies']['GET'] ) ){
			$_GET = $options['dependencies']['GET'];
		} else {
			$_GET = [];
		}
		if( isset( $options['dependencies']['POST'] ) ){
			$_POST = $options['dependencies']['POST'];
		} else {
			$_POST = [];
		}

		// TODO: Test the function in exec() to see if there are any fatal errors
		ob_start();
		$return = call_user_func_array( $new_function_name, $args );
		$html = ob_get_clean();

		$_GET = $get;
		$_POST = $post;


		if( $html ){
			$return = $html;

			// Test for notices
			fr_test( $function . ' unit test ' . $id . ' - PHP Notice check', function() use ( $function, $args, $dependencies, $html ){ 
				return [ '', '=', fr_extract( $html, "<br />\n<b>Notice</b>:", '<br />' ), [
					'function' => $function,
					'args' => $args,
					'dependencies' => $dependencies
				] ];
			} );

			// Test for warnings
			fr_test( $function . ' unit test ' . $id . ' - PHP Warning check', function() use ( $function, $args, $dependencies, $html ){ 
				return [ '', '=', fr_extract( $html, "<br />\n<b>Warning</b>:", '<br />' ), [
					'function' => $function,
					'args' => $args,
					'dependencies' => $dependencies
				] ];
			} );
		}

		$result = fr_global( 'fr_unit_test_result' );

		if( $result ){
			$return = array_merge( [ 'return' => $return ], $result );
		}

		foreach( $tests as $k => $test ){

			$test_name = $function . ' unit test ' . $id;

			if( !empty( $options['test_name'] ) ){
				$test_name .= ' - ' . $options['test_name'];
			}

			fr_test( $test_name, function() use ( $function, $args, $dependencies, $test, $return ){ 

				if( !isset( $test[1] ) ){
					$test[1] = '=';
				}

				return [ $test[0], $test[1], $return, [
					'function' => $function,
					'args' => $args,
					'dependencies' => $dependencies
				] ];
			} );
		}

		$returns[] = $return;
	}

	return $returns;
}


function fr_unit_test_dependancy( $function ){
	$dependencies = fr_global( 'fr_unit_test_dependancies' );
	$res = false;
	$var_index = false;
	$args = func_get_args();
	array_shift( $args );

	if( !isset( $dependencies[$function] ) ){
		for( $x = 0; $x < 10; $x++ ){
			if( isset( $dependencies[$function . '|' . $x] ) ){
				$function = $function . '|' . $x;
				$var_index = $x;
			} 
		}
	}

	if( isset( $dependencies[$function] ) ){
		if( is_callable( $dependencies[$function] ) ){
			$res = call_user_func_array( $dependencies[$function], $args );
		} else if( $var_index !== false && is_array( $dependencies[$function] ) ){
			$key = $args[$var_index];
			if( isset( $dependencies[$function][$key] ) ){
				$res = $dependencies[$function][$key];
			}
		} else {
			$res = $dependencies[$function];
		}
	}

	return $res;
}


class fr_unit_test_dependancy{
	var $args = [];
	var $var_index = false;
	var $function;
	var $dependencies;

	function __construct( $function ){ 
		$args = func_get_args();
		array_shift( $args );
		$dependencies = fr_global( 'fr_unit_test_class_dependancies' );
		$this->function = $function;
		$this->dependencies = $dependencies;

		if( isset( $dependencies[$function] ) ){
			if( is_callable( $dependencies[$function] ) ){
				return fr_call_if_func( $dependencies[$function], $params );
			} else {
				return $dependencies[$function];
			}
		}
	}

	function __call( $func, $params ){
		if( isset( $this->dependencies[$this->function . '|' . $func] ) ){
			$val = $this->dependencies[$this->function . '|' . $func];
			if( is_callable( $val ) ){
				return fr_call_if_func( $val, $params );
			} else {
				return $val;
			}
		}
	}
}


function fr_unit_test_add_to_result( $key, $val ){
	fr_global( 'fr_unit_test_result', [ $key => $val ], ARRAY_A, true );
}


function fr_unit_test_get_dependencies( $function ){
	$dependencies = [];

	$reflection = new ReflectionFunction( $function );
	if( !method_exists( $reflection, 'getFileName' ) ){
		fr_p( __( 'Can\'t find function: ' . $function, 'ab' ) );
		return;
	}

	$code = file_get_contents( $reflection->getFileName() );
	$code = explode( "\n", $code );
	$function_code = array_slice( $code, $reflection->getStartLine(), $reflection->getEndLine() - $reflection->getStartLine() - 1 );
	$function_code = implode( "\n", $function_code );

	// Adjust the function code
	// Normalize
	$function_code = str_replace( "\t", " ", $function_code );
	$function_code = str_replace( "(", "( ", $function_code );
	$function_code = str_replace( "&&", "&& ", $function_code );
	$function_code = str_replace( "||", "|| ", $function_code );
	$function_code = str_replace( "!", "! ", $function_code );
	$function_code = str_replace( "=", "= ", $function_code );


	// Extract functions
	// Remove names that are not functions
	$functions = fr_single_loop_extract( $function_code, ' ', '(' );
	foreach( $functions as $k => $function_name ){
		if( !is_callable( $function_name ) ){
			unset( $functions[$k] );
		}
	}

	$functions = array_unique( $functions );

	foreach( $functions as $k => $function_name ){
		$reflection = new ReflectionFunction( $function_name );

		// Remove function if it can't be found
		if( !method_exists( $reflection, 'getFileName' ) ){
			unset( $functions[$k] );
			continue;
		}

		// Remove functions that are not user defined
		if( !$reflection->isUserDefined() ){
			unset( $functions[$k] );
			continue;
		}
	}

	// Extract classes
	$classes = fr_single_loop_extract( $function_code, ' new ', '(' );
	$classes = array_merge( $classes, fr_single_loop_extract_reverse( $function_code, ' ', '( )->' ) );
	$classes = array_unique( $classes );

	$dependencies['functions'] = array_values( $functions );
	$dependencies['classes'] = array_values( $classes );

	return $dependencies;
}



function fr_test_dev(){
	$r = debug_backtrace( 0, 2 );

	$target = $r[1];
	$target['file'] = $r[0]['file'];
	$target['line'] = $r[0]['line'];

	$code = file_get_contents( $target['file'] );

	if( $target['function'] == '{closure}' ){
		$target['function'] = 'closure';
		$code_lines = explode( "\n", $code );
		$function = array_slice( $code_lines, $target['line'] );
		$function = implode( "\n", $function );
		$function = explode( "\n}", $function );
		$function = reset( $function );
		$args = $code_lines[ $target['line'] - 2 ];
		$args = strstr( $args, 'function(' );
		$args = substr( $args, 9 );
		$args = strstr( $args, '{', 1 );
		$args = trim( substr( trim( $args ), 0, -1 ) );
	} else {
		$function = explode( "function " . $target['function'] . '(', $code );
		$space = explode( "\n", $function[0] );
		$space = end( $space );

		$function = end( $function );
		$args = strstr( $function, '{', 1 );
		$args = trim( substr( trim( $args ), 0, -1 ) );
		$function = strstr( $function, '{' );
		$function = substr( $function, 1 );
		$function = explode( "\n" . $space . "}", $function );
		$function = reset( $function );
		$function = str_replace( 'fr_test_dev();', '', $function );
		$function = trim( $function, "\n" );
	}

	$arg_names = [];
	$args = trim( $args );
	$args_parts = str_replace( ',', ' ,', $args );
	$args_parts = str_replace( '=', ' , =', $args_parts );
	$args_parts = explode( '$', $args_parts );
	foreach( $args_parts as $k => $v ){
		if( $k ){
			if( strstr( $v, ' ' ) ){
				$arg_names[] = strstr( $v, ' ', 1 );
			} else {
				$arg_names[] = $v;
			}
		}
	}

	$output = htmlentities( $function );

	$lines = explode( "\n", $function );
	$wait = [];
	$x = 0;
	foreach( $lines as $k => $line ){
		$output = fr_test_dev_add_tooltip( $output, $k, "\t", $x . '|' . $k, 'fr_dev_tab' );

		// if( substr( $line, -1, 1 ) == ';' ){
		// 	$lines[$k] = $line = $line . 'fr_test_dev_output( "' . $x . '|' . $k . '", "" );';
		// }
		if( $line && ( substr( trim( $line ), 0, 1 ) == '$' || $wait ) ){
			$var_name = strstr( trim( $line ), '=', 1 );
			if( $wait ){
				 if( strstr( $line, ';' ) ){
				 	list( $var_name, $x ) = $wait;
				 	$x--;
				 	$wait = [];
				 } else {
				 	continue;
				 }
			}
			$x++;
			$output = fr_test_dev_add_tooltip( $output, $k, $var_name . '=', $x );
			if( !strstr( $line, ';' ) ){
				$wait = [ $var_name, $x ];
				continue;
			}
			
			$parts = explode( ';', $line );
			$last = array_pop( $parts );
			$parts = implode( ';', $parts );
			$var_name = trim( $var_name );
			if( substr( $var_name, -2, 2 ) == '[]' ){
				$var_name = 'end(' . substr( $var_name, 0, -2 ) . ')';
			}
			$new_line = $parts . '; fr_test_dev_output( ' . $x . ', ' . trim( $var_name ) . ' );' . $last;
			$lines[$k] = $new_line;
		}
	}
	$function = implode( "\n", $lines );

	$new_code = "<?php\nfunction fr_dev_test_code(" . $args . "){" . $function . "\n}";

	$test_file = WP_CONTENT_DIR . '/cache/fr_tests/test_dev.php';
	file_put_contents( $test_file, $new_code );
	include( $test_file );

	fr_global( 'fr_dev_output', $output );
	$return = call_user_func_array( "fr_dev_test_code", $target['args'] );
	$return = fr_test_dev_var_for_display( $return );

	$args = array_combine( array_slice( $arg_names, 0, count( $target['args'] ) ), $target['args'] );
	$output = fr_test_dev_add_text_with_tooltip( 'input', $args ) . "\n\n";
	$output .= fr_global( 'fr_dev_output' );
	$output .= "\n\n" . fr_test_dev_add_text_with_tooltip( 'return', $return );

	?>"></a></div></div></div></div></div></div></div></div></div></div></div></div></div>
	<style>
		body {
			overflow: hidden;
		}
		.fr_dev_output,
		.fr_dev_values {
			background-color: black;
			color: white;
			position: fixed;
			top: 0;
			left: 0;
			width: 50%;
			height: 100%;
			z-index: 10000;
			font-size: 18px;
			overflow: auto;
		}
		.fr_dev_output code,
		.fr_dev_values code {
			background-color: transparent;
		}
		.fr_dev_output .fr_dev_tooltip_value {
			position: fixed;
		    background-color: green;
		    color: white;
		    width: 100%;
		    left: 0;
		    top: 0;
    		display: none;
    		height: 100%;
    		overflow: auto;
    		z-index: 1;
		}
		.fr_dev_output .fr_dev_tooltip.fr_dev_active {
			cursor: pointer;
			color: yellow;
		}
		.fr_dev_output .fr_dev_tooltip.fr_dev_show_val {
			color: green;
		}
		.fr_dev_tab.fr_dev_active {
			background-color: rgba( 0, 255, 0, 0.3 );
		}
		.fr_dev_values {
			left: auto;
			right: 0;
			background-color: green;
		}
	</style>
	<div class="fr_dev_output"><pre><code><?php echo $output; ?></code></pre></div>
	<div class="fr_dev_values"><pre><code></code></pre></div>
	<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>

	<script>
		var $ = jQuery;
		$( '.fr_dev_tooltip.fr_dev_active' ).click( function(){
			$( '.fr_dev_values code' ).text( $( this ).find( '.fr_dev_tooltip_value' ).text() );
			$( '.fr_dev_show_val' ).removeClass( 'fr_dev_show_val' );
			$( this ).addClass( 'fr_dev_show_val' );
		} );
	</script>
	<?php

	exit;
}

function fr_test_dev_add_tooltip( $output, $line_nr, $string, $x, $class = '' ){
	$output = explode( "\n", $output );
	$output[$line_nr] = str_replace( $string, '<span class="fr_dev_tooltip ' . $class . ' ||' . $x . '-active||">' . $string . "<span class='fr_dev_tooltip_value'>||" . $x . "||</span></span>", $output[$line_nr] );
	$output = implode( "\n", $output );
	return $output;
}
function fr_test_dev_add_text_with_tooltip( $string, $value ){
	$output = '<span class="fr_dev_tooltip fr_dev_active">' . $string . "<span class='fr_dev_tooltip_value'>" . htmlentities( print_r( $value, 1 ) ) . "</span></span>";
	return $output;
}

function fr_test_dev_output( $x, $value ){
	$output = fr_global( 'fr_dev_output' );
	$value = fr_test_dev_var_for_display( $value );
	$value = print_r( $value, 1 );
	$output = str_replace( '||' . $x . '||', htmlentities( $value ), $output );
	$output = str_replace( '||' . $x . '-active||', 'fr_dev_active', $output );
	fr_global( 'fr_dev_output', $output );
}

function fr_test_dev_var_for_display( $value ){
	if( $value === FALSE ){
		$value = 'FALSE';
	} else if( $value === NULL ){
		$value = 'NULL';
	} else if( $value === '' ){
		$value = 'EMPTY';
	}
	return $value;
}







class fr_browser_tests {

	var $modules = [];
	var $tests = [];

	function __construct( $modules, $tests, $data ){
		$this->add_modules( $modules );
		$this->tests = $this->add_tests( $tests );
		$this->data = $data;

		if( !fr_is_ajax() && fr_session( 'fr_browser_tests' ) ){
			$this->execute( fr_session( 'fr_browser_tests' ) );
		}

		fr_GET_trigger( 'start_browser_tests', function( $val ){ 
			if( $val == 'start' ){
				$this->start();
			}
			if( $val == 'stop' ){
				$this->stop();
			}
			if( $val == 'results' ){
				$this->view_results();
			}
			exit;
		} );

		fr_ajax_action_in_object( $this );
		$this->file_upload_support( $data );
	}

	function file_upload_support( $data ){
		foreach( $_POST as $k => $v ){
			if( is_string( $v ) && substr( $v, 0, 13 ) == 'fr_test_file=' ){
				$files = substr( $v, 13 );
				$files = explode( ',', $files );
				foreach( $files as $file ){
					if( isset( $data['files'][$file] ) ){
						$file = $data['files'][$file];

						$tmp_file = @tempnam( sys_get_temp_dir(), 'fr-tmp-' );
						copy( $file, $tmp_file );

						$_FILES[$k] = [
							'name' => basename( $file ),
							'type' => fr_file_mime_type( $file ),
							'tmp_name' => $tmp_file,
							'error' => 0,
							'size' => filesize( $file )
						];
					}
				}
			}
		}

		if( isset( $this->data['gform_files'] ) ){
			$data = $this->data;
			add_filter( 'gform_field_validation', function( $validation, $fields, $form, $field ) use ( $data ){
				foreach( $data['gform_files'] as $file_data ){
					//fr_p( [ $field->id == $file_data['field'], $form['id'] == $file_data['form'], isset( $data['files'][$file_data['file']] ) ] );
					if( $field->id == $file_data['field'] && $form['id'] == $file_data['form'] && isset( $data['files'][$file_data['file']] ) ){
						$file = $data['files'][$file_data['file']];
						$dir = fr_gf_get_upload_form_directory( $form['id'] );
						$tmp_file = @tempnam( $dir, 'fr-tmp-' );
						$new_tmp_file = fr_file_change_extension( $dir . '/../../tmp/' . basename( $tmp_file ), fr_file_ext( $file ) );
						copy( $file, $new_tmp_file );

						$current_files = json_decode( stripslashes( $_POST['gform_uploaded_files'] ), 1 );
						$current_files = fr_make_array( $current_files );

						$current_files['input_' . $field->id] = [
							[
								'temp_filename' => basename( $new_tmp_file ),
								'uploaded_filename' => basename( $file )
							]
						];

						$_POST['gform_uploaded_files'] = json_encode( $current_files );
						$validation = [
							'is_valid' => true,
							'message' => ''
						];
					}
				}
				return $validation;
			}, -1, 4 );
		}
	}

	function add_tests( $tests ){
		foreach( $tests as $name => $test ){
			$tests[$name] = $this->recursive_tests( $test );
		}
		return $tests;
	}

	function recursive_tests( $tests, $parents = [] ){
		$all = [];
		foreach( $tests as $test => $subtests ){
			if( !is_array( $subtests ) ){
				$all[] = array_merge( $parents, [ $subtests ] );
			} else {
				$new = array_merge( $parents, [ $test ] );
				$res = $this->recursive_tests( $subtests, $new );
				$all = array_merge( $all, $res );
			}
		}
		return $all;
	}

	function add_modules( $modules ){

		// Normalize modules
		$new = [];
		foreach( $modules as $k => $groups ){

			$new[$k] = [];

			if( !is_numeric( key( $groups ) ) ){
				$groups = [ $groups ];
			}

			
			if( isset( $groups[0]['go_to'] ) && is_array( $groups[0]['go_to'] ) ){
				$tmp = [];
				foreach( $groups[0]['go_to'] as $link ){
					$conditions = $groups[0];
					unset( $conditions['go_to'] );
					$tmp = array_merge( $tmp, [ [ 'go_to' => $link ] ], $conditions );
				}
				$groups = $tmp;
			}


			foreach( $groups as $group ){
				if( isset( $group['go_to'] ) ){
					$group['go_to'] = get_permalink( $group['go_to'] );
				}

				if( isset( $group['if'] ) && !is_numeric( key( $group['if'] ) ) ){
					$group['if'] = [ $group['if'] ];
				}
				if( isset( $group['then'] ) ){
					if( !is_numeric( key( $group['then'] ) ) ){
						$group['then'] = [ $group['then'] ];
					}
					foreach( $group['then'] as $k2 => $v ){
						foreach( $v as $k3 => $v3 ){
							if( is_numeric( $k3 ) ){
								$group['then'][$k2]['action'] = $v3;
								unset( $group['then'][$k2][$k3] );
							}
						}
					}
				}
				if( isset( $group[0] ) ){
					$group['action'] = $group[0];
					unset( $group[0] );
				}
				$new[$k][] = $group;
			}
		}

		$this->modules = $new;
	}

	function start(){
		fr_session( 'fr_browser_tests', [ 0,0,0,0 ] );
		fr_test_log_clear();
	}

	function execute( $test ){
		$test = $test;
		fr_add_actions( [ 'wp_head', 'admin_head', 'login_enqueue_scripts' ], function() use ( $test ){
			list( $module, $test ) = $this->get_current_module( $test );
			?>
			<script id="fr_browser_tests" type="application/json">
				<?php echo json_encode( [ 'module' =>  $module, 'cookie' => $test ] ) ?>
			</script>
			<?php if( fr_admin_is_login() ){ ?>
				<script
			  src="https://code.jquery.com/jquery-3.4.1.min.js"
			  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
			  crossorigin="anonymous"></script>
			<?php } ?>
			<?php if( fr_admin_is_login() || is_admin() ){ ?>
				<script src="<?php echo FRAMEWORK_URL ?>/js/script.js"></script>
			<?php } ?>
			<?php 
		}, 100 );
	}

	function get_current_module( $test ){
		$tests = array_values( $this->tests );

		if( isset( $tests[$test[0]][$test[1]][$test[2]] ) ){
			$name = $tests[$test[0]][$test[1]][$test[2]];
			if( !isset( $this->modules[$name][$test[3]] ) ){
				$test[3] = 0;
				$test[2]++;
			}
		}

		if( !isset( $tests[$test[0]][$test[1]][$test[2]] ) ){
			$test[3] = 0;
			$test[2] = 0;
			$test[1]++;

			if( !isset( $tests[$test[0]][$test[1]] ) ){
				$test[1] = 0;
				$test[0]++;
			}

			// All test are finished
			if( !isset( $tests[$test[0]] ) ){
				$this->stop();
				return;
			}
		}

		$name = $tests[$test[0]][$test[1]][$test[2]];
		$module = $this->modules[$name];

		return [ $module, $test ];
	}

	function get_module_name( $test ){
		$tests = array_values( $this->tests );
		return $tests[$test[0]][$test[1]][$test[2]];
	}

	function view_results(){
		fr_p( fr_test_get_log() );
	}

	function stop(){
		fr_session( 'fr_browser_tests', [] );
	}

	function ajax_fr_browser_tests_save( $data ){
		$data['cookie'] = array_map( 'intval', $data['cookie'] );
		fr_session( 'fr_browser_tests', $data['cookie'] );
		list( $module, $test ) = $this->get_current_module( $data['cookie'] );

		// Log
		$log = $data;
		$log['current_url'] = $data['data']['current_url'];
		$log['current_module'] = $this->get_module_name( $data['data']['current_module'] );
		$log['executed_steps'] = $data['data']['executed_steps'];
		$log['js_errors'] = $data['data']['js_errors'];
		$log['next_test'] = $test;
		unset( $log['action'], $log['data'] );
		fr_test_log( $log );

		if( $module ){
			echo json_encode( [ 'module' =>  $module, 'cookie' => $test ] );
		}
		exit;
	}
	function ajax_fr_browser_tests_get( $data ){
		fr_test_log( $data );
	}
}





function fr_test_page_generation_time( $nr_of_tests, $links ){
	set_time_limit( 600 );

	fr_start_timer( 'fr_test_pages_generation_time' );

	$res = [];
	foreach( $links as $link ){
		$link = fr_get_permalink( $link );
		for( $x = 0; $x < $nr_of_tests; $x++ ){
			$data = fr_curl( $link );
			if( !isset( $res[$link] ) ){
				$res[$link] = [];
			}
			$res[$link][]= $data['info']['starttransfer_time'];
		}
	}

	foreach( $res as $k => $v ){
		$res[$k] = [
			'average' => round( array_sum( $v ) / count( $v ), 2 ),
			'max' => max( $v ),
			'min' => min( $v )
		];
		$res[$k]['varation'] = round( 100 / $res[$k]['average'] * max( $res[$k]['average'] - $res[$k]['min'], $res[$k]['max'] - $res[$k]['average'] ), 2 ) . '%';
	}

	$all_times = array_column( $res, 'average' );
	$res['total_average'] = round( array_sum( $all_times ) / count( $all_times ), 3 );
	$res['test_duration'] = round( fr_get_timer( 'fr_test_pages_generation_time' ) );
	$res['number_of_tests'] = $nr_of_tests * count( $links );
	
	return $res;
}




function fr_test_check_if_file_exists( $url, $page = false ){

	if( fr_url_is_base64( $url ) ){
		return true;
	}

	$url = fr_relative_url_to_absolute( $url, $page );

	if( fr_is_link_external( $url ) ){
		return fr_url_external_exists( $url );
	}

	$path = fr_url_to_path( $url );

	return file_exists( $path );
}
















