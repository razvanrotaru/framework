<?php
function fr_security( $default_redirects, $access ){
	global $wp_roles;

	$redirect = false;
	$message = false;
	foreach( $access as $key => $rule ){

		if( is_null( $rule['condition'] ) ){
			continue;
		}
		if( is_callable( $rule['condition'] ) ){
			if( !$rule['condition']() ){
				continue;
			}
		} else if( is_bool( $rule['condition'] ) ){
			if( !$rule['condition'] ){
				continue;
			}
		} else if( is_array( $rule['condition'] ) ){
			if( !in_array( get_the_ID(), $rule['condition'] ) ){
				continue;
			}
		} else if( !is_page( $rule['condition'] ) ){
			continue;
		}

		$rule['allow'] = fr_call_if_func( $rule['allow'] );

		if( is_bool( $rule['allow'] ) ){
			if( $rule['allow'] === false ){
				$redirect = true; 
			}
		} else {
			if( is_string( $rule['allow'] ) && $rule['allow'] == 'visitor' && !fr_user() ){
				continue;
			}
			if( !is_array( $rule['allow'] ) ){
				$rule['allow'] = [ $rule['allow'] ];
			}
			if( !fr_user() || !array_intersect( fr_user( 'roles' ), $rule['allow'] ) ){
				$redirect = true;
			}
		}
		if( $redirect === true ){
			if( !empty( $rule['redirect'] ) ){
				$redirect = $rule['redirect'];
			}
			if( !empty( $rule['redirect_if_user'] ) && fr_user() ){
				$redirect = $rule['redirect_if_user'];
			}
			if( !empty( $rule['message'] ) ){
				$message = $rule['message'];
			}
			break;
		}
	}

	if( $redirect ){
		if( is_bool( $redirect ) ){

			$roles = fr_user( 'roles' );

			if( $roles ){
				$matches = array_intersect_key( $default_redirects, array_flip( $roles ) );
			} else {
				$matches = [];
			}

			if( $matches ){
				$redirect = fr_call_if_func( reset( $matches ) );
			} else if( isset( $default_redirects['visitor'] ) && !fr_user() ){
				$redirect = fr_call_if_func( $default_redirects['visitor'] );
			} else if( isset( $default_redirects['default'] ) ){
				$redirect = fr_call_if_func( $default_redirects['default'] );
			} else {
				$redirect = get_bloginfo( 'url' );
			}
		}

		$redirect = fr_get_permalink( $redirect );
		$redirect_without_hash = explode( '#', $redirect );
		$redirect_without_hash = reset( $redirect_without_hash );

		// Redirect only if it's not the same link
		if( $redirect_without_hash && $redirect_without_hash != fr_get_current_url() ){
			if( $message ){
				fr_message( $message );
			}
			fr_wpml_redirect( $redirect );
		}
	}
}