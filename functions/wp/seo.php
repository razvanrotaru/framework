<?php 
function fr_seo_disable_wp_emojicons(){
  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  add_filter( 'emoji_svg_url', '__return_false' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', function( $plugins ) {
    if ( is_array( $plugins ) ) {
      return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
      return array();
    }
  });
}

function fr_seo_enable_cdn( $domain ){

  add_filter( 'wp_get_attachment_image_src', function( $image ) use ( $domain ){
    $image = str_replace( get_bloginfo( 'url' ) . '/wp-content/', 'https://' . $domain . '/wp-content/', $image );
    return $image;
  });

  add_action('wp_head',function(){
    ob_start();
  },0);

  add_action('wp_footer',function() use( $domain ){
    echo $_ENV['fr_seo_enable_cdn_content'] = ob_get_clean();
    $c = str_replace( get_bloginfo( 'url' ) . '/wp-content/', 'https://' . $domain . '/wp-content/', $c );
    echo $c;
  },9999 );
}

function fr_seo_disable_embeds(){
  add_action( 'wp_footer', function(){
    wp_deregister_script( 'wp-embed' );
  } );
}



function fr_seo_remove_script_version(){
  add_filter( 'style_loader_src', function( $src ) {
      if ( strpos( $src, 'ver=' ) )
          $src = remove_query_arg( 'ver', $src );
      return $src;
  }, 9999 );
  add_filter( 'script_loader_src', function( $src ) {
      if ( strpos( $src, 'ver=' ) )
          $src = remove_query_arg( 'ver', $src );
      return $src;
  }, 9999 );
}

function fr_seo_timestamp_script_version(){
  add_filter( 'style_loader_src', function( $src ) {
      $src = add_query_arg( 'ver', NOW, $src );
      return $src;
  }, 9999 );
  add_filter( 'script_loader_src', function( $src ) {
      $src = add_query_arg( 'ver', NOW, $src );
      return $src;
  }, 9999 );
}

function fr_seo_async_all_scripts( $exclude = [], $only = [], $type = 'js', $mode = 'defer' ){
  if( is_admin() ){
    return;
  }
  add_filter( 'script_loader_tag', function( $tag, $handle ) use ( $exclude, $only, $type, $mode ) {
      if ( FALSE === strpos( $tag, '.' . $type ) ) {
          return $tag;
      }
      foreach( $exclude as $v ){
        if( strstr( $tag, $v ) ){
          return $tag;
        }
      }
      if( $only ){
        $ok = false;
        foreach( $only as $v ){
          if( strstr( $tag, $v ) ){
            $ok = true;
          }
        }
        if( !$ok ){
          return $tag;
        }
      }
      return str_replace( ' src', ' ' . $mode . '="' . $mode . '" src', $tag );
  }, 10, 2 );
}

function fr_seo_lazy_load_scripts( $preload = [] ){
  if( is_admin() ){
    return;
  }
  add_action( 'init', function() use ( $preload ) { 
    ob_start( function( $html ) use ( $preload ) { 
      $dom = new fr_dom ( $html );

      $elements = $dom->find( 'link[rel=stylesheet][defer]' );
      $elements->each( function( $ob ) use ( $preload ) {
        $href = fr_dom( $ob )->attr( 'href' );
        $path = fr_url_to_path( $href );
        if( $path != $href && file_exists( $path ) ){
          fr_dom( $ob )->attr( 'data-lazy-href', $href );
          fr_dom( $ob )->removeAttr( 'href' );
          fr_dom( $ob )->removeAttr( 'defer' );

          $attr = [
            'rel' => 'stylesheet',
            'type' => 'text/css',
            'href' => $href
          ];
          if( fr_dom( $ob )->attr( 'media' ) ){
            $attr['media'] = fr_dom( $ob )->attr( 'media' );
          }

          fr_dom( $ob )->after( '<noscript><link ' . fr_array_to_tag_attr( $attr ) . ' ></noscript>' );

          $attr = [
            'rel' => 'preload',
            'href' => $href,
            'as' => 'style'
          ];
          foreach( $preload as $v ){
            if( strstr( $href, $v ) ){
              fr_dom( $ob )->after( '<link ' . fr_array_to_tag_attr( $attr ) . ' >' );
              break;
            }
          }
        }
      });

      $html = $dom->output();
      return $html;
    } );
  } );
}

function fr_seo_base64_scripts( $max_size, $condition_callback ){
    if( is_admin() ){
      return;
    }
    add_filter( 'clean_url', function( $url ) use ( $max_size, $condition_callback ) {
      if ( !strstr( $url, '.css' ) && !strstr( $url, '.js' ) ) {
          return $url;
      }
      if( !fr_call_if_func( $condition_callback, [ $url ] ) ){
        return $url;
      }
      $path = fr_url_to_path( $url );
      if( $path != $url && file_exists( $path ) ){
        if( filesize( $path ) < $max_size ){
          $url = fr_file_base64_encode( $path );
        }
      }

      return $url;
  }, 11, 1 );
}


function fr_seo_inline_style( $file, $url = false ){

  if( $url === false ){
    $url = fr_get_current_url();
  }

  add_action( 'wp_head', function() use ( $file, $url ) {
    $css = file_get_contents( $file );
    if( $url ){
      $css = fr_css_relative_urls_to_absolute( $css, $url );
    }
    ?>
    <style>
    /* <?php echo basename( $file ) ?> */
    <?php echo fr_css_minify( $css ); ?>
    </style>
    <?php 
  } );
}


function fr_seo_inline_css_essential( $file, $url ){
  add_action( 'wp_head', function() use ( $file, $url ) {
    ob_start( function( $html ) use ( $file, $url ){
      $css = file_get_contents( $file );

      if( $url ){
        $css = fr_css_relative_urls_to_absolute( $css, $url );
      }

      $css = fr_css_optimizer( $html, $css );
      $css = fr_css_minify( $css );
      
      $style = '<style>/* ' . basename( $file ) . '*/' . $css . '</style>';

      $html = str_replace( '</head>', $style . '</head>', $html );

      return $html;
    });
    
  } );
}

function fr_seo_inline_script( $file, $inline_here = false, $minify = true ){

  ob_start();

  ?>
  <script class="exclude_combining">
    /* <?php echo basename( $file ) ?> */
    <?php 
    $c = file_get_contents( $file );
    if( $minify ){
      $c = fr_js_minify( $c );
    }
    echo $c;
    ?>
  </script>
  <?php 

  $c = ob_get_clean();

  if( $inline_here ){
    echo $c;
    return;
  }

  $action = 'wp_head';
  if( did_action( $action ) ){
    $action = 'wp_footer';
  }

  add_action( $action, function() use ( $c ) { 
    echo $c;
  } );
}


function fr_preload( $as, $src, $where = 'wp_head', $lvl = 10 ){
  add_action( $where, function() use ( $as, $src ) { 
    ?><link rel="preload" href="<?php echo esc_attr( $src ) ?>" as="<?php echo $as ?>" crossorigin="anonymous"><?php 
  }, $lvl );
}

function fr_preconnect( $src, $where = 'wp_head' ){
  add_action( $where, function() use ( $src ) { 
    ?><link rel="preconnect" href="<?php echo esc_attr( $src ) ?>" crossorigin="anonymous"><?php 
  } );
}

function fr_dns_prefetch( $src ){
  add_action( 'wp_head', function() use ( $src ) { 
    ?><link rel="dns-prefetch" href="<?php echo esc_attr( $src ) ?>"><?php 
  } );
}


function fr_seo_inline_external_style( $url, $time ){

  add_action( 'wp_head', function() use ( $url, $time ){

    $contents = get_transient( 'fr_seo_inline_external_style_' . $url );

    if( $contents === false ){
      $contents = file_get_contents( $url );
      set_transient( 'fr_seo_inline_external_style_' . $url, $contents, $time );
    }

    if( !$contents ){
      return;
    }

    ?>
    <style>
      /* <?php echo basename( $url ) ?> */
      <?php echo fr_css_minify( $contents ); ?>
    </style>
    <?php 

  } );
}

