<?php 
//Create and returns a custom cache folder
function fr_cache_folder($folder,$permissions=0744){
	$folder=WP_CONTENT_DIR."/cache/".$folder;
	if(!file_exists($folder)){
		mkdir($folder,$permissions,1);
	}
	return $folder;
}

function fr_is_page_cache_enabled(){
	if ( !fr_is_ajax() && !fr_user() && function_exists( 'w3_instance' ) && !fr_fwp_is_refresh() ) {
		$config = w3_instance( 'W3_Config' );
		return $config->get_integer( 'pgcache.enabled' );
	}
}