<?php 
class fr_custom_permalinks {

	function __construct(){
		$this->display_post();
	}

	function display_post(){

		add_filter( 'request', function( $query ){

			$url = fr_get_current_url( true );
			$permalink = fr_post_get_permalink_from_url( $url );
			$permalink = urldecode( $permalink );

			if( !$permalink ){
				return $query;
			}

			$pid = fr_query_get_id( [ 
				'post_type' => 'any',
				'meta_query' => [
					'relation' => 'OR',
					[
						'key' => 'custom_permalink',
						'value' => $permalink
					],
					[
						'key' => 'custom_permalink',
						'value' => $permalink . '/'
					]
				],
				'post_status' => 'any'
			] );

			if( !$pid ){
				return $query;
			}

			$post_type = get_post_type( $pid );

			if( $post_type == 'page' ){
				$permalink = get_post_field( 'post_name', $pid );
				$query = [
					'page' => $permalink,
					'pagename' => $permalink
				];
			} else {
				$query = [
					'post_type' => $post_type,
					'p' => $pid
				];
			}

			return $query;

		}, 0 );
	}
}