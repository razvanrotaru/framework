<?php
function fr_wp_combine_files( $html, $options ){

	$options = wp_parse_args( $options, [
		'exclude_files' => [],
		'exclude_inline' => false,
		'debug' => false,
		'full_page' => false, 
		'inline_css' => false,
		'inline_js' => false,
		'inline_css_minify' => true,
		'inline_css_minify_remove_spaces' => false,
		'inline_exclude_classes' => [],
		'root' => false,
		'site_url' => false
	] );

	if( $options['full_page'] ){
		$lvl = 0;
	}else{
		$lvl = 99999;
	}
	
	$url = get_bloginfo( 'url' );

	$html = fr_combine_enqueue_from_html( $html, array( 'js', 'css' ), $url, $options['exclude_files'], $options['exclude_inline'] );
	$css_file = apply_filters( 'fr_wp_combined_file', fr_wp_combine_save( 'css', $options['inline_css'], $options['debug'] ), 'css', $options['inline_css'] );
	$js_file = apply_filters( 'fr_wp_combined_file', fr_wp_combine_save( 'js', $options['inline_js'], $options['debug'] ), 'js', $options['inline_js'] );

	if( $css_file ){

		if( $options['inline_css'] ){
			$css = $css_file;
			$css = fr_css_optimizer( $html, $css, $options['inline_exclude_classes'], $options['inline_exclude_selectors'] );
			if( $options['inline_css_minify'] ){
				$css = fr_css_minify( $css, [], false, $options['inline_css_minify_remove_spaces'] );
			}
			$html = str_replace( '</head>', '<style id="fr-combined-css">' . $css . '</style></head>', $html );
		} else {
			$html = str_replace( '</head>', '<link id="fr-combined-css" rel="stylesheet" href="' . $css_file . '"></head>', $html );
		}
	}
	if( $js_file ){
		$html = str_replace( '</body>', '<script id="fr-combined" async data-load-time="after_load" data-lazy-src="' . $js_file . '"></script></body>', $html );
	}

	return $html;
}

function fr_wp_combine_save( $type, $return_output = false, $debug = false ){

	if( !$return_output ){
		$uploads = fr_uploads_dir();
		$folder = $uploads . "/combined-static";
		$folder = apply_filters( 'fr_wp_combined_folder', $folder, $type );
	} else {
		$folder = false;
	}

    $file = fr_combine( $type, $folder, $debug );

    if( $return_output ){
    	return $file;
    }
 
    if( !$file ){
    	return false;
    }
    $file_src = fr_path_to_url( $file );
    $file_src = apply_filters( 'fr_wp_combine_save', $file_src, $type, $file );
	return $file_src;
}

function fr_async_script_files(){
	add_filter('script_loader_tag', function( $tag, $handle ) {
	    if ( 'js' !== $tag )
	        return $tag;
	    return str_replace( ' src', ' async="async" src', $tag );
	}, 10, 2);
}

function fr_wp_page_minify(){
	add_action( 'wp', function(){
		ob_start( function( $html ){ 
			$html = fr_html_minify( $html );
			return $html;
		});
	} );
}


function fr_amp_class_name_shortner( $exclude = [], $optimize_font_names = [] ){
	add_action( 'wp', function() use ( $exclude, $optimize_font_names ) {
		ob_start( function( $html ) use ( $exclude, $optimize_font_names ) { 

			$dom = new fr_dom( $html );
			$body = fr_extract( $html, '<body|>', '</body>' );
			$css = fr_extract( $html, '<style amp-custom>', '</style>' );

			$parts = explode( '/* NO_CNS */', $css );// Don't shorten the class names after this comment

			list( $body2, $css2 ) = fr_class_name_shortner( $body, array( $parts[0] ), $exclude, true );
			$html = str_replace( $body, $body2, $html );
			
			$css2 = reset( $css2 );
			if( isset( $parts[1] ) ){
				$css2 .= $parts[1];
			}
			$css2 = fr_css_minify( $css2, $optimize_font_names );

			$html = str_replace( $css, $css2, $html ); 

			$html = str_replace( '<script type="application/json"><![CDATA[', '<script type="application/json">', $html );
			$html = str_replace( ']]></script>', '</script>', $html );
			$html = str_replace( [ '%7B%7B', '%7D%7D' ], [ '{{', '}}' ], $html );
			$html = str_replace( '&amp;nbsp', '&nbsp', $html );
			$html = str_replace( '{{%20', '{{ ', $html );
			$html = str_replace( '%20}}', ' }}', $html );
			
			return $html;
		} );
	}, 11 );
}