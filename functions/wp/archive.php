<?php
class FR_WP_Archive {

	function __construct(){

		register_post_type( 'fr_archive', [
			'public' => false,
			'exclude_from_search' => true,
			'publicly_queryable' => false,
			'show_ui' => false
		] );

		// Hide archived users
		add_filter( 'pre_get_users', function( $q ){
			$exclude = $q->get( 'exclude' );
			$exclude = fr_make_array( $exclude );
			$meta = $q->get( 'meta_query' );

			$meta = fr_make_array( $meta );
			if( !isset( $meta['no_archive'] ) ){
				$archived = $this->get_archived_users();
				$exclude = array_merge( $exclude, $archived );
				$q->set( 'exclude', $exclude );
				$meta['no_archive'] = [];
				$q->set( 'meta_query', $meta );
			}
		} );
	}

	function get_archived_posts(){
		global $wpdb;

		$posts = fr_cache( 'fr_get_archived_posts' );
		if( $posts === false ){
			$query = new fr_wpdb_query( 'ID', 'posts' );
			$query->where( 'post_type = "fr_archive"' );
			$posts = $query->get_col( 'ID' );
			fr_cache( 'fr_get_archived_posts', $posts );
		}
		return $posts;
	}

	function get_archived_users(){
		global $wpdb;

		$users = fr_cache( 'fr_get_archived_users' );
		if( $users === false ){
			$query = new fr_wpdb_query( 'user_id', 'usermeta', 'GROUP BY user_id' );
			$query->where( 'meta_key = "fr_archive_time"' );
			$users = $query->get_col( 'user_id' );
			fr_cache( 'fr_get_archived_users', $users );
		}

		return $users;
	}

	function user( $uid ){

		global $wpdb;

		if( $this->is_user_archived( $uid ) ){
			return false;
		}

		if( fr_user_has_role( 'administrator', $uid ) ){
			return false;
		}

		if( !fr_user_exists( $uid ) ){
			return false;
		}

		
		// Archive user's posts
		
		$query = new fr_wpdb_query( 'ID', 'posts' );
		$query->where( 'post_type != "fr_archive" AND post_author = %d', $uid );
		$posts = $query->get_col( 'ID' );

		foreach( $posts as $post ){
			$this->post( $post );
		}


		// Update the password and other fields so that he can't login with it anymore
		
		$password = fr_user( 'user_pass', $uid ) . '__archive';
		$login = fr_user( 'user_login', $uid ) . '__archive';
		$email = fr_user( 'user_email', $uid ) . '__archive';


		$wpdb->update( $wpdb->prefix . 'users', [ 
			'user_pass' => $password,
			'user_login' => $login,
			'user_email' => $email
		], [ 'ID' => $uid ] );


		// Mark the user as being archived
		fr_um( 'fr_archive_time', DB_DATE_TIME, $uid );

		return true;
	}


	function unarchive_user( $uid ){

		global $wpdb;

		if( !$this->is_user_archived( $uid ) ){
			return false;
		}

		// Unarchive user's posts
		
		$query = new fr_wpdb_query( 'ID', 'posts' );
		$query->where( 'post_type = "fr_archive" AND post_author = %d', $uid );
		$posts = $query->get_col( 'ID' );
		
		foreach( $posts as $post ){
			$this->unarchive_post( $post );
		}


		// Update the password so that he can login
		
		$password = substr( fr_user( 'user_pass', $uid ), 0, -9 );
		$login = substr( fr_user( 'user_login', $uid ), 0, -9 );
		$email = substr( fr_user( 'user_email', $uid ), 0, -9 );

		$wpdb->update( $wpdb->prefix . 'users', [ 
			'user_pass' => $password,
			'user_login' => $login,
			'user_email' => $email
		], [ 'ID' => $uid ] );

		// Mark the user as being archived
		delete_user_meta( $uid, 'fr_archive_time' );
	}


	function post( $pid ){

		global $wpdb;

		if( $this->is_post_archived( $pid ) ){
			return false;
		}

		$post_type = get_post_type( $pid );

		$wpdb->update( $wpdb->prefix . 'posts', [ 'post_type' => 'fr_archive' ], [ 'ID' => $pid ] );

		fr_m( 'fr_archive_time', DB_DATE_TIME, $pid );
		fr_m( 'fr_archive_post_type', $post_type, $pid );


		// Archive translations
		if( fr_wpml() ){
			foreach( $this->wpml_get_translations( $pid ) as $translation ){
				$this->post( $translation );
			}
		}

		return true;
	}


	function unarchive_post( $pid ){

		global $wpdb;

		if( !$this->is_post_archived( $pid ) ){
			return false;
		}

		$post_type = fr_m( 'fr_archive_post_type', $pid );

		$wpdb->update( $wpdb->prefix . 'posts', [ 'post_type' => $post_type ], [ 'ID' => $pid ] );

		delete_post_meta( $pid, 'fr_archive_time' );
		delete_post_meta( $pid, 'fr_archive_post_type' );

		// Unarchive translations
		if( fr_wpml() ){

			foreach( $this->wpml_get_translations( $pid ) as $translation ){
				$this->unarchive_post( $translation );
			}
		}

		return true;
	}

	function wpml_get_translations( $pid ){
		$query = new fr_wpdb_query( 'post_id', 'postmeta' );
		$query->where( 'meta_key="_icl_lang_duplicate_of" AND meta_value=%s', $pid );
		$translations = $query->get_col( 'post_id' );
		return $translations;
	}

	function is_user_archived( $uid ){
		return fr_um( 'fr_archive_time', $uid );
	}

	function is_post_archived( $pids ){
		$pids = fr_make_array( $pids );
		$query = new fr_wpdb_query( 'ID', 'posts' );
		$query->where_in( 'ID', $pids );
		$query->where( 'post_type = "fr_archive"' );
		return $query->get_col( 'ID' );
	}
}