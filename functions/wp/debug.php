<?php

class fr_debug{
	var $unit_testing_errors;

	function __construct(){
		$this->unit_testing_errors=array();
	}

	function unit_testing($functions){

	    foreach($functions as $func=>$tests){
	        foreach($tests as $test){
	            $vars=$test[0];
	            $result=$test[1];
	            if(!is_array($vars) || !count($vars)){
	                $vars=array($vars);
	            }
	            if(!is_array($result)){
	                $result=array($result);
	            }echo $func;
	            $res=call_user_func_array($func,$vars);

	            //COMPAROR
	            if(empty($result[1])){
	                $result[1]="=";
	            }

	            //CUSTOM RESULT MODIFIER
	            if(!empty($result[2])){
	                $res=call_user_func_array($result[2],array($res));
	            }

	            //CUSTOM VALIDATOR
	            if(!empty($result[3])){
	                if($error=call_user_func_array($result[3],$vars)){
	                    if(is_array($res)){
	                        $res=array_slice($res,0,100,1);
	                    }
	                    $this->unit_testing_errors[$func]=array($error,"vars"=>$vars,"expected"=>$result,"result"=>$res);
	                    break 1;
	                }
	            }

	            //VALIDATOR
	            if($error=$this->recursive_compare_test($result[0],$res,$result[1])){
	                if(is_array($res)){
	                    $res=array_slice($res,0,100,1);
	                }
	                $this->unit_testing_errors[$func]=array($error,"vars"=>$vars,"expected"=>$result,"result"=>$res);
	                break 1;
	            }
	        }
	    }
	}

	function recursive_compare_test($result,$res,$compare,$lvl=0){
	    if($lvl>5){
	        return "MAX LVL";
	    }

	    //COMPARE VARIABLE TYPE
	    if(is_array($result) && !is_array($res)){
	        return "Expecting ".ucwords(gettype($result)).", received ".ucwords(gettype($res));
	    }else if(is_object($result) && !is_object($res)){
	        return "Expecting ".ucwords(gettype($result)).", received ".ucwords(gettype($res));
	    }else if($result===NULL && !is_null($res)){
	        return "Expecting ".ucwords(gettype($result)).", received ".ucwords(gettype($res));
	    }else if($result===false && $res!==false){
	        return "Expecting FALSE, received ".ucwords(gettype($res));
	    }
	    if($compare=="type"){
	        if($result=="ARRAY" && !is_array($res)){
	            return "Expecting ".$result.", received ".ucwords(gettype($res));
	        }else if($result=="OBJECT" && !is_object($res)){
	            return "Expecting ".$result.", received ".ucwords(gettype($res));
	        }else if($result=="INTEGER" && ucwords(gettype($res))!=$result){
	            return "Expecting ".$result.", received ".ucwords(gettype($res));
	        }else if($result=="DOUBLE" && ucwords(gettype($res))!=$result){
	            return "Expecting ".$result.", received ".ucwords(gettype($res));
	        }
	    }
	    if($compare!="type"){
	        if($result=="OBJECT" && is_object($res)){
	            return "Expecting NOT ".ucwords(gettype($result));
	        }else if($result=="INTEGER" && ucwords(gettype($res))==$result){
	            return "Expecting NOT ".ucwords(gettype($result));
	        }else if($result=="DOUBLE" && ucwords(gettype($res))==$result){
	            return "Expecting NOT ".ucwords(gettype($result));
	        }
	    }

	    if($compare!="type"){
	        if(is_array($result)){
	            foreach($result as $k=>$v){
	                if($error=$this->recursive_compare_test($v,$res[$k],$compare,$lvl+1)){
	                    return $error;
	                }
	            }
	        }else if(is_object($result)){
	            $result=(array)$result;
	            foreach($result as $k=>$v){
	                if($error=$this->recursive_compare_test($v,$res[$k],$compare,$lvl+1)){
	                    return $error;
	                }
	            }
	        }else{
	            if($compare=="=" && $result!=$res){
	                return "Results DON'T MATCH";
	            }
	            if($compare=="!=" && $result==$res){
	                return "Results DO MATCH";
	            }
	            if(($compare=="<" && $res>=$result) || ($compare=="<=" && $res>$result)){
	                return "Result Number TO BIG";
	            }
	            if(($compare==">" && $res<=$result) || ($compare==">=" && $res<$result)){
	                return "Result Number TO SMALL";
	            }
	        }
	    }
	    return;
	}
}





class fr_filters_exceution_time {
    public $data = array();
    function __construct() {
        add_action( 'all', array( $this, 'filter_start' ) );
        add_action( 'shutdown', array( $this, 'results' ) );
    }
    // This runs first for all actions and filters.
    // It starts a timer for this hook.
    public function filter_start() {
        $current_filter = current_filter();
        $this->data[ $current_filter ][]['start'] = microtime( true );
        add_filter( $current_filter, array( $this, 'filter_end' ), 99999 );
    }
    // This runs last (hopefully) for each hook and records the end time.
    // This has problems if a hook fires inside of itself since it assumes
    // the last entry in the data key for this hook is the matching pair.
    public function filter_end( $filter_data = false ) {
        $current_filter = current_filter();
        remove_filter( $current_filter, array( $this, 'filter_end' ), 99999 );
        end( $this->data[ $current_filter ] );
        $last_key = key( $this->data[ $current_filter ] );
        $this->data[ $current_filter ][ $last_key ]['stop'] = microtime( true );
        return $filter_data;
    }
    // Processes the results and var_dump()'s them. TODO: Debug bar panel?
    public function results() {
        $results = array();
        foreach ( $this->data as $filter => $calls ) {
            foreach ( $calls as $call ) {
                // Skip filters with no end point (i.e. the hook this function is hooked into)
                if ( ! isset( $call['stop'] ) )
                    continue;
                if ( ! isset( $results[ $filter ] ) )
                    $results[ $filter ] = 0;
                $results[ $filter ] = $results[ $filter ] + ( $call['stop'] - $call['start'] );
            }
        }
        asort( $results, SORT_NUMERIC );
 		$results = array_reverse( $results );
 		fr_p( $results );
    }
}



class fr_debug_filters {

	var $log = [];
	var $show_activity;
	var $time_limit;
	var $memory_limit;
	var $debug_callback = '';
	var $exec_time_log_threshhold = 0;

	function __construct( $exec_time_log_threshhold = 0 ){

		$this->exec_time_log_threshhold = $exec_time_log_threshhold;
		$this->log();

		// Add action to know which functions are called when a hook is executed for debugging purposes
		fr_fix_file( ABSPATH . 'wp-includes/class-wp-hook.php', '$this->callbacks[ $priority ][ $idx ] = array(', '$this->callbacks[ $priority ][ $idx ] = array(' . PHP_EOL . '\'hook_name\'      => $hook_name,' );
		fr_fix_file( ABSPATH . 'wp-includes/class-wp-hook.php', 'if ( 0 == $the_[\'accepted_args\'] ) {', '$start = microtime( true );' . PHP_EOL . 'if ( 0 == $the_[\'accepted_args\'] ) {' );
		fr_fix_file( ABSPATH . 'wp-includes/class-wp-hook.php', '$value = call_user_func_array( $the_[\'function\'], array_slice( $args, 0, (int) $the_[\'accepted_args\'] ) );
				}', '$value = call_user_func_array( $the_[\'function\'], array_slice( $args, 0, (int) $the_[\'accepted_args\'] ) );
				}' . PHP_EOL . 'if( $the_["hook_name"] !== "wp_apply_filters_exec_time" ){ do_action( "wp_apply_filters_exec_time", microtime( true ) - $start, $the_["hook_name"], $the_["function"], $value ); }' );
	}


	function get_results( $order_by = 'time' ){

		$log = $this->log;

		if( $order_by == 'time' ){

			usort( $log, function( $a, $b ){
				return $b['time'] <=> $a['time'];
			} );

		}

		$log = array_slice( $log, 0, 1000 );

		return $log;

	}


	function log(){
		
		add_action( 'wp_apply_filters_exec_time', function( $time, $hook, $callback ){

			if( is_admin() ){
				return;
			}

			if( $time < $this->exec_time_log_threshhold ){
				return;
			}
			
			$backtrace = debug_backtrace( false, 6 );
			$file = $backtrace[5]['file'];
			$line = $backtrace[5]['line'];
			$args = $backtrace[5]['args'];
			$args_json = json_encode( $args );

			if( strlen( $args_json ) > 1000 ){
				$args = [ key( $args ) => reset( $args ) ];
			}

			try {
				$refl = ( $callback instanceof \Closure ) || is_string( $callback ) ? new ReflectionFunction( $callback ) : '';
			} catch( Exception $e ){
				$refl = '';
			}

			if( is_array( $callback ) && isset( $callback[0] ) && is_object( $callback[0] ) ){
				$callback[0] = get_class( $callback[0] );
			}

			$this->log[$hook] = [
				'action' => $hook,
				'callback' => $callback,
				'callback_file' => $refl ? str_replace( ABSPATH, '', $refl->getFileName() ) . ':' . $refl->getStartLine() : '',
				'args' => $args,
				'apply_filter_file' => str_replace( ABSPATH, '', $file ) . ':' . $line,
				'time' => $time
			];

		}, 10, 3 );

	}

}





function fr_debug_at_repeat( $count ){

	$current = fr_global( 'fr_debug_at_repeat', 1, true );

	if( $current >= $count ){
		fr_debug();
	}

}


function fr_debug_get_filters( $hook ){

	global $wp_filter, $wp_actions;

	if( !isset( $wp_filter[$hook] ) ) return;
	
	$filters = [];

	foreach( $wp_filter[$hook]->callbacks as $k => $v ){
		foreach( $v as $k2 => $v2 ){
			$function = $v2['function'];
			
			if( is_array( $function ) ){

				if( is_object( $function[0] ) ){
					$function[0] = get_class( $function[0] );
				}
				
				$function = $function[0] . '->' . $function[1];
			} else if ( $function instanceof Closure ) {
				$function = 'Closure';
			}

			$filters[] = $function . ' ' . $k;
		}
	}

	return $filters;
	
}


