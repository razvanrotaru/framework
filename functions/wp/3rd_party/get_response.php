<?php 

class fr_get_response {

	var $api_key;
	var $campaign_id;
	var $endpoint = 'https://api.getresponse.com/v3';

	function __construct( $api_key, $campaign = '' ){
		$this->api_key = $api_key;
		$campaigns = $this->get_campaigns();
		$this->campaign_id = $campaigns[$campaign]['campaignId'];
	}

	function add_contact( $email, $name = "", $tags = array(), $fields = array(), $contact_id = 0 ){

		if( !$contact_id ){
			$existing_contact = $this->contact_exists( $email );
			if( $existing_contact ){
				$contact_id = $existing_contact['contactId'];
			}
		}
		
		if( $contact_id ){
			// Update his name is different
			return $this->update_contact( $contact_id, $name, $tags, $fields );
		}else{
			// Create new contact
			return $this->create_contact( $email, $name, $tags, $fields );
		}
	}


	function update_contact( $contact_id, $name = "", $custom_fields = array(), $fields = array() ){

		$custom_fields = $this->custom_fields_to_ids( $custom_fields );
		$fields['customFieldValues'] = array();
		foreach( $custom_fields as $field => $value ){
			$fields['customFieldValues'][] = array(
				'customFieldId' => $field,
				'value' => array( $value )
			);
		}
		if( $name ){
			$fields['name'] = $name;
		}

		// Returns an array if an error occured else it will return false
		return $this->post( 'contacts/' . $contact_id, $fields, $fields = array() );
	}


	function create_contact( $email, $name = "", $custom_fields = array() ){

		$fields['email'] = $email;
		$fields['campaign'] = array( 'campaignId' => $this->campaign_id );
		$custom_fields = $this->custom_fields_to_ids( $custom_fields );
		$fields['customFieldValues'] = array();
		foreach( $custom_fields as $field => $value ){
			$fields['customFieldValues'][] = array(
				'customFieldId' => $field,
				'value' => array( $value )
			);
		}
		if( $name ){
			$fields['name'] = $name;
		}

		// Returns an array if an error occured else it will return false
		return $this->post( 'contacts', $fields );
	}


	function get_tags( $fields = array(), $no_cache = false ){
		$key = 'fr_get_response_get_tags_' . md5( json_encode( $fields ) );
		$new = get_transient( $key );
		if( $new === false || $no_cache ){
			$tags = $this->get( 'tags', $fields );
			$new = array();
			foreach( $tags as $k => $tag ){
				$new[$tag['name']] = $tag;
			}
			set_transient( $key, $new, 3600 );
		}
		return $new;
	}

	function create_tags( $tags ){
		$new_tags = array();
		foreach( $tags as $tag ){
			$new_tag = $this->post( 'tags', array( 'name' => $tag ) );
			if( isset( $new_tag['tagId'] ) ){
				$new_tags[$new_tag['name']] = $new_tag;
			}
		}
		return $new_tags;
	}

	function tags_to_ids( $tags ){
		$tag_ids = array();
		if( $tags ){
			$clear_cahe = false;
			$all_tags = $this->get_tags();
			$new_tags = array();
			foreach( $tags as $tag ){
				if( !isset( $all_tags[$tag] ) ){
					$new_tags[] = $tag;
				}
			}

			if( $new_tags ){
				$this->create_tags( $new_tags );
				$all_tags = $this->get_tags( array(), 1 );
			}

			foreach( $tags as $k => $tag ){
				if( !empty( $all_tags[$tag] ) ){
					$tag_ids[] = $all_tags[$tag]['tagId'];
				}
			}
		}
		return $tag_ids;
	}

	function get_custom_fields( $fields = array(), $no_cache = false ){
		$key = 'fr_get_response_get_custom_fields_' . md5( json_encode( $fields ) );
		$new = wp_cache_get( $key );
		if( $new === false || $no_cache ){
			$tags = $this->get( 'custom-fields', $fields );
			$new = array();
			foreach( $tags as $k => $tag ){
				$new[$tag['name']] = $tag;
			}
			wp_cache_set( $key, $new );
		}
		return $new;
	}

	function custom_fields_to_ids( $custom_fields ){
		$all_fields = $this->get_custom_fields();
		$new_fields = array();
		foreach( $custom_fields as $field => $value ){
			if( isset( $all_fields[$field] ) ){
				$new_fields[$all_fields[$field]['customFieldId']] = $value;
			}
		}
		return $new_fields;
	}

	function get_campaigns( $fields = array() ){
		$fields['perPage'] = 1000;
		$key = 'fr_get_response_get_campaigns_' . md5( json_encode( $fields ) );
		$new = get_transient( $key );
		if( $new === false ){
			$campaigns = $this->get( 'campaigns', $fields );
			$new = array();
			foreach( $campaigns as $campaign ){
				$new[$campaign['name']] = $campaign;
			}
			set_transient( $key, $new, 3600 );
		}
		return $new;
	}

	/**
	 * Get contacts
	 * Documentation at: http://apidocs.getresponse.com/v3/resources/contacts
	 * @param  array  $fields List of arguments to pass to the api
	 * @return array List of contacts that matched the arguments
	 */
	function get_contacts( $fields = array() ){
		if( $this->campaign_id ){
			if( !isset( $fields['query'] ) ){
				$fields['query'] = array();
			}
			$fields['query']['campaignId'] = $this->campaign_id;
		}
		$key = 'fr_get_response_get_contacts_' . md5( json_encode( $fields ) );
		$contacts = wp_cache_get( $key );
		if( $contacts === false ){
			$contacts = $this->get( 'contacts', $fields );
			wp_cache_set( $key, $contacts );
		}
		return $contacts;
	}

	/**
	 * Get contacts
	 * Documentation at: http://apidocs.getresponse.com/v3/resources/contacts
	 * @param  array  $contact_id List of arguments to pass to the api
	 */
	function get_contact( $contact_id ){
		return $this->get( 'contacts/' . $contact_id );
	}

	/**
	 * Get contacts
	 * Documentation at: http://apidocs.getresponse.com/v3/resources/contacts
	 * @param  array  $contact_id
	 */
	function delete_contact( $contact_id ){
		return $this->delete( 'contacts/' . $contact_id );
	}

	/**
	 * Check if contact already exists
	 * Documentation at: http://apidocs.getresponse.com/v3/resources/contacts
	 * @param  string $email
	 * @param  array  $fields List of arguments to pass to the api
	 * @return array
	 */
	function contact_exists( $email, $fields = array() ){
		if( $this->campaign_id ){
			if( !isset( $fields['query'] ) ){
				$fields['query'] = array();
			}
			$fields['query']['campaignId'] = $this->campaign_id;
		}
		$fields['query']['email'] = $email;

		$key = 'fr_get_response_contact_exists_' . md5( json_encode( $fields ) );
		$contacts = wp_cache_get( $key );

		if( $contacts === false ){
			$contacts = $this->get( 'contacts', $fields );

			// Group campaigns
			if( $contacts ){
				$campaigns = array();
				foreach( $contacts as $contact ){
					$campaigns[] = $contact['campaign'];
				}
				$contacts = reset( $contacts );
				unset( $contacts['campaign'] );
				$contacts['campaigns'] = $campaigns;
			}
			wp_cache_set( $key, $contacts );
		}

		return $contacts;
	}

	public function get( $endpoint, $fields = array() ){

		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'X-Auth-Token: api-key ' . $this->api_key;
		$endpoint = $this->endpoint . '/' . $endpoint . '?' . http_build_query( $fields );

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $endpoint );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		$server_output = curl_exec ( $ch );
		curl_close ( $ch );
		$server_output = json_decode( $server_output, 1 );

		return $server_output;

	}

	public function post( $endpoint, $fields = array(), $headers = array() ){

		$headers[] = 'Content-Type: application/json';
		$headers[] = 'X-Auth-Token: api-key ' . $this->api_key;
		$endpoint = $this->endpoint . '/' . $endpoint;

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $endpoint );
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		$server_output = curl_exec ( $ch );
		curl_close ( $ch );
		$server_output = json_decode( $server_output, 1 );

		return $server_output;

	}

	public function delete( $endpoint, $headers = array() ){

		$headers[] = 'Content-Type: application/json';
		$headers[] = 'X-Auth-Token: api-key ' . $this->api_key;
		$endpoint = $this->endpoint . '/' . $endpoint;

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $endpoint );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "DELETE" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
		$server_output = curl_exec ( $ch );
		curl_close ( $ch );
		$server_output = json_decode( $server_output, 1 );

		return $server_output;

	}
}