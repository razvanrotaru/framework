<?php
function fr_ga_track( $type, $type2, $type3, $value = 1, $non_interaction=0 ){
	$track = fr_session( 'fr_ga_track' );
	if( !$track ){
		$track = [];
	}
	$track[]=array($type,$type2,$type3,$value,$non_interaction);
	fr_session( 'fr_ga_track', $track );
}

add_action( 'wp_head', function(){
	fr_add_js_setting( 'ga_track', fr_session( 'fr_ga_track' ) ? fr_session( 'fr_ga_track' ) : NULL );
	fr_session( 'fr_ga_track', [] );
} );


// Enable google analytics debug mode
function fr_ga_enable_debug_mode(){
	add_action( 'wp_head', function(){
		ob_start();
	}, -1 );

	add_action( 'wp_footer', function(){
		$code = ob_get_clean();
		$code = str_replace( 'https://www.google-analytics.com/analytics.js', 'https://www.google-analytics.com/analytics_debug.js', $code );
		echo $code;
	}, 1000 );
}