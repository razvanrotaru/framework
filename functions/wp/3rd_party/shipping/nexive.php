<?php 
/**
 * Returns traking page url where you can see the status of a delivery
 * @param  string $barcode Barcode from a Nexive shipping
 * @param  string $lang    Language code to display in. Supported options: EN, IT
 * @return string          URL of the traking page
 */
function fr_nexive_get_tracking_page_url( $barcode, $lang = 'EN' ){
	if( $barcode ){
		return 'https://www.sistemacompleto.it/Tracking-Spedizioni-Nexive.aspx?b=' . $barcode . '&lang=' . strtoupper( $lang );
	}
}