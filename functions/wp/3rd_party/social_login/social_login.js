var $ = jQuery;

$(window).ready(fr_init_social_login);
$(document).ajaxComplete(function(){
	setTimeout(fr_init_social_login,1);
});

function fr_init_social_login(){
 	var init_social_login=0;
	  if($(".fr_social_login").length){
	    (function (hello) {
	        if(hello && hello.services && hello.services.facebook && hello.services.facebook.get && hello.services.facebook.get.me) {

	  			//email,name,first_name,last_name,id,gender,timezone
	            hello.services.facebook.get.me = 'me?fields='+$(".fr_social_login .fr_btn[data-type='facebook']").data("fields");
	        }
	    })(hello);

	    // Listen to signin requests
	    hello.on('auth.login', function(r) {
	      // Get Profile
	      hello( r.network ).api( '/me' ).then( function(p) {
	        if(init_social_login==1){
	        	var input = $(".fr_social_login").data("target");

	        	$(input).val(JSON.stringify($.extend(r,p)));
	        	$(input).change();
	            //$(".fr_social_login").parents("form").submit();
	        }
	      });
	    });


	    var data = {};
	    $(".fr_social_login .fr_btn").each(function(){
	    	data[$(this).data("type")] = $(this).data("app-id");
	    });

	    // Intiate App credentials
	    hello.init(data,{
	      scope : $(".fr_social_login").data("scope").split(","),//email
	      redirect_uri: $(".fr_social_login").data("redirect")
	    });


	    // Bind events to the buttons on the page
	    $(".fr_social_login .fr_btn").click(function(e){
	      init_social_login=1;
	      e.preventDefault();
	      hello($(this).data("type")).login();
	    });
	  }
}