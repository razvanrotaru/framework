<?php 
function fr_init_date_picker(){
	add_action("wp_enqueue_scripts", 'fr_date_picker_enqueue_scripts' );
}

function fr_date_picker_enqueue_scripts(){
	wp_enqueue_style('zebra_datepicker', FRAMEWORK_URL . '/resources/zebra_datepicker/css/default/zebra_datepicker.min.css');
	wp_enqueue_script('zebra_datepicker', FRAMEWORK_URL . '/resources/zebra_datepicker/zebra_datepicker.min.js', array('jquery'),null, 1 );
}