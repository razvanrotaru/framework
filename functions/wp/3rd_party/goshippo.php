<?php 
/**
 * Returns the status for a shipping order
 *
 * @param string $traking_id 
 * @param string $api_key
 * @param string $carier Example: fedex
 */

function fr_goshippo_shipping_status($traking_id, $carrier, $api_key){
	$status = "UNKNOWN";
	if($traking_id && $carrier){
		require_once(FRAMEWORK_DIR.'/resources/apis/goshippo/Shippo.php');

		try {
			$result = Shippo_Track::get_status(array("id"=>$traking_id,"carrier"=>$carrier),$api_key);
			if(!empty($result->tracking_status->status)){
				$status = $result->tracking_status->status;
			}
		} catch ( Exception $e ){
			$status = $e;
		}
	}
	return $status;
}