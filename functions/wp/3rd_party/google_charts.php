<?php
// bar / line
function fr_gc( $chart_options, $headers, $options, $a ){
	$new = array();
	foreach( $a as $v ){
		$new[] = array_values( $v );
	}
	$index = fr_auto_index( 'fr_google_chart' );

	wp_enqueue_script( 'google_chart', 'https://www.gstatic.com/charts/loader.js' );

	$data = [
		'chart_type' => $chart_options['chart_type'],
		'rows' => $new,
		'header' => $headers,
		'options' => $options
	];
	?>
	<div id="fr_google_chart_<?php echo $index ?>" style="<?php if( !empty( $chart_options['height'] ) ){ echo 'height: ' . $chart_options['height'] . 'px;';  } ?>" class="fr_google_chart fr_google_chart_<?php echo $chart_options['chart_type'] ?>" data-json="<?php echo esc_attr( json_encode( $data ) ) ?>"></div>
	<?php 
}