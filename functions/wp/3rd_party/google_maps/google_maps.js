var $=jQuery;

var fr_google_maps_loaded=0;
$(window).load(function(){
	if(fr_google_maps_loaded==0){
		fr_google_maps_loaded=1;
		if (typeof google !== 'object' || typeof google.maps !== 'object') {
			fr_load_script('https://maps.googleapis.com/maps/api/js?v=3.exp&callback=fr_initialize_maps&language=en&key='+fr_google_maps_api_key);
		}
	}
});

function fr_initialize_maps(){
	fr_apply_filter("fr_initialize_maps");
}



/**
 * Determine a transient's name based on endpoint and parameters.
 *
 * @param object options Google Maps API options.
       {
        	address : "New York",
        	language : "en",
            componentRestrictions : {
        		country : "US"
          	}
        }
 * @param function callback Since the API is async it needs a function to call after it gets the results.
 * @return object
 */

function fr_google_maps_find_location( options, callback ){

	geocoder = fr_google_maps_get_geocoder();
	if(!geocoder){
		return;
	}

	var result = fr_cache( "fr_google_maps_find_location", options );
	if(result!==false){
		return callback( result );
	}

	geocoder.geocode(options, function( results, status ){

		var res=[];
		if(status=="OK" && results.length){
			for(x in results){
				var data={};
				for(key in results[x]["address_components"]){
					var item=results[x]["address_components"][key];
					var ok=0;
					for(y in item["types"]){
						data[item["types"][y]]=[item.long_name,item.short_name,0];
						break;
					}
				}
				var loc=results[x]["geometry"]["location"];
				data["latitude"]=loc.lat();
				data["longitude"]=loc.lng();
				data["address"]=results[x].formatted_address;
				res[x]=data;
				if(res.length>=10){break;}
			}
		}

		//Store value in cache
		fr_cache( "fr_google_maps_find_location", options, res );

		callback(res);
	});
}



/**
 * Get Google Maps geocoder object
 *
 * @return object
 */

var fr_google_maps_geocoder;
function fr_google_maps_get_geocoder(){
	if(typeof google=="undefined" || typeof google.maps=="undefined"){
		return;
	}
	if(!fr_google_maps_geocoder){
		fr_google_maps_geocoder = new google.maps.Geocoder();
	}
	return fr_google_maps_geocoder;
}