<?php 
/*
* Initate google maps module for additional options
*
* string $key Holds the Google Maps API key
* @return true
*/

function fr_init_google_maps($key=""){
	global $fr_google_maps_key;
	$fr_google_maps_key = $key;
	if(is_admin()){
		return;
	}
	add_action("wp_enqueue_scripts",function(){
		wp_enqueue_script('google_maps', FRAMEWORK_URL . '/functions/wp/3rd_party/google_maps/google_maps.js', array('jquery','fr_script'),null, 1 );
	});
	add_action("wp_head",function(){
		global $fr_google_maps_key;
		?>
		<script>
		var fr_google_maps_api_key='<?php echo $fr_google_maps_key ?>';
		</script>
		<?php 
	});
	return true;
}