<?php
add_action("init",function(){

	if( defined( 'DOING_CRON' ) ){
		return;
	}

	if( empty( $_GET["fr_force_download"] ) ){
		return;
	}

	if( !empty( $_GET["pass"] ) && md5(DOWNLOAD_PASS . "_" . $_GET["fr_force_download"])==$_GET["pass"]){
		$file = $_GET["fr_force_download"];
		$name = false;
		if( is_numeric( $_GET["fr_force_download"] ) ){
			$file = fr_file_path( $_GET["fr_force_download"] );
			$name = fr_wp_files_get_name( $_GET["fr_force_download"] );
		} else {
			// Limit files to be downloaded from the uploads directory
			$file = fr_uploads_dir() . $_GET["fr_force_download"];
		}

		if( file_exists( $file ) ){
			fr_force_download( $file, true, $name );
			return;
		}
	}


	add_action( 'template_redirect', function(){

		global $wp_query;
		$wp_query->set_404();
		status_header( 404 );

	} );
});



function fr_force_download_link( $file, $omit_check = false ){

	if( is_numeric( $file ) ){
		// If id is not an attachment abort
		if( !$omit_check && get_post_type( $file ) != 'attachment' ){

			return false;
		}
	} else {
		// Limit downloads to the uploads directory
		if( !strstr( $file, fr_uploads_dir() ) ){

			return false;
		}
		$file = str_replace( fr_uploads_dir(), '', $file );
	}

	return fr_build_link( get_bloginfo( 'url' ), array( 
		'fr_force_download' => $file, 
		'pass' => md5( DOWNLOAD_PASS . '_' . $file
	 ) ) );

}