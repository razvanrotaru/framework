<?php
function fr_admin_post_col( $post_type, $header, $rows = false, $lvl = 10 ){
	if( is_callable( $header ) ){
		add_filter( 'manage_' . $post_type . '_posts_columns', $header, $lvl );
	}
	if( is_callable( $rows ) ){
		add_action( 'manage_' . $post_type . '_posts_custom_column', $rows, $lvl, 2);
	}
}

function fr_admin_user_col( $header, $rows, $lvl = 10 ){
	if( is_callable( $header ) ){
		add_filter( 'manage_users_columns', $header, $lvl );
	}
	if( is_callable( $rows ) ){
		add_action( 'manage_users_custom_column', $rows, $lvl, 3);
	}
}

function fr_admin_remove_posts_columns( $post_type_target, $remove_columns, $lvl = 11 ){

	add_action( 'manage_post_posts_columns', function( $columns, $post_type = 'post' ) use ( $post_type_target, $remove_columns ) {

		if( $post_type_target == $post_type ){
			$columns = array_diff_key( $columns, array_flip( $remove_columns ) );
		}

		return $columns;
	}, $lvl, 2 );
}


function fr_admin_redirect( $link ){
	if( is_numeric( $link ) ){
		$link = get_edit_post_link( $link );
	}
	fr_redirect( admin_url( $link ) );
}

// Replace wp redirect
function fr_replace_admin_redirect( $l ){
	if( is_numeric( $link ) ){
		$link = get_edit_post_link( $link );
	}
	add_filter( 'wp_redirect', function() use ( $l ) { 
		return $l;
	} );
	$l = admin_url( $l );
	add_filter( 'wp_redirect_status', function() use ( $e ) { 
		return $e;
	} );
}

function fr_admin_filter( $name, $post_type, $options, $filter_callback = [] ){
	add_action( 'restrict_manage_posts', function() use ( $name, $post_type, $options ) {
	    if ( fr_screen_id() == 'edit-' . $post_type ){
	        $selected = isset( $_GET[$name] ) ? $_GET[$name] : '';
	        echo fr_html_select( $name, $options, $selected );
	    }
	} );

	if( $filter_callback ){
		add_filter( 'parse_query', 	function( $query ) use ( $name, $post_type, $filter_callback ) {
		    if ( fr_screen_id() == 'edit-' . $post_type && !empty( $_GET[$name] ) && $query->get( 'post_type' ) == $post_type && $query->is_main_query() ) {
	    		fr_call_if_func( $filter_callback, [ $query, $_GET[$name] ] );
		    }
		} );
	}
}


function fr_admin_post_col_sortable( $post_type, $col, $query_callback ){
	add_filter( 'manage_edit-' . $post_type . '_sortable_columns', function( $cols ) use ( $col ){ 
		$cols[$col] = 'custom_order_' . $col;
		return $cols;
	} );
	add_action( 'pre_get_posts', function( $query ) use ( $post_type, $col, $query_callback ){
		if( $query->get( 'post_type' ) != $post_type ){
			return;
		}

		if( $query->get( 'orderby' ) != 'custom_order_' . $col ){
			return;
		}
		
		fr_call_if_func( $query_callback, [ $query ] );
	} );
}


// Helper function to add new buttons in the backend editor
function fr_admin_editor_formats( $buttons ){
	add_filter('mce_buttons_2', function( $buttons ) {
	    array_unshift($buttons, 'styleselect');
	    return $buttons;
	}, 1000 );

	/**
	 * array(
	    'title' => 'Pink Button',  
	    'selector' => 'a',  
	    'classes' => 'pink'
	  )
	 */
	add_filter( 'tiny_mce_before_init', function( $init_array ) use ( $buttons ){
		if( !isset( $init_array['style_formats'] ) ){
			$init_array['style_formats'] = [];
		}else{
			$init_array['style_formats'] = json_decode( $init_array['style_formats'], 1 );
		}
		$init_array['style_formats'] = json_encode( array_merge( $init_array['style_formats'], $buttons ) );
		return $init_array;
	}, 1000 ); 

	add_action( 'init', function(){
		add_editor_style( THEME_URL . '/css/editor.css' );
	}, 1000 );
}

function fr_screen_id(){
	if( function_exists( 'get_current_screen' ) ){
		$screen = get_current_screen();
		if( $screen ){
			return $screen->id;
		}
	}
}



function fr_admin_add_row_action( $post_type, $name, $label, $link_callback ){
	add_filter( 'post_row_actions', function( $rows, $post ) use ( $post_type, $name, $label, $link_callback ) {
		if( $post->post_type == $post_type ){
			$link = fr_call_if_func( $link_callback, [ $post->ID ] );
			$attr = [];
			if( is_array( $link ) ){
				list( $link, $attr ) = $link;
			}
			if( $link ){
				$rows[$name] = '<a href="' . esc_attr( $link ) . '" ' . fr_array_to_tag_attr( $attr ) . '>' . $label . '</a>';
			}
		}
		return $rows;
	}, 10, 2 );
}


function fr_admin_remove_row_action( $post_type, $names ){
	if( !is_array( $names ) ){
		$names = [ $names ];
	}
	add_filter( 'post_row_actions', function( $rows, $post ) use ( $post_type, $names ) {
		if( $post->post_type == $post_type ){
			$rows = array_diff_key( $rows, array_flip( $names ) );
		}
		return $rows;
	}, 10, 2 );
}



/*
Example: fr_metabox_set_first( 'product', 'side', 'core', 'icl_div' );
 */
function fr_metabox_set_first( $post_type, $location, $priority, $new_priority, $id ){
	fr_metabox_set_position( $post_type, $location, $priority, $new_priority, $id, 'first' );
}
function fr_metabox_set_last( $post_type, $location, $priority, $new_priority, $id ){
	fr_metabox_set_position( $post_type, $location, $priority, $new_priority, $id, 'last' );
}

function fr_metabox_remove( $post_type, $location, $id ){

	if( did_action( 'admin_head' ) ){
		remove_meta_box( $id, $post_type, $location );
		return;
	}

	add_action( 'admin_head', function() use ( $post_type, $location, $id ){
		remove_meta_box( $id, $post_type, $location );
	} );
}

function fr_metabox_set_position( $post_type, $location, $priority, $new_priority, $id, $position ){
	global $wp_meta_boxes;

	if( !isset( $wp_meta_boxes[$post_type][$location][$priority][$id] ) ){
		return;
	}

	$box = $wp_meta_boxes[$post_type][$location][$priority][$id];
	unset( $wp_meta_boxes[$post_type][$location][$priority][$id] );

	if( $position == 'first' ){
		$wp_meta_boxes[$post_type][$location][$new_priority] = array_merge( [ $id => $box ], $wp_meta_boxes[$post_type][$location][$new_priority] );
	} else if( $position == 'last' ){
		$wp_meta_boxes[$post_type][$location][$new_priority] = array_merge( $wp_meta_boxes[$post_type][$location][$new_priority], [ $id => $box ] );
	} else {
		$wp_meta_boxes[$post_type][$location][$new_priority] = fr_array_relative_insert( $wp_meta_boxes[$post_type][$location][$new_priority], [ $id => $box ], $position );
	}
}

function fr_metaboxes( $post_type, $location = '', $priority = '' ){
	global $wp_meta_boxes;

	if( empty( $location ) ){
		return $wp_meta_boxes[$post_type];
	}
	if( empty( $priority ) ){
		return $wp_meta_boxes[$post_type][$location];
	}
	return $wp_meta_boxes[$post_type][$location][$priority];
}

function fr_metabox_change_location( $post_type, $location, $priority, $id, $new_location ){
	global $wp_meta_boxes;

	if( !isset( $wp_meta_boxes[$post_type][$location][$priority][$id] ) ){
		return;
	}

	$box = $wp_meta_boxes[$post_type][$location][$priority][$id];
	unset( $wp_meta_boxes[$post_type][$location][$priority][$id] );

	$wp_meta_boxes[$post_type][$new_location][$priority][$id] = $box;
}


function fr_metabox_close( $post_type, $id ){
	add_filter( 'postbox_classes_' . $post_type . '_' . $id, function( $classes ){
		$classes[] = 'closed';
		return $classes;
	} );
}


function fr_admin_editor_disable_publish_panel(){
	// For gutenberg
	if( get_current_screen()->is_block_editor() ){
		add_action( 'admin_footer', function(){ ?>
			<script>
				jQuery( window ).ready( function(){
					window.wp.data.dispatch('core/editor').disablePublishSidebar();
				} );
			</script>
			<?php 
		} );
		return;
	}
}


function fr_admin_change_submit_btn( $label ){


	// For gutenberg
	if( get_current_screen()->is_block_editor() ){
		add_action( 'admin_footer', function() use ( $label ){ ?>
			<script>

				jQuery(document).ready(function($) {

				    function changeButtonLabel() {
				        // Find the target button element, e.g., the Publish button
				        const targetButton = $('.editor-post-publish-button__button');
				        
				        // Check if the target button is found
				        if (targetButton.length) {
				            // Change the button label
				            targetButton.text( '<?php echo $label ?>' );
				        } else {
				            // If the target button is not found yet, try again after a short delay
				            setTimeout( changeButtonLabel, 10 );
				        }
				    }

				    // Call the changeButtonLabel function
				    changeButtonLabel();
				});

			</script>
			<?php 
		} );
		return;
	}




	add_filter( 'gettext', function( $translation, $text ) use ( $label ) {
		if ( $text == 'Publish' || $text == 'Update' ){
		    return $label;
		}
		return $translation;
	}, 10, 2 );

	add_action( 'admin_footer', function() use ( $label ){ ?>
		<script>
			jQuery( window ).ready( function( $ ){
				$( '#publishing-action input' ).remove();
				$( '#publishing-action' ).append( '<input type="submit" name="publish" id="fr-submit-btn" class="button button-primary button-large" value="<?php echo $label ?>">' );
			} );
		</script>
		<?php 
	} );
}




function fr_admin_add_submit_btn( $name, $label, $url = '#', $attr = [] ){

	if( get_current_screen()->is_block_editor() ){
		add_action( 'admin_footer', function() use ( $name, $label, $url, $attr ){
			$attr['id'] = 'fr-' . $name;
			$attr['class'] = 'components-button is-secondary';
			$attr['href'] = $url;
			?>
			<script>
				jQuery( window ).ready( function( $ ){

					var interval = setInterval( function(){
						if( $( '.edit-post-header__settings, .editor-header__settings' ).length ){
							clearInterval( interval );
							$( '.edit-post-header__settings, .editor-header__settings' ).append( '<a <?php echo fr_array_to_tag_attr( $attr ) ?>><?php echo $label ?></a>' );
						}
					}, 100 );

				} );
			</script>
			<?php 
		} );
	} else {
		add_action( 'post_submitbox_start', function() use ( $name, $label ){ ?>
			<button type="submit" name="<?php echo $name ?>" id="fr-<?php echo $name ?>-btn" class="button button-large"><?php echo $label ?></button>
			<?php 
		} );
	}
}



function fr_admin_disable_post_delete( $post_types ){
	if( !is_array( $post_types ) ){
		$post_types = [ $post_types ];
	}
	add_filter( 'map_meta_cap', function( $caps, $cap, $user_id, $args ) use ( $post_types ) {
	    // Nothing to do
	    if( 'delete_post' !== $cap || empty( $args[0] ) )
	        return $caps;

	    // Target the payment and transaction post types
	    if( in_array( get_post_type( $args[0] ), $post_types, true ) )
	        $caps[] = 'do_not_allow';       

	    return $caps;    
	}, 10, 4 );
}


function fr_admin_remove_bulk_action( $screen_id, $remove_actions ){
	if( !is_array( $remove_actions ) ){
		$remove_actions = [ $remove_actions ];
	}
	
	add_filter( 'bulk_actions-' . $screen_id, function( $actions ) use ( $remove_actions ){
		$actions = array_diff_key( $actions, array_flip( $remove_actions ) );

		return $actions;
	} );
}




function fr_admin_is_login(){
	return $GLOBALS['pagenow'] === 'wp-login.php';
}


function fr_admin_get_menu(){
	global $menu;
	return fr_array_column( $menu, 2 );
}

function fr_admin_change_menu_positions( $rules ){
	add_action( 'admin_menu', function() use ( $rules ) { 
		global $wp_post_types;
		global $menu;

		ksort( $menu, SORT_NUMERIC );

		// Normalize post_type names to menu ids
		$post_types = get_post_types();
		foreach( $rules as $id => $pos ){
			if( isset( $post_types[$id] ) ){
				$new_id = 'edit.php?post_type=' . $id;
				if( $id == 'post' ){
					$new_id = 'edit.php';
				}
				$rules = fr_array_relative_insert( $rules, [ $new_id => $pos ], $id );
				unset( $rules[$id] );
			}
		}

		$current_pos = [];
		foreach( $menu as $pos => $options ){
			$current_pos[$options[2]] = $pos;
		}

		$to_add = [];
		foreach( $rules as $id => $position ){
			if( isset( $current_pos[$id] ) ){
			  	$to_add[] = [
			  		'options' => $menu[$current_pos[$id]],
			  		'pos' => $position
			  	];
			  	unset( $menu[$current_pos[$id]] );
			}
		}

		foreach( $to_add as $rule ){
			$position = $rule['pos'] . '';
		  	while( isset( $menu[$position] ) ){
		  		$position++;
		  	}
		  	$menu[$position] = $rule['options'];
		}

		ksort( $menu, SORT_NUMERIC );
	}, 100 );
}



function fr_admin_add_menu_link( $menu, $position, $title, $link, $capabilities = 'manage_options' ){
	add_action( 'admin_menu', function() use ( $menu, $position, $title, $link, $capabilities ) {
	    global $submenu;

	    while( isset( $submenu[$menu][$position] ) ){
	    	$position++;
	    }
		$submenu[$menu][$position] = array( $title, $capabilities , $link );
	}, 100 );
}



function fr_admin_remove_menu_item( $item ){
	$func = function() use ( $item ){
		global $menu;
		foreach( $menu as $k => $v ){
			if( isset( $v[5] ) && $v[5] == $item ){
				unset( $menu[$k] );
			}
		}
	};

	if( did_action( 'admin_menu' ) ){
		$func();
	} else {
		add_action( 'admin_menu', $func );
	}
}


function fr_admin_users_order_by_date(){

	fr_admin_user_col( function( $cols ){ 
	  $cols['date_registered'] = __( 'Date Registered', 'vc' );
	  return $cols;
	}, function( $val, $col, $uid ){
	  if( $col == 'date_registered' ){
	    return fr_user( 'user_registered', $uid );
	  }
	} );

	add_action( 'pre_user_query', function( $query ) {
	    global $pagenow;

	    if ( ! is_admin() || 'users.php' !== $pagenow || isset( $_GET['orderby'] ) ) {
	        return;
	    }
	    $query->query_orderby = 'ORDER BY user_registered DESC';
	} );
}


function fr_is_admin(){
	return is_admin() && !fr_is_ajax();
}


function fr_disable_new_user_email(){
	remove_action( 'register_new_user', 'wp_send_new_user_notifications' );
	remove_action( 'edit_user_created_user', 'wp_send_new_user_notifications', 10, 2 );
}


function fr_admin_make_post_type_private( $target_post_types ){
	$target_post_types = fr_make_array( $target_post_types );

	add_filter( 'register_post_type_args', function( $args, $post_type ) use ( $target_post_types ){ 
		if( in_array( $post_type, $target_post_types ) ){
			$args['public'] = false;
			$args['publicly_queryable'] = false;
			$args['query_var'] = false;
		}
		return $args;
	}, 10, 2 );
}




function fr_admin_manage_post_status_filter( $post_type, $remove_statuses, $keep_only_statuses = [] ){
	$remove_statuses = fr_make_array( $remove_statuses );
	$keep_only_statuses = fr_make_array( $keep_only_statuses );
	add_filter( 'views_edit-' . $post_type, function( $views ) use ( $remove_statuses, $keep_only_statuses ){

		if( $remove_statuses ){
			$views = array_diff_key( $views, array_flip( $remove_statuses ) );
		}

		if( $keep_only_statuses ){
			$views = array_intersect_key( $views, array_flip( $keep_only_statuses ) );
		}

		return $views;
	}, 1000 );
}



function fr_add_menu_item_in_admin_bar( $id, $link, $title, $position = 10 ){

  add_action( 'admin_bar_menu', function( $wp_admin_bar ) use ( $id, $link, $title ) {

    if( is_callable( $title ) ){
    	$title = fr_call_if_func( $title );
    }

    $args = [
    	'id' => $id
    ];

    if( $link ){
    	$args['href'] = $link;
    }

    if( is_array( $title ) ){
    	$args = array_merge( $args, $title );
    } else {
        $args['title'] = $title;
    }

    $wp_admin_bar->add_node( $args );

  }, $position );

}








