<?php
function fr_menu_as_custom_select( $menu, $name = 'fr_menu_as_custom_select' ){
	$items = wp_get_nav_menu_items( $menu );
	$a = [];
	foreach( $items as $item ){
		$a[$item->url] = $item->title;
	}
	$select = fr_get_current_url( 1 );
	if( !isset( $a[$select] ) ){
		$parents = get_post_ancestors( get_the_ID() );
		foreach( $parents as $parent ){
			$link = fr_get_permalink( $parent );
			if( isset( $a[$link] ) ){
				$select = $link;
				break;
			}
		}
	}
	fr_custom_select( $name, $a, $select );
}


function fr_menu_get_item_children( $menu, $item ){
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object( $menu );
    $menu_items = wp_get_nav_menu_items( $menu->term_id );
    $found = array();
    foreach( $menu_items as $menu_item ){
        if( $menu_item->menu_item_parent == $item ){
            $found[] = $menu_item;       
        }
    }
    return $found;
}