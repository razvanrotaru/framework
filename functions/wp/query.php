<?php 

fr_on_post_meta_update_clear_cache();
fr_on_user_meta_update_clear_cache();



function fr_on_post_meta_update_clear_cache(): void {

    add_action( 'updated_post_meta', function(){
        fr_cache_clear( 'fr_query_get_postmeta_meta' );
    } );

}


function fr_on_user_meta_update_clear_cache(): void {

    add_action( 'updated_post_meta', function(){
        fr_cache_clear( 'fr_query_get_usermeta_meta' );
    } );

}



add_filter( 'posts_where', function( $where, $wp_query ){
    global $wpdb;
    if ( $title = $wp_query->get( 'title_like' ) ) {
        if( substr( $title, 0, 1 ) == '%' && substr( $title, -1, 1 ) == '%' ){
            $where .= ' AND ' . $wpdb->posts . '.post_title LIKE "%' . $wpdb->esc_like( str_replace( '%', '', $title ) ) . '%"';
        } else if( substr( $title, 0, 1 ) == '%' ){
            $where .= ' AND ' . $wpdb->posts . '.post_title LIKE "%' . $wpdb->esc_like( str_replace( '%', '', $title ) ) . '"';
        } else {
            $where .= ' AND ' . $wpdb->posts . '.post_title LIKE "' . $wpdb->esc_like( str_replace( '%', '', $title ) ) . '%"';
        }
    }
    return $where;
}, 10, 2 );



function fr_query_get_ids($q2=array(),$ppp=1000000,$keep_order=false, $no_cache = false ){

    if( is_object( $q2 ) ){
        $ids = [];
        foreach( $q2->posts as $post ){
            $ids[] = $post->ID;
        }
        return $ids;
    }

    $defaults = array(
        'no_found_rows' => true,
    );
    
    if(!isset($q2["orderby"]) && !isset($q2["order"]) && !$keep_order){
        $defaults["orderby"]="none";
    }

    $q2 = array_merge( $defaults, $q2 );

    $q2['fields'] = 'ids';

    if( $no_cache ){
        $q2['cache_results'] = false;
        $q2['update_post_term_cache'] = false;
        $q2['update_post_meta_cache'] = false;
    }

    $q2['posts_per_page'] = $ppp;

    $res = new WP_Query( $q2 );
    $ids = $res->posts;
    $ids = apply_filters( 'fr_query_get_ids', $ids, $q2 );

    return $ids;
}

function fr_query_get_id( $q2=array(), $ppp=1000000, $keep_order=false, $no_cache = false ){
    $ids = fr_query_get_ids($q2,$ppp,$keep_order,$no_cache);
    if( $ids ){
        return reset( $ids );
    }
}



function fr_query_get_terms_by_names( $keywords, $taxonomies = [], $like = false ){

    global $wpdb;

    $taxonomies = fr_make_array( $taxonomies );


    if( $like ){

        
        $query = "
            SELECT t.*, tt.*
            FROM {$wpdb->terms} AS t
            INNER JOIN {$wpdb->term_taxonomy} AS tt ON t.term_id = tt.term_id
            WHERE tt.taxonomy IN ('" . implode( "','", $taxonomies ) . "')
        ";

        // add wildcards for each keyword with space before, after or both
        
        $keyword_query = [];

        foreach( $keywords as $key => $keyword ){

            $keyword = strtolower( $keyword );
            $keyword = $wpdb->esc_like( $keyword );

            $keyword_query[] = "LOWER(t.name) = '" . $keyword . "'";
            $keyword_query[] = "LOWER(t.name) LIKE '% " . $keyword . " %'";
            $keyword_query[] = "LOWER(t.name) LIKE '" . $keyword . " %'";
            $keyword_query[] = "LOWER(t.name) LIKE '% " . $keyword . "'";

            // Plural variant
            $keyword_query[] = "LOWER(t.name) = '" . $keyword . "s'";
            $keyword_query[] = "LOWER(t.name) LIKE '% " . $keyword . "s %'";
            $keyword_query[] = "LOWER(t.name) LIKE '" . $keyword . "s %'";
            $keyword_query[] = "LOWER(t.name) LIKE '% " . $keyword . "s'";

        }

        $keyword_query = implode( ' OR ', $keyword_query );

        $query .= " AND ( $keyword_query )";

    } else {
       
        $placeholders = implode(', ', array_fill(0, count($keywords), '%s'));

        $query = "
            SELECT t.*, tt.*
            FROM {$wpdb->terms} AS t
            INNER JOIN {$wpdb->term_taxonomy} AS tt ON t.term_id = tt.term_id
            WHERE tt.taxonomy IN ('" . implode( "','", $taxonomies ) . "')
            AND LOWER(t.name) IN ($placeholders)
        ";

        $query = $wpdb->prepare($query, $keywords);
        
    }

    $terms = $wpdb->get_results( $query, ARRAY_A );

    return $terms;

}


function fr_query_get_total( $q ){

    $q = array_merge( $q, [
        'fields' => 'ids',
        'no_found_rows' => false,
        'posts_per_page' => 1,
        'orderby' => 'none',
        'paged' => 1
    ] );

    $res = new WP_Query( $q, 1 );

    return $res->found_posts;
}

function fr_user_query( $query = [] ){
    $default_query =  [
        'fields' => 'all'
    ];
    $query = array_merge( $default_query, $query );
    $res = new WP_User_Query( $query );
    return $res->results;
}


function fr_user_query_get_ids( $query = [], $number = -1, $keep_order = false, $no_cache = false ){
    $defaults = array(
        'fields' => 'ids',
        'no_found_rows' => true,
        'number' => $number,
        'count_total' => false
    );

    if( $no_cache ){
        $defaults['cache_results'] = false;
    }
    
    if( empty( $query["orderby"] ) && empty( $query["order"] ) && !$keep_order ){
        $defaults['orderby'] = 'none';
    }

    $query = array_merge( $defaults, $query );

    $res = new WP_User_Query( $query );
    $ids = $res->results;
    $ids = apply_filters( 'fr_user_query_get_ids', $ids, $query );
    return $ids;
}


function fr_user_query_get_id( $query = [], $number = -1, $keep_order = false, $no_cache = false ){
    $ids = fr_user_query_get_ids( $query, $number, $keep_order, $no_cache );
    if( $ids ){
        return reset( $ids );
    }
}


function fr_show_db_queries(){
    global $wpdb;
    $q=$wpdb->queries;
    usort($q,function($a,$b){return $a[1] <=> $b[1];});
    $total=0;
    foreach($q as $v){
        $total+=$v[1];
    }
    fr_p("query time: ".$total);//ok
    fr_p("query count: ".count($q));//ok
    foreach($q as $v){
        unset($v[2]);
        fr_p($v[0],$v[1]);//ok
    }
}






/**
 * Helper function to retrieve meta data for multiple posts at the same time
 * It makes queries more efficient
 * @param  int $id   ID of the post
 * @param  array $meta  An array of meta keys to be retrieved. If empty it will return meta values for all meta keys
 * @param  bool  $return_only_first_key  It will return the value for only one key
 * @param  bool  @add_default If the meta doesn't exist for that post type it will set it as empty by default
 * @return array        An array of post ids as keys which contain their respective meta
 */
function fr_query_get_post_meta( $pid, $meta = [], $return_only_first_key = false, $add_default = true ){

    $pid = $pid ?: 0;
    $res = fr_query_get_posts_meta( [ $pid ], $meta, $return_only_first_key, $add_default );
    
    return reset( $res );
}






/**
 * Helper function to retrieve meta data for multiple posts at the same time
 * It makes queries more efficient
 * @param  array $ids   An array of post ids
 * @param  array $meta  An array of meta keys to be retrieved. If empty it will return meta values for all meta keys
 * @param  bool  $return_only_first_key  It will return the value for only one key
 * @param  bool  @add_default If the meta doesn't exist for that post type it will set it as empty by default
 * @return array        An array of post ids as keys which contain their respective meta
 */
function fr_query_get_posts_meta( $ids, $meta_keys = [], $return_only_first_key = false, $add_default = true ){
    return fr_query_get_meta( 'postmeta', 'post_id', $ids, $meta_keys, $return_only_first_key, $add_default );
}






/**
 * Lightweight function to get only the post ids
 * @param  int    $post_id    The ID of the post
 * @param  array $taxonomies  An array of taxonomies
 * @return array              An array of taxonomies
 */
function fr_query_get_post_terms( int $post_id, array $taxonomies ): array {

   $post_id = $post_id ?: 0;
   $terms = fr_query_get_posts_terms( [ $post_id ], $taxonomies );
   $terms = $terms[$post_id];

   return $terms;

}







/**
 * Return terms grouped by multiple taxonomies grouped by multiple post ids
 * @param  array  $post_ids   An array of post ids
 * @param  array  $taxonomies An array of taxonomies
 * @return array
 */
function fr_query_get_posts_terms( array $post_ids, array $taxonomies ): array {

    global $wpdb;

    // Abort if no post ids
    if( !$post_ids ){
        return [];
    }

    $post_ids = array_map( 'intval', $post_ids );
    $taxonomies = array_map( 'esc_sql', $taxonomies );

    // Query the database using the $wpdb global variable
    $results = $wpdb->get_results(
        "SELECT *
        FROM $wpdb->term_relationships AS tr
        INNER JOIN $wpdb->term_taxonomy AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
        INNER JOIN $wpdb->terms AS t ON tt.term_id = t.term_id
        WHERE tr.object_id IN (" . implode( ',', $post_ids ) . ")
        AND tt.taxonomy IN ('" . implode( "','", $taxonomies ) . "')",
        ARRAY_A
    );

    // Set the arrays for each post_id in $terms
    foreach( $post_ids as $post_id ){
        $terms[$post_id] = [];

        foreach( $taxonomies as $taxonomy ){
            $terms[$post_id][$taxonomy] = [];
        }
    }

    $terms = [];

    // Set default taxonomies
    foreach( $post_ids as $post_id ){
        foreach( $taxonomies as $taxonomy ){
            $terms[$post_id][$taxonomy] = [];
        }
    }

    // Loop through the results and group the terms by taxonomy
    foreach( $results as $result ){
        $terms[ $result['object_id'] ] = $terms[ $result['object_id'] ] ?? [];
        $terms[ $result['object_id'] ][ $result['taxonomy'] ] = $terms[ $result['object_id'] ][ $result['taxonomy'] ] ?? [];
        $terms[ $result['object_id'] ][ $result['taxonomy'] ][ $result['term_id'] ] = $result;
    }

    return $terms;

}






/**
 * By default fr_query_get_posts_terms groups by post ID
 * This function groups by taxonomy => term_id => post_id
 */
function fr_group_term_query_taxonomy_to_terms_to_posts( $posts_with_terms ){

    $grouped_terms = [];

    foreach( $posts_with_terms as $talent_id => $taxonomies ){

        foreach( $taxonomies as $taxonomy => $terms ){

            $grouped_terms[$taxonomy] = $grouped_terms[$taxonomy] ?? [];

            foreach( $terms as $term ){
                $grouped_terms[$taxonomy][$term['term_id']] = $grouped_terms[$taxonomy][$term['term_id']] ?? [];
                $grouped_terms[$taxonomy][$term['term_id']][] = $talent_id;
            }
        }

    }

    return $grouped_terms;

}






/**
 * By default fr_query_get_posts_terms groups by post ID
 * This function groups by taxonomy => post_id => terms
 */
function fr_group_term_query_taxonomy_to_posts_to_terms( $posts_with_terms, $return_only_term_ids = false ){

    $grouped_terms = [];

    foreach( $posts_with_terms as $talent_id => $taxonomies ){

        foreach( $taxonomies as $taxonomy => $terms ){

            $grouped_terms[$taxonomy] = $grouped_terms[$taxonomy] ?? [];
            $grouped_terms[$taxonomy][$talent_id] = $return_only_term_ids ? array_keys( $terms ) : $terms;

        }

    }

    return $grouped_terms;

}









/**
 * Helper function to retrieve meta data
 * It makes queries more efficient
 * @param  array $table DB table name without prefix
 * @param  array $table_key   DB table key to match the IDs
 * @param  array $ids   An array of ids
 * @param  array $meta_keys  An array of meta keys to be retrieved. If empty it will return meta values for all meta keys
 * @param  bool  $return_only_first_key  It will return the value for only one key
 * @param  bool  @add_default If the meta doesn't exist for that post type it will set it as empty by default
 * @param  bool  $cache Whatever to cache or not the query. Defaults to true
 * @return array        An array of ids as keys which contain their respective meta
 * HAS TEST
 */
function fr_query_get_meta( $table, $table_key, $ids, $meta_keys = [], $return_only_first_key = false, $add_default = true, $cache = true ){
    global $wpdb;

    // Abort if no post ids
    if( !$ids ){
        return [];
    }

    // If ids is not an array it means a single post id was given and we make it an array
    if( !is_array( $ids ) ){
        $ids = [ $ids ];
    }

    $ids = array_unique( $ids );

    // Make sure all ids are int
    $ids = array_map( 'intval', $ids );

    if( !is_array( $meta_keys ) ){
        $meta_keys = [ $meta_keys ];
    }



    // Predefine the list

    $actual_values = [];
    $placeholder_values = [];

    foreach( $ids as $id ){
        
        $actual_values[$id] = [];
        $placeholder_values[$id] = [];

        foreach( $meta_keys as $meta_key ){
            $placeholder_values[$id][$meta_key] = '';
        }

    }



    // Check the WP cache first
    
    if( $cache ){
        foreach ( $ids as $id ) {

            $cached_metas = wp_cache_get( $id, str_replace( 'meta', '_meta', $table ) );

            if( !$cached_metas ){
                continue;
            }
            
            foreach( $cached_metas as $k => $v ){
                if( is_serialized( $v[0] ) ){
                    $cached_metas[$k][0] = unserialize( $v[0] );
                }
            }

            if( $meta_keys ){
                foreach ($meta_keys as $key) {

                    if ( !isset($cached_metas[$key])) {
                        continue;
                    }

                    // keep only the first value
                    if( isset( $actual_values[$id][$key] ) ){
                        continue;
                    }
                    
                    $placeholder_values[$id][$key] = $actual_values[$id][$key] = reset( $cached_metas[$key] );
                }
            } else {

                foreach( $cached_metas as $meta_key => $meta_value ){
                    $placeholder_values[$id][$meta_key] = $actual_values[$id][$meta_key] = reset( $meta_value );
                }

            }

        }
    }



    $meta_key_count = count( $meta_keys );
    $ids_to_search = [];

    foreach( $ids as $id ){

        if( $meta_key_count == 0 ){
            if( empty( $actual_values[$id] ) ){
                $ids_to_search[] = $id;
            }
        } else {
            if( count( $actual_values[$id] ) != $meta_key_count ){
                $ids_to_search[] = $id;
            }
        }

    }



    if( $ids_to_search ){
        

        $query = 'SELECT ' . $table_key . ', meta_key, meta_value FROM ' . $wpdb->prefix . $table . ' WHERE ' . $table_key . ' IN ( ' . implode( ",", $ids_to_search ) . ' )';
        
        // Sanitize meta keys
        if( $meta_keys ){
            $sinitized_meta_keys = [];
            foreach( $meta_keys as $key => $meta_key ){
                $sinitized_meta_keys[$key] = $wpdb->prepare( '%s', $meta_key );
            }
            $query .= ' AND meta_key IN ( ' . implode( ",", $sinitized_meta_keys ) . ' )';
        }


        // check our cache

        $cache_key = [ 'fr_query_get_' . $table . '_meta', $query, $return_only_first_key, $add_default ];
        $results = $cache ? fr_cache( $cache_key ) : false;


        if( $results === false ){

            $results = $wpdb->get_results( $query, ARRAY_A );
            fr_cache( $cache_key, $results );

        }



        foreach( $results as $result ){

            $id = $result[$table_key];
            $key = $result['meta_key'];
            $value = $result['meta_value'];

            if( isset( $actual_values[$id][$key] ) ){
                continue;
            }

            if( is_serialized( $value ) ){
                $value = unserialize( $value );
            }

            $placeholder_values[$id][$key] = $actual_values[$id][$key] = $value;

        }

    }


    if( $add_default ){
        $final_list = $placeholder_values;
    } else {
        $final_list = $actual_values;
    }


    if( $return_only_first_key ){
        foreach( $final_list as $id => $values ){
            $final_list[$id] = reset( $values );
        }
    }

    
    return $final_list;
}





/**
 * Helper function to retrieve meta data for multiple users at the same time
 * It makes queries more efficient
 * @param  int $id   ID of the user
 * @param  array $meta  An array of meta keys to be retrieved. If empty it will return meta values for all meta keys
 * @param  bool  $return_only_first_key  It will return the value for only one key
 * @param  bool  @add_default If the meta doesn't exist for that post type it will set it as empty by default
 * @return array        An array of post ids as keys which contain their respective meta
 */
function fr_query_get_user_meta( $id, $meta_keys = [], $return_only_first_key = false, $add_default = true ){

    $id = $id ?: 0;
    $res = fr_query_get_users_meta( [ $id ], $meta_keys, $return_only_first_key, $add_default );

    return reset( $res );

}





/**
 * Helper function to retrieve meta data for multiple users at the same time
 * It makes queries more efficient
 * @param  array $ids   An array of user ids
 * @param  array $meta  An array of meta keys to be retrieved. If empty it will return meta values for all meta keys
 * @param  bool  $return_only_first_key  It will return the value for only one key
 * @param  bool  @add_default If the meta doesn't exist for that post type it will set it as empty by default
 * @return array        An array of post ids as keys which contain their respective meta
 */
function fr_query_get_users_meta( $ids, $meta_keys = [], $return_only_first_key = false, $add_default = true ){
    return fr_query_get_meta( 'usermeta', 'user_id', $ids, $meta_keys, $return_only_first_key, $add_default );
}




/**
 * Helper function to retrieve post fields data for multiple posts at the same time
 * It makes queries more efficient
 * @param  array $ids   An array of post ids
 * @param  array $field_keys  An array of post field keys to be retrieved. If empty it will return all post fields
 * @param  bool  $return_only_first_key  It will return the value for only one key
 * @return array        An array of post ids as keys which contain their respective fields
 */
function fr_query_get_posts_fields( $ids, $field_keys = [], $return_only_first_key = false ){
    return fr_query_get_table_fields( 'posts', 'ID', $ids, $field_keys, $return_only_first_key );
}



/**
 * Helper function to retrieve post fields data for a single post
 * It makes queries more efficient
 * @param  array $id   Post id
 * @param  array $field_keys  An array of post field keys to be retrieved. If empty it will return all post fields
 * @param  bool  $return_only_first_key  It will return the value for only one key
 * @return array        An array of post ids as keys which contain their respective fields
 */
function fr_query_get_post_fields( $ids, $field_keys = [], $return_only_first_key = false ){
    
    $fields = fr_query_get_posts_fields( [ $ids ], $field_keys, $return_only_first_key );
    return reset( $fields );

}




/**
 * Helper function to retrieve fields data for multiple ids at the same time
 * It makes queries more efficient
 * @param  array $table   WP table name without the prefix
 * @param  array $id_col  The name of the column that contains the id
 * @param  array $ids   An array of ids
 * @param  array $field_keys  An array of post field keys to be retrieved. If empty it will return all post fields
 * @param  bool  $return_only_first_key  It will return the value for only one key
 * @return array        An array of post ids as keys which contain their respective fields
 */
function fr_query_get_table_fields( $table, $id_col, $ids, $field_keys = [], $return_only_first_key = false ){
    
    global $wpdb;

    // Abort if no post ids
    if( !$ids ){
        return [];
    }

    // If ids is not an array it means a single post id was given and we make it an array
    if( !is_array( $ids ) ){
        $ids = [ $ids ];
    }

    // Make sure all ids are int
    $ids = array_map( 'intval', $ids );

    $field_keys = fr_make_array( $field_keys );

    if( $field_keys ){
        $field_keys[] = $id_col;
        $field_keys = array_unique( $field_keys );
    } else {
        $field_keys = [ '*' ];
    }

    $query = 'SELECT ' . implode( ',', $field_keys ) . ' FROM ' . $wpdb->prefix . $table . ' WHERE ID IN ( ' . implode( ",", $ids ) . ' )';

    // Make the query
    $results = $wpdb->get_results( $query, ARRAY_A );

    // Group the results by post id
    $posts = [];

    foreach( $results as $fields ){
        $id = $fields[$id_col];
        if( $return_only_first_key ){
            $posts[$id] = reset( $fields );
        } else {
            unset( $fields[$id_col] );
            $posts[$id] = $fields;
        }
    }

    uksort( $posts, function( $a, $b ) use( $ids ) { return array_search( $a, $ids ) <=> array_search( $b, $ids ); } );

    return $posts;
}







/**
 * Helper function to retrieve post fields data for multiple users at the same time
 * It makes queries more efficient
 * @param  array $ids   An array of post ids
 * @param  array $field_keys  An array of post field keys to be retrieved. If empty it will return all post fields
 * @param  bool  $return_only_first_key  It will return the value for only one key
 * @return array        An array of post ids as keys which contain their respective fields
 */
function fr_query_get_users_fields( $ids, $field_keys = [], $return_only_first_key = false ){

    return fr_query_get_table_fields( 'users', 'ID', $ids, $field_keys, $return_only_first_key );

}









// To enable set in wp-config.php: define('SAVEQUERIES', true);
function fr_log_queries( $file ){

    if( !file_exists( $file ) ){
        file_put_contents( $file, '' );
    }

    add_action('shutdown', function() use ( $file ){
        global $wpdb;
        if( !$wpdb->queries ){ return; };
        $log_file = fopen( $file, 'a' );
        $total = array();
        foreach( $wpdb->queries as $v ){
            $total[] = $v[1];
        }
        fwrite( $log_file, 'Page:' . fr_get_current_url() . ' ' . date("F j, Y, g:i:s a") . ' count:' . count( $total ) . ' total:' . array_sum( $total ) . "\n" );
        usort( $wpdb->queries, function( $a, $b ){ return $a[1] <=> $b[1]; });
        foreach( $wpdb->queries as $k => $q ) {
            $query = str_replace( "\n", " ", $q[0] );
            $query = str_replace( "\t", " ", $query );
            $query = str_replace( "  ", " ", $query );
            $query = str_replace( "  ", " ", $query );
            fwrite($log_file, $k . ". " . $query . " - ($q[1] s)" . "\n");
        }
        fwrite( $log_file, "\n\n\n" );
        fclose($log_file);
    });
}


class fr_wpdb_query {

    var $where = [];
    var $prepare = [];
    var $select = '';
    var $table = '';
    var $after = '';
    var $last_query = '';

    function __construct( $select, $table, $after = '' ){
        $this->select = $select;
        $this->table = $table;
        $this->after = $after;
    }

    function where( $where ){
        $args = func_get_args();
        array_shift( $args );
        if( $args ){
            $this->prepare = array_merge( $this->prepare, $args );
        }
        if( strstr( $where, ' OR ' ) ){
            $where = '(' . $where . ')';
        }
        $this->where[] = $where;
    }
    
    function where_in( $col, $ids ){

        if( !$ids ){
            $ids = [ -1 ];
        }

        $ids = array_map( 'intval', $ids );
        $this->where[] = $col . ' IN (' . implode( ',', $ids ) . ')';
    }

    function where_not_in( $col, $ids ){

        if( !$ids ){
            $ids = [ -1 ];
        }

        $ids = array_map( 'intval', $ids );
        $this->where[] = $col . ' NOT IN (' . implode( ',', $ids ) . ')';
    }

    function prepare( $prepare ){
        $this->prepare[] = $prepare;
    }

    function get(){
        global $wpdb;
        $query = 'SELECT ' . $this->select . ' FROM ' . $wpdb->prefix . $this->table . ' WHERE ' . implode( ' AND ', $this->where ) . ' ' . $this->after;
        if( !$this->where ){
            $query = str_replace( ' WHERE ', '', $query );
        }

        if( $this->prepare ){
            $query = $wpdb->prepare( $query , $this->prepare );
        }
        $this->last_query = $query;
        return $wpdb->get_results( $query, ARRAY_A );
    }

    function get_row(){
        $rows = $this->get();
        return reset( $rows );
    }

    function get_last_query(){
        return $this->last_query;
    }

    function get_var(){
        $res = $this->get();
        if( $res ){
            $res = reset( $res );
            return reset( $res );
        }
    }

    function get_col( $col ){
        $res = $this->get();
        $res = array_column( $res, $col );
        return $res;
    }
}




/**
 * Extracts the IDs from a WP_Query
 */
function fr_wp_query_get_ids( $query ){
    $ids = $query->posts;
    if( $ids ){
        foreach( $ids as $k => $id ){
            $ids[$k] = $id->ID;
        }
    } else {
        $ids = [];
    }
    return $ids;
}


function fr_wpdb_last(){
    global $wpdb;
    return $wpdb->last_query;
}


function fr_hijack_post_meta_value( $pid, $meta_key, $meta_value ){
    $meta_cache = wp_cache_get($pid, 'post_meta');
    if ( !$meta_cache ) {
        $meta_cache = update_meta_cache( 'post', array( $pid ) );
        $meta_cache = $meta_cache[$pid];
    }
    // works only for single values
    $meta_cache[$meta_key] = array(
        $meta_value
    );

    wp_cache_set( $pid, $meta_cache, 'post_meta' );
}


function fr_wpdb_vars(){
    global $wpdb;
    $res = $wpdb->get_results( 'SHOW VARIABLES' );
    return array_column( $res, 'Value', 'Variable_name' );
}

function fr_wpdb_var( $k, $v = false ){
    global $wpdb;
    if( $v === false ){
        $res = $wpdb->get_results( 'SHOW VARIABLES LIKE "' . $k . '"' );
        return array_column( $res, 'Value', 'Variable_name' );
    } else {
        return $wpdb->query( 'SET GLOBAL ' . $k . ' = "' . $wpdb->escape( $v ) . '"' );
    }
}



/**
 * WP by default doesn't have functionality to sort by post status
 * This function adds that functionality
 */
function fr_wp_sort_by_status(){
    add_action( 'posts_orderby', function( $orderby, $query ){

        if( !isset( $query->query_vars['orderby'] ) || is_array( $query->query_vars['orderby'] ) || !str_contains( $query->query_vars['orderby'], 'post_status' ) ){
            return $orderby;
        }

        $table = strstr( $orderby, '.', true );
        $orderby = $table . '.post_status ' . $query->query_vars['order'];

        return $orderby;

    }, 10, 2 );
}



function fr_strip_sql_file_of_other_tables($file_path, $table_name, $new_table_name = '', $action = 'download') {
    $processor = new fr_strip_sql_file_of_other_tables($file_path, $table_name, $new_table_name, $action );
    return $processor->process();
}





class fr_strip_sql_file_of_other_tables {

    private $file_path;
    private $table_name;
    private $new_table_name;
    private $pattern;
    private $new_file_path;
    private $in_relevant_table = false;
    private $found_table_declaration = false;
    private $action;

    public function __construct( $file_path, $table_name, $new_table_name = '', $action = 'download' ) {

        $this->file_path = $file_path;
        $this->table_name = $table_name;
        $this->new_table_name = $new_table_name;
        $this->pattern = '/\b' . preg_quote( $table_name, '/' ) . '\b/i';
        $this->action = $action;
        $this->set_new_file_path();

    }

    public function process() {

        if( !file_exists( $this->file_path ) ) {
            return false;
        }
        set_time_limit( 3600 );

        $input = fopen( $this->file_path, 'r' );
        $output = fopen( $this->new_file_path, 'w' );
        if( !$input || !$output ) {
            return false;
        }

        $buffer = '';
        while( !feof( $input ) ) {
            $buffer .= fread( $input, 8192 );
            $buffer = $this->process_buffer( $buffer, $output );
        }
        $this->process_buffer( $buffer, $output, true );

        fclose( $input );
        fclose( $output );

        if( $this->action == 'download' ) {
            $this->force_download();
            unlink( $this->new_file_path );
            return true;
        } elseif( $this->action == 'import' ) {
            $result = fr_import_sql_file( $this->new_file_path );
            unlink( $this->new_file_path );
            return $result;
        }

        return $this->new_file_path;

    }

    private function process_buffer( $buffer, $output, $force_process = false ) {

        $lines = explode( "\n", $buffer );
        $last_line = array_pop( $lines );
        
        foreach( $lines as $line ) {
            $line = trim( $line );
            if( empty( $line ) ) continue;

            if( $this->is_table_declaration( $line ) ) {
                $this->in_relevant_table = $this->is_relevant_table( $line );
                if( $this->in_relevant_table ) {
                    $this->found_table_declaration = true;
                }
            }

            if( $this->should_include_line( $line ) ) {
                $this->write_line( $output, $line );
            }
        }

        if( $force_process && !empty( trim( $last_line ) ) ) {
            if( $this->should_include_line( $last_line ) ) {
                $this->write_line( $output, $last_line );
            }
            return '';
        }

        return $last_line;

    }

    private function is_table_declaration( $line ) {

        return preg_match( '/^(?:CREATE TABLE|INSERT INTO|DROP TABLE IF EXISTS) `([^`]+)`/', $line );

    }

    private function is_relevant_table( $line ) {

        return preg_match( $this->pattern, $line );

    }

    private function should_include_line( $line ) {

        if( $this->in_relevant_table ) {
            return true;
        }
        
        if( $this->found_table_declaration && preg_match( $this->pattern, $line ) ) {
            return true;
        }

        if( !$this->found_table_declaration && $this->is_table_declaration( $line ) && $this->is_relevant_table( $line ) ) {
            return true;
        }

        return false;

    }

    private function write_line( $output, $line ) {

        if( $this->new_table_name ) {
            $line = preg_replace( $this->pattern, $this->new_table_name, $line );
        }
        fwrite( $output, $line . "\n" );

    }

    private function set_new_file_path() {

        $path_info = pathinfo( $this->file_path );
        $new_file_name = $path_info['filename'] . '_' . ( $this->new_table_name ?: $this->table_name );
        $this->new_file_path = $path_info['dirname'] . DIRECTORY_SEPARATOR . 
                               $new_file_name . ( !empty( $path_info['extension'] ) ? '.' . $path_info['extension'] : '' );

    }

    private function force_download() {

        fr_force_download( $this->new_file_path );

    }

}




function fr_import_sql_file( $file_path, $old_table_name = '', $new_table_name = '' ) {

    if( !file_exists( $file_path ) ) {
        return false;
    }

    set_time_limit( 3600 );

    global $wpdb;

    $file = fopen( $file_path, 'r' );
    if( !$file ) {
        return false;
    }

    $executed_queries = 0;
    $statement = '';

    while( !feof( $file ) ) {

        $line = fgets( $file );
        if( $new_table_name ){
            $line = preg_replace( "/\b" . preg_quote( $old_table_name, '/') . "\b/i", $new_table_name, $line );
        }
        $statement .= $line;

        if( substr( trim( $line ), -1 ) === ';' ) {

            $statement = trim( $statement );
            if( !empty( $statement ) ) {

                try {
                    $result = $wpdb->query( $statement );
                    if( $result !== false ) {
                        $executed_queries++;
                    }
                } catch( Exception $e ) {
                    error_log( 'Error executing SQL statement: ' . $e->getMessage() );
                }

            }
            $statement = '';

        }

    }

    fclose( $file );

    return $executed_queries;

}


