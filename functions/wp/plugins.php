<?php 

/**
 * Remove plugin update notifications
 *
 * @param string $plugin Path to plugin. Example: plugin_folder/plugin_file.php
 */

function fr_plugin_remove_update_notification($plugin){
	global $fr_plugin_remove_update_notifications;
	if(empty($fr_plugin_remove_update_notifications)){
		$fr_plugin_remove_update_notifications = array();
	}

	$fr_plugin_remove_update_notifications[]=$plugin;
}

add_filter('site_transient_update_plugins', 'fr_plugins_remove_update_notification');
function fr_plugins_remove_update_notification($plugins) {
	global $fr_plugin_remove_update_notifications;

	if(!is_object($plugins)){
		return $plugins;
	}

	if(!empty($fr_plugin_remove_update_notifications)){
		foreach($fr_plugin_remove_update_notifications as $plugin){
		    unset($plugins->response[ $plugin ]);
		}
    }
    return $plugins;
} 