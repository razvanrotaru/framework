<?php


//Converts system path to URL

function fr_path_to_url( $path ){

	$root = str_replace( DIRECTORY_SEPARATOR, '/', ABSPATH );
	$path = str_replace( '', '/', $path ); // Compensate for windows dirs
	$path = str_replace( $root, get_site_url() . '/', $path );
	$path = str_replace( ':// ', ':// /', $path );
	$path = str_replace( '// ', '/', $path );

	return $path;

}


//Converts URL to system path

function fr_url_to_path( $url, $root = false, $site_url = false ){

	if( !$root ){
		$root = ABSPATH;
	}

	if( !$site_url ){
		$site_url = get_site_url() . '/';
	}
	
	$http_site_url = str_replace( 'https://', 'http://', $site_url );
	
	$path = urldecode( $url );
	$path = explode( '?', $path );
	$path = reset( $path );
	$path = explode( '#', $path );
	$path = reset( $path );
	$path = str_replace( $site_url, $root, $path );
	$path = str_replace( $http_site_url, $root, $path );
	$path = str_replace( '/', DIRECTORY_SEPARATOR, $path ); // Compensate for windows dirs

	return $path;

}



/**
 * Automatically register shortcodes from a directory
 */

function fr_register_shortcodes( $dir = '' ){
	if( !$dir ){
  		$dir=THEME_DIR."/shortcodes";
  	}
	if(file_exists($dir)){
		$files=scandir($dir);
		foreach($files as $k=>$v){
			$file=$dir."/".$v;
			if(strstr($file,".php")){
				$shortcode=substr($v,0,-4);
				add_shortcode( $shortcode,function($attr,$content,$tag) use ( $dir, $shortcode ){
					do_action( 'fr_do_shortcode', $tag, $attr, $content );

					// WP sometimes returns unclosed <p> tags
					if( substr( $content, 0, 4 ) == '</p>' ){
						$content = substr( $content, 4 );
					}
					if( substr( $content, -3, 3 ) == '<p>' ){
						$content = substr( $content, 0, -3 );
					}

					$content = trim( $content );

					if( empty( $attr ) ){
						$attr = [];
					}

					ob_start();
						include( $dir."/".$tag.".php");
					$c=ob_get_clean();

					return $c;
				});
			}
	  	}
	}
}



function fr_shortcode_filter( $name, $callback ){
	add_action( 'template_redirect', function() use ( $name, $callback ){ 
		global $shortcode_tags;

		if( isset( $shortcode_tags[$name] ) ){
			$orig_callback = $shortcode_tags[$name];
			$shortcode_tags[$name] = function( $atts, $content = '', $tag = '' ) use ( $callback, $orig_callback ){ 
				$result = fr_call_if_func( $callback, [ $atts, $content, $tag ] );
				if( is_array( $result ) ){
					return call_user_func_array( $orig_callback, $result );
				} else {
					return $result;
				}
			};
		}
	} );
}



/**
 * Allow WP to parse shortcodes in menus
 */

function fr_enable_menu_shortcodes(){
	if( !is_admin() ){
		add_filter('nav_menu_item_title', function ( $title, $item, $args, $depth ) {
		    return do_shortcode( $title );
		}, 10, 4);
	}
}



function fr_get_filters( $hook = '', $priority = false ) {
    global $wp_filter;

    if( $priority !== false ){
    	if( isset( $wp_filter[$hook][$priority] ) ){
	    	return $wp_filter[$hook][$priority];
	    }
    } else if( $hook ){
        if( isset( $wp_filter[$hook] ) ){
        	return $wp_filter[$hook];
        }
    } else {
    	return $wp_filter;
    }
}


function fr_remove_filter($hook,$priority=false,$action="", $contains = '' ){
    global $wp_filter;
    if($priority===false){
        unset($wp_filter[$hook]);
    } else if( $contains ){
    	foreach( $wp_filter[$hook][$priority] as $k => $v ){
    		$v = print_r( $v, 1 );
    		if( strstr( $v, $contains ) ){
    			remove_filter( $hook, $v, $priority );
    		}
    	}
    } else if($action==""){
        unset($wp_filter[$hook][$priority]);
    }else{
        remove_filter( $hook, $action, $priority );
    }
}


/**
 * fr_uploads_dir
 * v2.0
 * - support for real_dir
 */
function fr_uploads_dir( $real_dir = false ){
	$uploads = wp_upload_dir();
	$dir = $uploads["basedir"];
	if( $real_dir ){
		$dir = explode( '/wp-content/', $dir );
		$dir = ABSPATH . 'wp-content/' . $dir[1];
	}
	return $dir;
}

function fr_uploads_url(){
	$uploads=wp_upload_dir();
	return $uploads["baseurl"];
}


function fr_days_from_now($date,$string="%s days from now"){
	$days=(strtotime(date("Y-m-d 00:00:00",strtotime($date)))-strtotime(date("Y-m-d 00:00:00",fr_time())))/(3600*24);
	if($days==-1){
		$day = "Yesterday";
	}else if($days==0){
		$day=__("Today");
	}else if($days==1){
		$day=__("Tomorrow");
	}else{
		$day=sprintf(__($string),round($days));
	}
	return $day;
}


//uses wp timezone setting
function fr_time( $time = null, $offset = false ){

	if( $offset === false ){
		$offset = get_option('gmt_offset');
	}

	if( $time && !is_int( $time ) ){
		$time = strtotime( $time );
	}

	$offset=$offset*60;
	if($offset>0){
		$offset="+".abs($offset);
	}else{
		$offset="-".abs($offset);
	}
	$offset.=" minutes";
	return $time ? strtotime($offset, $time ) : strtotime($offset);
}


/**
 * Automatically load mo files from a directory
 * @param  string $dir Directory path
 * @return string      The path to the new mo file 
 */
function fr_autoload_mo_files( $dir = "" ){
  // Set the theme as the default directory
  if( !$dir ){
    $dir = get_stylesheet_directory() . '/languages/';
  }

  // Set the new mo files if they exist
  add_filter( 'load_textdomain_mofile', function ( $mofile, $domain ) use ( $dir ) {
    $file = $dir . $domain . '-' . get_locale() . '.mo';
    if ( file_exists( $file ) ) {
      $mofile = $file;
    }
    return $mofile;
  }, 10, 2 );
}

fr_autoload_mo_files();


// Helper function to add new buttons in the backgend editor
function fr_editor_buttons( $buttons ){

	/**
	 * array(
	    'title' => 'Pink Button',  
	    'selector' => 'a',  
	    'classes' => 'pink'
	  )
	 */
	add_filter( 'tiny_mce_before_init', function( $init_array ) use ( $buttons ){

		if( !empty( $init_array['style_formats'] ) ){
			$buttons = array_merge( json_decode( $init_array['style_formats'], true ), $buttons );
		}

		$init_array['style_formats'] = json_encode( $buttons );

		return $init_array;
	}, 99 ); 

	add_action( 'init', function(){
		add_editor_style( 'css/editor.css' );
	} );
}

function fr_enable_text_editor(){
	wp_enqueue_script( 'jquery-te', FRAMEWORK_URL . '/resources/jquery_te/jquery-te-1.4.0.min.js',array("jquery"),"",1);
	wp_enqueue_style( 'jquery-te',FRAMEWORK_URL . '/resources/jquery_te/jquery-te-1.4.0.css');
}

function fr_enable_select_filter(){
	wp_enqueue_script( 'selectivity', FRAMEWORK_URL . '/resources/selectivity/selectivity-jquery.min.js',array("jquery"),"",1);
	wp_enqueue_style( 'selectivity',FRAMEWORK_URL . '/resources/selectivity/selectivity-jquery.min.css');
}

/**
 * Function to generate unique string with number at the end based on custom validation
 * @param  string $name The name to check and change
 * @param  string $prefix A string to be in front fo the number
 * @param  func $func Function to validate the result
 * @return string The unique string name
 */
function fr_unique_name( $name, $prefix, $func, $depth = 0 ){
	if( $depth > 1000 ){
		return new WP_Error( 'To much recursion' );
	}
	if( $func( $name ) ){
		if(preg_match_all('/\d+/', $name, $numbers)){
    		$lastnum = end($numbers[0]);
    		if( substr( $name, -strlen( $lastnum ), strlen( $lastnum )  ) != $lastnum ){
    			$lastnum = "";
    		}
		}else{
			$lastnum = "";
		}
		$new_num = (int)$lastnum+1;
		$name = substr( $name, 0, strlen($name)-strlen($lastnum) );

		if( substr( $name, -strlen($prefix), strlen($prefix) ) == $prefix ){
			$name = substr( $name, 0, strlen($name)-strlen($prefix) );
		}
		$name = $name . $prefix . $new_num;
		$name = fr_unique_name( $name, $prefix, $func, $depth + 1 );
	}
	return $name;
}

/**
 * Builds an enumeration string if given an array of words
 */
function fr_string_enum( $a, $delimiter = ', ', $end = '' ){
	if( !$end ){
		$end = __( ' and ', 'framework' );
	}

	$string = '';
	if( !is_array( $a ) ){
		$string = $a;
	} else if( count( $a ) == 1 ){
		$string = reset( $a );
	}else if( count( $a ) == 2 ){
		$string = implode( $end, $a );
	}else if( count( $a ) > 2 ){
		$last = array_pop( $a );
		$string = implode( $delimiter, $a ) . $end . $last;
	}
	
	return $string;
}


// Allow shortcodes in WP menu item links
add_filter( 'nav_menu_link_attributes', function( $atts, $item, $args ) {
	if( !empty( $atts['href'] ) ){
		$atts['href'] = str_replace( '%5B', '[', $atts['href'] );
		$atts['href'] = str_replace( '%5D', ']', $atts['href'] );
		$atts['href'] = do_shortcode( $atts['href'] );
		$atts['href'] = fr_remove_double_http( $atts['href'] );
	}
    return $atts;
}, 10, 3 );

//Display shortcodes in menu items
add_filter('wp_nav_menu_items', 'do_shortcode');


function fr_get_error( $error ){
	return reset( $error->errors );
}


fr_add_actions( [ 'wp_footer', 'admin_footer', 'login_enqueue_scripts' ], 'fr_show_js_settings', 11 );



function fr_get_object_from_url( $url = '' ){
	if( !$url ){
		$url = fr_get_current_url();
	}
	$url = str_replace( SITE_URL, '', $url );
	$info = parse_url( $url );
	if( isset( $info['path' ] ) ){
		$path = explode( '/', trim( $info['path' ], '/' ) );

		// Search posts
		$post_types = get_post_types();
		foreach( $post_types as $post_type ){
			$post_type_data = get_post_type_object( $post_type );
	    	$post_type_slug = $post_type_data->rewrite['slug'];
	    	if( $path[0] == $post_type_slug ){
	    		$pid = fr_get_post_by_slug( $path[1], $post_type );
	    		if( $pid ){
		    		return array(
		    			'type' => 'post',
		    			'post_type' => $post_type,
		    			'ID' => $pid,
		    			'slug' => $path[1]
		    		);
		    	}
	    	}
	    }

	    // Search taxonomies
	    $taxonomies = get_taxonomies();
	    foreach( $taxonomies as $tax ){
			$tax_data = get_taxonomy( $tax );
	    	$taxonomy_slug = $tax_data->rewrite['slug'];
	    	if( $path[0] == $taxonomy_slug ){
	    		$term = get_term_by( 'slug', $path[1], $tax );
	    		if( $term ){
		    		return array(
		    			'type' => 'term',
		    			'taxonomy' => $tax,
		    			'term_id' => $term->term_id,
		    			'slug' => $path[1]
		    		);
		    	}
	    	}
	    }
	}
}


function fr_get_post_by_slug( $total ){

	$res = new WP_Query( 	$args = array(
	  'post_type' => $post_type,
	  'post_status' => 'any',
	  'post_name' => $slug,
	  'parent' => $parent,
	  'fields' => 'ids'
	) );
	return reset( $res );
}



function fr_pagination( $total, $per_page, $args = [] ){
	$big = 999999999;
	ob_start();
	if( $total ){
		$at = (int)get_query_var( 'paged' );
		?><div class="fr_pagination lrnm" data-until-end="<?php echo ceil( $total / $per_page ) - $at ?>"><?php 
			$args = array_merge( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => ceil( $total / $per_page )
			), $args );
			$html = paginate_links( $args ) ?? '';
			$links = fr_single_loop_extract( $html, '<a class=\'page-numbers\'', '</a>' );

			foreach( $links as $link ){
				$number = fr_extract( $link, '>' );
				$html = str_replace( '<a class=\'page-numbers\'' . $link . '</a>', '<a data-page=\'' . $number . '\' class=\'page-numbers\'' . $link . '</a>', $html );
			}
			$current = fr_single_loop_extract( $html, '<span aria-current', '</span>' );
			foreach( $current as $link ){
				$number = fr_extract( $link, '>' );
				$html = str_replace( '<span aria-current' . $link . '</span>', '<span data-page=\'' . $number . '\' aria-current' . $link . '</span>', $html );
			}
			echo $html;
		?></div><?php 
	}
	$c = ob_get_clean();

	if( $total / $per_page > 1 ){
		return $c;
	}
}



function fr_wp_sanitize( $value, $rules ){
	$rules = explode( '>', $rules );
	if( count( $rules ) == 1 ){
		$rule = reset( $rules );
		if( $rule == 'textarea' ){
			$value = sanitize_textarea_field( $value );
		} else if( $rule == 'text' ){
			$value = sanitize_text_field( $value );
		} else if( $rule == 'email' ){
			$value = sanitize_email( $value );
		} else if( $rule == 'boolean' ){
			$value = (bool)$value;
		} else if( $rule == 'int' ){
			$value = (int)$value;
		} else if( $rule == '00:00' ){
			if( date( 'H:i', strtotime( $value ) ) != $value ){
				$value = '';
			}
		} else if( $rule == 'country' ){
			$data = fr_data_countries();
			if( !isset( $data[$value] ) ){
				$value = '';
			}
		} else {
			fr_log_debug( 'rule not found for: ' . $value );
		}
	} else if( $rules[0] == 'taxonomy' ){
		$options = fr_taxonomy_options( $rules[1] );
		$options = array_keys( $options );
		if( !is_array( $value ) ){
			if( !in_array( $value, $options ) ){
				$value = '';
			}
		}else{
			if( array_diff( $value, $options ) ){
				$value = array();
			}
		}
	} else if( $rules[0] == 'user' ){
		if( $rules[1] == 'acf' ){
			$object = fr_acf_get_field_object( $rules[2], 'user_' . fr_user() );
			$options = array_keys( $object['choices'] );

			if( !is_array( $value ) ){
				if( !in_array( $value, $options ) ){
					$value = '';
				}
			}else{
				if( array_diff( $value, $options ) ){
					$value = array();
				}
			}
		}
	}
	return $value;
}


function fr_remove_class_action( $key, $class, $function, $lvl = 10 ){
	global $wp_filter;
	if( isset( $wp_filter[$key]->callbacks[$lvl] ) ){
		foreach( $wp_filter[$key]->callbacks[$lvl] as $k => $v ){
			if( is_array( $v['function'] ) && count( $v['function'] ) == 2 && ( is_object( $v['function'][0] ) && get_class( $v['function'][0] ) == $class || $v['function'][0] == $class ) && $v['function'][1] == $function ){
				unset( $wp_filter[$key]->callbacks[$lvl][$k] );
			}
		}
	}
}

function fr_enqueued_styles(){
	global $wp_styles;
	return $wp_styles->queue;
}

function fr_enqueued_scripts(){
	global $wp_scripts;
	return $wp_scripts->queue;
}

function fr_remove_enqueued_script( $enqueued, $type ){
	if( $type == 'css' ){
		$scripts = fr_enqueued_styles();
	}else{
		$scripts = fr_enqueued_scripts();
	}

	foreach( $scripts as $script ){
		foreach( $enqueued as $v ){
			$to_match = str_replace( '*', '', $v );
			if( $script ==  $to_match || ( strstr( $v, '*' ) && substr( $script, 0, strlen( $to_match ) ) == $to_match ) ){
				if( $type == 'css' ){
					wp_dequeue_style( $script );
				}else{
					wp_dequeue_script( $script );
				}
			}
		}
	}
}


function fr_wp_init( $callback ){
	if( !did_action( 'init' ) ){
		add_action( 'init', $callback );
	}else{
		fr_call_if_func( $callback );
	}
}





function fr_page_nr(){
	$at = (int)get_query_var( 'paged' );
	if( !$at ){
		$at = 1;
	}
	return $at;
}


// Stop WP from setting default statues
function fr_disable_post_statuses( $post_type, $statuses ){
	add_action( 'transition_post_status', function( $new_status, $old_status, $post ) use ( $post_type, $statuses ){
		if( $post_type == $post->post_type && in_array( $new_status, $statuses ) ){
			fr_change_post_status( $post->ID, $old_status );
		}
	}, 10, 3 );
}



function fr_register_post_statuses( $statuses, $post_type, $options = [] ){
	foreach( $statuses as $status => $label ){
		$options_new = array_merge( array(
	        'label'                     => $label,
	        'public'                    => true,
	        'exclude_from_search'       => false,
	        'show_in_admin_all_list'    => true,
	        'show_in_admin_status_list' => true,
	        'post_type' => $post_type,
	    ), $options );

		fr_register_post_status( $status, $post_type, $options_new );
	}
}


function fr_register_post_status( $post_status, $post_type, $options ){
	register_post_status( $post_status, $options );

	add_action('admin_footer-edit.php',function() use ( $post_status, $post_type, $options ) { 
		$screen = get_current_screen();
		if( $screen->id == 'edit-' . $post_type ){
			?>
			<script>
				jQuery(document).ready(function($){
			   		$( 'select[name="_status"]' ).append( '<option value="<?php echo $post_status ?>"><?php echo $options['label'] ?></option>' );
			   	});
			</script>
			<?php 
		}
	});

	add_action('admin_footer-post.php', function() use ( $post_status, $post_type, $options ) {
		
		global $post;
		$complete = '';
		$label = '';
		if( $post->post_type == $post_type ){
			if( $post->post_status == $post_status ){
			   $complete = ' selected="selected"';
			   $label = '<span id="post-status-display"> ' . $options['label'] . '</span>';
			}
			?>
			<script>
			jQuery(document).ready(function($){
			   $("select#post_status").append('<option value="<?php echo $post_status ?>" <?php echo $complete ?>><?php echo $options['label'] ?></option>');
			   $("#post-status-display").append('<?php echo $label ?>');
			});
			</script>
			<?php 
	  }
	} );


	// EDD FES plugin specific code
	if( $post_type == 'download' ){
		add_filter( 'fes_get_the_download_status', function( $status, $post, $css ) use ( $post_status, $options ) {
			if( $css ){
				if ( $post->post_status === $post_status ) {
					$status = $post_status;
				}
			}else{
				if ( $post->post_status === $post_status ) {
					$status = $options['label'];
				}
			}
			return $status;
		}, 10, 3 );
	}
	
}


/**
 * Add multiple filters to a closure
 *
 * @param $tags
 * @param $function_to_add
 * @param int $priority
 * @param int $accepted_args
 *
 * @return bool true
 */
function fr_add_filters( $tags, $function_to_add, $priority = 10, $accepted_args = 1 ){
	if( !is_array( $tags ) ){
		$tags = explode( ' ', $tags );
	}

	foreach($tags as $index => $tag){
		add_filter($tag, $function_to_add, (int)(is_array($priority) ? $priority[$index] : $priority), (int)(is_array($accepted_args) ? $accepted_args[$index] : $accepted_args));
	}

	return true;
}

/**
 * Add multiple actions to a closure
 *
 * @param $tags
 * @param $function_to_add
 * @param int $priority
 * @param int $accepted_args
 *
 * @return bool true
 */
function fr_add_actions( $tags, $function_to_add, $priority = 10, $accepted_args = 1 ){
  return fr_add_filters($tags, $function_to_add, $priority, $accepted_args);
}


function fr_change_post_status( $pid, $status ){
	if( !$pid ){
		return;
	}
	return wp_update_post( [
		'ID' => $pid,
		'post_status' => $status
	] );
}



function fr_block_staging_access( $block = 1 ){
	if( $block && !fr_user() && !is_admin() && !fr_wp_is_login() && $_SERVER['REMOTE_ADDR'] != $_SERVER['SERVER_ADDR'] && $_SERVER['REMOTE_ADDR'] != $_SERVER['HTTP_X_FORWARDED_FOR'] ){
		fr_exit();
	}
}

function fr_wp_is_login(){
    $ABSPATH_MY = str_replace(array('\\','/'), DIRECTORY_SEPARATOR, ABSPATH);
    return ((in_array($ABSPATH_MY.'wp-login.php', get_included_files()) || in_array($ABSPATH_MY.'wp-register.php', get_included_files()) ) || (isset($_GLOBALS['pagenow']) && $GLOBALS['pagenow'] === 'wp-login.php') || $_SERVER['PHP_SELF']== '/wp-login.php');
}

function fr_get_status_label( $status ){
	if( is_numeric( $status ) ){
		$status = get_post_status( $status );
	}
	$object = get_post_status_object( $status );

	if( $object ){
		return $object->label;
	}
}

function fr_wp_remove_url_root( $url ){
	$root = get_site_url();
	return str_replace( $root, '', $url );
}
function fr_wp_add_url_root( $url ){
	$root = get_site_url();

	if( strstr( $url, $root ) ){
		return $url;
	}

	$first_char = substr( $url, 0, 1 );

	if( !fr_str_ends_with( $root, '/' ) && !in_array( $first_char, [ '/' ] ) ){
		$url = '/' . $url;
	}

	return $root . $url;
}

function fr_wp_update_post_field( $id, $field, $value ){
	return wp_update_post( [
		'ID' => $id,
		$field => $value
	] );
}

function fr_wp_disable_depracated_errors(){
	add_filter( 'deprecated_argument_trigger_error', '__return_false' );
}


function fr_wp_filters_slice( $hook, $priority, $start, $end = null ){

	global $wp_filter;

	if( !isset( $wp_filter[$hook] ) ) return;
	if( !isset( $wp_filter[$hook][$priority] ) ) return;

	$wp_filter[$hook][$priority] = array_slice( $wp_filter[$hook][$priority], $start, $end, true );

	return true;

}


/** This function will best handle dequeuing scripts no matter when they are inserted */
function fr_dequeue_script( $handle, $type = 'js', $in_admin = false ){
	$dequeue = function() use ( $handle, $type ) {
		if( $type == 'js' ){
			wp_dequeue_script( $handle );
		} else {
			wp_dequeue_style( $handle );
		}
	};

	if( $in_admin && is_admin() ){
		dd_action( 'admin_enqueue_scripts', $dequeue, 9999 );
		add_action( 'admin_head', $dequeue, 9999 );
		add_action( 'admin_footer', $dequeue, -9999 );
		add_action( 'admin_footer', $dequeue, 9999 );
	} else {
		add_action( 'wp_enqueue_scripts', $dequeue, 9999 );
		add_action( 'wp_head', $dequeue, 9999 );
		add_action( 'wp_footer', $dequeue, -9999 );
		add_action( 'wp_footer', $dequeue, 9999 );
	}
}


function fr_file_get_contents_from_url( $src, $root, $url ){

    $src = fr_relative_url_to_absolute( $src, $url );
    $src = fr_url_to_path( $src, $root, $url );

    if( file_exists( $src2 ) && ( substr( $src2, -3, 3 ) == '.js' || substr( $src2, -4, 4 ) == '.css' ) ){
    	return file_get_contents( $src2 );
    }
}

function fr_copy_parent_theme_settings(){
	$theme = wp_get_theme();
	$child_theme = $theme->get( 'TextDomain' );
	$parent_theme = $theme->get( 'Template' );
	if( $parent_theme ){
		$settings= get_option( 'theme_mods_' . $parent_theme );
		update_option( 'theme_mods_' . $child_theme, $settings );
	}
}



function fr_enable_script_manager(){

	add_action( 'wp_head', function(){
		display_scripts( 'head' );
	} );
	add_action( 'wp_body', function(){
		display_scripts( 'body' );
	} );
	add_filter( 'comments_template', function( $template ){
		display_scripts( 'after_post' );
		return $template;
	} );

	function display_scripts( $location ){
		$scripts = get_field( 'scripts', 'options' );
		if( !$scripts ){
			return;
		}
		
		foreach( $scripts as $script ){
			if( $script['status'] == 'disabled' ){
				continue;
			}
			if( $script['location'] != $location ){
				continue;
			}
			if( in_array( 'hide_for_users', $script['display'] ) && fr_user() ){
				continue;
			}
			if( in_array( 'hide_in_preview', $script['display'] ) && is_preview() ){
				continue;
			}
			if( in_array( $script['load'], [ 'at_load', 'after_load', 'on_view' ] ) && strstr( $script['script'], '<script' ) ){
				
				$id = fr_unique_id( 'fr_enable_script_manager' );

				$scripts = fr_single_loop_extract( $script['script'], '<script|>', '</script>' );
				foreach( $scripts as $s ){
					$script['script'] = str_replace( $s, 'fr_onload( "' . $script['load'] . '", "' . $id . '", function(){ ' . $s . ' } );', $script['script'] );
				}

				// Add the ID to locate the script in the page
				$script['script'] = fr_str_replace_once( '<script', '<div data-lazy-script-id="' . $id . '"></div><script', $script['script'] );
			}
			echo "<!-- {$script['name']} -->\n\n" . $script['script'] . "\n\n";
		}
	}
}


function fr_extract_embeds_from_content( $content ){
	$content = explode( "\n", $content );
	$content = array_map( 'trim', $content );

	$embeds = [];
	foreach( $content as $line ){
		if( filter_var( $line, FILTER_VALIDATE_URL ) ){
			$embeds[] = $line;
		}
	}
	return $embeds;
}

function fr_ajax_url(){
	return admin_url( 'admin-ajax.php' );
}



/**
 * Determines if the current view is a front end view or not
 * @return boolean
 */
function fr_is_frontend(){
	return !is_admin() && !fr_is_admin_ajax();
}


function fr_on_shutdown_or_redirect( $callback ){
	fr_add_actions( [ 'shutdown', 'wp_redirect' ], function( $value ) use ( $callback ){
		
		fr_call_if_func( $callback );

		return $value;
	} );
}


function fr_redirect_stop(){
	add_filter( 'wp_redirect', function(){
		fr_exit();
	} );
}

function fr_redirect_debug(){
	add_filter( 'wp_redirect', function(){
		fr_debug( 20, false, true, 2 );
	} );
}


function fr_wp_includes_file( $file ){
	return site_url() . '/wp-includes/' . $file;
}




function fr_enqueue_styles( $dir, $type, $return = false, $exclude = [] ){

	$styles = [];

	$folders = [
		'desktop' => [
			'frontend',
			'frontend_admin'
		],
		'admin' => [
			'frontend_admin',
			'admin'
		],
		'inline' => [
			'inline'
		]
	];

	if( !isset( $folders[$type] ) ){
		return;
	}

	$folders = $folders[$type];

	foreach( $folders as $k => $folder ){
		$folder_path = $dir . $folder;
		if( !file_exists( $folder_path ) ){
			unset( $folders[$k] );
		}

		$files = fr_scan_dir( $folder_path, $exclude, [ 'css' ] );
		foreach( $files as $file ){
			$depth = fr_strstra( $file['path'], $dir );
			$depth = explode( '/', $depth );
			$depth = count( $depth );
			$styles[] = $file['path'];
		}
	}
	
	usort( $styles, function( $a, $b ){
		return strcmp( fr_file_name( $a ), fr_file_name( $b ) );
	} );

	if( $return ){
		return $styles;
	}

	foreach( $styles as $path ){
		$url = fr_path_to_url( $path );
		$name = fr_strstra( $path, $dir );
		$name = sanitize_title( $name );
		wp_enqueue_style( $name, $url, [], filemtime( $path ) );
	}

}


function fr_is_single( $post_type ){
	return is_single() && get_post_type() == $post_type;
}



/**
 * Any missing images will be loaded from the live site
 * @param  string Root URL from where the images will be taken from
 * @return void
 */
function fr_uploads_dir_use_external( string $external_url = '' ): void {

	$change_url = function( $url, $external_url ){

		if( $url ){
			$path = fr_url_to_path( $url );
			if( $path && !file_exists( $path ) ){
				$url = str_replace( site_url(), $external_url, $url );
			}
		}

		return $url;
	};

	add_filter( 'wp_get_attachment_url', function( $url, $pid ) use ( $external_url, $change_url ){
		return $change_url( $url, $external_url );
	}, 10, 2 );

	add_filter( 'wp_get_attachment_image_src', function( $url, $pid ) use ( $external_url, $change_url ){
		if( is_array( $url ) ){
			if( !empty( $url[0]) ){
				$url[0] = $change_url( $url[0], $external_url );
			}
		}
		return $url;
	}, 10, 2 );


	add_filter( 'wp_calculate_image_srcset', function( $sources ) use ( $external_url, $change_url ){
		foreach( $sources as $k => $data ){
			$sources[$k]['url'] = $change_url( $data['url'], $external_url );
		}
		return $sources;
	}, 10, 2 );


	foreach( [ 'the_content', 'acf_content' ] as $filter ){
		add_filter( $filter, function( $content ) use ( $external_url, $change_url ){
			
			if( strstr( $content, '<img' ) ){
				$srcs = fr_single_loop_extract( $content, '<img|src="', '"' );
				$srcs = array_unique( $srcs );

				foreach( $srcs as $src ){
					$new_src = $change_url( $src, $external_url );
					if( $src != $new_src ){
						$content = str_replace( $src, $new_src, $content );
					}
				}
				$srcsets = fr_single_loop_extract( $content, '<img|srcset="', '"' );
				$srcsets = array_unique( $srcsets );

				foreach( $srcsets as $srcset ){
					if( !$srcset ){
						continue;
					}
					$new_srcset = explode( ',', $srcset );
					foreach( $new_srcset as $k => $v ){
						$v = explode( ' ', $v );
						$v[0] = $change_url( $v[0], $external_url );
						$new_srcset[$k] = implode( ' ', $v );
					}
					$new_srcset = implode( ',', $new_srcset );
					$content = str_replace( $srcset, $new_srcset, $content );
				}
			}

			return $content;
		}, 10, 2 );
	}

}




function fr_wp_content( $content ){
	return apply_filters( 'the_content', $content );
}






/**
 * This aims to solve the problem where WP triggers functions before all the data has been saved
 * Using this will trigger the callback to any action only when the PHP execution has finished
 * v1.1
 * - in v1.1 fixed the aborting condition to support multiple actions
 * @param  string  $action      Any WP action
 * @param  callback  $callback  Any valid function or method callback
 * @param  integer $lvl         Level of the action
 * @param  integer $arg_count Argument count for action
 */
function fr_action_delay_callback( $action, $callback, $lvl = 10, $arg_count = 1 ){

    add_action( $action, function( $arg = false ) use ( $action, $callback ){
        $args = func_get_args();
        $function = function( $arg = false ) use ( $action, $callback, $args ){

            if( !isset( $_ENV['fewo_trigger_callback_at_end'] ) ){
                $_ENV['fewo_trigger_callback_at_end'] = [];
            }

            // If already called then abort
            foreach( $_ENV['fewo_trigger_callback_at_end'] as $actions ){
                if( $actions['action'] === $action && $actions['callback'] === $callback ){
                    return $arg;
                }
            }

            $_ENV['fewo_trigger_callback_at_end'][] = [
                'action' => $action,
                'callback' => $callback
            ];

            ob_start();
            call_user_func_array( $callback, $args );
            ob_get_clean();

            return $arg;
        };

        add_action( 'wp_redirect', $function );
        add_action( 'shutdown', $function );

        return $arg;

    }, $lvl, $arg_count );
}




function fr_backup_field( $meta_key, $value, $pid ){
	$meta_key = '_backup_' . $meta_key;
	if( metadata_exists( 'post', $pid, $meta_key ) ){
		return;
	}
	fr_m( $meta_key, $value, $pid );
}

function fr_backup_field_restore( $meta_key, $pid ){
	$backup_meta_key = '_backup_' . $meta_key;
	if( !metadata_exists( 'post', $pid, $backup_meta_key ) ){
		return;
	}
	$value = fr_m( $backup_meta_key, $pid );
	fr_m( $meta_key, $value, $pid );
	delete_post_meta( $pid, $backup_meta_key );
}







/**
 * Function meant to handle multiple redirects into one
 * v1.0
 */
function fr_redirect_old_urls(){

	add_action( 'template_redirect', function(){
	
		$current_url = fr_get_current_url();
		$new_url = fr_update_url( $current_url );

		if( $current_url == $new_url ){
			return;
		}

		fr_redirect( $new_url, 301 );

	}, 11 );

}



function fr_update_url( $url ){

	$cache = fr_cache( [ 'fr_update_url', $url ] );

	if( $cache ){
		return $cache;
	}

	for( $x = 0; $x < 50; $x++ ){

		$info = parse_url( $url );
		$info['path_parts'] = trim( $info['path'], '/' );
		$info['path_parts'] = explode( '/', $info['path_parts'] );
		$new_url = apply_filters( 'fr_redirect_old_urls', $url, $info );

		if( $url == $new_url ){
			break;
		}

		$url = $new_url;
	}

	if( $x >= 20 ){
		fr_validate( true, false, 'Too many redirects', $url );
		return $url;
	}

	fr_cache( [ 'fr_update_url', $url ], $new_url );

	return $new_url;

}





/**
 * *** Needs update to support nested blocks
 */
function fr_blocks_strip( $c, $keep ){

	$keep = explode( ' ', $keep );
	$keep = array_flip( $keep );

	$blocks = fr_single_loop_extract( $c, '<!-- wp:', ' ' );
	$blocks = array_unique( $blocks );

	foreach( $blocks as $block ){

		$block = 'wp:' . $block;

		if( isset( $keep[$block] ) ){
			continue;
		}
	
		$c = fr_single_loop_extract_remove( $c, '<!-- ' . $block, '<!-- /' . $block . ' -->' );
		$c = str_replace( '<!-- /' . $block . ' -->', '', $c );
	}

	$c = strrev( $c );
	$c = fr_single_loop_extract_remove( $c, '>--/', ':pw --!<' );
	$c = strrev( $c );

	return $c;
}





function fr_disable_feeds(){
	
	fr_add_actions( 'do_feed do_feed_rdf do_feed_rss do_feed_rss2 do_feed_atom do_feed_rss2_comments do_feed_atom_comments', function(){
		$url = fr_get_current_url( true );
		$url = explode( '/feed/', $url );
		$url = $url[0] . '/';
		fr_redirect( $url, 301 );
	}, 1 );

	add_action( 'wp_head', function(){
		remove_action( 'wp_head', 'feed_links', 2 );
    	remove_action( 'wp_head', 'feed_links_extra', 3 );
	}, 1 );
}





function fr_gutenberg_block_embed_render( $name, $callback ){

	fr_global( 'fr_gutenberg_block_embed_render', [ $name => $callback ], ARRAY_A, true );




	add_action( 'after_setup_theme', function() use ( $name, $callback ){

		register_block_type( 'core/embed', [
			'render_callback' => function( $atts, $content ) use ( $name, $callback ) {

				$blocks = fr_global( 'fr_gutenberg_block_embed_render' );
			
				if( !isset( $blocks[$atts['providerNameSlug']] ) ){
					return $content;
				}
			
				return call_user_func( $blocks[$atts['providerNameSlug']], $atts, $content );
			}
		] );

	} );

}





function fr_img_resize( $image, $width = null, $height = null, $crop = true, $attr = [], $return_url = false ){

	$file = fr_file_path( $image );
	$file_name = fr_file_name( $file );
	$new_file_name = $file_name . '-' . $width . 'x' . $height . 'x' . $crop;
	$new_file = fr_file_change_name( $file, $new_file_name );
	$new_file_src = fr_path_to_url( $new_file );

	if( !file_exists( $new_file ) ){

		if( file_exists( $new_file ) ){
			unlink( $new_file );
		}

		$new = wp_get_image_editor( $file );

		if( is_wp_error( $new ) ){
			return false;
		}

		$new->resize( $width, $height, $crop );
		$new->save( $new_file );

		if( !file_exists( $new_file ) ){
			return false;
		}
	}

	if( $return_url ){
		return $new_file_src;
	}

	$title = fr_img_alt_from_file_name( $file );
	$attr = array_merge( [
		'src' => $new_file_src,
		'width' => $width,
		'height' => $height,
		'alt' => $title,
	], $attr );

	echo '<img ' . fr_array_to_tag_attr( $attr ) . ' />';
}




function fr_img_alt_from_file_name( $path ){
	$title = fr_file_name( $path );
	$title = str_replace( '-', ' ', $title );
	$title = str_replace( '_', ' ', $title );
	$title = ucwords( $title );
	return $title;
}



function fr_wp_change_texts( $texts ){

	add_filter( 'gettext', function( $text ) use ( $texts ){

		if( isset( $texts[$text] ) ){
			$text = $texts[$text];
		}

		return $text;
	} );
}




function fr_permalink_slug( $post_id ){
	return str_replace( get_bloginfo( 'url' ), '', get_permalink( $post_id ) );
}




function fr_wp_get_embed_data( $url ){
	$oembed = _wp_oembed_get_object();
	return $oembed->get_data( $url );
}



function fr_is_administration(){

	return is_admin() && !wp_doing_ajax();

}


/**
 * Executes WP Query and groups the results by ID
 */
function fr_wp_query_group_by_id( array $query ): array {

	$results = new WP_Query( $query );
	$results = array_combine( array_column( $results->posts, 'ID' ), $results->posts );
	return $results;

}


