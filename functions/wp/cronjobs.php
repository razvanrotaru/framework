<?php
define("IS_CRON",defined( 'DOING_CRON' ));

global $fr_cron_schedules,$fr_cron_active;
$fr_cron_schedules=array();
$fr_cron_active=array();

//initiate scheduales for specific intervals
//@interval int seconds
function fr_add_cronjob($interval,$function,$start_time = 0){
	global $fr_cron_schedules,$fr_cron_active;
	$fr_cron_schedules[$interval]=$interval;
	if(empty($fr_cron_active[$interval])){
		$fr_cron_active[$interval]=array();
	}
    $fr_cron_active[$interval][]=$function;

    if( !$start_time ){
		$start_time = strtotime( date( 'Y-m-d 00:00:00' ) );
	}
 
	if ( !wp_next_scheduled ( "fr_do_crons", array($interval) )) { 
		wp_schedule_event($start_time, "fr_".$interval."_seconds", "fr_do_crons",array($interval));
    }
}


//register interval
add_filter( 'cron_schedules', function ( $schedules ) {
	global $fr_cron_schedules;
	foreach($fr_cron_schedules as $interval){
	    $schedules["fr_".$interval."_seconds"] = array(
	        'interval'  => $interval,
	        'display'   => sprintf(__( 'Every %s seconds'),$interval)
	    ); 
	}
    return $schedules;
});


//execute crons for specific interval
add_action("fr_do_crons",function($interval){
	set_time_limit(3600);
	global $fr_cron_active;
	if(!empty($fr_cron_active[$interval])){
		foreach($fr_cron_active[$interval] as $function){
			if(is_callable($function)){
				fr_start_timer( 'fr_do_crons' );
				do_action( 'fr_cron_started', $interval, $function );
				call_user_func_array( $function, array() );
				do_action( 'fr_cron_ended', $interval, $function, fr_get_timer( 'fr_do_crons' ) );
			}
		}
	}
},10,2);


//remove intervals that are no longer required
add_action("template_redirect", function(){
	global $fr_cron_active;
	$crons=_get_cron_array();
	$inactive=array(); 
	foreach($crons as $group){
		foreach($group as $name=>$subgroup){
			foreach($subgroup as $data){
				if($name=="fr_do_crons"){
					foreach($fr_cron_active as $interval=>$cron){
						if($data["interval"]==$interval){
							continue 2;
						}
					}
					$inactive[$name]=$data;
				}
			}
		}
	}
	
	foreach($inactive as $name=>$data){
		wp_clear_scheduled_hook( $name, $data["args"] );
	}
});