<?php 
add_filter( 'wp_get_attachment_image_attributes', function( $attr, $attachment, $size ){
    if( isset( $attr['src'] ) ){
        $attr['src'] = esc_url( $attr['src'] );
    }
    return $attr;
}, 10, 3 );




function fr_img($id=0,$size="",$url=false,$attr="",$lazy = false, $options = [] ){

	$attr = fr_html_attr_to_array( $attr );

    $options = fr_default( $options, [
        'amp' => false,
        'retina' => false, // If an image is taken at full size it will srink it to half that
        'path' => false // used with CDN where the url differs from the installation url
    ] );

	//Show a theme image
	if(!is_numeric($id) && is_string($id)){
		if( !strstr( $id, '/' ) ){
			$img = get_stylesheet_directory_uri() . "/images/" . $id;
		}else{
			$img = $id;
		}
		if($url){
			return $img;
		}
        $path = fr_url_to_path( $img );

        if( $options['path'] ){
            $path = $options['path'];
        }



		if(file_exists($path) ){
			if( $size == 'base64' ){
				$img = fr_file_base64_encode( $path );
			}
			if( !isset( $attr['alt'] ) ){
				$alt = explode( '.', basename( $path ) );
				$alt = reset( $alt );
				$alt = str_replace( [ ',', '-', '_' ], ' ', $alt );
				$alt = ucwords( $alt );
				$attr['alt'] = $alt;
			}

			
			if( !isset( $attr['width'] ) && !isset( $attr['height'] ) ){
				list( $width, $height ) = getimagesize( $path );
				if( $width && $height ){
					$attr['width'] = $width;
					$attr['height'] = $height;
				}
			}

            if( $options['retina'] ){
                $default_attr['width'] = round( $attr['width'] / 2 );
                $default_attr['height'] = round( $attr['height'] / 2 );
            }

			if( $lazy && !$options['amp'] ){
				$attr['data-lazy-src'] = $img;
				$attr['src'] = 'data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
			}else{
				$attr['src'] = $img;
			}

            if( $options['amp'] ){
                return '<amp-img ' . fr_array_to_tag_attr( $attr ).'></amp-img>';
            }else{
			 return '<img ' . fr_array_to_tag_attr( $attr ).'>';
            }
		}
	}

	//If ID is empty get the current post attachment id
    	if(!$id){
        	$id=get_post_thumbnail_id();
    	}

    	//If Id is object it means that is a post object, thus retrive the post ID
    	if(is_object($id)){
        	if(!empty($id->ID)){
            	$id=$id->ID;
        	}
    	}

    	//If ID is not an attachment than get the attachment from that post
    	if(get_post_type($id)!="attachment"){
        	$id=get_post_thumbnail_id($id);
    	}

    	if($id){
	        $image_url=wp_get_attachment_image_url($id,$size);
	        if(!$url){
	        	//If image is a SVG embed the contents so we can change the color dinamically
		            if(substr($image_url,-4,4)==".svg"){
		                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
		                $data=file_get_contents($image_url);
		                return strstr($data,"<svg ");
		            }else{
		                $img = wp_get_attachment_image($id,$size,0,$attr);
                        if( $options['amp'] ){
                            list( $img ) = fr_amp_convert_content( $img );
                        }
		                if( $lazy && !$options['amp'] ){
		                	$img = str_replace( 'src=', 'src="data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-lazy-src=', $img );
		                }
		                return $img;
		            }
	        }else if($url){
	            return $image_url;
	        }
    	}
}

function fr_img_bas64_svg( $file, $attr = [], $url = false, $color = false ){
    if( !file_exists( $file ) ){
        return;
    }
    $c = file_get_contents( $file );
    $c = fr_html_minify( $c );
    if( $color ){
        $colors = fr_single_loop_extract( $c, '"#', '"' );
        foreach( $colors as $v ){
            if( strlen( $v ) < 3 ){
                continue;
            }
            $c = str_replace( '#' . $v, $color, $c );
        }
    }
    $c = base64_encode( $c );
    $c = 'data:image/svg+xml;base64,' . $c;
    if( $url ){
        return $c;
    }

    if( !isset( $attr['alt'] ) ){
        $attr['alt'] = fr_img_alt_from_file_name( $file );
    }

    return '<img src="' . $c . '" ' . fr_array_to_tag_attr( $attr ) . '>';
}



function fr_img_extract_main_color( $file, $default = array() ){

        if( !$file ){
                return $default;
        }

        if( !file_exists( $file ) ){
                return $default;
        }
        $img = fr_img_to_obj( $file );
        $colors = fr_img_obj_to_array( $img );

        $final = [];
        foreach( $colors as $y => $rows ){
            foreach( $rows as $x => $color ){
                $final['red'][] = $color['r'];
                $final['green'][] = $color['g'];
                $final['blue'][] = $color['b'];
            }
        }

        foreach( $final as $color => $values ){
            $final[$color] = round( fr_average( $values ) );
        }

        return $final;
}



function fr_img_to_obj( $file ){
        $info = getimagesize( $file );
        if ($info['mime'] == 'image/jpeg') $src = imagecreatefromjpeg($file);
        elseif ($info['mime'] == 'image/gif') $src = imagecreatefromgif($file);
        elseif ($info['mime'] == 'image/png') $src = imagecreatefrompng($file);
        return $src;
}

function fr_img_obj_to_array( $img, $sample_count = 10 ){

        $width = imagesx( $img );
        $height = imagesy( $img );
        $colors = array();

        $sample_count_x = min( $width, $sample_count );
        $sample_count_y = min( $height, $sample_count );
        $sample_count_x_step = round( $width / $sample_count_x );
        $sample_count_y_step = round( $height / $sample_count_y );

        for ($y = 0; $y < $height; $y += $sample_count_y_step ){
                for ($x = 0; $x < $width;    $x += $sample_count_x_step ){
                        $cols = imagecolorsforindex($img, imagecolorat($img, $x, $y));
                        $colors[$y][$x] = array("r"=>$cols["red"],"g"=>$cols["green"],"b"=>$cols["blue"]);
                }
        }

        return $colors;
}

function fr_img_is_alpha( $filename ){
        if ( strlen( $filename ) == 0 || !file_exists( $filename ) )
                return false;
        
        if ( ord ( file_get_contents( $filename, false, null, 25, 1 ) ) & 4 )
                return true;
        
        $contents = file_get_contents( $filename );
        if ( stripos( $contents, 'PLTE' ) !== false && stripos( $contents, 'tRNS' ) !== false )
                return true;
        
        return false;
}


function fr_img_placeholder_data(){
    return 'data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
}


/**
 * Generates a dynamic placeholder with the same dimensions as the original image
 * @param    string $file
 * @return string
 */
function fr_img_placeholder_transparent( $file ){

    ini_set('memory_limit', '1024M');

    $size = getimagesize( $file );

    if( !$size ){
        return fr_img_placeholder_data();
    }

    $output = fr_cache( $size[0] . '|' . $size[1] );
    if( $output === false ){

            // Create Image
            $image = imagecreatetruecolor( $size[0], $size[1] );
            imagetruecolortopalette($image, false, 1);
            imagesavealpha( $image, true );
            $color = imagecolorallocatealpha($image, 0, 0, 0, 127);
            imagefill($image, 0, 0, $color);

            // Ouput
            ob_start();
            imagepng( $image );
            $output = ob_get_clean();
            imagedestroy( $image );

            $output = 'data:image/png;base64,' . base64_encode( $output );
            fr_cache( $size[0] . '|' . $size[1], $output );
    }

    return $output;
}


function fr_img_wp_base64( $fid = false, $size = 'full', $attr = [] ){

    if( $fid === false ){
        $fid = get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if( is_object ( $fid ) ){
        if( !empty( $fid->ID ) ){
            $fid = $fid->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if( get_post_type( $fid ) != 'attachment' ){
        $fid = get_post_thumbnail_id( $fid );
    }

    if( !$fid ){
        return false;;
    }

    $attr = [];
    $file_data = image_get_intermediate_size( $fid, $size );
    $uploads_dir = wp_upload_dir();

    if( !$file_data ){
        return fr_img( $fid, $size, false, $attr );
    }

    $file_url = $uploads_dir['baseurl'] . '/' . $file_data['path'];
    $file_path = $uploads_dir['basedir'] . '/' . $file_data['path'];

    if( !file_exists( $file_path ) ){
        return fr_img( $fid, $size, $attr );
    }

    $alt_text = get_post_meta( $fid , '_wp_attachment_image_alt', true );
    $base64 = fr_image_base64( $file_path );
    $file_path = $base64 ? $base64 : $file_url;

    $attr['src'] = $file_path;
    $attr['width'] = $file_data['width'];
    $attr['height'] = $file_data['height'];
    $attr['alt'] = $alt_text;

    return '<img ' . fr_array_to_tag_attr( $attr ) . '>';
}


function fr_img_get_full_size( $img ){
    $info = pathinfo( $img );
    $name = $info['filename'];
    $name = explode( '-', $name );
    $size = end( $name );
    $size_parts = explode( 'x', $size );
    if( count( $name ) > 1 && count( $size_parts ) == 2 && is_numeric( $size_parts[0] ) && is_numeric( $size_parts[1] ) ){
        $img = $info['dirname'] . '/' . substr( $info['filename'], 0, -strlen( $size ) - 1 ) . '.' . $info['extension'];
    }
    return $img;
}


// Create sizes on the fly
function fr_img_make_intermediate_size( $img, $size, $width, $height = 0, $crop = false, $quality = 0 ){

    if( !$width ){
        $width = 1000000;
    }

    if( !$height ){
        $height = 1000000;
    }

    // Create size if it doesn't exist
    $meta = fr_m( '_wp_attachment_metadata', $img );

    if( empty( $meta['sizes'][$size] ) ){
        $quality_func = function() use ( $quality ){
            return $quality;
        };
        if( $quality ){
            add_filter( 'jpeg_quality', $quality_func );
        }
        $new_img = image_make_intermediate_size( fr_wp_file( $img ), $width, $height, $crop );
        if( $new_img ){
            $meta['sizes'][$size] = $new_img;
        }
        if( $quality ){
            remove_filter( 'jpeg_quality', $quality_func );
        }
        fr_m( '_wp_attachment_metadata', $meta, $img );
    }
    return fr_img( $img, $size, 1 );
}


function fr_img_id_from_url( $img ){

    global $wpdb;

    if( !$img ){
        return;
    }

    if( !strstr( $img, '/uploads/' ) ){
        return;
    }

    $id = fr_cache( 'fr_img_id_from_url' . $img    );

    if( $id !== false ){
        return $id;
    }

    $full_size = fr_img_get_full_size( $img );
    $full_size = strstr( $full_size, '#' ) ? strstr( $full_size, '#', true ) : $full_size;
    $full_size = strstr( $full_size, '?' ) ? strstr( $full_size, '?', true ) : $full_size;

    $id = 0;

    $id = $wpdb->get_var( 'SELECT ID FROM ' . $wpdb->prefix . 'posts WHERE post_type = "attachment" AND guid = "' . $wpdb->escape( $full_size ) . '" LIMIT 0,1' );

    if( !$id ){
        $file_uploads_path = explode( '/uploads/', $full_size );
        if( count( $file_uploads_path ) > 1 ){
            $file_uploads_path = end( $file_uploads_path );
            $id = $wpdb->get_var( 'SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key = "_wp_attached_file" AND BINARY meta_value = "' . $wpdb->escape( $file_uploads_path ) . '" LIMIT 0,1' );
        }
    }

    fr_cache( 'fr_img_id_from_url' . $img, $id    );

    return $id;

}



function fr_img_remote_size( $url ) {
        // Set headers        
        $headers = array( 'Range: bytes=0-131072' );        
        if ( !empty( $referer ) ) { array_push( $headers, 'Referer: ' . $referer ); }

    // Get remote image
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
    $data = curl_exec( $ch );
    $http_status = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
    $curl_errno = curl_errno( $ch );
    curl_close( $ch );

    if( !$data ){
        return false;
    }
        
    // Get network stauts
    if ( $http_status != 200 ) {
        echo 'HTTP Status[' . $http_status . '] Errno [' . $curl_errno . ']';
        return [0,0];
    }

    // Process image
    $image = @imagecreatefromstring( $data );


    if( !$image ){
        return false;
    }

    $dims = [ imagesx( $image ), imagesy( $image ) ];
    imagedestroy( $image );

    return $dims;
}


function fr_img_enable_svg_upload(){
    add_filter('upload_mimes', function( $mimes ) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    });
}




function fr_img_get_all_sizes(){
    global $_wp_additional_image_sizes;
    return $_wp_additional_image_sizes;
}




function fr_get_file_url_from_meta( array $meta, string $size = '' ){

    $file_url = '';

    if( !empty( $meta['_wp_attached_file'] ) ){
        $file_url = fr_uploads_url() . '/' . $meta['_wp_attached_file'];
    }

    if( !empty( $meta['_wp_attachment_metadata']['sizes'] ) ){
        $sizes = $meta['_wp_attachment_metadata']['sizes'];
        if( !empty( $sizes[$size] ) ){
            $file_url = fr_uploads_url() . '/' . dirname( $meta['_wp_attached_file'] ) . '/' . $sizes[$size]['file'];
        }
    
    }

    return $file_url;

}



function fr_get_attachments_url( array $post_ids, string $image_size = '' ): array {

    $meta = fr_query_get_posts_meta( $post_ids, [ '_wp_attached_file', '_wp_attachment_metadata' ] );
    $attachments = [];

    foreach( $meta as $post_id => $post_meta ){
        $attachments[$post_id] = fr_get_file_url_from_meta( $post_meta, $image_size );
    }

    return $attachments;

}






function fr_regenerate_attachment_metadata( int $file_id ): bool {

    $file = get_attached_file( $file_id );

    if ( false !== $file ) {

        require_once(ABSPATH . 'wp-admin/includes/image.php');
        require_once(ABSPATH . 'wp-admin/includes/file.php');
        require_once(ABSPATH . 'wp-admin/includes/media.php');

        $metadata = wp_generate_attachment_metadata( $file_id, $file );

        if ( ! is_wp_error( $metadata ) && ! empty( $metadata ) ) {
            wp_update_attachment_metadata( $file_id, $metadata );
            return true;
        }
    }

    return false;

}


