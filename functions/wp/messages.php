<?php 

add_action("init",function(){
	//TRACK STATUS MESSAGES PASSED FROM URL

	if(!empty($_GET["fr_message"])){
		fr_message($_GET["fr_message"]);
		fr_clean_get_redirect("fr_message");
	}
	add_action( 'admin_notices', 'fr_admin_notice', 0);
});




function fr_admin_notice(){
	if( $message = fr_get_message() ){

		if( get_current_screen()->is_block_editor() ){

			$id = 'fr_admin_notice_' . fr_unique_id( 'fr_admin_notice' );
			?>
			<script>
			    jQuery(window).ready(function () {
			        setTimeout(function () {

			            let message = `<?php echo str_replace(array("\r", "\n"), '', addslashes($message['text'])); ?>`;
			            let noticeInterval = setInterval(function() {

						    let noticeElement = jQuery('.components-notice__content');
						    if (noticeElement.length) {
						        noticeElement.html(message);
						        clearInterval(noticeInterval);
						    }
						}, 100 );

			        }, 1);
			    });
			</script>
			<?php 

		} else {
			?>
			<div class="notice notice-<?php echo $message["type"] ?> is-dismissible">
				<p><?php echo $message["text"] ?></p>
			</div>
			<?php
		}

		fr_clear_message();
	}
}