<?php

function fr_rank_math_redirect_create( $from, $to, $comparison = 'exact' ){

	global $wpdb;

	if( $from == $to ){
		return false;
	}
	
	$from = str_replace( site_url(), '', $from );
	$to = str_replace( site_url(), '', $to );

	if( fr_rank_math_redirect_exists( $from ) ){
		return;
	}

	$wpdb->insert( $wpdb->prefix . 'rank_math_redirections', [
		'sources' => serialize( [
			[
				'pattern' => $from,
				'comparison' => $comparison
			]
		] ),
		'url_to' => $to,
		'header_code' => 301,
		'status' => 'active'
	] );

	return true;
}



function fr_rank_math_redirect_exists( $from ){

	global $wpdb;

	$from = str_replace( site_url(), '', $from );

	$rows = $wpdb->get_results( 'SELECT * FROM ' . $wpdb->prefix . 'rank_math_redirections WHERE BINARY sources LIKE "%' . esc_sql( '"' . $from . '";s:' ) . '%" LIMIT 0,1' );

	return reset( $rows );
}


function fr_rank_math_redirect_remove( $from ){

	global $wpdb;

	$from = str_replace( site_url(), '', $from );

	$wpdb->query( 'DELETE FROM ' . $wpdb->prefix . 'rank_math_redirections WHERE BINARY sources LIKE "%' . esc_sql( '"' . $from . '";s:' ) . '%"' );

	return true;
}



function fr_rank_math_redirect_update( $from, $to ){

	global $wpdb;

	$from = str_replace( site_url(), '', $from );
	$to = str_replace( site_url(), '', $to );

	$wpdb->query( 'UPDATE ' . $wpdb->prefix . 'rank_math_redirections SET url_to = "' . esc_sql( $to ) . '" WHERE BINARY url_to = "' . esc_sql( $from ) . '"' );
}










