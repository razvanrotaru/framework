<?php
//Do shortcodes in meta titles
add_filter( 'wpseo_title', 'do_shortcode', 10, 1 );

//Do shortcodes in meta description
add_filter( 'wpseo_metadesc', 'do_shortcode', 10, 1 );


// Disable YOAST automatic redirect generation for specific post types
function fr_yoast_disable_auto_redirects( $post_types ){
	$post_types = fr_make_array( $post_types );
	add_filter('Yoast\WP\SEO\post_redirect_slug_change', function( $value ) use ( $post_types ) {
		if( is_admin() && !empty( $_POST['post_type'] ) && in_array( $_POST['post_type'], $post_types ) ){
			$value = true;
		}
		return $value;
	} );
}