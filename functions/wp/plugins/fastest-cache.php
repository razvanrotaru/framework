<?php
function fr_fc_remove_admin_col(){
	add_action( 'admin_init', function(){
		fr_remove_class_action( 'manage_posts_columns', 'WpFastestCacheColumn', 'wpfc_clear_column_head' );
	}, 11 );
}

function fr_fc_clear_cache( $post_id = false ){
	if( class_exists( 'WpFastestCache' ) ){
		$wpfc = new WpFastestCache();
		if( $post_id ){
			$wpfc->singleDeleteCache( false, $post_id );
		} else {
			$wpfc->deleteCache();
		}
	}
}