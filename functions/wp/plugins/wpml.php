<?php
function fr_wpml(){
	return function_exists( 'icl_object_id' );
}
function fr_wpml_get_permalink( $pid, $lang = false ){
	global $sitepress;

	if( !$lang ){
		$lang = fr_wpml_current_lang();
	}
	 
	$link = fr_get_permalink($pid);
	return apply_filters( 'wpml_permalink', $link , $lang );
}

function fr_wpml_get_id($pid,$type="",$lang=""){
	if(!$type){
		$type=get_post_type($pid);
	}
	if(!$lang){
		$lang=fr_wpml_current_lang();
	}
	return apply_filters( 'wpml_object_id', $pid, $type, 1, $lang );
}

// Redirect to the page in the current language
function fr_wpml_redirect( $url, $code = '303' ){
	if( fr_wpml() && is_numeric( $url ) ){
		$url = fr_wpml_get_id( $url );
	}
	fr_redirect( $url, $code );
}

function fr_wpml_current_lang(){
	return apply_filters( 'wpml_current_language', NULL );
}

function fr_wpml_current_real_lang(){
	if( !empty( $_ENV['fr_wpml_temp_lang'] ) ){
		return reset( $_ENV['fr_wpml_temp_lang'] );
	} else {
		return apply_filters( 'wpml_current_language', NULL );
	}
}

function fr_wpml_set_lang($lang, $set_cookie = false ){
	global $sitepress;


	// Fix. Modify the REQUEST_URI to make WPML think this is the current language

	if( !fr_is_ajax() && !is_admin() ){
		$langs = fr_wpml_get_all_languages();
		$path = explode( '?', $_SERVER['REQUEST_URI'] );
		$path = reset( $path );
		$path = explode( '/', trim( $path, '/' ) );
		if( isset( $langs[$path[0]] ) ){
			$_SERVER['REQUEST_URI'] = substr( $_SERVER['REQUEST_URI'], 3 );
		}
		if( $sitepress->get_default_language() != $lang ){
			$_SERVER['REQUEST_URI'] = '/' . $lang . $_SERVER['REQUEST_URI'];
		}
	}

	$sitepress->switch_lang($lang, $set_cookie);
}

function fr_wpml_get_default_lang(){
	global $sitepress;
	return $sitepress->get_default_language();
}

function fr_wpml_is_post_duplicate( $pid ){
	return get_post_meta( $pid, '_icl_lang_duplicate_of', true );
}

/**
 * Allows to create duplicates of posts in all languages
 * @param  int $pid The ID of the post
 * @return array An array of IDs. Each ID represents the post translated in a certain language
 */
function fr_wpml_create_duplicate_post( $pid ){

	if( !$pid ){
		return false;
	}

	// Abort if the post is not the original
	if( fr_wpml_is_post_duplicate( $pid ) ){
		return fr_wpml_post_in_all_languages( $pid );
	}

	$posts = fr_wpml_post_in_all_languages( $pid );

	if( count( $posts ) <= 1 ){
		wpml_admin_make_post_duplicates_action( $pid );
		$posts = fr_wpml_post_in_all_languages( $pid );
	}

	// Sync taxonomies
	$taxonomies = get_post_taxonomies( $pid );

	$post_lang = fr_wpml_get_post_language( $pid );

	foreach( $posts as $lang => $post ){

		// Don't update the current post
		if( $lang == $post_lang ){
			continue;
		}

		foreach( $taxonomies as $taxonomy ){
			$terms = wp_get_post_terms( $pid, $taxonomy, array( 'fields' => 'ids' ) );
			foreach( $terms as $k => $term ){
				$terms[$k] = fr_wpml_get_id( $term, $taxonomy, $lang );
			}
			if( $terms ){
				wp_set_post_terms( $post->element_id, $terms, $taxonomy );
			}
		}
	}

	return $posts;
}


/**
 * Translate acf values across all duplicate posts
 * @param  int $pid The ID of the post
 * @param array $fields Array of acf fields specifing the type of object 
 * @return array An array of IDs. Each ID represents the post translated in a certain language
 */
function fr_wpml_translate_acf_fields( $pid, $key, $value, $fields = array() ){

	// Abort if the post is not the original
	if( fr_wpml_is_post_duplicate( $pid ) ){
		return;
	}
	
	$cur_lang = fr_wpml_get_post_language( $pid );
	$posts = fr_wpml_post_in_all_languages( $pid );
	$values = array( $key => $value );

	// Loop trough each post language and update the acf object fields
	foreach( $posts as $lang => $post ){

		// Ignore updating for posts in the same language
		if( $lang == $cur_lang ){
			continue;
		}

		fr_wpml_set_temp_lang( $lang );
		$translated_values = fr_wpml_translate_acf_objects( $values, $fields, $lang );
	
		foreach( $translated_values as $key => $value ){
			if( $value !== $values[$key] ){
				update_field( $key, $value, $post->element_id );
			}
		}
		fr_wpml_reset_temp_lang();
	}
}


/**
 * Translate an array of acf values by identifying the objects by keys
 * The function is recursive
 * @param  array $values Values to translate
 * @param  array $fields Acf object fields to match and translate
 * @param  string $lang  The language in which to translate
 * @return array         Return the translated values
 */
function fr_wpml_translate_acf_objects( $values, $fields, $lang ){
	foreach( $values as $k => $value ){
		$name = fr_acf_field_name( $k );
		if( isset( $fields[$name] ) ){
			if ( !is_array( $value ) ){
				// For single value
				$values[$k] = fr_wpml_get_id( $value, $fields[$name], $lang );
			}else{
				// For multiple values
				foreach( $value as $k2 => $v ){
					$values[$k][$k2] = fr_wpml_get_id( $v, $fields[$name], $lang );
				}
			}
		}else if( is_array( $value ) ){
			$values[$k] = fr_wpml_translate_acf_objects( $value, $fields, $lang );
		}
	}
	if( $values )
	return $values;
}


function fr_wpml_get_post_language( $pid ){
	$post_language_information = apply_filters( 'wpml_post_language_details', NULL, $pid ) ;
	if( is_wp_error( $post_language_information ) ){
		return false;
	}
	return $post_language_information['language_code'];
}

function fr_wpml_get_term_language( $tid, $tax ){
	$language_code = apply_filters( 'wpml_element_language_code', null, array( 'element_id'=> (int)$tid, 'element_type'=> $tax ) );
	return $language_code;
}

function fr_wpml_get_all_languages(){
	return icl_get_languages( 'skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str' );
}


/**
 * Return post in all languages
 * @param  int $pid Post ID
 * @return array    An array of posts
 */
function fr_wpml_post_in_all_languages( $pid, $type = 'post_post' ){
	global $sitepress;
	$trid = $sitepress->get_element_trid( $pid, $type );
	$posts = $sitepress->get_element_translations( $trid, $type, false, true );
	return $posts;
}

/**
 * Return post ids in all languages
 * @param  int $pid Post ID
 * @return array    An array of posts
 */
function fr_wpml_post_ids_in_all_languages( $pid ){
	$posts = fr_wpml_post_in_all_languages( $pid );
	return array_map( function( $a ){ return $a->element_id; }, $posts );
}


/**
 * Set temporary language. Useful if content from 2 or more languages is displayed on the same page
 * @param  string $lang Language code to switch to
 */
function fr_wpml_set_temp_lang( $lang, $dir = 'ltr' ){
	global $wp_locale;
	if( empty( $_ENV['fr_wpml_temp_lang'] ) ){
		$_ENV['fr_wpml_temp_lang'] = array();
		$_ENV['fr_wpml_temp_lang_dir'] = array();
	}
	$_ENV['fr_wpml_temp_lang'][] = fr_wpml_current_lang();
	$_ENV['fr_wpml_temp_lang_dir'][] = $wp_locale->text_direction;
	fr_wpml_set_lang( $lang );
	$wp_locale->text_direction = $dir;
}

/**
 * Reset temporary language.
 */
function fr_wpml_reset_temp_lang(){
	global $wp_locale;
	if( !empty( $_ENV['fr_wpml_temp_lang'] ) ){
		$lang = array_pop( $_ENV['fr_wpml_temp_lang'] );
		$dir = array_pop( $_ENV['fr_wpml_temp_lang_dir'] );
		fr_wpml_set_lang( $lang );
		$wp_locale->text_direction = $dir;
	}
}

/**
 * Get original language before temporary language is set
 */
function fr_wpml_get_page_lang(){
	return !empty( $_ENV['fr_wpml_temp_lang'] ) ? reset( $_ENV['fr_wpml_temp_lang'] ) : fr_wpml_current_lang();
}




function fr_wpml_show_hreflang_links(){
  $languages = fr_wpml_get_all_languages();
  foreach( $languages as $language ){
    ?>
    <link rel="alternate" hreflang="<?php echo $language['default_locale'] ?>" href="<?php echo $language['url'] ?>" />
		<?php 
  }
}


/**
 * Given an array of ids it will return the corresponding ids from a different language
 * @param  array $ids
 * @param  string $type Can be the post type or taxonomy name
 * @param  string $lang
 * @return array
 */
function fr_wpml_get_ids_in_lang( $ids, $type, $lang ){
	foreach( $ids as $k => $id ){
		$ids[$k] = fr_wpml_get_id( $id, $type, $lang );
	}
	return $ids;
}
















