<?php
add_theme_support( 'woocommerce' );

function fr_wc_send_email($email,$subject,$heading,$message,$headers="",$attachments=array()){
    global $woocommerce;
    $mailer = $woocommerce->mailer();
    $plain_text = strip_tags($message);
    $message = $mailer->wrap_message(__( $heading ), $message );
    return $mailer->send( $email, $subject, $message, $headers, $attachments );
}


function fr_wc_resend_email_action($action,$order){
	do_action( 'woocommerce_before_resend_order_emails', $order );

	// Ensure gateways are loaded in case they need to insert data into the emails.
	WC()->payment_gateways();
	WC()->shipping();

	// Load mailer.
	$mailer = WC()->mailer();
	$email_to_send = str_replace( 'send_email_', '', $action );
	$mails = $mailer->get_emails();

	if ( ! empty( $mails ) ) {
		foreach ( $mails as $mail ) {
			if ( $mail->id == $email_to_send ) {
				$mail->trigger( $order->id );
				$order->add_order_note( sprintf( __( '%s email notification manually sent.', 'woocommerce' ), $mail->title ), false, true );
			}
		}
	}

	do_action( 'woocommerce_after_resend_order_email', $order, $email_to_send );
}

function fr_wc_order_note($order,$message){
	if(is_numeric($order)){
		if(get_post_type($order)=="shop_order"){
			$order = new WC_Order( $order );
		}else if(get_post_type($order)=="shop_subscription"){
			$order = new WC_Subscription( $order );
		}
	}

	if($order && is_object($order) && method_exists($order,"add_order_note")){
		$notes=fr_wc_get_order_notes($order->get_order_number());
		$note=reset($notes);

		//if last note is the same as this one, skip to avoid flooding the comments with messages
		if($note && $note["note_content"]==$message){
			return;
		}
		$order->add_order_note( $message, false, false );
	}
}

function fr_wc_get_order_notes($order_id){
    global $wpdb;

    $table_perfixed = $wpdb->prefix . 'comments';
    $results = $wpdb->get_results("
        SELECT *
        FROM $table_perfixed
        WHERE  `comment_post_ID` = $order_id
        AND  `comment_type` LIKE  'order_note'
        ORDER BY `comment_ID` DESC
    ");

    $order_note=array();
    foreach($results as $note){
        $order_note[]  = array(
            'note_id'      => $note->comment_ID,
            'note_date'    => $note->comment_date,
            'note_author'  => $note->comment_author,
            'note_content' => $note->comment_content,
        );
    }
    return $order_note;
}

function fr_wc_get_product_dimensions($pid,$variation=""){
	if(is_array($pid)){
		$variation=$pid["variation_id"];
		$pid=$pid["product_id"];
	}
    if(is_object($pid)){
        $pid=$pid->get_product_id();
    }
	if($variation){
 		$variation = wc_get_product($variation);
 		$w=$variation->weight;
 		$l=$variation->length;
 		$wi=$variation->width;
 		$h=$variation->height;
 	}else{
 		$w=get_post_meta($pid,"_weight",1);
 		$l=get_post_meta($pid,"_length",1);
 		$wi=get_post_meta($pid,"_width",1);
 		$h=get_post_meta($pid,"_height",1);
 	}
 	return array(
 		"weight"=>$w,
 		"length"=>$l,
 		"width"=>$wi,
 		"height"=>$h
 		);
}





function fr_wc_new_columns( $columns ) {
    $columns['items'] = 'Items';
    return $columns;
}
add_filter( 'manage_edit-shop_order_columns', 'fr_wc_new_columns' );

function fr_wc_new_columns_content( $column ) {
    global $post;

    if ( 'items' === $column ) {

        $order    = wc_get_order( $post->ID );
        $items = $order->get_items();
        $text=array();
        foreach($items as $item){
            $text[]=$item["name"];
        }
        echo implode("<br>",$text);
    }
}
add_action( 'manage_shop_order_posts_custom_column', 'fr_wc_new_columns_content' );



/**
 * Get all variation ids of a product
 *
 * @param int $product
 * @return array 
 */


function fr_wc_product_variations($product){
    return fr_query_get_ids(array(
        "post_type"=>"product_variation",
        "post_parent"=>$product,
        'orderby'       => 'menu_order',
        'order'         => 'asc',
        "post_type"=>"any",
        'post_status'   => array( 'private', 'publish' )
        ));
}



/**
 * Function to create a WC variation to a specic product
 *
 * @param int $product
 * @param string $title
 * @param string $content
 * @param array $meta Array of meta key and values to be inserted
 * @return int The id of the new variation
 */

function fr_wc_create_variation($product,$title,$content="",$meta=array()){


    //make current product variable if not set
    $data = get_post_meta($product,"_product_attributes",1);
    if(empty($data)){
        $data = array();
    }
    if(empty($data["levels"])){
        $data["levels"] = array();
    }
    $data["levels"]["is_variation"]=1;
    update_post_meta($product,"_product_attributes",$data);

    //create variation
    $pid = wp_insert_post(array(
        "post_type"=>"product_variation",
        "post_title"=>$title,
        "post_content"=>$content,
        "post_parent"=>$product,
        "post_status"=>"publish"
        ));

    //save variation meta
    foreach($meta as $k=>$v){
        update_post_meta($pid,$k,$v);
    }
    return $pid;
}




function fr_wc_admin_add_product_fields( $fields ){

    add_action( 'woocommerce_product_options_general_product_data', function() use ( $fields ){
        ?>
        <div class="options_group">
            <?php
            foreach( $fields as $field ){
                $field['value'] = fr_m( $field['id'] );
                echo fr_wc_field( $field );
            }
            ?>
        </div>
        <?php 
    } );


    add_action( 'woocommerce_process_product_meta', function( $pid ) use ( $fields ){
        $data = [];
        foreach( $fields as $field ){
            $value = isset( $_POST[$field['id']] ) ? $_POST[$field['id']] : '';
            fr_m( $field['id'], $value, $pid );
        }
    } ); 
}


// Options: https://gist.github.com/igorbenic/1106ccf44c1061d80c2738bcbe09181d
function fr_wc_field( $field ){
    ob_start();
    if( $field['type'] == 'textarea' ){
        woocommerce_wp_textarea_input( $field );
    } else if( $field['type'] == 'select' ){
        woocommerce_wp_select( $field );
    } else if( $field['type'] == 'hidden' ){
        woocommerce_wp_hidden_input( $field );
    } else if( $field['type'] == 'radio' ){
        woocommerce_wp_radio( $field );
    } else if( $field['type'] == 'checkbox' ){
        woocommerce_wp_checkbox( $field );
    } else {
        woocommerce_wp_text_input( $field );
    }
    return ob_get_clean();
}



function fr_wc_admin_add_product_attribute_fields( $fields ){
    add_action( 'woocommerce_after_product_attribute_settings', function( $attribute, $i ) use ( $fields ) {
        $pid = !empty( $_POST['post_id'] ) ? $_POST['post_id'] : get_the_ID();
        foreach( $fields as $field ){
            $field['value'] = fr_wc_get_attribute_custom_field( $attribute->get_name(), 'attribute_in_quote', $pid );
            $label = $field['label'];
            $field['label'] = '';
            $field['name'] = $field['id'] . '[' . $i . ']';
            ?>
            <tr>
                <td colspan="2">
                    <?php if( $field['type'] == 'checkbox' ){ ?>
                        <label>
                            <?php echo fr_strip_only_tags( fr_wc_field( $field ), 'p' ) ?>
                            <?php echo $label ?>
                        </label>
                    <?php } ?>
                </td>
            </tr>
            <?php 
        }
    }, 10, 2 );


    add_action( 'woocommerce_after_product_object_save', function( $ob, $data ) use ( $fields ) {

        if( !fr_is_ajax( 'woocommerce_save_attributes' ) ){
            return;
        }

        $data = urldecode( $_POST['data'] );
        parse_str( $data, $data );

        if( !isset( $data['attribute_names'] ) ){
            return;
        }

        $attr = [];
        foreach( $data['attribute_names'] as $k => $name ){

            $attr[$name] = [];
            foreach( $fields as $field ){
                $attr[$name][$field['id']] = isset( $data[$field['id']][$k] ) ? $data[$field['id']][$k] : false;
            }
        }
        
        fr_m( '_custom_product_attribute_fields', $attr, $ob->get_id() );
    }, 10, 3 );
}


function fr_wc_get_attribute_custom_field( $attribute, $field, $pid = false ){
    $attr = fr_m( '_custom_product_attribute_fields', $pid );
    $attr = fr_make_array( $attr );
    if( isset( $attr[$attribute][$field] ) ){
        return $attr[$attribute][$field];
    } else {
        return false;
    }
}


function fr_wc_get_attribute( $key, $pid = false, $options = [ 'fields' => 'ids' ] ){
    $pid = $pid ? $pid : get_the_ID();
    return wp_get_post_terms( $pid, $key, $options );
}

