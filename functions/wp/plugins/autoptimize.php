<?php
class fr_autoptimize_aggregate_only{

	var $start_queue = [];
	var $include = [];
	var $include_by_file = [
		'js' => [],
		'css' => []
	];
	var $exclude_by_file = [
		'js' => [],
		'css' => []
	];

	function __construct(){
		$ob = $this;
		add_filter( 'autoptimize_filter_js_exclude', function ( $exclude_scripts ){
			$exclude_scripts = explode( ',', $exclude_scripts );
			$exclude_scripts = $this->generate_js_list( $exclude_scripts );
			return implode( ',', $exclude_scripts );
		},  10, 2 );

		add_filter( 'autoptimize_filter_css_exclude', function ( $exclude_scripts ){
			$exclude_scripts = explode( ',', $exclude_scripts );
			$exclude_scripts = $this->generate_css_list( $exclude_scripts );
			return implode( ',', $exclude_scripts );
		},  10, 2 );
	}

	function generate_js_list( $exclude_scripts ){
		global $wp_scripts;
		$exclude = array_diff( $wp_scripts->queue, $this->include['js'] );
		$exclude = fr_array_remove_empty( $exclude );

		foreach( $exclude as $script ){
			if( isset( $wp_scripts->registered[$script] ) ){
				$script = $wp_scripts->registered[$script]->src;
				$script = str_replace( SITE_URL . '/wp-content/', '', $script );
				$exclude_scripts[] = $script;
			}
		}

		$exclude_scripts = array_merge( $exclude_scripts, $this->exclude_by_file['js'] );
		$exclude_scripts = fr_array_remove_empty( $exclude_scripts );
		$exclude_scripts = array_unique( $exclude_scripts );

		
		foreach( $this->include_by_file['js'] as $file ){
			foreach( $exclude_scripts as $k => $v ){
				if( strstr( $v, $file ) ){
					unset( $exclude_scripts[$k] );
				}
			}
		}

		do_action( 'fr_autoptimize_aggregate_only', $this->include['js'], $this->include_by_file['js'], $this->exclude_by_file['js'], 'js' );

		return $exclude_scripts;
	}

	function generate_css_list( $exclude_scripts = [] ){
		global $wp_styles;
		$exclude = array_diff( $wp_styles->queue, $this->include['css'] );
		$exclude = fr_array_remove_empty( $exclude );

		foreach( $exclude as $script ){
			if( isset( $wp_styles->registered[$script] ) ){
				$script = $wp_styles->registered[$script]->src;
				$script = str_replace( SITE_URL . '/wp-content/', '', $script );
				$exclude_scripts[] = $script;
			}
		}

		foreach( $this->include_by_file['css'] as $file ){
			foreach( $exclude_scripts as $k => $v ){
				if( strstr( $v, $file ) ){
					unset( $exclude_scripts[$k] );
				}
			}
		}

		do_action( 'fr_autoptimize_aggregate_only', $this->include['css'], $this->include_by_file['css'], $this->exclude_by_file['css'], 'css' );

		return $exclude_scripts;
	}

	function get_included_files( $type ){
		global $wp_styles, $wp_scripts;
		$included = $this->include_by_file[$type];
		foreach( $this->include[$type] as $name ){
			if( $type == 'css' ){
				if( isset( $wp_styles->registered[$name] ) ){
					$script = $wp_styles->registered[$name]->src;
					$included[] = $script;
				}
			}else{
				if( isset( $wp_scripts->registered[$name] ) ){
					$script = $wp_scripts->registered[$name]->src;
					$included[] = $script;
				}
			}
		}
		return $included;
	}
	

	function start(){
		global $wp_scripts, $wp_styles;
		$this->start_queue['js'] = $wp_scripts->queue;
		$this->start_queue['css'] = $wp_styles->queue;
	}

	function end(){
		global $wp_scripts, $wp_styles;
		$this->include['js'] = fr_array_remove_empty( array_diff( $wp_scripts->queue, $this->start_queue['js'] ) );
		$this->include['css'] = fr_array_remove_empty( array_diff( $wp_styles->queue, $this->start_queue['css'] ) );
	}

	function add_js( $script ){
		$this->include_by_file['js'][] = $script;
	}

	function add_css( $script ){
		$this->include_by_file['css'][] = $script;
	}

	function exclude_js( $script ){
		$this->exclude_by_file['js'][] = $script;
	}

	function exclude_css( $script ){
		$this->exclude_by_file['css'][] = $script;
	}
}