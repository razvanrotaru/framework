<?php
// Allow for shortcodes in messages
add_filter('acf/load_field/type=message', function( $field  ) {
	if( fr_screen_id() ){
		$screen = get_current_screen();
		if( 
			( $screen->base == 'post' && $screen->id != 'acf-field-group' ) || // Editing a post
			$screen->base == 'edit-tags' || // Create term
			$screen->base == 'term' || // Edit term
			$screen->base == 'user-edit' // Edit User
		){
		    $field['message'] = do_shortcode( $field['message'] );
		}
	}
    return $field;
}, 10, 3 );

//Converts date from 20160301 to acf date format
function fr_acf_time( $date ){
	$y=substr($date,0,4);
	$m=substr($date,4,2);
	$d=substr($date,6,2);
	return strtotime( "$y-$m-$d 00:00:00" );
}

//Converts date from 20160301 to acf date format
function fr_acf_date( $date, $format="Y-m-d" ){
	return date( $format, fr_acf_time( $date ) );
}

/**
 * If a field contains a valid URL then display an url next to the field
 */
add_action( 'acf/render_field', function ( $field ) {
  if($field["value"]){
    if(filter_var($field["value"], FILTER_VALIDATE_URL)){
      echo '<p class="fr_acf_url"><a href="'.$field["value"].'" target="_blank">'.__("View Link").'</a></p>';
    }
  }
}, 10, 1 );

/**
 * Adds a link under the field to the post admin
 * @param  string $post_type  Post type to which this should apply too
 * @param  string $field_name The name of the field
 */
function fr_acf_field_admin_post_link( $post_type, $field_name ){
		add_action( 'acf/render_field', function ( $field ) use ( $post_type, $field_name ) {
			if( fr_screen_id() != $post_type ){
				return;
			}

		    if( $field['_name'] == $field_name && $field["value"] && !isset( $field['ajax'] ) ){
		    	$link = admin_url( sprintf( '/post.php?post=%d&action=edit', $field["value"] ) );
			    echo '<p><a href="' . $link . '">' . get_the_title( $field["value"] ) . '</a></p>';
		    }
		}, 10, 1 );
}

/**
 * Link automatically the checkbox and radio fields options from gravity forms to show up on ACF options
 * To use, add in a gravity field the option: gf_field_1_2. Where 1 is the gravity form ID and 2 is the gravity form field ID.
 */

function fr_acf_get_gf_options_filter( $field ) {

 	// don't load custom values on group admin page
 	if( is_admin() && function_exists('get_current_screen') ){
 		$screen = get_current_screen();
	 	if( !empty( $screen->parent_file ) && $screen->parent_file == 'edit.php?post_type=acf-field-group' ){
	 		return $field;
	 	}
	 }
	 
	if(!empty($field["choices"]) ){
		$choice = reset($field["choices"]);
		if( substr($choice,0,9)=="gf_field_"){
			$options = str_replace("gf_field_","",reset($field["choices"]));
			$options = explode("_",$options);
			if(count($options)==2){
			 	$form = RGFormsModel::get_form_meta($options[0]);
			 	$gf_field = fr_gf_get_field($form,$options[1]);
			    if($gf_field){
				    $field['choices'] = array();
			        foreach( $gf_field['choices'] as $choice ) {
			            $field['choices'][ $choice["value"] ] = $choice["text"];
			        }
				}
			}
		}
	}
    return $field;
}

add_filter('acf/load_field', 'fr_acf_get_gf_options_filter');


function fr_acf_field_name( $name, $pid = 0 ){
	if( !empty( $name['name'] ) ){
		$name = $name['name'];
	} else if( substr( $name, 0, 6 ) == 'field_' ){
		global $wpdb;
		$field = get_field_object($name);
		$name = $field['name'];
	}
	return $name;
}

function fr_acf_is_top_field( $field, $pid = 0 ){
	if( !$field ){
		return;
	}
	if( is_string( $field ) ){
		$field = get_field_object( $field, $pid );
	}
	return get_post_type( $field['parent'] ) != 'acf-field';
}


function fr_acf_get_field_object( $selector, $post_id = false ){
	return acf_maybe_get_field( $selector, $post_id, false );
}

function fr_acf_get_field_options( $selector ){
	$object = fr_acf_get_field_object( $selector );
	if( isset( $object['choices'] ) ){
		return $object['choices'];
	}
}

function fr_acf_get_field_option_label( $selector, $key, $extra_options = [] ){
	$options = fr_acf_get_field_options( $selector );
	$options = $extra_options + $options;

	if( isset( $options[$key] ) ){
		return $options[$key];
	}
}

/**
 * Creates a new group option to be able to select what user role can view the group
 */
function fr_acf_add_group_visibility(){

	add_action( 'acf/render_field_group_settings', function(){
		global $field_group;

		$roles = get_editable_roles();
		$roles = fr_array_column( $roles, 'name' );

		acf_render_field_wrap( array(
			'label'			=> __( 'Visible to','acf' ),
			'instructions'	=> '',
			'type'			=> 'checkbox',
			'name'			=> 'visible_to',
			'prefix'		=> 'acf_field_group',
			'value'			=> isset( $field_group['visible_to'] ) ? $field_group['visible_to'] : [],
			'choices' 		=> $roles
		));
	} );

	add_filter( 'acf/get_field_groups', function( $field_groups ){
		foreach( $field_groups as $k => $v ){
			if( !empty( $v['visible_to'] ) && ( !fr_user( 'roles' ) || !array_intersect( fr_user( 'roles' ), $v['visible_to'] ) ) ){
				unset( $field_groups[$k] );
			}
		}
		return $field_groups;
	} );
}




function fr_acf_group_add_rule( $new_rule_key, $new_rule_label, $type = 'post_type' ){
	add_filter( 'acf/location/rule_values', function( $values, $rule ) use ( $new_rule_key, $new_rule_label, $type ) {
		if( $rule['param'] == $type ){
			$values[$new_rule_key] = $new_rule_label;
		}
		return $values;
	}, 100, 2 );
}



// Be carefull to not hide it everywhere else the fields will be missing in the export or other parts
function fr_acf_hide_field( $fields_to_hide = [] ){
	if( !is_array( $fields_to_hide ) ){
		$fields_to_hide = [ $fields_to_hide ];
	}
	add_filter('acf/get_fields', function( $fields, $parent ) use ( $fields_to_hide ) {
		if( fr_screen_id() != 'acf-field-group' ){
			foreach( $fields as $k => $field ){
				if( in_array( $field['name'], $fields_to_hide ) ){
					unset( $fields[$k] );
				} else if( !empty( $field['sub_fields'] ) ){
					foreach( $field['sub_fields'] as $k2 => $subfield ){
						if( in_array( $field['name'] . '_' . $subfield['name'], $fields_to_hide ) ){
							unset( $fields[$k]['sub_fields'][$k2] );
						}
					}
				}
			}
		}

		return $fields;
	}, 20, 2);
}



function fr_acf_wpml_options_from_original_lang(){
	add_filter( 'acf/validate_post_id', function( $post_id ){
		if( fr_str_starts_with( $post_id, 'options_' ) ){
			$post_id = 'options';
		}
		return $post_id;
	}, 10, 1 );
}

function fr_acf_copy_post_meta( $from_pid, $to_pid, $meta = [] ){
	foreach( $meta as $key ){
		update_field( $key, get_field( $key, $from_pid ), $to_pid );
	}
}



function fr_acf_disable_options_page_for_non_admins( $name ){
	add_action('admin_init', function() use ( $name ) {
	   remove_menu_page( $name );
	}, 99);
}



/** 
Function to efficiently get multidimensional values from ACF fields
It only works for posts
 */
function fr_acf_get_field( $name, $pid = false, $meta = false, $group_id = false ){

	global $wpdb;

	if( $pid === false ){
		$pid = get_the_ID();
	}

	if( $pid == 'options' ){
		if( substr( $name, -1, 1 ) == '*' ){
			// Multiple
			$name = 'options_' . substr( $name, 0, -1 );
			$res = $wpdb->get_results( 'SELECT option_name, option_value FROM ' . $wpdb->prefix . 'options WHERE option_name LIKE "' . esc_sql( $name ) . '%"', ARRAY_A );
			$res = array_column( $res, 'option_value', 'option_name' );
			$res = fr_array_map_keys( function( $k ) use ( $name ){
				return substr( $k, strlen( $name ) );
			}, $res );
			return $res;
		} else {
			// Single
			return get_option( 'options_' . $name );
		}
	}


	if( $meta === false ){
		$meta = get_post_meta( $pid );
		$meta = array_map( function( $a ){ return $a[0]; }, $meta );
	}

	// Build a hash value based on all values
	$new_hash = [];

	if( $group_id ){
		$new_hash['modified_date'] = get_post_field( 'post_modified_date', $group_id );
	}

	foreach( $meta as $k => $v ){
		
		if( $name != $k && !fr_str_starts_with( $k, $name . '_' ) ){
			continue;
		}

		if( in_array( $k, [ $name . '_acf_hash', $name . '_acf_hash_data' ] ) ){
			continue;
		}

		$new_hash[$k] = $v;
	}


	$new_hash = md5( json_encode( $new_hash ) );

	if( !isset( $meta[$name . '_acf_hash'] ) || !isset( $meta[$name . '_acf_hash_data'] ) || $meta[$name . '_acf_hash'] != $new_hash ){
		$data = get_field( $name, $pid, false );
		fr_m( $name . '_acf_hash', $new_hash, $pid );
		fr_m( $name . '_acf_hash_data', $data, $pid );
	} else {
		$data = $meta[$name . '_acf_hash_data'];
		if( !is_array( $data ) ){
			$data2 = is_serialized( $data ) ? unserialize( $data ) : false;
			if( $data2 ){
				$data = $data2;
			}
		}
	}

	$pid = acf_get_valid_post_id( $pid );
	$field = acf_maybe_get_field( $name, $pid );
	if( $field ){
		$data = acf_format_value( $data, $pid, $field );
	}

	return $data;
}



function fr_acf_get_repeater_field( $field_name, $pid = 0 ){

	global $wpdb;

	$pid = $pid ?: get_the_ID();

	// cache
	$data = wp_cache_get( $pid . '_' . $field_name, 'fr_acf_get_repeater_field' );

	if( $data !== false ){
		return $data;
	}
	
	$metas = $wpdb->get_results( 'SELECT meta_key, meta_value FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key LIKE "' . esc_sql( $field_name ) . '_%" AND post_id = ' . $pid, ARRAY_A );
	$data = [];

	foreach( $metas as $meta ){
		$key = fr_strstra( $meta['meta_key'], $field_name . '_' );
		$index = strstr( $key, '_', true );

		if( !is_numeric( $index ) ){
			continue;
		}

		$key = fr_strstra( $key, '_' );
		$data[$index] = $data[$index] ?? [];

		$data[$index][$key] = $meta['meta_value'];
	}

	ksort( $data, SORT_NUMERIC );

	wp_cache_set( $pid . '_' . $field_name, $data, 'fr_acf_get_repeater_field' );

	return $data;

}





function fr_acf_export_data( $settings ){
	$data = [];
	foreach( $settings as $pid => $metas ){
		$metas = explode( ' ', $metas );
		foreach( $metas as $meta ){
			$data[$pid][$meta] = get_field( $meta, $pid, false );
		}
	}
	return json_encode( $data );
}


function fr_acf_import_data( $data ){

	$data = json_decode( $data, true );

	foreach( $data as $pid => $metas ){
		foreach( $metas as $meta => $value ){
			update_field( $meta, $value, $pid );
		}
	}
}


function fr_acf_copy_field( $from_key, $from_pid, $to_key, $to_pid ){
	$from_key = explode( '.', $from_key );
	$to_key = explode( '.', $to_key );
	$value = get_field( $from_key[0], $from_pid );
	if( count( $from_key ) > 1 ){
		array_shift( $from_key );
		$value = fr_get_value_recursively( $value, $from_key );
	}
	
	if( count( $to_key ) > 1 ){
		$main_key = $to_key[0];
		$current_value = get_field( $main_key, $to_pid );
		array_shift( $to_key );
		$current_value = fr_set_value_recursively( $current_value, $to_key, $value );
		update_field( $main_key, $current_value, $to_pid );
	} else {
		update_field( $to_key[0], $value, $to_pid );
	}
}



function fr_acf_load_save_json( $dir ){
	add_filter( 'acf/settings/save_json', function( $path ) use ( $dir ) {
	    return $dir;
	});
	add_filter( 'acf/settings/load_json', function( $paths ) use ( $dir ) {
	    $paths[0] = $dir;
	    return $paths;
	});
}