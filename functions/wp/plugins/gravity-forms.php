<?php
/**
 * Helper function to add custom options to drop downs and radio fields
 *
 * @param array $options Associative array with options for the field
 * @param int/string $field The ID or the Parameter Name of the field
 * @param int $form_id The ID of the form to which this is applied. If empty the options will apply to all forms
 * @param string/array $selected The key or array of keys of the options that need to be selected by default
 * @return true;
 */

function fr_gf_field_options($options,$field,$form_id=0,$selected=array()){
    global $fr_gf_field_options;
    if(empty($fr_gf_field_options)){
        $fr_gf_field_options=array();
    }

    if(!is_array($selected)){
        $selected = array($selected);
    }

    $fr_gf_field_options[]=array(
        "form_id"=>$form_id,
        "field"=>$field,
        "options"=>$options,
        "selected"=>$selected
    );

    return true;
}



/**
 * Gravity forms filter for field options
 *
 * @param array $form Current form information
 */

function fr_gf_custom_options($form){
    global $fr_gf_field_options;

    if(!empty($fr_gf_field_options)){
        foreach($fr_gf_field_options as $field){
            if(!$field["form_id"] || $field["form_id"]==$form["id"]){
                foreach($form["fields"] as $k=>$v){
                    if($field["field"]==$v->id || $field["field"]==$v->inputName){
                        $options = array();
                        foreach($field["options"] as $key=>$val){
                            $selected = false;
                            if(in_array($key,$field["selected"])){
                                $selected = true;
                            }
                            $options[]=array(
                                "text"=>$val,
                                "value"=>$key,
                                "isSelected"=>$selected
                                );
                        }

                        //If it is a function, call to retrieve the options
                        $options = fr_call_if_func($options,array($field, $form));

                        if(!is_array($options)){
                            $options = array();
                        }
                               
                        $v->choices = $options;
                    }
                }
            }
        }
    }
    return $form;
}

add_filter( 'gform_pre_render', 'fr_gf_custom_options' );
add_filter( 'gform_pre_validation', 'fr_gf_custom_options' );
add_filter( 'gform_pre_submission_filter', 'fr_gf_custom_options' );
add_filter( 'gform_admin_pre_render', 'fr_gf_custom_options' );




/**
 * Gravity forms apply shortcodes
 *
 * @param array $form Current form information
 */

function fr_gf_apply_shortcodes($form){
    foreach($form["fields"] as $field){
        $field->content = do_shortcode( $field->content );
        $field->description = do_shortcode( $field->description );
    }
    return $form;
}

add_filter( 'gform_pre_render', 'fr_gf_apply_shortcodes' );
add_filter( 'gform_pre_validation', 'fr_gf_apply_shortcodes' );
add_filter( 'gform_pre_submission_filter', 'fr_gf_apply_shortcodes' );



/**
 * Gravity forms fields to be conditionaly required
 *
 * @param array $forms
 * @param string/function $callback
 */

function fr_gf_required_field_conditionaly($forms=array(),$callback = ''){
    global $fr_gf_required_field_conditionaly_options;
    if(empty($fr_gf_required_field_conditionaly_options)){
        $fr_gf_required_field_conditionaly_options=array();
    }
    if( !is_array( $forms ) ){
        $forms = array( $forms );
    }

    foreach( $forms as $form ){
        $fr_gf_required_field_conditionaly_options[]=array(
            "form_id"=>$form,
            "callback"=>$callback
            );
    }
    return true;
}


function fr_gf_required_field_conditionaly_filter($form){
    global $fr_gf_required_field_conditionaly_options;

    if(empty($fr_gf_required_field_conditionaly_options)){
        $fr_gf_required_field_conditionaly_options=array();
    }

    foreach($fr_gf_required_field_conditionaly_options as $option){
        if($option["form_id"]==$form["id"]){
            foreach($form["fields"] as $field){
                $field->isRequired = $option["callback"]($field->isRequired,$field->name,$field->id,$field);
            }
        }
    }
    return $form;
}

add_filter( 'gform_pre_render', 'fr_gf_required_field_conditionaly_filter' );
add_filter( 'gform_pre_validation', 'fr_gf_required_field_conditionaly_filter' );
add_filter( 'gform_pre_submission_filter', 'fr_gf_required_field_conditionaly_filter' );




/**
 * Add terms as options to checkboxes and radiobuttons
 *
 * @param string $taxonomy Taxonomy name
 * @param string $default The default option. If set to false it won't have a default option
 * @param string/array $selected Pre-selected options
 * @param function $callback Function to post process the options
 */

function fr_gf_taxonomy_options($taxonomy, $default="", $selected=array(), $callback=""){
    fr_wp_init( function() use ( $taxonomy, $default, $selected, $callback ) {
        $tax_ob = get_taxonomy( $taxonomy );

        $terms = get_terms( array(
            "taxonomy"=>$taxonomy,
            "hide_empty"=>false,
            'parent' => 0
        ));
 
        $options = fr_taxonomy_options( $taxonomy, $tax_ob->hierarchical );

        if( $default === '' ){
            $default = __( "Select", "fr" );
        }
        if( $default !== false ){
            $options = array( '' => $default ) + $options;
        }

        if( is_callable( $callback ) ){
            $options = $callback( $options );
        }

        fr_gf_field_options($options,"tax_".$taxonomy,0,$selected);
    });
}



/**
 * Validate gravity fields easly
 *
 * @param string/array $fields One or more field ids or Parameter Names
 * @param array $forms Form ID
 * @param bolean $must_be_valid It will call the callback function only if the field is valid
 * @param string/function $callback Function callback to validate the field
 */

function fr_gf_field_validation($fields,$forms=array(),$callback = ''){
    global $fr_gf_field_validation;
    if(empty($fr_gf_field_validation)){
        $fr_gf_field_validation = array();
    }
    if(!is_array($fields)){
        $fields = array($fields);
    }
    if(!is_array($forms)){
        $forms = array($forms);
    }

    foreach( $forms as $form ){
        foreach($fields as $field){
            $fr_gf_field_validation[] = array(
                "field"=>$field,
                "form"=>$form,
                "callback"=>$callback
                );
        }
    }
}



function fr_gf_field_validation_filter($result, $value, $form, $field){
    global $fr_gf_field_validation;

    if(empty($result["is_valid"])){
        return $result;
    }

    if(empty($fr_gf_field_validation)){
        $fr_gf_field_validation = array();
    }
    $files = json_decode( rgpost('gform_uploaded_files'), 1 );

    foreach($fr_gf_field_validation as $data){
        if($data["field"]==$field->id || $data["field"]==$field->inputName){
            if(!$data["form"] || $form["id"]==$data["form"]){

                //add uploaded files to value
                if( isset( $files['input_'.$field->id] ) ){
                    $value = $files['input_'.$field->id];
                }

                if($issue=fr_call_if_func($data["callback"],array($field, $value, $form))){
                    $result['is_valid'] = false;
                    $result['message'] = $issue;
                }
            }
        }
    }
    return $result;
}


add_filter("gform_field_validation","fr_gf_field_validation_filter",10,4);






/**
 * Callback function for gravity forms submission
 *
 * @param int/array $form Form ID
 * @param string/function $callback Function callback to validate the field
 */

function fr_gf_on_submit($forms,$callback){
    global $fr_gf_on_submit;
    if(empty($fr_gf_on_submit)){
        $fr_gf_on_submit = array();
    }
    if( !is_array( $forms ) ){
        $forms = array( $forms );
    }

    foreach( $forms as $form ){
        $fr_gf_on_submit[] = array(
            "form"=>$form,
            "callback"=>$callback
            );
    }
}

function fr_gf_on_submit_filter($entry,$form){
    global $fr_gf_on_submit;

    if(empty($fr_gf_on_submit)){
        $fr_gf_on_submit = array();
    }
    foreach($fr_gf_on_submit as $data){
        if(!$data["form"] || $form["id"]==$data["form"]){
            fr_call_if_func($data["callback"],array($entry,$form));
        }
    }
    return $form;
}

add_action("gform_after_submission","fr_gf_on_submit_filter",10,2);




/**
 * Get field options. Usefull for getting value name from key
 * @param  int $form  
 * @param  int $field
 * @return array
 */
function fr_gf_get_field_options($form,$field){
    $field = fr_gf_get_field($form,$field);
    $options = array();
    if(isset($field["choices"])){
        foreach($field["choices"] as $v){
            $options[$v["value"]]=$v["text"];
        }
    }
    return $options;
}



/**
 * Get field data.
 * @param  int/array $form Can be gravity forms object or id
 * @param  int $field
 * @return array
 */
function fr_gf_get_field($form,$field){

    if( !class_exists( 'GFAPI' ) ){
        return false;
    } 

    if(is_numeric($form)){
        $form = GFAPI::get_form( $form );
    }
    if(isset($form["fields"])){
        foreach($form["fields"] as $f){
            if( !empty( $f->inputs ) ){
                foreach( $f->inputs as $input ){
                    if( $input['id'] == $field ){
                        $f->inputName = $input['name'];
                        return $f;
                    }
                }
            }else{
                if( $f->id == $field ){
                    return $f;
                }
            }
        }
    }
}



function fr_gf_field_value_from_name($value,$field,$name,$pid=0,$uid=0, $lang = '' ){

    if( fr_wpml() ){
        if( !$lang ){
            $lang = fr_wpml_current_lang();
        }

        if( strstr( $name, '_lang_' ) ){
            $t = explode( '_lang_', $name );
            $name = $t[0] . substr( $t[1], 2 );
            $lang = substr( $t[1], 0, 2 );
            $pids = fr_wpml_post_ids_in_all_languages( $pid );
            if( isset( $pids[$lang] ) ){
               $pid = $pids[$lang];
            }else{
                return '';
            }
        }
    }


    if(substr($name,0,5)=="user_"){
        if(substr($name,0,14)=="user_has_role_"){
            $value = fr_user_has_role(substr($name,14));
        } else {
            $value = fr_user(substr($name,5),$uid);
        }
    }
    if(substr($name,0,6)=="userm_"){
        $value = get_user_meta($uid,substr($name,6),1);
    }
    if(substr($name,0,8)=="acfuser_"){
        $value = get_field(substr($name,8),"user_".$uid);
    }
    if(substr($name,0,11)=="acfoptions_"){
        $value = get_field(substr($name,11),"options");
    }
    if(substr($name,0,4)=="edd_"){
        $value = fr_edd_get_customer_address( $uid, $name );
    }
    if(substr($name,0,8)=="options_"){
        $value = get_option(substr($name,8));
    }

    if( $pid ){
        if(substr($name,0,5)=="meta_"){
            $value = get_post_meta($pid,substr($name,5),1);
        }
        if(substr($name,0,4)=="acf_"){
            $value = get_field(substr($name,4),$pid);
        }
        if(substr($name,0,4)=="tax_"){
            $tax = substr($name,4);
            $terms = wp_get_post_terms($pid,$tax);
            $value = array();
            if($terms){
                foreach($terms as $term){
                    $value[] = $term->term_id;
                }
            }
            if(empty($field->choices)){
                $value = reset($value);
                if( fr_wpml() ){
                    $value = fr_wpml_get_id( $value, $tax, $lang );
                }
            }
        }
        if(substr($name,0,5)=="post_"){
            $value = get_post_field(substr($name,5),$pid);
        }
        if($name=="get_the_title"){
            $value = get_the_title($pid);
        }
        if($name=="get_the_content"){
            $value = fr_get_content($pid);
        }
        if($name=="get_the_excerpt"){
            $value = get_the_excerpt($pid);
        }
        if($name=="get_the_content_no_html"){
            $value = html_entity_decode( fr_strip_tags_keep_spacing( get_post_field( 'post_content', $pid ) ) );
        }
        if($name=="get_the_excerpt_no_html"){
            $value = html_entity_decode( fr_strip_tags_keep_spacing( get_post_field( 'post_excerpt', $pid ) ) );
        }
    }
    return $value;
}



function fr_gf_save_field_value_from_name( $value, $field, $name, $pid=0, $uid=0, $lang = '' ){
    
    if( substr( $name, 0, 5 ) == "user_" ){
        wp_update_user( [ 'ID' => $uid, substr( $name, 5 ) => $value ] );
    }
    if( substr( $name, 0, 6 ) == "userm_" ){
        $value = fr_um( substr( $name, 6 ), $value, $uid );
    }
    if(substr($name,0,8)=="acfuser_"){
        update_field(substr($name,8),$value,"user_".$uid);
    }
    if(substr($name,0,4)=="edd_"){

        $map = [
            'line1' => 'address',
            'line2' => 'address2',
            'zip' => 'postal_code',
            'state' => 'region'
        ];

        $key = substr( $name, 4 );
        
        if( isset( $map[$key] ) ){
            $key = $map[$key];
        }

        $customer = edd_get_customer_by( 'user_id', $uid );

        if( $customer ){
            $addresses = edd_get_customer_addresses( [ 'customer_id' => $customer->id ] );
            if( $addresses ){
               edd_update_customer_address( $addresses[0]->id, [
                    $key => $value
                ] );
            } else {
                edd_add_customer_address( [
                    'customer_id' => $customer->id,
                    $key => $value
                ] );
            }
        }
    }

    if( $pid ){
        if(substr($name,0,5)=="meta_"){
            $value = update_post_meta($pid,substr($name,5),$value);
        }
        if(substr($name,0,4)=="acf_"){
            $value = update_field(substr($name,4),$value,$pid);
        }
        if(substr($name,0,4)=="tax_"){
            $tax = substr($name,4);
            if( !is_array( $value ) ){
                $value = array( $value );
            }
            foreach( $value as $k => $v ){
                if( (int)$v && (int)$v == $v ){
                    $value[$k] = (int)$v;
                }
            }
            $terms = wp_set_post_terms($pid,$value,$tax);
        }
        if($name=="post_post_title"){
            wp_update_post( [ 
                'ID' => $pid,
                'post_title' => $value
            ]);
        }
        if($name=="post_post_content"){
            wp_update_post( [ 
                'ID' => $pid,
                'post_content' => $value
            ]);
        }
        if($name=="post_post_excerpt"){
            wp_update_post( [ 
                'ID' => $pid,
                'post_excerpt' => $value
            ]);
        }
    }
}


function fr_gf_entry_meta( $entry_id, $field_id, $value = false ){
    global $wpdb;
    if( $value !== false ){
        $data = $wpdb->get_results( $wpdb->prepare( 'SELECT * FROM ' . $wpdb->prefix . 'gf_entry_meta WHERE meta_key=%s AND entry_id=%s', $field_id, $entry_id ), ARRAY_A );
        $data = reset( $data );
        if( $data ){
            $wpdb->update( $wpdb->prefix . 'gf_entry_meta', array( 'meta_value' => $value ), array( 'meta_key' => $field_id, 'entry_id' => $entry_id ) );
        }else{
            $wpdb->insert( $wpdb->prefix . 'gf_entry_meta', array( 'meta_value' => $value, 'meta_key' => $field_id, 'entry_id' => $entry_id ) );

        }
    }else{
        return $wpdb->get_var( $wpdb->prepare( 'SELECT meta_value FROM ' . $wpdb->prefix . 'gf_entry_meta WHERE meta_key=%s AND entry_id=%s', $field_id, $entry_id ) );
    }
    return false;
}

function fr_gf_get_entry( $entry_id ){
    return GFAPI::get_entry( $entry_id );
}


function fr_gf_update_entry_field( $entry_id, $field, $value ){
    global $wpdb;
    $wpdb->update( $wpdb->prefix . 'gf_entry', array( $field => $value ), array( 'id' => $entry_id ) );
}


function fr_gf_display_images( $form_id_target, $field_id_target, $images, $image_size = 'thumbnail' ){
    add_filter( 'gform_field_content', function( $content, $field, $value, $lead_id, $form_id ) use ( $form_id_target, $field_id_target, $images, $image_size ) {
        if( $form_id_target == $form_id && $field_id_target == $field->id ){
            
            $buttons = apply_filters( 'fr_gf_display_images_btns', [
                'delete' => [ 
                    'content' => 'x',
                    'title' => __( 'Remove the image', 'framework' )
                ]
            ], $form_id, $field );

            ob_start();
            ?>
            <div class="fr_gf_images">
                <?php foreach( $images as $image ){
                    if( !$image ){ continue; }
                    $file_name = basename( fr_wp_file( $image ) );
                    ?>
                    <div class="fr_gf_image" data-id="<?php echo $image ?>" data-name="<?php echo esc_attr( $file_name ) ?>">
                        <div class="fr_buttons">
                            <?php foreach( $buttons as $name => $attr ){ 
                                ?><span title="<?php echo esc_attr( $attr['title'] ) ?>" class="fr_button <?php echo $name ?>"><?php echo $attr['content'] ?></span><?php 
                            } ?>
                        </div>
                        <?php echo fr_img( $image, $image_size, false, apply_filters( 'fr_display_image_attr', [], $image, $form_id, $field ) ); ?>
                        <span class="fr_name"><?php echo esc_html( $file_name ) ?></span>
                    </div>
                <?php } ?>
                <input type="hidden" name="fr_gf_images_<?php echo $field_id_target ?>" value="<?php echo implode( ',', $images ) ?>">
            </div>
            <?php 
            $content .= ob_get_clean();
        }
        return $content;
    }, 11, 5 );
}


function fr_gf_change_field_html( $field_types, $callback ){
    if( !is_array( $field_types ) ){
        $field_types = array( $field_types );
    }
    add_filter( 'gform_field_content', function( $content, $field, $value, $lead_id, $form_id ) use ( $field_types, $callback ) {
        if( $field_types && in_array( $field->type, $field_types ) ){
           
            ob_start();
            $answer = fr_call_if_func( $callback, array( $field, $form_id, $value, $content ) );
            if( $answer === false ){
                return $content;
            }
            if( $answer ){
                return $answer;
            }
            $answer = ob_get_clean();


            ob_start();
                ?>
                <label class="gfield_label" for="input_<?php echo $form_id ?>_<?php echo $field->id ?>">
                    <?php echo $field->label ?>
                    <?php if( $field->isRequired ){ ?>
                        <span class="gfield_required">*</span>
                    <?php } ?>
                </label>
                <div class="ginput_container ginput_container_<?php echo $field->type ?>">
                    <?php if( $field->maxFileSize ){ ?>
                        <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo fr_mb_to_bytes( $field->maxFileSize ) ?>">
                    <?php } ?>

                    <?php 
                    echo $answer;
                    ?>
                </div>
                <?php if( !empty( $field->validation_message ) ){ ?>
                    <div class="validation_message"><?php echo $field->validation_message; ?></div>
                <?php } ?>
                <?php if( $field->description ){ ?>
                    <div class="gfield_description"><?php echo $field->description ?></div>
                <?php } ?>
                <?php 
            $content = ob_get_clean();
        }
        return $content;
    }, 10, 5 );
}


function fr_gf_countries(){
    $c =fr_data_countries();
    foreach( $c as $k => $v ){
        $c[$k] = $v . '|' . $k;
    }
    return implode( "\n", $c );
}



function fr_gf_custom_select(){
    // Custom Select Field
    fr_gf_change_field_html( array( 'select', 'multiselect' ), function( $field, $form_id, $selected  ){
        $options = [];

        if( empty( $selected ) ){
            $selected = [];
            foreach( $field->choices as $choice ){
                if( $choice['isSelected'] ){
                    $selected[] = $choice['value'];
                }
            }
        }

        foreach( $field->choices as $option ){
            $options[$option['value']] = $option['text'];
        }
        fr_custom_select( 'input_' . $field->id, $options, $selected, $field->placeholder, $field->type == 'multiselect' );
    });
}


function fr_gf_field_acf_options( $gf_field_name, $form_id, $acf_selector, $acf_post_id ){
    $acf_field = fr_acf_get_field_object( $acf_selector, $acf_post_id );
    $gf_field = fr_gf_get_field( $form_id, $gf_field_name );
    $options = $acf_field['choices'];
    $uid = 0;
    $pid = 0;
    if( substr( $acf_post_id, 0, 5 ) == 'user_' ){
        $uid = substr( $acf_post_id, 5 );
    }else{
        $pid = $acf_post_id;
    }
    $selected = fr_gf_field_value_from_name( array(), $gf_field, $gf_field_name, $pid, $uid );
    fr_gf_field_options($options,$gf_field_name,$form_id,$selected );
}


function fr_gf_get_uploaded_file_path( $field_id, $data = [] ){
    $file = '';
    if( !empty( $data[$field_id] ) ){
        $file = fr_url_to_path( $data[$field_id] );
    }else {
        $input = 'input_' . $field_id;
        if( !empty( $_FILES[$input]["tmp_name"] ) ){
            $file = $_FILES[$input]["tmp_name"];
        }
    }
    return $file;
}

function fr_gf_get_temp_file_path( $file, $form_id ){
    $form_upload_path = GFFormsModel::get_upload_path( $form_id );
    if( is_array( $file ) ){
        $file = $file['temp_filename'];
    }
    return $form_upload_path . "/tmp/" . $file;
}

function fr_gf_remove_inline_scripts(){
    add_filter("gform_init_scripts_footer", "__return_false");
    add_filter("gform_get_form_filter", function( $form_string, $form ) {
      return $form_string = preg_replace( '#<script(.*?)>(.*?)</script>#is', '', $form_string );
    }, 10, 2);
}

function fr_gf_scripts_in_footer(){
    add_filter("gform_init_scripts_footer", "__return_true");
}

/**
 * Load Gravity Forms inline script after page load
 */
function fr_gf_init_inline_scripts_on_load(){
    add_filter( 'gform_cdata_open', function( $content = '' ) {
        if( !fr_is_ajax() ){
            $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
        }
        return $content;
    } );
    
    add_filter( 'gform_cdata_close', function( $content = '' ) {
        if( !fr_is_ajax() ){
            $content = ' }, false );';
        }
        return $content;
    } );
}


function fr_gf_form_submitted( $id ){
    return isset( $_POST['gform_submit'] ) && $_POST['gform_submit'] == $id;
}


function fr_gf_get_upload_form_directory( $form_id ){
    $res = fr_gf_get_upload_form_location( $form_id );
    return $res['path'];
}

function fr_gf_get_upload_form_url( $form_id ){
    $res = fr_gf_get_upload_form_location( $form_id );
    return $res['url'];
}

function fr_gf_get_upload_form_location( $form_id ){
    $time = current_time( 'mysql' );
    $y                = substr( $time, 0, 4 );
    $m                = substr( $time, 5, 2 );
    $target_root      = GFFormsModel::get_upload_path( $form_id ) . "/$y/$m/";
    $target_root_url  = GFFormsModel::get_upload_url( $form_id ) . "/$y/$m/";
    $upload_root_info = array( 'path' => $target_root, 'url' => $target_root_url );
    $upload_root_info = gf_apply_filters( 'gform_upload_path', $form_id, $upload_root_info, $form_id );
    return $upload_root_info;
}


function fr_gf_get_entry_by_field( $form_id, $key, $value ){
    $res = GFAPI::get_entries( $form_id, [ 
        'field_filters' => [
            [
                'key' => $key,
                'value' => $value
            ]
        ]
    ], [ 
        'key' => 'id', 
        'direction' => 'DESC', 
        'is_numeric' => true
    ] );

    return reset( $res );
}




function fr_gf_disable_spam_check( $forms = [] ){
    $forms = fr_make_array( $forms );
    add_filter( 'gform_entry_is_spam', function( $is_spam, $form ) use ( $forms ){
        if( !$forms || in_array( $form['id'], $forms ) ){
            $is_spam = false;
        }
        return $is_spam;
    }, 10, 2 );
}


