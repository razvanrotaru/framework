<?php
function fr_shortpixel_optimize_images(){
	$file = ABSPATH . 'wp-content/plugins/shortpixel-image-optimiser/wp-shortpixel-req.php';
	if( file_exists( $file ) && class_exists( 'WPShortPixel' ) ){
		require_once( $file );
	    $shortPixelPluginInstance = new WPShortPixel;
		$_POST["bulkProcess"] = 1;
		$shortPixelPluginInstance->bulkProcess();
	}
}