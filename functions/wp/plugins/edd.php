<?php


fr_on_order_meta_update_clear_cache();


function fr_on_order_meta_update_clear_cache(): void {

    add_action( 'updated_edd_order_meta', function(){
        fr_cache_clear( 'fr_query_get_edd_ordermeta_meta' );
    } );

}


function fr_edd_add_to_cart_link( $pid, $price_id = false ){
	$attr = array(
		'edd_action' => 'add_to_cart',
		'download_id' => $pid
	);
	if( $price_id ){
		$attr['edd_options'] = array(
			'price_id' => $price_id
		);
	}
	return edd_get_checkout_uri() . '?' . http_build_query( $attr );
}

function fr_edd_add_to_cart( $download, $variation = 0, $qty = 1, $options = [] ){
	$default_options = [
		'quantity' => $qty
	];
	if( $default_options ){
		$default_options['price_id'] = $variation;
	}
	$options = array_merge( $default_options, $options );

	edd_add_to_cart( $download, $options );
}

function fr_edd_item_download_count( $pid ) {
	global $edd_logs;
	return $edd_logs->get_log_count( $pid, 'file_download', array(
		'relation'	=> 'AND',
		array(
			'key' 	=> '_edd_log_file_id'
		),
		array(
			'key' 	=> '_edd_log_payment_id'
		)
	) );
}


function fr_edd_add_checkout_field( $name, $default_value, $label, $required = false, $options = false, $description = false, $valdiation_callback = false, $field_callback = false, $class = '' ){

	$label_as_placeholder = apply_filters( 'fr_edd_add_checkout_field_placeholder', true );

	if( !$field_callback ){
		add_action( 'edd_purchase_form_user_info_fields', function() use ( $name, $default_value, $label, $options, $description, $required, $label_as_placeholder, $class ) {
			$type = 'text';
			if( strstr( $name, 'password' ) ){
				$type = 'password';
			}
			$placeholder = '';
			if( $label_as_placeholder ){
				$placeholder = $label;
			}
			if( $placeholder && $required ){
				$placeholder .= '*';
			}
			if( $required ){
				$class .= ' required_field';
			}
			?>
		    <p id="edd-<?php echo $name ?>-wrap" class="<?php echo $class ?>">
		        <label class="edd-label" for="edd-<?php echo $name ?>"><span class="name"><?php echo $label ?></span> <?php if( $required ){ echo '<span class="required">*</span>'; } ?></label>
		        <?php if( $description ){ ?>
			        <span class="edd-description">
			        	<?php echo esc_html( $description ) ?>
			        </span>
			    <?php } ?>

			    <span class="edd-field">
				    <?php if( !$options ){ ?>
			        	<input class="edd-input" type="<?php echo $type ?>" name="<?php echo $name ?>" id="edd-<?php echo $name ?>" placeholder="<?php echo esc_attr( $placeholder ) ?>" value="<?php echo $default_value ?>" />
			        <?php } else { ?>
			        	<?php
			        	if( $placeholder ){
				        	if( isset( $options[''] ) && empty( $options[''] ) ){
				        		$options[''] = $placeholder;
				        	}
							if( $required ){
								$options[''] .= ' *';
							}
						}

						ob_start();
			        	?>
						<select name="<?php echo $name ?>" id="<?php echo $name ?>" class="<?php echo $name ?> edd-select<?php if( $required ) { echo ' required'; } ?>">
							<?php
								foreach( $options as $option_key => $option_value ) {
									echo '<option value="' . $option_key . '"' . selected( $option_key, $default_value, false ) . '>' . $option_value . '</option>';
								}
							?>
						</select>
			        	<?php 
			       		$c = ob_get_clean();
			       		$c = apply_filters( 'fr_edd_add_checkout_field_select_html', $c, $name, $required, $options, $default_value );
			       		echo $c;
			    	}
			    	?>
		        </span>
		    </p>
		    <?php
		} );
	}else{
		add_action( 'edd_purchase_form_user_info_fields', $field_callback );
	}

	if( $required ){
		add_filter( 'edd_purchase_form_required_fields', function( $required_fields ) use ( $name, $label ) {
			$required_fields[$name] = array(
		        'error_id' => 'invalid_' . $name,
		        'error_message' => sprintf( __( '%s is required', 'fr' ), $label )
		    );
		    return $required_fields;
		} );
	}


	add_action( 'edd_checkout_error_checks', function( $valid_data, $data ) use ( $name, $label, $description, $required, $valdiation_callback ) {
	    
	    if ( $required && empty( $data[$name] ) ) {
	        edd_set_error( 'invalid_' . $name, sprintf( __( '%s is required', 'fr' ), $label ) );
	    }
	    fr_call_if_func( $valdiation_callback, array( $valid_data, $data, $label, $description, $name, $required ) );
	    
	}, 10, 2 );


	add_filter( 'edd_payment_meta', function( $payment_meta, $payment_data ) use ( $name ) {

		if( $payment_data['status'] == 'refunded' ){
			return $payment_meta;
		}

		if( !strstr( $name, 'password' ) && apply_filters( 'fr_edd_add_to_payment_meta', true, $name ) && function_exists( 'edd_get_order_item' ) ){
			$item = edd_get_order_item( $payment_data['cart_details'][0]['order_item_id'] );
			edd_update_payment_meta( $item->order_id, $name, sanitize_text_field( $_POST[$name] ?? '' ) );
		}

		return $payment_meta;

	}, 10, 2 );


	if( substr( $name, 0, 5 ) != 'card_' ){
		add_action( 'edd_payment_personal_details_list', function( $payment_meta, $user_info ) use ( $name, $label ) {
			$value = isset( $payment_meta[$name] ) ? $payment_meta[$name] : 'none';
			if( !apply_filters( 'fr_edd_add_to_payment_meta', true, $name ) ){
				return;
			}
			?>
		    <div class="column-container">
		    	<div class="column">
		    		<strong><?php echo $label ?>: </strong>
		    		 <?php echo $value; ?>
		    	</div>
		    </div>
			<?php
		}, 10, 2 );
	}


	add_action( 'edd_add_email_tags', function() use ( $name, $label ) {
		edd_add_email_tag( $name, $label, function( $payment_id ) use ( $name ) {
			$payment_data = edd_get_payment_meta( $payment_id );
			return $payment_data[$name];
		} );
	} );
}

function fr_edd_get_variations( $pid ){
	$options = fr_m( 'edd_variable_prices', $pid );
	return $options;
}

function fr_edd_get_variation( $pid, $price_id ){
	$options = fr_edd_get_variations( $pid );
	if( isset( $options[$price_id] ) ){
		return $options[$price_id];
	}
}

function fr_edd_update_variations( $pid, $val ){
	return fr_m( 'edd_variable_prices', $val, $pid );
}

function fr_edd_get_customer_downloads( $uid, $return_ids = false ){
	$customer = new EDD_Customer( $uid, true );
	$payments = $customer->get_payments();
	$all = array();

	foreach( $payments as $payment ){
		if( $payment->post_status != 'publish' ){
			continue;
		}
		$downloads = edd_get_payment_meta_downloads( $payment->ID );
		foreach( $downloads as $k => $v ){
			$downloads[$k]['payment_id'] = $payment->ID;
			$downloads[$k]['payment_date'] = $payment->date;
		}
		$all = array_merge( $all, $downloads );
	}

	if( $return_ids ){
		$all = array_column( $all, 'id' );
	}

	return $all;
}


function fr_edd_download_file( $pid ){
	$data = fr_m( 'edd_download_files', $pid );
	$data = reset( $data );
	if( !empty( $data['attachment_id'] ) ){
		return $data['attachment_id'];
	}
}

function fr_edd_withdrawals( $uid ){
	global $wpdb;
	$res = $wpdb->get_results( $wpdb->prepare( 'SELECT *,SUM( amount ) as amount, COUNT( id ) as count,status,date_paid FROM ' . $wpdb->prefix . 'edd_commissions WHERE user_id=%d GROUP BY date_paid,status ORDER BY status DESC,date_paid DESC', $uid ), ARRAY_A );
	return $res;
}

function fr_edd_get_payment_downloads( $payment_id ){
	return edd_get_payment_meta_downloads( $payment_id );
}

function fr_edd_get_customer_id( $uid ){
	global $wpdb;
	return $wpdb->get_var( $wpdb->prepare( 'SELECT id FROM ' . $wpdb->prefix . 'edd_customers WHERE user_id=%d LIMIT 0,1', $uid ) );
}

function fr_edd_get_active_subscriptions( $uid ){

	global $wpdb;

	$customer = fr_edd_get_customer_id( $uid );

	if( !$customer ){
		return [];
	}

	$result = $wpdb->get_results( $wpdb->prepare( 'SELECT id FROM ' . $wpdb->prefix . 'edd_subscriptions WHERE customer_id=%d AND expiration > NOW() AND status = "active"', $customer ), ARRAY_A );
	$result = array_column( $result, 'id' );
	return $result;
}

function fr_edd_get_subscription_payment( $subscription_id ){
	global $wpdb;
	return $wpdb->get_var( $wpdb->prepare( 'SELECT parent_payment_id FROM ' . $wpdb->prefix . 'edd_subscriptions WHERE id=%d', $subscription_id ) );
}



function fr_edd_get_last_subscription( $customer_id ){
	global $wpdb;
	return $wpdb->get_var( $wpdb->prepare( 'SELECT id FROM ' . $wpdb->prefix . 'edd_subscriptions WHERE customer_id=%d', $customer_id ) );
}


function fr_edd_get_subscribers( $callback = false ){
	global $wpdb;
	$query = new fr_wpdb_query( 'contributor', 'edd_subscriptions', 'GROUP BY contributor' );
	$query->where( 'expiration > NOW() AND status = "active"' );
	fr_call_if_func( $callback, [ $query ] );
	$subscribers = $query->get_col( 'contributor' );
	return $subscribers;
}

function fr_edd_subscription( $id, $key = false ){
	global $wpdb;
	$result = $wpdb->get_results( $wpdb->prepare( 'SELECT * FROM ' . $wpdb->prefix . 'edd_subscriptions WHERE id=%d', $id ), ARRAY_A );
	$result = reset( $result );

	if( $key && isset( $result[$key] ) ){
		$result = $result[$key];
	}

	return $result;
}

function fr_edd_subscription_change_status( $id, $status ){
	global $wpdb;
	$wpdb->update( $wpdb->prefix . 'edd_subscriptions', [ 'status' => $status ], [ 'id' => $id ] );
}


function fr_edd_clear_cart(){

	$cart = edd_get_cart_contents();

	foreach( $cart as $cart_key => $item ){
		edd_remove_from_cart( $cart_key );
	}

	foreach( EDD()->fees->get_fees() as $fee_key => $fee ){
		EDD()->fees->remove_fee( $fee_key );
	}

}



function fr_edd_get_orders_fields( array $ids, $field_keys = [], bool $return_only_first_key = false ): array {
    return fr_query_get_table_fields( 'edd_orders', 'id', $ids, $field_keys = [], $return_only_first_key = false );
}



/**
 * Function that makes common queries way faster than native function
 * Supports: parent, order_number, status, type, user_id, customer_id, meta_query, number
 */
function fr_edd_get_orders( array $query ): array {

	global $wpdb;

	$query = array_merge( [
		'orderby' => 'date_created',
		'order' => 'DESC',
	],$query );

	$query_string = 'SELECT ' . ( isset( $query['fields'] ) ? 'id' : '*' ) . ' FROM ' . $wpdb->prefix . 'edd_orders';
	$where = [];

	foreach( [
		'parent',
		'order_number',
		'status',
		'type',
		'user_id',
		'customer_id'
	] as $key ){
		if( !empty( $query[$key] ) ){
			$values = fr_make_array( $query[$key] );
			$values = array_map( 'esc_sql', $values );
			$where[] = $key . ' IN ( "' . implode( '","', $values ) . '" )';
		}
	}


	if( !empty( $query['meta_query'] ) ){
		$meta_query = $query['meta_query'];
		$meta_query = fr_make_array( $meta_query );
		foreach( $meta_query as $sub_query ){

			$q = $wpdb->prepare( 'meta_key = %s', $sub_query['key'] );

			if( isset( $sub_query['value'] ) ){
				$value = fr_make_array( $sub_query['value'] );
				$value = array_map( 'esc_sql', $value );
				$q .= ' AND meta_value IN ( "' . implode( '","', $value ) . '" )';
			}

			$where[] = 'id IN ( SELECT edd_order_id FROM ' . $wpdb->prefix . 'edd_ordermeta WHERE ' . $q . ' )';
		}
	}


	if( !empty( $query['date_query'] ) ){

		$date_query = $query['date_query'];
		$date_query = fr_make_array( $date_query );
		
		foreach( $date_query as $sub_query ){

			if( !empty( $sub_query['after'] ) ){
				$where[] = 'date_created > "' . esc_sql( $sub_query['after'] ) . '"';
			}
			if( !empty( $sub_query['before'] ) ){
				$where[] = 'date_created < "' . esc_sql( $sub_query['before'] ) . '"';
			}
		}
	}


	if( !empty( $where ) ){
		$query_string .= ' WHERE ' . implode( ' AND ', $where );
	}

	if( $query['orderby'] != 'none' ){
		$query_string .= ' ORDER BY `' . esc_sql( $query['orderby'] ) . '`';

		if( !empty( $query['order'] ) ){
			$query_string .= ' ' . esc_sql( $query['order'] );
		}
	}

	if( !empty( $query['number'] ) ){
		$query_string .= ' LIMIT 0, ' . $query['number'];
	}

	if( isset( $query['fields'] ) ){
		$result = $wpdb->get_col( $query_string );
	} else {
		$result = $wpdb->get_results( $query_string, ARRAY_A );
	}

	return $result;

}



/**
 * Update multiple meta values for one order
 * @param  int    $order_id 
 * @param  array  $metas    	An array of meta key => value pairs
 * @param  array  $old_metas    An optional old array of meta key => value pairs. Used for performance
 * @return void
 */
function fr_edd_update_order_metas( int $order_id, array $metas, $old_metas = false ):bool {

	if( !$metas ){
		return false;
	}

	if( $old_metas === false ){
		$old_metas = fr_edd_get_order_meta( $order_id, array_keys( $metas ), false, false );
	}

	$saved = false;

	foreach( $metas as $key => $value ){

		$value = is_array( $value ) || is_object( $value ) ? $value : (string)$value;

		if( isset( $old_metas[$key] ) && $old_metas[$key] === $value ){
			continue;
		}

		edd_update_order_meta( $order_id, $key, $value );

		$saved = true;
	}

	return $saved;

}


/**
 * Get meta information for one order
 * @param  int     $order_id              
 * @param  array   $keys                  An array of keys
 * @param  boolean $return_only_first_key If true returns only first key
 * @param  boolean $add_default           If true adds by default an empty value for each meta_key
 * @param  boolean $cache                 If true it caches it can return a cached result
 */
function fr_edd_get_order_meta( int $order_id, $keys = [], bool $return_only_first_key = false, bool $add_default = true ) {

	$order_id = $order_id ?: 0;
	$res = fr_query_get_meta( 'edd_ordermeta', 'edd_order_id', [ $order_id ], $keys, $return_only_first_key, $add_default );

	return reset( $res );

}


/**
 * Get meta information for multiple orders
 * @param  array   $order_ids             An array of order IDs
 * @param  array   $keys                  An array of keys
 * @param  boolean $return_only_first_key If true returns only first key
 * @param  boolean $add_default           If true adds by default an empty value for each meta_key
 * @param  boolean $cache                 If true it caches it can return a cached result
 * @return array                          An multidimensional array of meta information for each id
 */
function fr_edd_get_orders_meta( array $order_ids, $keys = [], bool $return_only_first_key = false, bool $add_default = true, bool $cache = true ): array {
	return fr_query_get_meta( 'edd_ordermeta', 'edd_order_id', $order_ids, $keys, $return_only_first_key, $add_default, $cache );
}


/**
 * Get meta information for one order item
 */
function fr_edd_get_order_item_meta( int $order_item_id, $keys = [], bool $return_only_first_key = false, bool $add_default = true ) {

	$order_item_id = $order_item_id ?: 0;
	$res = fr_query_get_meta( 'edd_order_itemmeta', 'edd_order_item_id', [ $order_item_id ], $keys, $return_only_first_key, $add_default );

	return reset( $res );

}


/**
 * Get meta information for multiple order items
 */
function fr_edd_get_order_items_meta( array $order_item_ids, $keys = [], bool $return_only_first_key = false, bool $add_default = true, bool $cache = true ): array {
	return fr_query_get_meta( 'edd_order_itemmeta', 'edd_order_item_id', $order_item_ids, $keys, $return_only_first_key, $add_default, $cache );
}






function fr_edd_create_download( $data ){

	$data = array_merge( [
		'post_type' => 'download',
		'post_status' => 'publish'
	], $data );

	$download = wp_insert_post( $data );

	if( !is_wp_error( $download ) ){
		fr_m( '_edd_download_earnings', '0.00', $download );
		fr_m( '_edd_download_sales', '0', $download );
		fr_m( 'edd_price', '0.00', $download );
		fr_m( 'edd_variable_prices', [], $download );
		fr_m( '_edd_bundled_products', [ 0 ], $download );
		fr_m( '_edd_bundled_products_conditions', [ 1 => 'all' ], $download );
		fr_m( '_variable_pricing', '1', $download );
		fr_m( '_edd_default_price_id', '1', $download );
		return $download;
	}
}


function fr_edd_stripe_get_secret_key(){
	$settings = get_option( 'edd_settings' );
	if( $settings ){
		if( !empty( $settings['test_mode'] ) ){
			$key = $settings['test_secret_key'];
		} else {
			$key = $settings['live_secret_key'];
		}
		return $key;
	}
}

function fr_edd_settings( $key = '' ){
	$settings = get_option( 'edd_settings' );
	if( $key ){
		if( isset( $settings[$key] ) ){
			return $settings[$key];
		}
	} else {
		return $settings;
	}
}


function fr_edd_cart_cookie_expiration( $time ) {
	add_action( 'init', function() use ( $time ){

		if( headers_sent() ){
			return;
		}

	    if ( isset( $_COOKIE['edd_items_in_cart'] ) ) {
	        $items = $_COOKIE['edd_items_in_cart'];
	        @setcookie( 'edd_items_in_cart', $items, time() + $time, COOKIEPATH, COOKIE_DOMAIN, false );
	    } else {
	    	if( !function_exists( 'edd_get_cart_contents' ) ){
	    		return;
	    	}
	    	
		    $cart = edd_get_cart_contents();
	    	if( $cart !== false ){
		        $items = count( $cart );
		        @setcookie( 'edd_items_in_cart', $items, time() + $time, COOKIEPATH, COOKIE_DOMAIN, false );
		    }
	    }
	} );
}






/**
 * Created this function because for some reason the EDD function doesn't return anything
 */
function fr_edd_get_customer_address( $uid = false, $name = false ){


    $uid = $uid === false ? fr_user() : $uid;
    $customer = edd_get_customer_by( 'user_id', $uid );

    if( $customer ){
        $addresses = edd_get_customer_addresses( [ 'customer_id' => $customer->id ] );

        if( $addresses ){

            $address = [
                'id' => $addresses[0]->id,
                'line1' => $addresses[0]->address,
                'line2' => $addresses[0]->address2,
                'city' => $addresses[0]->city,
                'state' => $addresses[0]->region,
                'country' => $addresses[0]->country,
                'zip' => $addresses[0]->postal_code,
            ];

            if( $name === false ){
                return $address;
            }

            $key = substr( $name,4 );

            if( isset( $address[$key] ) ){
                return $address[$key];
            }
        }
    }

    return $name ? '' : [];
}







function fr_edd_get_total_results( array $query ): int {
	return edd_get_orders( array_merge( $query, [ 'count' => true ] ) );
}




function fr_edd_get_order_discounts( int $order_id ): array {
	
	global $wpdb;
	$discounts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}edd_order_adjustments WHERE object_id = %d AND object_type = 'order' AND type = 'discount'", $order_id ), ARRAY_A );
	return $discounts;

}



function fr_edd_get_order_discount( int $order_id ): array {

	$discounts = fr_edd_get_order_discounts( $order_id );

	if( !$discounts ){
		return [];
	}

	return reset( $discounts );

}


