<?php
/**
 * Converts an HTML string with tag attributes to array
 * @param  string $attr HTML string of attributes
 * @return array        Associative array
 * v2.0 Added support for values with special characters
 */

function fr_html_wp_attr_to_array( $attr ): array {

    if( is_array( $attr ) ){
        return $attr;
    }

    // Remove windows \r char, we don't need it
    $attr = str_replace( "\r", '', $attr );

    $array = [];

    $x = 0;

    while( 1 ){

        $x++;

        // Make sure there is no bug to do an infinite loop
        if( $x > 100 ){
            fr_log( 'fr_html_wp_attr_to_array() looped more than 100 times', $attr, true );
            break;
        }

        // Get rid of all the white spaces, we need the first key
        $attr = trim( $attr );

        // If no more characters then we reached the end
        if( !mb_strlen( $attr ) ){
            break;
        }

        // Check which char is first
        $char = fr_strstr_first_match( $attr, [ ' ', "\t", "\n", '=', "'", '"' ] );

        // If no match then it means there is only a key without value, we reached the end
        if( $char === false ){
            $array[$attr] = '';
            break;
        }

        // Extract the key
        $pos = mb_strpos( $attr, $char );
        $attr_key = mb_substr( $attr, 0, $pos );
        $attr_before_trim = mb_substr( $attr, mb_strlen( $attr_key ) );

        // Trim the rest of the string to get to the equal sign
        $attr = trim( $attr_before_trim );
        $first_char = $attr[0];

        // If the first char after key is a quote char then it means the equal sign is missing, let's add it
        if( $first_char == '"' || $first_char == "'" ){
            $attr = '=' . $attr;
            $first_char = $attr[0];
        }

        // If first char after key is not "=" then it means it's a key without value
        if( $first_char !== '=' ){
            $array[$attr_key] = '';
            continue;
        }

        // Remove the equal char from the rest of the string
        $attr = mb_substr( $attr, 1 );
        $attr = trim( $attr );
        $first_char = $attr[0];

        
        if( $first_char == '"' ){ // Double quotes value wrapping

            $attr = mb_substr( $attr, 1 );
            $pos = mb_strpos( $attr, '"' );

            // If second quote is missing it means it wasn't closed properly. Set the rest of the string as the value and end.
            if( $pos === false ){
                $array[$attr_key] = $attr;
                break;
            }

            $array[$attr_key] = mb_substr( $attr, 0, $pos );
            $attr = mb_substr( $attr, $pos + 1 );

        } else if( $first_char == "'" ){ // Single quotes value wrapping

            $attr = mb_substr( $attr, 1 );
            $pos = mb_strpos( $attr, "'" );

            // If second quote is missing it means it wasn't closed properly. Set the rest of the string as the value and end.
            if( $pos === false ){
                $array[$attr_key] = $attr;
                break;
            }

            $array[$attr_key] = mb_substr( $attr, 0, $pos );
            $attr = mb_substr( $attr, $pos + 1 );

        } else { // No quotes value wrapping

            $char = fr_strstr_first_match( $attr, [ ' ', "\t", "\n" ] );

            // If no white space character is present it means we reached the end
            if( $char === false ){
                $array[$attr_key] = $attr;
                break;
            }

            $pos = mb_strpos( $attr, $char );
            $array[$attr_key] = mb_substr( $attr, 0, $pos );
            $attr = mb_substr( $attr, $pos );

        }

    }


    return $array;
}



/**
 * Function that combines two strings or arrays of HTML attributes
 * Supports classes to have an array of values
 * @param  array $new     New HTML attribute string or associative array
 * @param  array $default Default HTML attribute string or associative array
 * @return array          Associative array
 */
function fr_parse_default_html_attr( $new, $default ){

    $new = fr_html_wp_attr_to_array( $new );
    $default = fr_html_wp_attr_to_array( $default );
    $all = $default;


    foreach( $new as $attr => $value ){
        if( $attr == 'class' ){
            if( !is_array( $value ) ){
                $value = explode( ' ', $value );
            }
        }

        if( isset( $all[$attr] ) ){

            if( $attr == 'class' ){
                $all[$attr] = fr_make_array( $all[$attr] );
            }

            if( is_array( $all[$attr] ) ){
                $value = fr_make_array( $value );
                $all[$attr] = array_merge( $all[$attr], $value );
                $all[$attr] = array_unique( $all[$attr] );
            } else {
                $all[$attr] = $value;
            }
        } else {
            $all[$attr] = $value;
        }
    }

    return $all;
}