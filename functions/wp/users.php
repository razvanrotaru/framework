<?php 
function fr_role_exists($role){
	if( ! empty( $role ) ) {
    	return $GLOBALS['wp_roles']->is_role( $role );
  	}
  
  	return false;
}

function fr_user_all_roles() {

	global $wp_roles;
	return array_keys( $wp_roles->roles );
	
}


function fr_author( $post_id = 0 ) {

	if( !$post_id && ( is_page() || is_single() ) ) {
		$post_id = get_the_ID();
	}

	if( is_author() ) {
		$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
		
		if( !is_wp_error( $author ) && $author ) {
			return $author->ID;
		}
	}

	return get_post_field( 'post_author', $post_id );
	
}


function fr_user( $data = '', $uid = false, $acf = 0 ) {

	if( $uid == false ) {
		$uid = get_current_user_id();
	}



	if( !$uid ) {
		return false;
	}



	if( $data == 'meta' ) {

		$d = get_user_meta( $uid );

		foreach( $d as $k => $v ) {
			$d[$k] = reset( $v );
		}

		return $d;

	}
	


	if( $data && !$acf ) {

		$d = get_userdata( $uid );

		if( $data == 'all' ) {
			return $d;
		}

	}



	if( !$data ) {

		return $uid;

	} else if( $data == 'role' && isset( $d->roles ) ) {

		if( $d->roles ) {
			return reset( $d->roles );
		}

	} else if( isset( $d->$data ) ) {

		$r = $d->$data;
		
		if( $data == 'role' ) {
			$r = reset( $r );
		}

		return $r;

	} else {

		if( $acf ) {
			return get_field( $data, 'user_' . $uid );
		}

		if( $data == 'full_name' ) {
			$value = get_user_meta( $uid, 'first_name', 1 ) . ' ' . get_user_meta( $uid, 'last_name', 1 );
		} else {
			$value = get_user_meta( $uid, $data, 1 );
		}

		return $value;
	}
	
}


function fr_login_by_id( $user_id ) {

	$user = get_user_by( 'id', $user_id );

	if( $user ) {

		if( fr_user() ) {
			wp_logout();
		}

		wp_set_current_user( $user_id, $user->user_login );
		wp_set_auth_cookie( $user_id );
		do_action( 'wp_login', $user->user_login, $user );

	}

}
	


function fr_sync_user_login_across_sites($domains,$cookie_domain,$user_roles){
	global $fr_sulas;
	$fr_sulas=array();
	$fr_sulas["domains"]=$domains;
	$fr_sulas["domains"][]=get_bloginfo("url");
	$fr_sulas["cookie_domain"]=$cookie_domain;
	$fr_sulas["user_roles"]=$user_roles;


	//DELETE COOKIE ON LOGOUT
	add_action("wp_logout",function(){
		global $fr_sulas;
		if(!empty($_COOKIE['sulas'])){
			unset($_COOKIE['sulas']);
		    setcookie('sulas', null, -1, '/', ".".$fr_sulas["cookie_domain"]);
		}
	});

	//SET COOKIE WHEN USER LOGS IN
	add_action("wp_login",function($user_login){
		global $fr_sulas;
		$user=get_user_by("login",$user_login);
		if(array_intersect($user->roles,$fr_sulas["user_roles"])){
			$data=$settings["sync_user_login_across_sites"];
			$key=get_user_meta($user->ID,"account_access_key",1);
			if(!$key){
				$key=wp_generate_password();
				update_user_meta($user->ID,"account_access_key",$key);
			}
			setcookie("sulas", $key."|".get_bloginfo("url"), time() + 3600*24*365, '/', ".".$fr_sulas["cookie_domain"]);
		}
	});


	//AUTO LOGIN FROM COOKIE
	add_action("template_include",function($template){
		global $fr_sulas;

		//IF LOGGED IN AND COOKIE NOT SET, LOG THE USER OUT
		if(fr_user() && array_intersect(fr_user("roles"),$fr_sulas["user_roles"]) && empty($_COOKIE["sulas"])){
			if(get_user_meta(fr_user(),"account_access_key",1)){
				wp_logout();
				fr_clean_post_redirect();
			}
		};

		if(!empty($_GET["fr_sulas"])){
			$q=array(
				"meta_query"=>array(
					array(
						"key"=>"account_access_key",
						"value"=>$_GET["fr_sulas"]
						)
					),
				"number"=>1
				);
			$users=new WP_User_Query($q);
			if($users->results){
				$user=reset($users->results);
				if(array_intersect($user->roles,$fr_sulas["user_roles"])){
					$data=array(
						"user"=>$user,
						"meta"=>get_user_meta($user->ID)
						);
					echo json_encode($data);
					fr_exit();
				}
			}
		}

		//IF NOT LOGGED IN AND COOKIE SET, LOG THE USER IN
		if(!fr_user() && !empty($_COOKIE["sulas"])){
			list($key,$domain)=explode("|",$_COOKIE["sulas"]);
			$q=array(
				"meta_query"=>array(
					array(
						"key"=>"account_access_key",
						"value"=>$key
						)
					),
				"number"=>1
				);
			$users=new WP_User_Query($q);
			if($users->results){
				$user=reset($users->results); 
				if(!array_intersect($user->roles,$fr_sulas["user_roles"])){
					$user="";
				}
			}else{
				if(!empty($domain)){
					if(in_array($domain,$fr_sulas["domains"])){
						$res=file_get_contents($domain."?fr_sulas=".urlencode($key));
						$res=json_decode($res,1);
						if($res && array_intersect($res["user"]["roles"],$fr_sulas["user_roles"])){
							$user=get_user_by("email",$res["user"]["data"]["user_email"]);
							if(!$user){
								$uid=wp_create_user($res["user"]["data"]["user_login"],$res["user"]["data"]["user_password"],$res["user"]["data"]["user_email"]);
								if(is_int($uid)){
									$user=get_user_by("id",$uid);
									$userdata = array(
										"ID"=>$user->ID,
										"first_name"=>$res["meta"]["first_name"][0],
										"last_name"=>$res["meta"]["last_name"][0],
										"role"=>$res["user"]["roles"][0],
										);
									wp_update_user( $userdata );

									global $wpdb;
									$wpdb->update($wpdb->prefix."users",array("user_password"=>$res["user"]["data"]["user_password"]),array("ID"=>$user->ID));
								}
							}
						}
					}
				}
			}

			if($user){
				update_user_meta($user->ID,"account_access_key",$key);
				fr_login_by_id($user->ID);
				fr_clean_post_redirect();
			}
		}


		return $template;
	});
}


/**
 * Returns if current user has a specific role
 *
 * @param string $role Role name
 * @param int $uid User ID
 * @return bolean
 */

function fr_user_has_role( $user_roles, $uid = 0 ){
	if( !$uid ){
		$uid = fr_user();
	}
	$user_roles = fr_make_array( $user_roles );
	if( $uid ){
		$roles = fr_user( 'roles', $uid );
		if( $roles ){
			return (bool)array_intersect( $user_roles, $roles );
		}
	}
	return false;
}


function fr_user_can( $option ){
	return user_can( fr_user(), $option );
}


function fr_admin_email(){
	return get_option( 'admin_email' );
}

function fr_user_admin( $uid = 0 ){
	if( !$uid ){
		$uid = fr_user();
	}
	return user_can( $uid, 'manage_options' );
}


function fr_user_link( $uid = 0 ){

	if( !$uid ){
		$uid = fr_user();
	}

	if( !$uid ){
		return false;
	}

	return get_author_posts_url( $uid );
}


function fr_user_generate_display_name( $uid, $display_name = false ){
	$email = fr_user( 'user_email', $uid );
	if( $display_name == false ){
		$display_name = strstr( $email, '@', 1 );
	}
	$display_name = str_replace( [ '_', '-', '.', '+' ], ' ', $display_name );
	$display_name = ucwords( $display_name );
	$display_name = fr_unique_name( $display_name, '', function( $name ) use ( $uid ) { 
		$users = new WP_User_Query( [
			'search' => $name,
   			'search_columns' => array( 'display_name' ),
   			'fields' => 'ids',
   			'number' => 1,
   			'exclude' => [ $uid ]
		] );
		return $users->get_results();
	} );

	return $display_name;
}

function fr_user_display_name_exists( $display_name, $exclude_users = [] ){
	$users = new WP_User_Query( [
			'search' => $display_name,
   			'search_columns' => array( 'display_name' ),
   			'fields' => 'ids',
   			'number' => 1,
   			'exclude' => $exclude_users
		] );
	return $users->get_results();
}


add_filter( 'user_search_columns', function( $search_columns ) {
    $search_columns[] = 'display_name';
    return $search_columns;
} );


function fr_change_author_slug( $slug ){
	add_action( 'init', function() use ( $slug ) {
	    global $wp_rewrite;
	    $wp_rewrite->author_base = $slug;
	    $wp_rewrite->author_structure = '/' . $wp_rewrite->author_base . '/%author%';
	} );
}

/**
 * Stop specific user roles from acccessing the administration
 * @param  array $denied_roles
 * @param  string $redirect_url
 */
function fr_user_roles_remove_admin_access( $denied_roles, $redirect_url = '' ){

	if( !fr_user() ){
		return;
	}
	if( !is_admin() || fr_is_ajax() ){
		return;
	}

	$allowed = array_diff( fr_user( "roles" ), $denied_roles );

	if( $allowed ){
		return;
	}

	if( !$redirect_url ){
		$redirect_url = get_bloginfo( 'url' );
	}
	
	// Delay the redirect to compensate for the GDPR plugin saving issue
	fr_add_actions( [ 'admin_enqueue_scripts', 'wp_die_handler' ], function() use ( $redirect_url ){
		fr_redirect( $redirect_url );
	}, 9999 );
}


function fr_user_clone_role( $role, $target_role, $label, $capabilities = [] ){
	global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $caps = $wp_roles->get_role( $target_role );
    if( !$caps ){
    	$caps = [];
    } else {
    	$caps = $caps->capabilities;
    }
    $caps = array_merge_recursive( $caps, $capabilities );

    $wp_roles->add_role( $role, $label, $caps );
}

function fr_user_redirect_after_login( $roles, $url ){
	if( !is_array( $roles ) ){
		$roles = [ $roles ];
	}
	add_filter( 'login_redirect', function( $redirect_to, $request, $user ) use( $roles, $url ){
		if ( isset( $user->roles ) && is_array( $user->roles ) && array_intersect( $user->roles, $roles ) ) {
	        $redirect_to =  $url;
	    }
	    return $redirect_to;
	}, 20, 3 );
}

function fr_user_add_cap( $role, $caps ){
	add_filter( 'user_has_cap', function( $allcaps, $cap, $args ) use ( $role, $caps ) { 
		if( fr_user() && isset( $args[1] ) && in_array( $role, fr_user( 'roles', $args[1] ) ) ){
			if( is_callable( $caps ) ){
				$allcaps = call_user_func_array( $caps, [ $allcaps, $cap, $args ] );
			} else {
				$allcaps = array_merge( $allcaps, $caps );
			}
		}
		return $allcaps;
	}, 10, 3 );
}

// Adds a setting to a post that is specific to the user
function fr_user_set_post_setting( $k, $v, $pid = false, $uid = false ){
	if( $pid === false ){
		$pid = get_the_ID();
	}
	if( $uid === false ){
		$uid = fr_user();
	}
	return fr_m( 'fr_user_setting_' . $uid . '_' . $k, $v, $pid );
}
function fr_user_get_post_setting( $k, $pid = false, $uid = false ){
	if( $pid === false ){
		$pid = get_the_ID();
	}
	if( $uid === false ){
		$uid = fr_user();
	}
	return fr_m( 'fr_user_setting_' . $uid . '_' . $k, $pid );
}


function fr_user_id( $uid = false ){
	if( $uid === false || is_null( $uid ) ){
		$uid = fr_user();
	}
	return $uid;
}



function fr_user_exists( $users = [] ){

	if( !$users ){
		return [];
	}

	$users = fr_make_array( $users );
	return fr_user_query_get_ids( [
		'include' => $users
	] );
}




function fr_export_users( $q, $user_fields, $user_meta = [], $condition_callback = false, $export_as = 'csv' ){
	$users = fr_user_query_get_ids( $q );

	$a = [];
	foreach( $users as $uid ){
		if( $condition_callback && !fr_call_if_func( $condition_callback, [ $uid ] ) ){
			continue;
		}
		$new = [];
		foreach( $user_fields as $v ){
			$new[] = fr_user( $v, $uid );
		}
		foreach( $user_meta as $v ){
			$new[] = fr_um( $v, $uid );
		}
		$a[$uid] = $new;
	}

	if( $export_as == 'csv' ){
		$a = fr_array_to_csv( $a );
	}

	return $a;
}


function fr_enable_login_as_any_user(){
	add_action( 'init', function(){

		if( current_user_can( 'manage_options' ) || !empty( $_SESSION['fr_previously_logged_in_as'] ) || fr_is_development() ){
			fr_GET_trigger( 'login_as', function( $uid ){

				if( fr_user() ){
					$_SESSION['fr_previously_logged_in_as'] = fr_user();
					wp_logout();
					fr_redirect( fr_get_current_url() );
				}

				fr_login_by_id( $uid );
				fr_redirect_without_param( 'login_as' );
			} );
		}

		if( !empty( $_SESSION['fr_previously_logged_in_as'] ) ){
			fr_GET_trigger( 'reset_login_as', function(){

				if( fr_user() ){
					wp_logout();
					fr_redirect( fr_get_current_url() );
				}

				fr_login_by_id( $_SESSION['fr_previously_logged_in_as'] );
				$_SESSION['fr_previously_logged_in_as'] = '';
				fr_redirect_without_param( 'reset_login_as' );
			} );
		}
		
	} );
}





function fr_role_change( $uid, $new_role ) {

	$user = new WP_User( $uid );

	if( $user ) {

		foreach( $user->roles as $role ) {
			$user->remove_role( $role );
		}

		$user->add_role( $new_role );

	}

}







