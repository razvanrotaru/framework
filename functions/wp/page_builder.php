<?php
/**
 * A simple ACF based page builder
 */

add_shortcode( 'fr_page_builder_export_settings_link', function(){
    if( empty( $_GET['post'] ) ){
        return;
    }
    return fr_html_a( fr_modify_url_params( [ 'fr_export_page_builder_settings' => $_GET['post'] ] ), __( 'Export Settings', 'fr' ), [ 'target' => '_blank' ] ); 
} );

fr_GET_trigger( 'fr_export_page_builder_settings', function( $value ){
    if( fr_user_has_role( 'administrator' ) ){
        echo esc_html( base64_encode( json_encode( get_field( 'rows', $value ) ) ) );
        exit;
    }
} );

add_filter( 'acf/update_value/name=fr_page_builder_import_settings', function( $value, $pid ){
    $value = base64_decode( $value );
    $value = json_decode( $value, 1 );
    if( $value ){
        update_field( 'rows', $value, $pid );
    }
}, 10, 2 );


function fr_display_page_builder( $options = [] ){
    $template_id = fr_page_builder_get_template_id( $options );
    return fr_page_builder( $template_id, $options );
}

function fr_page_builder_get_template_id( $options = [] ){
    ob_start();
    if( !get_the_ID() && have_posts() ){
        the_post();
    }
    ob_get_clean();

    if( !empty( $options['template_id'] ) ){
        return $options['template_id'];
    }

    $template_id = get_the_ID();
    $object = get_queried_object();

    $layout = [];

    if( is_single() || is_page() ){
        $layout[] = 'single_' . get_post_type();
    }
    if( is_author() ){
        $layout[] = 'author';
        $roles = fr_user( 'roles', fr_author() );
        foreach( $roles as $role ){
            $layout[] = 'user_' . $role;
        }
    }
    if( !empty( $object->taxonomy ) ){
        $layout[] = 'taxonomy_' . $object->taxonomy;
    }
    if( is_404() || http_response_code() == 404 ){
        $layout[] = '404';
    }

    if( $layout ){
        $q = array(
            'post_type' => 'page',
            'post_status' => array( 'publish', 'private' ),
            'meta_query' => array(
                'relation' => 'OR',
            )
        );
        foreach( $layout as $k => $v ){
            $q['meta_query'][] = array(
                'key' => 'layout_for',
                'value' => $v,
                'compare' => 'LIKE'
            );
        }

        $id = fr_query_get_id( $q );
        
        if( $id ){
            $template_id = $id;
        }
    }

    return $template_id;
}




function fr_page_builder_replace_variables( $sections, $variables ){
    
    if( !is_array( $sections ) ){
        return $sections;
    }

    foreach( $sections as $k => $section ){
        if( is_array( $section ) ){
            $sections[$k] = fr_page_builder_replace_variables( $section, $variables );
        } else {
            foreach( $variables as $variable ){
                if( $variable['type'] == 'text' ){
                    $value = $variable['text'];
                } else {
                    $value = $variable['editor'];
           
                    $content = explode( '[var-' . $variable['name'] . ']', $sections[$k] );
                    foreach( $content as $content_k => $part ){
                        if( isset( $content[$content_k + 1] ) ){
                            
                            if( substr( $part, -1, 1 ) == '>' ){
                                $part = strrev( $part );
                                $part = strstr( $part, '<' );
                                $part = substr( $part, 1 );
                                $part = strrev( $part );
                            }
                        }

                        if( $content_k ){
                            if( substr( $part, 0, 2 ) == '</' ){
                                $part = strstr( $part, '>' );
                                $part = substr( $part, 1 );
                            }
                        }

                        $content[$content_k] = $part;
                    }

                    $sections[$k] = implode( '[var-' . $variable['name'] . ']', $content );
                }
                $sections[$k] = str_replace( '[var-' . $variable['name'] . ']', $value, $sections[$k] );

            }
        }
    }

    return $sections;
}




function fr_page_builder( $template_id, $options = [] ){

    do_action( 'fr_page_builder', $template_id );

    $_ENV['template_id'] = $template_id;
    $rows = get_field("rows", $template_id );

    global $fr_page_builder_variables;
    if( !$rows ){
        return false;
    }
    $fr_page_builder_variables = array();
    ?>
    <div class="fr_page_builder">
        <?php 

        // Functionality to copy rows from other pages
        $new_rows = array();
        foreach( $rows as $k => $row ){

            if( !empty( $row['sections'] ) ){
                $new_sections = [];
                foreach( $row['sections'] as $section_k => $section ){
                    if( $section['acf_fc_layout'] == 'copy_block' ){

                        if( empty( $section['page'] ) && !empty( $options['template'] ) ){
                            $section['page'] = $options['template'];
                        }

                        $s_rows = get_field( 'rows', $section['page'] );
                        if( !$section['row_id'] ){
                            $new_rows = array_merge( $new_rows, $s_rows );
                            continue 2;
                        }
                        foreach( $s_rows as $s_row ){
                            if( $s_row['id'] == $section['row_id'] ){
                                if( empty( $section['column_type'] ) ){
                                    
                                    $variables = [];
                                    if( !empty( $section['variables'] ) ){
                                        foreach( $section['variables']  as $variable_key => $variable ){
                                            if( $variable['type'] == 'text' ){
                                                $variables[$variable['name']] = $section['variables'][$variable_key]['text'] = do_shortcode( $variable['text'] );
                                            } else {
                                                $variables[$variable['name']] = $section['variables'][$variable_key]['editor'] = do_shortcode( $variable['editor'] );
                                            }
                                        }
                                    }

                                    if( !empty( $section['copy'] ) && $section['copy'] == 'sections' ){
                                        $new_sections = array_merge( $new_sections, $s_row['sections'] );

                                        if( $variables ){
                                            $new_sections = fr_page_builder_replace_variables( $new_sections, $section['variables'] );
                                            foreach( $new_sections as $new_section_k => $new_section ){
                                                $new_sections[$new_section_k]['variables'] = $variables;
                                            }
                                        }

                                       

                                        continue 2;
                                    }

                                    if( $variables ){
                                        $s_row = fr_page_builder_replace_variables( $s_row, $section['variables'] );
                                        foreach( $s_row['sections'] as $new_section_k => $new_section ){
                                            $s_row['sections'][$new_section_k]['variables'] = $variables;
                                        }
                                    }
                                    
                                    $new_rows[] = $s_row;

                                    continue 3;
                                }
                                foreach( $s_row['sections'] as $s_sec_k => $s_sec ){
                                    if( $s_sec['acf_fc_layout'] == $section['column_type'] ){
                                        $row['sections'][$section_k] = $s_sec;
                                        break 3;
                                    }
                                }
                                continue 3;
                            }
                        }
                        continue 2;
                    }

                    $new_sections[] = $section;
                }
                $row['sections'] = $new_sections;
            }
            $new_rows[] = $row;
        }
        $rows = $new_rows;


        foreach( $rows as $k => $row ){
            if( !empty( $row['variables'] ) ){
                $fr_page_builder_variables = array_merge( $fr_page_builder_variables, $row['sections'] );
            }
        }


        foreach( $rows as $k => $row_of_sec ){

            $row_of_sec = apply_filters( 'fr_page_builder_row', $row_of_sec, $template_id );

            if( !apply_filters( 'fr_display_page_builder_show_row', true, $template_id, $row_of_sec ) ){
                continue;
            }

            if( empty( $row_of_sec['sections'] ) && empty( $row_of_sec['background_image'] ) ){
                continue;
            }
            if( !empty( $row_of_sec['variables'] ) ){
                continue;
            }

            $class = array();
            $style = array();
            $lazy_style = array();
            $mobile_style = array();
            $tablet_style = [];
            $attr = array();

            if( empty( $row_of_sec['id'] ) ){
                $row_of_sec['id'] = 'pb_sec_' . fr_unique_id( 'fr_page_builder' );
            }

            $class[] = 'fr_pb_row';


            if( !empty( $row_of_sec['background_image'] ) ){
                if( !empty( $row_of_sec["parallax"] ) ){
                    $class[] = 'fr_pb_parallax';
                    $attr['data-image'] = fr_img( $row_of_sec['background_image'], 'full', 1 );
                    $attr['data-fr-parallax'] = 'scroll';
                    $style['background_image'] = "background-image: url( '" . fr_img( $row_of_sec['background_image'], 'full', 1 ) . "' )";
                }else{
                    $style['background_image'] = "background-image: url( '" . fr_img( $row_of_sec['background_image'], 'full', 1 ) . "' )";
                    if( !empty( $row_of_sec['background_image_retina'] ) ){
                        $style['background_image'] .= "; background-image: -webkit-image-set( url('" . fr_img( $row_of_sec['background_image'], 'full', 1 ) . "') 1x, url('" . fr_img( $row_of_sec['background_image_retina'], 'full', 1 ) . "') 2x );";
                    }

                    if( !empty( $row_of_sec['lazy_load_images'] ) && empty( $row_of_sec["parallax"] ) ){
                        $lazy_style[] = $style['background_image']; 
                        unset( $style['background_image'] );
                    }
                }
            }

            if( !empty( $row_of_sec['background_position'] ) ){
                $class[] = 'fr_pb_bg_pos_' . $row_of_sec['background_position'];
            }

            if( !empty( $row_of_sec['background_size'] ) ){
                $class[] = 'fr_pb_bg_size_' . $row_of_sec['background_size'];
            }

            if( !empty( $row_of_sec['background_color'] ) ){
                $style[] = 'background-color: ' . $row_of_sec['background_color'];
            }

            if( !empty( $row_of_sec['text_color'] ) ){
                $class[] = 'fr_pb_text_color_' . $row_of_sec['text_color'];
            }

            if( !empty( $row_of_sec['parallax'] ) ){
                $class[]="fr_pb_parallax";
                wp_enqueue_script( 'parallax', FRAMEWORK_URL . '/resources/parallax.min.js', array("jquery"), "", 1 );
            }

            $class[] = 'fr_pb_min_height_' . $row_of_sec['row_min_height'];
            if( $row_of_sec['row_min_height'] == 'custom' ){
                $style[] = 'height: ' . fr_css_nr( $row_of_sec['custom_row_min_height'] );
                if( isset( $row_of_sec['vertical_align'] ) ){
                    $class[] = 'vertical_align_' . $row_of_sec['vertical_align'];
                }
            }

            if( !empty( $row_of_sec['padding_top'] ) ){
                $class[] = 'fr_pb_padding_top_' . $row_of_sec['padding_top'];
                if( $row_of_sec['padding_top'] == 'custom' && !empty( $row_of_sec['custom_padding_top'] ) ){
                    $style[] = 'padding-top: ' . fr_css_nr( $row_of_sec['custom_padding_top'] );
                    if( !empty( $row_of_sec['custom_padding_top_for_mobile'] ) ){
                        $mobile_style[] = 'padding-top: ' . fr_css_nr( $row_of_sec['custom_padding_top_for_mobile'] );
                    }
                }
            }

            if( !empty( $row_of_sec['padding_bottom'] ) ){
                $class[] = 'fr_pb_padding_bottom_' . $row_of_sec['padding_bottom'];
                if( $row_of_sec['padding_bottom'] == 'custom' && !empty( $row_of_sec['custom_padding_bottom'] ) ){
                    $style[] = 'padding-bottom: ' . fr_css_nr( $row_of_sec['custom_padding_bottom'] );
                    if( !empty( $row_of_sec['custom_padding_bottom_for_mobile'] ) ){
                        $mobile_style[] = 'padding-bottom: ' . fr_css_nr( $row_of_sec['custom_padding_bottom_for_mobile'] );
                    }
                }
            }


            if( !empty( $row_of_sec['padding_top_tablet'] ) ){
                $class[] = 'fr_pb_padding_top_tablet_' . $row_of_sec['padding_top_tablet'];
                if( $row_of_sec['padding_top_tablet'] == 'custom' && !empty( $row_of_sec['custom_padding_top_tablet'] ) ){
                    $tablet_style[] = 'padding-top: ' . fr_css_nr( $row_of_sec['custom_padding_top_tablet'] );
                }
            }

            if( !empty( $row_of_sec['padding_bottom_tablet'] ) ){
                $class[] = 'fr_pb_padding_bottom_tablet_' . $row_of_sec['padding_bottom_tablet'];
                if( $row_of_sec['padding_bottom_tablet'] == 'custom' && !empty( $row_of_sec['custom_padding_bottom_tablet'] ) ){
                    $tablet_style[] = 'padding-bottom: ' . fr_css_nr( $row_of_sec['custom_padding_bottom_tablet'] );
                }
            }


            if( !empty( $row_of_sec['padding_top_mobile'] ) ){
                $class[] = 'fr_pb_padding_top_mobile_' . $row_of_sec['padding_top_mobile'];
                if( $row_of_sec['padding_top_mobile'] == 'custom' && !empty( $row_of_sec['custom_padding_top_mobile'] ) ){
                    $mobile_style[] = 'padding-top: ' . fr_css_nr( $row_of_sec['custom_padding_top_mobile'] );
                }
            }

            if( !empty( $row_of_sec['padding_bottom_mobile'] ) ){
                $class[] = 'fr_pb_padding_bottom_mobile_' . $row_of_sec['padding_bottom_mobile'];
                if( $row_of_sec['padding_bottom_mobile'] == 'custom' && !empty( $row_of_sec['custom_padding_bottom_mobile'] ) ){
                    $mobile_style[] = 'padding-bottom: ' . fr_css_nr( $row_of_sec['custom_padding_bottom_mobile'] );
                }
            }

            if( !empty( $row_of_sec['visibility'] ) ){
                $class[] = 'fr_pb_visibility_' . $row_of_sec['visibility'];
            }

            if( !empty( $row_of_sec['class'] ) ){
                $class[] = $row_of_sec['class'];
            }

            if( !empty( $row_of_sec['background_video'] ) ){
                $class[] = 'fr_pb_background_video';
            }

            $style = apply_filters( 'fr_page_builder_row_style', $style, $row_of_sec, $template_id );

            $class = apply_filters( 'fr_page_builder_row_class', $class, $row_of_sec, $template_id );

            $class = array_unique( $class );
            $attr['class'] = implode( ' ', $class );
            $attr['id'] = $row_of_sec['id'];
            $attr['data-lazy-style'] = implode( ';', $lazy_style );
            if( isset( $row_of_sec['row_link'] ) ){
                $attr['data-link'] = $row_of_sec['row_link'];
            }

            ?>

            <style>
                #<?php echo $row_of_sec['id'] ?> {
                    <?php echo implode( ";\r\n", $style ); ?>
                }

                <?php if( $tablet_style ){ ?>
                    @media( max-width: 1024px ){
                        #<?php echo $row_of_sec['id'] ?> {
                            <?php echo implode( ";\r\n", $tablet_style ); ?>
                        }
                    }
                <?php } ?>

                <?php if( $mobile_style ){ ?>
                    @media( max-width: 768px ){
                        #<?php echo $row_of_sec['id'] ?> {
                            <?php echo implode( ";\r\n", $mobile_style ); ?>
                        }
                    }
                <?php } ?>
            </style>

            <section <?php echo fr_array_to_tag_attr( $attr ) ?>>
                <?php 

                do_action( 'fr_page_builder_before_row', $row_of_sec );


                if( $row_of_sec["background_image"] && empty( $row_of_sec['lazy_load_images'] ) && empty( $row_of_sec["parallax"] ) ){
                    ?><noscript><?php 
                        echo fr_img( $row_of_sec["background_image"], 'full', false, [ 'class' => 'hide' ] );
                    ?></noscript><?php 
                }

                if( !empty( $row_of_sec['background_video'] ) ){
                    $mime = wp_check_filetype( fr_file_path( $row_of_sec['background_video'] ) );
                    ?>
                    <video playsinline autoplay muted loop>
                        <source src="<?php echo fr_file_url( $row_of_sec['background_video'] ) ?>" type="<?php echo $mime['type'] ?>">
                    </video>
                    <?php 
                }

                ?>
                <div class="fr_pb_all_sections">
                    <div class="fr_pb_inner">
                        <?php 
                        if( !empty( $row_of_sec['sections'] ) ){
                            foreach( $row_of_sec['sections'] as $section ){

                                do_action( 'fr_display_page_builder_section', $template_id, $row_of_sec, $section );

                                if( !apply_filters( 'fr_display_page_builder_show_section', true, $template_id, $row_of_sec, $section ) ){
                                    continue;
                                }

                                $show = true;
                                $class_wrap = [];
                                $style_wrap = [];
                                $class_wrap[] = 'fr_pb_wrap';
                                $class_wrap[] = 'fr_pb_width_' . $row_of_sec['wrapper_width'];
                                $class_wrap[] = 'fr_pb_wrap_' . $section['acf_fc_layout'];

                                if( $row_of_sec['wrapper_width'] == 'custom' ){
                                    $style_wrap[] = 'width: ' . $row_of_sec['wrapper_custom_width'] . 'px' ;
                                }

                                $class = array();
                                $class[] = 'fr_pb_section';
                                $class[] = 'fr_pb_sec_' . $section['acf_fc_layout'];
                                if( !empty( $section['margin_top'] ) ){
                                    $class[] = 'fr_pb_margin_top_' . $section['margin_top'];
                                }
                                if( !empty( $section['margin_bottom'] ) ){
                                    $class[] = 'fr_pb_margin_bottom_' . $section['margin_bottom'];
                                }
                                if( $section['acf_fc_layout'] == 'shortcode' ){
                                    $shortcode = trim( $section['shortcode'] );
                                    $shortcode = trim( $shortcode, '[' );
                                    $shortcode = trim( $shortcode, ']' );
                                    $shortcode = explode( ' ', $shortcode );
                                    $shortcode = reset( $shortcode );
                                    $class[] = 'fr_pb_shortcode_' . sanitize_title( $shortcode );
                                }
                                ob_start();
                                ?>
                                <div class="<?php echo esc_attr( implode( ' ', $class_wrap ) ) ?>" style="<?php echo esc_attr( implode( '; ', $style_wrap ) ) ?>">
                                    <div class="<?php echo esc_attr( implode( ' ', $class ) ) ?>">
                                        <?php 
                                        $file = get_stylesheet_directory() . '/sections/' . $section['acf_fc_layout'] . '.php';
                                        if( file_exists( $file ) ){
                                            ob_start();
                                            include( $file );
                                            echo $res = ob_get_clean();
                                            if( !$res ){
                                                $show = false;
                                            }
                                        }
                                        ?>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <?php 
                                $c = ob_get_clean();
                                $c = apply_filters( 'fr_display_page_builder_section_html', $c, $template_id, $row_of_sec, $section );
                                if( $show ){
                                    echo $c;
                                }
                            }
                        } ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>
        <?php } ?>
    </div>
    <?php

    return true;
}



function fr_page_builder_var( $k ){
    global $fr_page_builder_variables;
    if( !isset( $fr_page_builder_variables ) ){
        return;
    }
    foreach( $fr_page_builder_variables as $v ){
        if( isset( $v['variable_name'] ) && $v['variable_name'] == $k ){
            return $v['content'];
        }
    }
}