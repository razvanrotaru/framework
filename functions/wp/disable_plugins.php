<?php
/*
Disable plugins so that search suggestions load faster
Add this class and initiated it in wp-config.php
 */
if( !class_exists( 'fr_disable_plugins' ) ){
	class fr_disable_plugins{

		function get( $plugins, $keep_plugins_callback = false, $exclude_plugins_callback = false ){
			$keep_plugins = [];
			$exclude_plugins = [];

			// Ajax view
			if ( $this->is_multi_ajax() ){
				$keep_plugins = $this->allow_plugins( $keep_plugins, $plugins );
			}

			if ( isset( $_GET['wc-ajax'] ) ){
				$keep_plugins = array_merge( $keep_plugins, array(
					'woocommerce*',
					'sitepress-multilingual-cms',
					'advanced-custom-fields-pro',
					'wc-custom-thank-you'
				) );
			}

			if( is_callable( $keep_plugins_callback ) ){
				$keep_plugins = call_user_func_array( $keep_plugins_callback, array( $keep_plugins, $plugins, $this ) );
			}
			if( is_callable( $exclude_plugins_callback ) ){
				$exclude_plugins = call_user_func_array( $exclude_plugins_callback, array( $exclude_plugins, $plugins, $this ) );
			}
			if( $keep_plugins ){
				$plugins = $this->filter_plugins( $plugins, $keep_plugins );
			}
			if( $exclude_plugins ){
				$plugins = $this->exclude_plugins( $plugins, $exclude_plugins );
			}
			return $plugins;
		}

		function is_admin(){
			return strstr( $_SERVER['REQUEST_URI'], '/wp-admin/' );
		}

		function is_ajax( $key = '' ){
			return strstr( $_SERVER['REQUEST_URI'], '/wp-admin/' ) && !empty( $_POST['action'] ) && ( !$key || $_POST['action'] == $key );
		}

		function is_multi_ajax( $key = '' ){
			if( !$key ){
				return $this->is_admin() && isset( $_POST['action'] ) && $_POST['action'] == 'multi_ajax';
			}else{
				if( $this->is_multi_ajax() ){
					$data = $this->decode_ajax();
					$actions = array_column( $data, 0 );
					print_r( $actions );
					return in_array( $key, $actions );
				}
			}
		}

		function allow_plugins( $active_plugins ){
			$data = $this->decode_ajax();
			$plugins = array();
			foreach($data as $k=>$v){
				if( !empty( $v[1]['enable_plugins'] ) ){

					if( is_array( $active ) ){
						$plugins = array_merge( $plugins, $v[1]['enable_plugins'] );
					}else{
						return $active_plugins;
					}
				}
			}
			
			return $plugins;
		}

		function decode_ajax(){
			$data = isset( $_POST['data'] ) ? json_decode( urldecode( base64_decode( $_POST['data'] ) ), 1 ) : array();
			return $data;
		}

		// Filters by matching the directory name of the plugins
		function filter_plugins( $plugins, $active ){
			$new = array();
			foreach( $plugins as $plugin ){
				foreach( $active as $active_plugin ){
					if( strstr( $active_plugin, '*' ) ){
						$active_plugin = str_replace( '*', '', $active_plugin );
						$dir = explode( '/', $plugin );
						$dir = reset( $dir );
						if( strstr( $dir, $active_plugin ) ){
							$new[] = $plugin;
						}
					}else{
						if( strstr( $plugin, '/', 1 ) == $active_plugin ){
							$new[] = $plugin;
						}
					}
				}
			}
			$new = array_unique( $new );
			return $new;
		}

		// Filters by maatching the directory name of the plugins
		function exclude_plugins( $plugins, $exclude ){
			$exclude = $this->filter_plugins( $plugins, $exclude );
			$plugins = array_diff( $plugins, $exclude );
			return $plugins;
		}

		function current_url( $no_get = 0 ){
			$protocol = (isset($_SERVER['HTTPS']) ? "https" : "http");
			$r = $protocol."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			if($no_get){
				$r=explode("?",$r);
				$r=reset($r);
			}
			return $r;
		}

		function is_json( $k ){
			$url = $this->current_url( 1 );
			$part = site_url() . '/wp-json';
			if( substr( $url, 0, strlen( $part ) ) === $part ){
				$endpoint = str_replace( $part, '', $url );
				$endpoint = trim( $endpoint, '/' );
				return $k === $endpoint;
			}
		}
	}
}


// Clear pause plugins when a plugin is activated / deactivated
function fr_disable_plugins_init(){
	add_action( 'updated_option', function( $option_name ){
		if( $option_name == 'active_plugins' ){
			update_option( 'paused_plugins', '' );
		}
	}, 10, 3 );
}
