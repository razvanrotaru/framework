<?php 

function fr_wp_file_insert( $file, $post_id = 0, $data = [], $update_attachment = true ){
	$file = fr_url_to_path( $file );

	if( !file_exists( $file ) ){
		return;
	}

    $default_data = array(
        'guid'           => fr_path_to_url( $file ), 
        'post_mime_type' => fr_file_mime_type( $file ),
        'post_title'     => fr_file_name( $file ),
        'post_content'   => '',
        'post_status'    => 'inherit'
    );
    if( !is_array( $data ) ){
        fr_debug( 20, false, true, 1, DEBUG_BACKTRACE_IGNORE_ARGS );
    }

    $data = array_merge( $default_data, $data );

    $attach_id = wp_insert_attachment( $data, $file, $post_id );
    if( $update_attachment && $attach_id && !is_wp_error( $attach_id ) ){
        fr_wp_file_update_attachement( $attach_id, $file );
    }
    return $attach_id;
}



/**
 * Upload a file to WordPress from binary data.
 *
 * @param string $file_name The desired file name.
 * @param string $data Binary data of the file.
 * @param array $post_data Additional post data for attachment.
 * @return int|WP_Error Attachment ID on success, WP_Error on failure.
 */
function fr_wp_file_upload_from_binary( $file_name, $data, $post_data = [] ) {

    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );
    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    $tmpfname = tempnam( sys_get_temp_dir(), 'fr_' );
    file_put_contents( $tmpfname, $data );

    $file_array = [
        'name'     => $file_name,
        'tmp_name' => $tmpfname,
    ];

    $id = media_handle_sideload( $file_array, 0, null, $post_data );

    @unlink( $tmpfname );

    return $id;
}





/**
 * Allow WP to upload a file from the tmp directory
 * @param  string  $file_name      The key of $_FILES
 * @param  integer $post_id   
 * @param  array   $post_data 
 * @param  string  $directory
 * @return int
 */
function fr_wp_file_upload( $file_name, $post_id = 0, $post_data = [], $directory = '', $update_attachment = true ){
    if( !$directory ){
        return media_handle_upload( $file_name, $post_id, $post_data );
    } else {
        // Upload to custom directory
        
        if( $_FILES[$file_name]['error'] ){
            return;
        }

        fr_required_dir( $directory );
        $file = $directory . '/' . sanitize_file_name( $_FILES[$file_name]['name'] );
        $file = fr_unique_file_path( $file );

        $default_data = [
            'post_title' => fr_file_name( $_FILES[$file_name]['name'] )
        ];
        $post_data = array_merge( $default_data, $post_data );
       
        if( copy( $_FILES[$file_name]['tmp_name'], $file ) ){
            unlink( $_FILES[$file_name]['tmp_name'] );
            return fr_wp_file_insert( $file, $post_id, $post_data, $update_attachment );
        }
    }
}




function fr_wp_file_update_attachement( $attach_id, $file = '' ){
    if( !$file ){
        $file = fr_file_path( $attach_id );
    }

    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );

    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    wp_update_attachment_metadata( $attach_id, $attach_data );
}

function fr_wp_file_url( $pid ){
    return wp_get_attachment_url( $pid );
}

function fr_wp_file( $pid ){
    return get_attached_file( $pid );
}

function fr_wp_file_get_by_name( $file ) {
    if( strstr( $file, '/uploads/' ) ){
        $file = explode( '/uploads/', $file );
        $file = end( $file );
    }
    $ids = fr_query_get_ids( array(
        'post_type' => 'attachment',
        'post_status' => 'any',
        'meta_key' => '_wp_attached_file',
        'meta_value' => $file
    ), 1 );
    return reset( $ids );
}



/**
 * Check if the given files are different from each other
 * Return only the different ones
 * @param  array $files File attachment ids
 * @return array
 */
function fr_wp_files_unique( $files ){
    $files = array_unique( $files );
    foreach( $files as $k => $v ){
        $files[$k] = get_attached_file( $v );
    }
    $files = fr_files_unique( $files );
    return $files;
}


function fr_wp_regenerate_thumbnails( $fid ){
    $file = get_attached_file( $fid );
    $dir = dirname( $file );

    // Delete existing attachment sizes
    $meta = fr_m( '_wp_attachment_metadata', $fid );
    if( !empty( $meta['sizes'] ) ){
        foreach( $meta['sizes'] as $k => $size ){
            $file_path = $dir . '/' . $size['file'];
            if( file_exists( $file_path ) ){
                unlink( $file_path );
            }
            unset( $meta['sizes'][$k] );
        }
        fr_m( '_wp_attachment_metadata', $meta, $fid );
    }

    // Generate attachment sizes
    $attach_data = wp_generate_attachment_metadata($fid, $file );
    wp_update_attachment_metadata($fid,  $attach_data );
}


// Returns the original file name that was uploaded
function fr_wp_files_get_name( $fid, $path = false, $name = false ){
    if( $name === false ){
        $name = html_entity_decode( get_the_title( $fid ) );
    }
    if( $path === false ){
        $path = fr_file_path( $fid );
    }
    if( !$name ){
        $name = fr_file_name( $path );
    }

    $name = fr_file_change_extension( $name, fr_file_ext( $path ) );

    return $name;
}



function fr_uploads_relative_path( $file ){
    $dir=wp_upload_dir();
    $file = str_replace( $dir["basedir"] . '/', '', $file );
    return $file;
}

function fr_uploads_relative_url( $file ){
    $dir=wp_upload_dir();
    $file = str_replace( $dir["baseurl"] . '/', '', $file );
    return $file;
}


/**
 * Because attachments happen to have the same title as posts they force the post slug to have "-2" at the end
 * If the attachments urls are not public then this option can be used to make their post_name a unique random string
 */
function fr_attachment_make_unique_slug_on_upload(){
    add_filter( 'wp_insert_attachment_data', function( $data ){ 
        $data['post_name'] = fr_attachment_make_unique_slug( $data );
        return $data;
    } );
}

function fr_attachment_make_unique_slug( $data ){
    return md5( json_encode( $data ) . rand( 0, 1000000 ) );
}


function fr_attachment_make_unique_slug_retroactively(){
    $ids = fr_query_get_ids( [
        'post_type' => 'attachment',
        'post_status' => 'any'
    ] );
  
    foreach( $ids as $id ){
        
        $name = get_post_field( 'post_name', $id );
        if( !fr_is_md5( $name ) ){
            wp_update_post( [
                'ID' => $id,
                'post_name' => fr_attachment_make_unique_slug( $id )
            ] );
        }
    }
}











