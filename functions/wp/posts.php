<?php
// Allow random posts to have seed
add_filter( 'posts_orderby', function( $orderby_statement, $wp_query ){
	if( !empty( $wp_query->query['orderby'] ) && $wp_query->query['orderby'] == 'rand' && !empty( $wp_query->query['orderby_seed'] ) ){
		$orderby_statement = 'RAND(' . $wp_query->query['orderby_seed'] . ')';
	}
	return $orderby_statement;
}, 10, 2 );



//Reduce the excerpt length

function fr_get_excerpt($size,$pid=0,$after=""){

	if( is_object( $pid ) ){
		$e = $pid->post_excerpt;
		if( !$e ){
			$e = fr_get_excerpt_from_content( $pid->post_content, $size + 100 );
		}
	} else {
		$e=get_the_excerpt($pid);
		if(!$e){
			$e=fr_get_excerpt_from_content(fr_get_content($pid));
		}
	}

	$e = fr_cut_excerpt( $e, $size, $after );
	return $e;
}

function fr_cut_excerpt( $e, $size, $after = '' ){
  $e=strip_tags($e);
  $e=str_replace("\n"," ",$e);
  $oe=$e=trim($e);
  $e=mb_substr($e,0,$size, 'utf-8');
  if($e && $e!=$oe){
    $e.=$after;
  }
  return $e;
}

function fr_cut_text( $s, $max_paragraphs = 0, $max_length = 0 ){
  
	if( $max_length ){
		$s = substr( $s, 0, $max_length );
	}

	$s = fr_html_format_text_based_on_tags( $s );

	$s = explode( "\n", $s );

	if( count( $s ) == 1 ){
		$s = reset( $s );
		$s = strip_tags( $s );
	} else {
		if( $max_paragraphs ){
			$s = array_slice( $s, 0, $max_paragraphs );
		} else {
			array_pop( $s );
		}
		$s = implode( "\n", $s );
	}

  return $s;
}



function fr_get_content($pid=0, $filters = true ){
	$pid = fr_get_current_id( $pid );
	$content = get_post_field( 'post_content', $pid );
	if( !$filters ){
		return $content;
	}
	return apply_filters('the_content', $content );
}


function fr_get_current_id($pid=0){
	if(!$pid && (is_page() || is_single())){
		$pid=get_the_ID();
	}
	return $pid;
}

function fr_get_excerpt_from_content($content, $max_size = 0 ){
	//Select 1st paragraph from content as excerpt
	$excerpt=$content;
	$excerpt=explode("</p>",$excerpt);
	$excerpt=reset($excerpt);
	$excerpt=trim(strip_tags($excerpt));
	$excerpt = str_replace( "\r\n", "\n", $excerpt );
	$excerpt=explode("\n",$excerpt);

	if( !$max_size ){
		return reset( $excerpt );
	}

	$return = '';
	foreach( $excerpt as $v ){
		$return .= ' ' . $v;
		if( strlen( $return ) > $max_size ){
			$return = substr( $return, 0, $max_size );
			$return = trim( $return );
			break;
		}
	}

	return $return;
}

function fr_post_exists( $pid ){
	$status = get_post_status( $pid );
	return $status && $status != 'trash';
}


function fr_post_is_type( $pid, $post_types ){
	$post_types = fr_make_array( $post_types );
	return  $pid && in_array( get_post_type( $pid ), $post_types );
}


function fr_is_page( $rules ){
	$pid = get_the_ID();
	foreach( $rules as $v ){
		if( $v == $pid ){
			return true;
		}
	}
	return false;
}

function fr_post_get_permalink_from_url( $url ){

	$parts = parse_url( $url );
	$url = trim( $parts['path'] ?? '', '/' );

	return $url;
}


function fr_enable_duplicate_post( $post_types, $exclude_meta = array(), $label = '', $show_action = true, $allowed_callback = true, $callback_after = false ){
	if( !$label ){
		$label = __( 'Duplicate', 'framework' );
	}
	if( !is_array( $post_types ) ){
		$post_types = array( $post_types );
	}

	if( !fr_user() ){
		return;
	}

	if( $show_action ){

		$link_callback = function( $actions, $post ) use ( $post_types, $allowed_callback, $label ){
            if ( fr_call_if_callable( $allowed_callback, [ $post->ID ] ) && in_array( $post->post_type, $post_types ) ){
                $nonce = wp_create_nonce( 'duplicate_post_nonce_' . $post->ID );
                $duplicate_url = fr_build_link( fr_ajax_get_original_url(),[
                    'duplicate_post' => $post->ID,
                    'duplicate_nonce' => $nonce
                ] );
                $actions['duplicate_post'] = '<a rel="noopener" href="' . esc_url( $duplicate_url ) . '">' . esc_html( $label ) . '</a>';
            }
            return $actions;
        };

		add_filter( 'post_row_actions', $link_callback, 10, 2 );
		add_filter( 'page_row_actions', $link_callback, 10, 2 );

	}


	add_action( 'admin_init', function() use ( $post_types, $exclude_meta, $allowed_callback, $callback_after ){ 
		if( !empty( $_GET['duplicate_post'] ) && in_array( get_post_type( $_GET['duplicate_post'] ), $post_types ) ){
			$pid = $_GET['duplicate_post'];
			$prev_nonce = $_SESSION['fr_duplicate_post_nonce_used'] ?? '';

			if( $prev_nonce && isset( $prev_nonce['time'] ) && time() - $prev_nonce['time'] > 60 ){
				unset( $_SESSION['fr_duplicate_post_nonce_used'] );
				$prev_nonce = false;
			}

			// Verify nonce
            $nonce = $_GET['duplicate_nonce'] ?? '';
            if ( ! wp_verify_nonce( $nonce, 'duplicate_post_nonce_' . $pid ) || ( !empty( $prev_nonce['nonce'] ) && $prev_nonce['nonce'] == $nonce ) ){
				fr_message( 'Cloning post failed because the post was already cloned less than 60 seconds ago.' );
                fr_redirect_without_param( [ 'duplicate_post', 'duplicate_nonce' ] );
                return;
            }

			$_SESSION['fr_duplicate_post_nonce_used'] = [ 'nonce' => $nonce, 'time' => time() ];

			if( fr_call_if_callable( $allowed_callback, [ $pid ] ) ){
				
				$new_id = fr_duplicate_post( $pid, $exclude_meta );

				if( $callback_after ){
					fr_call_if_callable( $callback_after, [ $new_id, $pid ] );
				}

				fr_message( __( 'Post duplicated', 'framework' ) );
				fr_redirect_without_param( [ 'duplicate_post', 'duplicate_nonce' ] );
			}
		}
	} );
}



function fr_duplicate_post( $pid, $exclude_meta = [], $clone = false ){

	// Copy post data
	$post = (array)get_post( $pid );
	unset( $post['ID'] );
	if( !$clone ){
		$post['post_title'] .= __( ' - copy', 'framework' );
		$post['post_status'] = 'draft';
		$post['post_date'] = current_time( 'mysql' );
		$post['post_date_gmt'] = current_time( 'mysql', 1 );
		$post['post_modified'] = $post['post_date'];
		$post['post_name'] = $post['post_name'] . '-' . time();
		$post['post_modified_gmt'] = $post['post_date_gmt'];
		$post['comment_count'] = 0;
		if( fr_user() ){
			$post['fr_author'] = fr_user();
		}
	}

	fr_global( 'fr_duplicate_post', true );

	$post['post_content'] = wp_slash( $post['post_content'] );

	$new_pid = wp_insert_post( $post );

	wp_update_post( array(
		'ID' => $new_pid,
		'guid' => site_url() . '?p=' . $new_pid
	) );

	// Copy post meta
	$meta = get_post_meta( $pid );
	$parent_id = fr_m( 'duplicate_of', $pid ) ? fr_m( 'duplicate_of', $pid ) : $pid;
	$meta['duplicate_of'] = [ $parent_id ];
	$exclude_meta[] = '_edit_lock';

	foreach( $meta as $k => $vals ){
		if( in_array( $k, $exclude_meta ) ){
			continue;
		}

		foreach( $vals as $val ){
			if( is_serialized( $val ) ){
	            $val = unserialize( $val );
	        }
			add_post_meta( $new_pid, $k, $val );
		}
	}

	// Copy post terms
	fr_copy_post_terms( $pid, $new_pid );

	fr_global( 'fr_duplicate_post', false );

	return $new_pid;
}


function fr_copy_post_terms( $from_pid, $to_pid, $taxs = [] ){
	if( !$taxs ){
		$taxs = get_object_taxonomies( get_post_type( $from_pid ), 'names' );
	}
	foreach( $taxs as $tax ){
		$terms = wp_get_post_terms( $from_pid, $tax, array( 'fields' => 'ids' ) );
		wp_set_post_terms( $to_pid, $terms, $tax );
	}
}


function fr_copy_post_meta( $from_pid, $to_pid, $meta = [] ){
	$all_meta = get_post_meta( $from_pid );
	$all_meta = array_intersect_key( $all_meta, array_flip( $meta ) );

	foreach( $all_meta as $k => $vals ){
		foreach( $vals as $val ){
			update_post_meta( $to_pid, $k, $val );
		}
	}
}




function fr_update_post_field( $k, $v, $pid ){
	return wp_update_post( [
		'ID' => $pid,
		$k => $v
	] );
}

function fr_post_updated_messages( $post_type, $singular, $plural, $remove_preview_links = false ){
	add_filter( 'post_updated_messages', function( $messages ) use ( $post_type, $singular, $plural, $remove_preview_links ) {
	  global $post, $post_ID;

	  $messages[$post_type] = array(
	    0 => '', // Unused. Messages start at index 1.
	    1 => sprintf( __('%s updated. <a href="%s">View %s</a>'), $singular, esc_url( get_permalink($post_ID) ), strtolower( $singular ) ),
	    2 => __('Custom field updated.'),
	    3 => __('Custom field deleted.'),
	    4 => sprintf( __('%s updated.'), $singular ),
	    /* translators: %s: date and time of the revision */
	    5 => isset($_GET['revision']) ? sprintf( __('%s restored to revision from %s'), $singular, wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
	    6 => sprintf( __('%s published. <a href="%s">View %s</a>'), $singular, esc_url( get_permalink($post_ID) ), strtolower( $singular ) ),
	    7 => sprintf( __('%s saved.'), $singular ),
	    8 => sprintf( __('%s submitted. <a target="_blank" href="%s">Preview %s</a>'), $singular, esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ), strtolower( $singular ) ),
	    9 => sprintf( __('%1$s scheduled for: <strong>%2$s</strong>. <a target="_blank" href="%3$s">Preview %1$s</a>'),
	    	$singular,
			date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), 
			esc_url( get_permalink($post_ID) ) ),
	    10 => sprintf( __('%s draft updated. <a target="_blank" href="%s">Preview %s</a>'), $singular, esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ), strtolower( $singular ) ),
	  );

	  if( $remove_preview_links ){
	  	foreach( $messages[$post_type] as $k => $v ){
	  		if( strstr( $v, '<a' ) ){
		  		$messages[$post_type][$k] = trim( strstr( $v, '<a', 1 ) );
		  	}
	  	}
	  }
	  
	  return $messages;
	} );
}




function fr_change_post_labels( $post_type, $old_singular, $old_plural, $new_singular, $new_plural ){
	global $wp_post_types;

	if( !isset( $wp_post_types[$post_type] ) ){
		return;
	}

	$labels = (array)$wp_post_types[$post_type]->labels;

	foreach( $labels as $type => $label ){
		if( strstr( $label, $old_plural ) ){
			$label = str_replace( $old_plural, $new_plural, $label );
		} else if( strstr( $label, strtolower( $old_plural ) ) ){
			$label = str_replace( strtolower( $old_plural ), strtolower( $new_plural ), $label );
		} else if( strstr( $label, $old_singular ) ){
			$label = str_replace( $old_singular, $new_singular, $label );
		} else if( strstr( $label, strtolower( $old_singular ) ) ){
			$label = str_replace( strtolower( $old_singular ), strtolower( $new_singular ), $label );
		}
		$labels[$type] = $label;
	}

	$wp_post_types[$post_type]->labels = (object)$labels;
}



/**
 * Check if a post status is valid or not
 * @param  string $post_type    The post type to check against
 * @param  string $post_status  The post status to check
 * @return boolean
 */
function fr_post_status_exists( $post_type, $post_status ){
    global $wp_post_statuses;
    return isset( $wp_post_statuses[$post_status] );
}







/**
 * This function triggers a callback when a post or meta field has updated
 * @param  string $post_type
 * @param  string  $fields    An string of post column names of meta keys separated by spaces
 * @param  callable $callback  The function to trigger when a change is detected
 */
function fr_on_post_field_update( $post_type, $fields, $callback ){

    global $wpdb;

    $fields = explode( ' ', $fields );
    $columns = fewo_get_table_columns( $wpdb->prefix . 'posts' );
    $post_columns = array_intersect( $fields, $columns );
    $meta_keys = array_diff( $fields, $columns );

    if( $post_columns ){
  
        // Triggers only when a post is created
        add_action( 'save_post_' . $post_type, function( $pid, $post, $update ) use ( $callback ) {
            
            if( $update ){
                return;
            }

            if( get_post_status( $pid ) == 'auto-draft' ){
                return;
            }
            
            call_user_func( $callback, $pid );

        }, 10, 3 );


        // Triggers when a post is updated
        add_action( 'post_updated', function( $pid, $post_after, $post_before ) use ( $post_type, $post_columns, $callback ) {
            
            if( $post_after->post_type != $post_type ){
                return;
            }

            $fields_changed = array_diff( (array)$post_after, (array)$post_before );

            if( !array_intersect_key( $fields_changed, array_flip( $post_columns ) ) ){
                return;
            }

            call_user_func( $callback, $pid );
            
        }, 10, 3 );
    }


    if( $meta_keys ){

        fewo_add_actions( 'added_post_meta updated_post_meta', function( $meta_id, $pid, $meta_key ) use ( $post_type, $meta_keys, $callback ) {
            
            if( !in_array( $meta_key, $meta_keys ) ){
                return;
            }

            if( get_post_type( $pid ) != $post_type ){
                return;
            }
            
            call_user_func( $callback, $pid, $meta_key );

        }, 10, 3 );
    }
}