<?php
function fr_lazy_load_file( $file, $attr = 'src', $load_time = 'after_load' ){
	fr_global( 'fr_lazy_load_attr', [
		'file' => $file,
		'attr' => $attr,
		'load_time' => $load_time
	], ARRAY_A );
}



function fr_lazy_load_files_replace( $html, $data = false ){

	if( $data === false ){
		$data = fr_global( 'fr_lazy_load_attr' );
	}

	if( $data ){
		foreach( $data as $file ){
			$html = str_replace( ' ' . $file['attr'] . '="' . $file['file'], ' data-load-time="' . $file['load_time'] . '" data-load-range="' . $file['range'] . '" data-lazy-' . $file['attr'] . '="' . $file['file'], $html );
			$html = str_replace( ' ' . $file['attr'] . '=\'' . $file['file'], ' data-load-time="' . $file['load_time'] . '" data-load-range="' . $file['range'] . '" data-lazy-' . $file['attr'] . '=\'' . $file['file'], $html );
			$html = str_replace( "\t" . $file['attr'] . '="' . $file['file'], "\t" . 'data-load-time="' . $file['load_time'] . '" data-load-range="' . $file['range'] . '" data-lazy-' . $file['attr'] . '="' . $file['file'], $html );
			$html = str_replace( "\t" . $file['attr'] . '=\'' . $file['file'], "\t" . 'data-load-time="' . $file['load_time'] . '" data-load-range="' . $file['range'] . '" data-lazy-' . $file['attr'] . '=\'' . $file['file'], $html );
		}
	}
	
	return $html;
};