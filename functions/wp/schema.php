<?php

/**
 * Function that outputs a schema of @type Rating
 * @param  int $pid        The ID of the post for which to show the rating
 * @param  int $rating     1 - 5 score
 * @param  string $subject The subject being rated
 * @return string          The JSON html
 */
function fr_schema_post_rating( $pid, $rating, $subject, $logo ){
	$post = get_post( $pid );
	$img_id = get_post_thumbnail_id( $pid );
	$image_data = wp_get_attachment_metadata( $img_id );
	$rating = max( $rating, 1 );
	$rating = min( $rating, 5 );
	$data = [
		'@context' => 'http://schema.org/',
		'@type' => 'Review',
		'identifier' => $post->guid,
		'datePublished' => fr_date_iso( $post->post_date_gmt ),
		'dateModified' => fr_date_iso( $post->post_modified_gmt ),
		'headline' => $post->post_title,
		'reviewBody' => $post->post_excerpt,
		'author' => [
			'@type' => 'Person',
			'name' => fr_user( 'display_name', $post->post_author ),
		],
		'mainEntityOfPage' => [
			'@type' => 'WebPage',
			'@id' => get_permalink( $post )
		],
		'publisher' => [
			'@type' => 'Organization',
			'name' => get_bloginfo( 'site_name' ),
			'logo' => [
				'@type' => 'ImageObject',
				'url' => fr_img( $logo, '', 1 )
			]
		],
		'image' => [
			'@type' => 'ImageObject',
			'url' => fr_img( $pid, '', 1 ),
			'width' => $image_data['width'],
			'height' => $image_data['height']
		],
		'itemReviewed' => [
			'@type' => 'Product',
			'name' => $subject,
			'aggregateRating' => [
				'@type' => 'aggregateRating',
				'ratingValue' => $rating,
				'reviewCount' => 1,
				'worstRating' => 1,
				'bestRating' => 5
			]
		],
		'reviewRating' => [
			'@type' => 'Rating',
			'ratingValue' => $rating,
			'worstRating' => 1,
			'bestRating' => 5
		]
	];

	return fr_schema_output( $data );
}

function fr_schema_output( $data ){
	return '<script type="application/ld+json">' . json_encode( $data ) . '</script>';
}


function fr_schema_time_requird_to_read( $word_count ){
	$minutes = ceil( $word_count / 250 );
	$hours = floor( $minutes / 60 );
	$minutes -= $hours * 60;

	$parts = [
		'PT'
	];

	if( $hours ){
		$parts[] = $hours . 'H';
	}

	$parts[] = $minutes . 'M';

	return implode( '', $parts );
}