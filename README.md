# WP Framework
PHP Framework intended for fast WordPress theme and plugin development.

Installation:
```
// Include the framework in functions.php
require_once( dirname(__FILE__) . '/framework/index.php' );

// Initiate framework to load all wp related functions
$FRAMEWORK->settings( array(
	'platform' => 'wp'
) );
```


## A few exmples of usueful functions
The framework covers a wide range of possible situations that are encountered usually.

### Schedualed tasks / Cronjobs
Since in WP it can be quite tricky to register schedualed tasks the framework  contains a simple function that can be used to register these easly:
```
// Call function_to_be_called() every hour
fr_add_cronjob( 3600, 'function_to_be_called' );
```

### Images
```
// Display the featured image of the current post
echo fr_img();

// Get URL of a image with ID = 100
$url = fr_img( 100, 'thumbnail', 1 );

// Display an image in /images/ directory
echo fr_img( 'logo.png' );
```

### SEND custom email using the WC email template
```
fr_wc_send_email( 'email@email.com', 'Hi John!', 'We need your feedback', 'Hi John,<br>We need your feedback asap.' );
```

More documentation will be provided in the WIKI section