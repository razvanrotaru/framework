<?php
// Key doesn't exist
fr_unit_test( 'fr_global', [
	'args' => [
		'fr_test',
		NULL, 
		false,
		false
	],

	'tests' => [
		[ 
			false, 
			'=' 
		]
	]
] );

// Add a value
fr_unit_test( 'fr_global', [
	'args' => [
		'fr_test',
		1, 
		false,
		false
	],

	'tests' => [
		[ 
			true, 
			'=' 
		]
	]
] );

// Increment value
fr_unit_test( 'fr_global', [
	'args' => [
		'fr_test',
		1, 
		true,
		false
	],

	'tests' => [
		[ 
			true, 
			'=' 
		]
	]
] );


// Retrive value
fr_unit_test( 'fr_global', [
	'args' => [
		'fr_test',
		NULL, 
		false,
		false
	],

	'tests' => [
		[ 
			2, 
			'=' 
		]
	]
] );


// Increment array
fr_unit_test( 'fr_global', [
	'args' => [
		'fr_test_array',
		1, 
		ARRAY_A,
		false
	],

	'tests' => [
		[ 
			true, 
			'=' 
		]
	]
] );


// Increment array with another array
fr_unit_test( 'fr_global', [
	'args' => [
		'fr_test_array',
		[ 'two' => 2, 'three' => 3 ], 
		ARRAY_A,
		true
	],

	'tests' => [
		[ 
			true, 
			'=' 
		]
	]
] );


// Increment array with another array by merge
fr_unit_test( 'fr_global', [
	'args' => [
		'fr_test_array',
		[ 'two' => 'two' ], 
		ARRAY_A,
		true
	],

	'tests' => [
		[ 
			true, 
			'=' 
		]
	]
] );


// Get array value
fr_unit_test( 'fr_global', [
	'args' => [
		'fr_test_array',
		NULL, 
		ARRAY_A,
		false
	],

	'tests' => [
		[ 
			[ 1, 'two' => 'two', 'three' => 3 ], 
			'=' 
		]
	]
] );