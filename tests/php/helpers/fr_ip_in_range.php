<?php

Fr_Tests::do_unit_tests('fr_ip_in_range', [
    'IPv4 in range' => [
        'input' => ['192.168.1.5', '192.168.1.0/24'],
        'output' => true,
    ],
    'IPv4 not in range' => [
        'input' => ['192.168.2.5', '192.168.1.0/24'],
        'output' => false,
    ],
    'IPv4 exact match' => [
        'input' => ['192.168.1.1', '192.168.1.1'],
        'output' => true,
    ],
    'IPv4 with implicit /32' => [
        'input' => ['192.168.1.1', '192.168.1.1'],
        'output' => true,
    ],
    'IPv4 invalid IP' => [
        'input' => ['256.0.0.1', '192.168.1.0/24'],
        'output' => false,
    ],
    'IPv4 invalid range' => [
        'input' => ['192.168.1.1', '192.168.1.0/33'],
        'output' => false,
    ],
    'IPv6 in range' => [
        'input' => ['2001:db8::1', '2001:db8::/64'],
        'output' => true,
    ],
    'IPv6 not in range' => [
        'input' => ['2001:db9::1', '2001:db8::/64'],
        'output' => false,
    ],
    'IPv6 exact match' => [
        'input' => ['2001:db8::1', '2001:db8::1'],
        'output' => true,
    ],
    'IPv6 with implicit /128' => [
        'input' => ['2001:db8::1', '2001:db8::1'],
        'output' => true,
    ],
    'IPv6 invalid IP' => [
        'input' => ['2001:db8::g', '2001:db8::/64'],
        'output' => false,
    ],
    'IPv6 invalid range' => [
        'input' => ['2001:db8::1', '2001:db8::/129'],
        'output' => false,
    ],
    'Mixed IP versions' => [
        'input' => ['192.168.1.1', '2001:db8::/64'],
        'output' => false,
    ],
    'Google IP range' => [
        'input' => ['34.39.1.72', '34.32.0.0/11'],
        'output' => true,
    ],
]);