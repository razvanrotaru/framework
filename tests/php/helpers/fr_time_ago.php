<?php 

Fr_Tests::do_unit_tests('fr_time_ago', [
    'Default parameters, 1 day ago' => [
        'input' => ['2023-05-01 12:00:00'],
        'output' => 'yesterday',
        'dependencies' => [
            'fr_date_difference' => [
                [
                    'input' => ['2023-05-01 12:00:00', null],
                    'output' => ['days' => 1, 'hours' => 0, 'minutes' => 0, 'seconds' => 0]
                ]
            ]
        ]
    ],
    'Both dates provided, 2 hours ago' => [
        'input' => ['2023-05-01 10:00:00', '2023-05-01 12:00:00'],
        'output' => '2 hours ago',
        'dependencies' => [
            'fr_date_difference' => [
                [
                    'input' => ['2023-05-01 10:00:00', '2023-05-01 12:00:00'],
                    'output' => ['days' => 0, 'hours' => 2, 'minutes' => 0, 'seconds' => 0]
                ]
            ]
        ]
    ],
    'Custom strings, 3 minutes ago' => [
        'input' => [
            '2023-05-01 11:57:00',
            '2023-05-01 12:00:00',
            ['ago' => '%s in the past']
        ],
        'output' => '3 minutes in the past',
        'dependencies' => [
            'fr_date_difference' => [
                [
                    'input' => ['2023-05-01 11:57:00', '2023-05-01 12:00:00'],
                    'output' => ['days' => 0, 'hours' => 0, 'minutes' => 3, 'seconds' => 0]
                ]
            ]
        ]
    ],
    'Short names, 4 days ago' => [
        'input' => ['2023-04-27 12:00:00', '2023-05-01 12:00:00', [], true],
        'output' => '4d ago',
        'dependencies' => [
            'fr_date_difference' => [
                [
                    'input' => ['2023-04-27 12:00:00', '2023-05-01 12:00:00'],
                    'output' => ['days' => 4, 'hours' => 0, 'minutes' => 0, 'seconds' => 0]
                ]
            ]
        ]
    ],
    '5 seconds ago' => [
        'input' => ['2023-05-01 11:59:55', '2023-05-01 12:00:00'],
        'output' => '5 seconds ago',
        'dependencies' => [
            'fr_date_difference' => [
                [
                    'input' => ['2023-05-01 11:59:55', '2023-05-01 12:00:00'],
                    'output' => ['days' => 0, 'hours' => 0, 'minutes' => 0, 'seconds' => 5]
                ]
            ]
        ]
    ],
    'Just now' => [
        'input' => ['2023-05-01 12:00:00', '2023-05-01 12:00:00'],
        'output' => 'just now',
        'dependencies' => [
            'fr_date_difference' => [
                [
                    'input' => ['2023-05-01 12:00:00', '2023-05-01 12:00:00'],
                    'output' => ['days' => 0, 'hours' => 0, 'minutes' => 0, 'seconds' => 0]
                ]
            ]
        ]
    ],
    'Singular hour' => [
        'input' => ['2023-05-01 11:00:00', '2023-05-01 12:00:00'],
        'output' => '1 hour ago',
        'dependencies' => [
            'fr_date_difference' => [
                [
                    'input' => ['2023-05-01 11:00:00', '2023-05-01 12:00:00'],
                    'output' => ['days' => 0, 'hours' => 1, 'minutes' => 0, 'seconds' => 0]
                ]
            ]
        ]
    ],
    'Custom strings and short names' => [
        'input' => [
            '2023-04-30 12:00:00',
            '2023-05-01 12:00:00',
            ['day' => ['s' => 'D']],
            true
        ],
        'output' => '2D ago',
        'dependencies' => [
            'fr_date_difference' => [
                [
                    'input' => ['2023-04-30 12:00:00', '2023-05-01 12:00:00'],
                    'output' => ['days' => 2, 'hours' => 0, 'minutes' => 0, 'seconds' => 0]
                ]
            ]
        ]
    ],
    'Custom strings overriding default' => [
        'input' => [
            '2023-05-01 11:59:00',
            '2023-05-01 12:00:00',
            ['minute' => ['l' => ['singular' => 'minute', 'plural' => 'mins']]]
        ],
        'output' => '1 minute ago',
        'dependencies' => [
            'fr_date_difference' => [
                [
                    'input' => ['2023-05-01 11:59:00', '2023-05-01 12:00:00'],
                    'output' => ['days' => 0, 'hours' => 0, 'minutes' => 1, 'seconds' => 0]
                ]
            ]
        ]
    ],
]);