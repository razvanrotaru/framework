<?php 

Fr_Tests::do_unit_tests( 'fr_time_string_to_seconds', [
    'test days only' => [
        'input' => ['2d'],
        'output' => 172800,
    ],
    'test hours only' => [
        'input' => ['3h'],
        'output' => 10800,
    ],
    'test minutes only' => [
        'input' => ['45m'],
        'output' => 2700,
    ],
    'test seconds only' => [
        'input' => ['30s'],
        'output' => 30,
    ],
    'test days and hours' => [
        'input' => ['1d12h'],
        'output' => 129600,
    ],
    'test days, hours, and minutes' => [
        'input' => ['2d6h30m'],
        'output' => 196200,
    ],
    'test all units' => [
        'input' => ['1d5h30m15s'],
        'output' => 106215,
    ],
    'test zero values' => [
        'input' => ['0d0h0m0s'],
        'output' => 0,
    ],
    'test single-digit values' => [
        'input' => ['1d1h1m1s'],
        'output' => 90061,
    ],
    'test large numbers' => [
        'input' => ['100d24h60m60s'],
        'output' => 8730060,
    ],
    'test missing units' => [
        'input' => ['2d30m'],
        'output' => 174600,
    ],
    'test empty string' => [
        'input' => [''],
        'output' => 0,
    ],
    'test invalid input' => [
        'input' => ['invalid'],
        'output' => 0,
    ],
    'test float' => [
        'input' => ['1h1.5s'],
        'output' => 3601.5,
    ],
]);