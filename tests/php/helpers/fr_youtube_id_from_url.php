<?php

$video_id = 'dQw4w9WgXcQ';

Fr_Tests::do_unit_tests('fr_youtube_id_from_url', [
	'invalid_url' => [
		'input' => ['not a url'],
		'output' => ['', []]
	],
	'youtu_be_short_link' => [
		'input' => ['https://youtu.be/' . $video_id],
		'output' => [$video_id, []]
	],
	'youtube_com_embed_url' => [
		'input' => ['https://www.youtube.com/embed/' . $video_id],
		'output' => [$video_id, []]
	],
	'youtube_com_v_url' => [
		'input' => ['https://www.youtube.com/v/' . $video_id],
		'output' => [$video_id, []]
	],
	'youtube_com_watch_url' => [
		'input' => ['https://www.youtube.com/watch?v=' . $video_id . '&t=10s'],
		'output' => [$video_id, ['t' => '10s']]
	],
	'youtube_com_shorts_url' => [
		'input' => ['https://www.youtube.com/shorts/' . $video_id],
		'output' => [$video_id, []]
	],
	'non_youtube_url' => [
		'input' => ['https://example.com'],
		'output' => ['', []]
	],
	'youtube_com_url_without_video_id' => [
		'input' => ['https://www.youtube.com/'],
		'output' => ['', []]
	]
]);