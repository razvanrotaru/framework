<?php

Fr_Tests::do_unit_tests('fr_dom_custom->init', [
    'simple valid HTML' => [
        'input' => ['<div>Hello</div>'],
        'output' => [
            [
                'name' => 'div',
                'type' => 'start',
                'start' => 0,
                'end' => 5,
                'attr' => '',
                'start_tag' => false,
                'end_tag' => 2
            ],
            [
                'name' => 'Hello',
                'type' => 'text',
                'start' => 5,
                'end' => 5,
                'attr' => false,
            ],
            [
                'name' => 'div',
                'type' => 'end',
                'start' => 10,
                'end' => 6,
                'attr' => '',
                'start_tag' => 0,
                'end_tag' => false
            ]
        ],
        'dependencies' => [
            '$this->$$void_tags' => ['img' => 0, 'br' => 1, 'hr' => 2]
        ]
    ],
    'HTML with void tag' => [
        'input' => ['<p>Text<br>More text</p>'],
        'output' => [
            [
                'name' => 'p',
                'type' => 'start',
                'start' => 0,
                'end' => 3,
                'attr' => '',
                'start_tag' => false,
                'end_tag' => 4
            ],
            [
                'name' => 'Text',
                'type' => 'text',
                'start' => 3,
                'end' => 4,
                'attr' => false,
            ],
            [
                'name' => 'br',
                'type' => 'void',
                'start' => 7,
                'end' => 4,
                'attr' => '',
                'start_tag' => false,
                'end_tag' => false
            ],
            [
                'name' => 'More text',
                'type' => 'text',
                'start' => 11,
                'end' => 9,
                'attr' => false,
            ],
            [
                'name' => 'p',
                'type' => 'end',
                'start' => 20,
                'end' => 4,
                'attr' => '',
                'start_tag' => 0,
                'end_tag' => false
            ]
        ],
        'dependencies' => [
            '$this->$$void_tags' => ['img' => 0, 'br' => 1, 'hr' => 2]
        ]
    ],
    'Malformed HTML' => [
        'input' => ['<div>Unclosed tag'],
        'output' => [
            [
                'name' => 'div',
                'type' => 'start',
                'start' => 0,
                'end' => 5,
                'attr' => '',
                'start_tag' => false,
                'end_tag' => false
            ]
        ],
        'dependencies' => [
            '$this->$$void_tags' => ['img' => 0, 'br' => 1, 'hr' => 2]
        ]
    ],
    'Empty string' => [
        'input' => [''],
        'output' => [],
        'dependencies' => [
            '$this->$$void_tags' => ['img' => 0, 'br' => 1, 'hr' => 2]
        ]
    ],
    'Infinite loop test' => [
        'input' => ['<div><'],
        'output' => [
            [
                'name' => 'div',
                'type' => 'start',
                'start' => 0,
                'end' => 5,
                'attr' => '',
                'start_tag' => false,
                'end_tag' => false
            ]
        ],
        'dependencies' => [
            '$this->$$void_tags' => ['img' => 0, 'br' => 1, 'hr' => 2]
        ]
    ]
]);






