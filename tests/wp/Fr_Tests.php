<?php

// Test cases for Fr_Tests class



function unit_test_get_full_name( $user_id ) {

    $data = get_userdata( $user_id );

    return $data->first_name . ' ' . $data->last_name;

}

Fr_Tests::do_unit_tests( 'unit_test_get_full_name', [
    'test name' => [
        'input' => ['user_id' => 1],
        'output' => 'Alice Smith',
        'dependencies' => [
            'get_userdata' => [
                [
                    'input' => [1],
                    'output' => (object)[
                        'first_name' => 'Alice',
                        'last_name' => 'Smith',
                    ]
                ]
            ]
        ]
    ],
] );









class unit_test_user_manager {

    public function get_full_name( $user_id ) {

        $data = get_userdata( $user_id );

        return $data->first_name . ' ' . $data->last_name;

    }

}

Fr_Tests::do_unit_tests( 'unit_test_user_manager->get_full_name', [
    'test class method' => [
        'input' => [1],
        'output' => 'John Doe',
        'dependencies' => [
            'get_userdata' => [
                [
                    'input' => [1],
                    'output' => (object)[
                        'first_name' => 'John',
                        'last_name' => 'Doe',
                    ]
                ]
            ]
        ]
    ],
] );








class user_manager_with_method {

    public function get_full_name( $user_id ) {

        $data = $this->fetch_user_data( $user_id );

        return $data['first_name'] . ' ' . $data['last_name'];

    }

}

Fr_Tests::do_unit_tests( 'user_manager_with_method->get_full_name', [
    'test class method' => [
        'input' => [1],
        'output' => 'John Doe',
        'dependencies' => [
            '$this->fetch_user_data' => [
                [
                    'input' => [1],
                    'output' => [
                        'first_name' => 'John',
                        'last_name' => 'Doe',
                    ]
                ]
            ]
        ]
    ],
] );







class unit_test_static_user_manager {

    public static function get_full_name( $user_id ) {

        $data = self::fetch_user_data( $user_id );

        return $data['first_name'] . ' ' . $data['last_name'];

    }

    protected static function fetch_user_data( $user_id ) {

        return [];

    }

}

Fr_Tests::do_unit_tests( 'unit_test_static_user_manager::get_full_name', [
    'test static method' => [
        'input' => [1],
        'output' => 'Jane Doe',
        'dependencies' => [
            'self::fetch_user_data' => [
                [
                    'input' => [1],
                    'output' => [
                        'first_name' => 'Jane',
                        'last_name' => 'Doe',
                    ]
                ]
            ]
        ]
    ],
] );











class unit_test_global_user_manager {

    public function get_username( $user_id ) {

        $class = new unit_test_fetch_user_data();
        $data = $class->get( $user_id );

        return $data['username'];

    }

}

class unit_test_fetch_user_data {

    function get( $user_id ) {

        return ['username' => 'jane_doe'];

    }

}

Fr_Tests::do_unit_tests( 'unit_test_global_user_manager->get_username', [
    'test global instance method' => [
        'input' => [2],
        'output' => 'jane_doe',
        'dependencies' => [
            'unit_test_fetch_user_data->get' => [
                [
                    'input' => [2],
                    'output' => [
                        'username' => 'jane_doe'
                    ]
                ]
            ]
        ]
    ],
] );












class unit_test_global_user_manager_static {

    public function get_username( $user_id ) {

        return unit_test_fetch_user_data_static::get( $user_id )['username'];

    }

}

class unit_test_fetch_user_data_static {

    static function get( $user_id ) {

        return ['username' => 'jane_doe'];

    }

}

$user_manager = new unit_test_global_user_manager_static();

Fr_Tests::do_unit_tests( 'unit_test_global_user_manager_static->get_username', [
    'test global instance method' => [
        'input' => [2],
        'output' => 'jane_doe',
        'dependencies' => [
            'unit_test_fetch_user_data_static::get' => [
                [
                    'input' => [2],
                    'output' => [
                        'username' => 'jane_doe'
                    ]
                ]
            ]
        ]
    ],
] );









function unit_test_global_vars_and_classes( $post_id ) {

    global $wpdb;
    global $id;
    global $func;

    if( $id == 1 ){
        return false;
    }

    if( $func( $post_id ) == 1 ){
        return true;
    }

    $data = $wpdb->get_results( 'SELECT * FROM wp_posts WHERE ID = ' . $post_id );

    return $data[0]->post_title;

}

Fr_Tests::do_unit_tests( 'unit_test_global_vars_and_classes', [
    'global class dependency' => [
        'input' => [1],
        'output' => 'Alice Smith',
        'dependencies' => [
            '$$id' => 0,
            '$func' => [
                [
                    'input' => [1],
                    'output' => 1
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => [ 'SELECT * FROM wp_posts WHERE ID = 1' ],
                    'output' => [ 
                        (object)[
                            'post_title' => 'Alice Smith',
                        ]
                    ]
                ]
            ]
        ]
    ],
    'global function dependency' => [
        'input' => [1],
        'output' => true,
        'dependencies' => [
            '$func' => 1
        ]
    ],
    'global variable dependency' => [
        'input' => [1],
        'output' => false,
        'dependencies' => [
            '$$id' => 1
        ]
    ],
] );









function unit_test_classes_with_variables( $post_id ) {

    global $unit_test_class_name;

    $res = $post_id . $unit_test_class_name->var1 . $unit_test_class_name::$var2;

    return $res;

}

Fr_Tests::do_unit_tests( 'unit_test_classes_with_variables', [
    'class with variables' => [
        'input' => [1],
        'output' => '123',
        'dependencies' => [
            '$unit_test_class_name->$$var1' => 2,
            '$unit_test_class_name::$$var2' => 3,
        ]
    ]
] );
