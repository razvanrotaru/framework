<?php

Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with empty input' => [
        'input' => ['postmeta', 'post_id', []],
        'output' => [],
    ],
]);



Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with single ID (non-array input)' => [
        'input' => ['postmeta', 'post_id', 1, ['meta_key1']],
        'output' => [1 => ['meta_key1' => 'value1']],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => false
                ]
            ],
            '$wpdb->prepare' => [
                [
                    'input' => ['%s', 'meta_key1'],
                    'output' => "'meta_key1'"
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => ["SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1 ) AND meta_key IN ( 'meta_key1' )", 'ARRAY_A'],
                    'output' => [
                        ['post_id' => 1, 'meta_key' => 'meta_key1', 'meta_value' => 'value1']
                    ]
                ]
            ],
            'fr_cache' => [
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1 ) AND meta_key IN ( 'meta_key1' )", false, true]],
                    'output' => false
                ],
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1 ) AND meta_key IN ( 'meta_key1' )", false, true], null],
                    'output' => null
                ]
            ],
            '$wpdb->$$prefix' => 'wp_'
        ]
    ],
]);






Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with multiple IDs' => [
        'input' => ['postmeta', 'post_id', [1, 2, 3], ['meta_key1', 'meta_key2']],
        'output' => [
            1 => ['meta_key1' => 'value1_1', 'meta_key2' => 'value1_2'],
            2 => ['meta_key1' => 'value2_1', 'meta_key2' => 'value2_2'],
            3 => ['meta_key1' => 'value3_1', 'meta_key2' => 'value3_2']
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => false
                ],
                [
                    'input' => [2, 'post_meta'],
                    'output' => false
                ],
                [
                    'input' => [3, 'post_meta'],
                    'output' => false
                ]
            ],
            '$wpdb->prepare' => [
                [
                    'input' => ['%s', 'meta_key1'],
                    'output' => "'meta_key1'"
                ],
                [
                    'input' => ['%s', 'meta_key2'],
                    'output' => "'meta_key2'"
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => ["SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2,3 ) AND meta_key IN ( 'meta_key1','meta_key2' )", 'ARRAY_A'],
                    'output' => [
                        ['post_id' => 1, 'meta_key' => 'meta_key1', 'meta_value' => 'value1_1'],
                        ['post_id' => 1, 'meta_key' => 'meta_key2', 'meta_value' => 'value1_2'],
                        ['post_id' => 2, 'meta_key' => 'meta_key1', 'meta_value' => 'value2_1'],
                        ['post_id' => 2, 'meta_key' => 'meta_key2', 'meta_value' => 'value2_2'],
                        ['post_id' => 3, 'meta_key' => 'meta_key1', 'meta_value' => 'value3_1'],
                        ['post_id' => 3, 'meta_key' => 'meta_key2', 'meta_value' => 'value3_2']
                    ]
                ]
            ],
            'fr_cache' => [
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2,3 ) AND meta_key IN ( 'meta_key1','meta_key2' )", false, true]],
                    'output' => false
                ],
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2,3 ) AND meta_key IN ( 'meta_key1','meta_key2' )", false, true], null],
                    'output' => null
                ]
            ],
            '$wpdb->$$prefix' => 'wp_'
        ]
    ],
]);





Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with no meta keys specified' => [
        'input' => ['postmeta', 'post_id', [1, 2], []],
        'output' => [
            1 => ['meta_key1' => 'value1_1', 'meta_key2' => 'value1_2'],
            2 => ['meta_key1' => 'value2_1', 'meta_key3' => 'value2_3']
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => false
                ],
                [
                    'input' => [2, 'post_meta'],
                    'output' => false
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => ["SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2 )", 'ARRAY_A'],
                    'output' => [
                        ['post_id' => 1, 'meta_key' => 'meta_key1', 'meta_value' => 'value1_1'],
                        ['post_id' => 1, 'meta_key' => 'meta_key2', 'meta_value' => 'value1_2'],
                        ['post_id' => 2, 'meta_key' => 'meta_key1', 'meta_value' => 'value2_1'],
                        ['post_id' => 2, 'meta_key' => 'meta_key3', 'meta_value' => 'value2_3']
                    ]
                ]
            ],
            'fr_cache' => [
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2 )", false, true]],
                    'output' => false
                ]
            ],
            '$wpdb->$$prefix' => 'wp_'
        ]
    ],
]);








Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with return_only_first_key set to true' => [
        'input' => ['postmeta', 'post_id', [1, 2], ['meta_key1', 'meta_key2'], true],
        'output' => [
            1 => 'value1_1',
            2 => 'value2_1'
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => false
                ],
                [
                    'input' => [2, 'post_meta'],
                    'output' => false
                ]
            ],
            '$wpdb->prepare' => [
                [
                    'input' => ['%s', 'meta_key1'],
                    'output' => "'meta_key1'"
                ],
                [
                    'input' => ['%s', 'meta_key2'],
                    'output' => "'meta_key2'"
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => ["SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2 ) AND meta_key IN ( 'meta_key1','meta_key2' )", 'ARRAY_A'],
                    'output' => [
                        ['post_id' => 1, 'meta_key' => 'meta_key1', 'meta_value' => 'value1_1'],
                        ['post_id' => 1, 'meta_key' => 'meta_key2', 'meta_value' => 'value1_2'],
                        ['post_id' => 2, 'meta_key' => 'meta_key1', 'meta_value' => 'value2_1'],
                        ['post_id' => 2, 'meta_key' => 'meta_key2', 'meta_value' => 'value2_2']
                    ]
                ]
            ],
            'fr_cache' => [
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2 ) AND meta_key IN ( 'meta_key1','meta_key2' )", true, true]],
                    'output' => false
                ]
            ],
            '$wpdb->$$prefix' => 'wp_'
        ]
    ],
]);










Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with add_default set to false' => [
        'input' => ['postmeta', 'post_id', [1, 2], ['meta_key1', 'meta_key2'], false, false],
        'output' => [
            1 => ['meta_key1' => 'value1_1', 'meta_key2' => 'value1_2'],
            2 => ['meta_key1' => 'value2_1']
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => false
                ],
                [
                    'input' => [2, 'post_meta'],
                    'output' => false
                ]
            ],
            '$wpdb->prepare' => [
                [
                    'input' => ['%s', 'meta_key1'],
                    'output' => "'meta_key1'"
                ],
                [
                    'input' => ['%s', 'meta_key2'],
                    'output' => "'meta_key2'"
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => ["SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2 ) AND meta_key IN ( 'meta_key1','meta_key2' )", 'ARRAY_A'],
                    'output' => [
                        ['post_id' => 1, 'meta_key' => 'meta_key1', 'meta_value' => 'value1_1'],
                        ['post_id' => 1, 'meta_key' => 'meta_key2', 'meta_value' => 'value1_2'],
                        ['post_id' => 2, 'meta_key' => 'meta_key1', 'meta_value' => 'value2_1']
                    ]
                ]
            ],
            'fr_cache' => [
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2 ) AND meta_key IN ( 'meta_key1','meta_key2' )", false, false]],
                    'output' => false
                ]
            ],
            '$wpdb->$$prefix' => 'wp_'
        ]
    ],
]);









Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with cache set to false' => [
        'input' => ['postmeta', 'post_id', [1, 2], ['meta_key1'], false, true, false],
        'output' => [
            1 => ['meta_key1' => 'value1_1'],
            2 => ['meta_key1' => 'value2_1']
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => [
                        'meta_key1' => ['cached_value1_1'],
                        'meta_key2' => ['cached_value1_2']
                    ]
                ],
                [
                    'input' => [2, 'post_meta'],
                    'output' => [
                        'meta_key1' => ['cached_value2_1'],
                        'meta_key2' => ['cached_value2_2']
                    ]
                ]
            ],
            '$wpdb->prepare' => [
                [
                    'input' => ['%s', 'meta_key1'],
                    'output' => "'meta_key1'"
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => ["SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2 ) AND meta_key IN ( 'meta_key1' )", 'ARRAY_A'],
                    'output' => [
                        ['post_id' => 1, 'meta_key' => 'meta_key1', 'meta_value' => 'value1_1'],
                        ['post_id' => 2, 'meta_key' => 'meta_key1', 'meta_value' => 'value2_1']
                    ]
                ]
            ],
            '$wpdb->$$prefix' => 'wp_'
        ]
    ],
]);









Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with cached data' => [
        'input' => ['postmeta', 'post_id', [1, 2], ['meta_key1', 'meta_key2']],
        'output' => [
            1 => ['meta_key1' => 'cached_value1_1', 'meta_key2' => 'cached_value1_2'],
            2 => ['meta_key1' => 'cached_value2_1', 'meta_key2' => 'cached_value2_2']
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => [
                        'meta_key1' => ['cached_value1_1'],
                        'meta_key2' => ['cached_value1_2']
                    ]
                ],
                [
                    'input' => [2, 'post_meta'],
                    'output' => [
                        'meta_key1' => ['cached_value2_1'],
                        'meta_key2' => ['cached_value2_2']
                    ]
                ]
            ]
        ]
    ],
]);









Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with cached data and no meta keys defined' => [
        'input' => ['postmeta', 'post_id', [1, 2], []],
        'output' => [
            1 => ['meta_key1' => 'cached_value1_1', 'meta_key2' => 'cached_value1_2'],
            2 => ['meta_key1' => 'cached_value2_1', 'meta_key2' => 'cached_value2_2']
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => [
                        'meta_key1' => ['cached_value1_1'],
                        'meta_key2' => ['cached_value1_2']
                    ]
                ],
                [
                    'input' => [2, 'post_meta'],
                    'output' => [
                        'meta_key1' => ['cached_value2_1'],
                        'meta_key2' => ['cached_value2_2']
                    ]
                ]
            ]
        ]
    ],
]);








Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with serialized meta values' => [
        'input' => ['postmeta', 'post_id', [1], ['serialized_key']],
        'output' => [
            1 => ['serialized_key' => ['key1' => 'value1', 'key2' => 'value2']]
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => false
                ]
            ],
            '$wpdb->prepare' => [
                [
                    'input' => ['%s', 'serialized_key'],
                    'output' => "'serialized_key'"
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => ["SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1 ) AND meta_key IN ( 'serialized_key' )", 'ARRAY_A'],
                    'output' => [
                        ['post_id' => 1, 'meta_key' => 'serialized_key', 'meta_value' => 'a:2:{s:4:"key1";s:6:"value1";s:4:"key2";s:6:"value2";}']
                    ]
                ]
            ],
            'fr_cache' => [
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1 ) AND meta_key IN ( 'serialized_key' )", false, true]],
                    'output' => false
                ]
            ],
            '$wpdb->$$prefix' => 'wp_'
        ]
    ],
]);








Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with non-existent meta keys' => [
        'input' => ['postmeta', 'post_id', [1, 2], ['non_existent_key']],
        'output' => [
            1 => ['non_existent_key' => ''],
            2 => ['non_existent_key' => '']
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => false
                ],
                [
                    'input' => [2, 'post_meta'],
                    'output' => false
                ]
            ],
            '$wpdb->prepare' => [
                [
                    'input' => ['%s', 'non_existent_key'],
                    'output' => "'non_existent_key'"
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => ["SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2 ) AND meta_key IN ( 'non_existent_key' )", 'ARRAY_A'],
                    'output' => []
                ]
            ],
            'fr_cache' => [
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2 ) AND meta_key IN ( 'non_existent_key' )", false, true]],
                    'output' => false
                ]
            ],
            '$wpdb->$$prefix' => 'wp_'
        ]
    ],
]);










Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with partially cached data' => [
        'input' => ['postmeta', 'post_id', [1, 2, 3], ['meta_key1', 'meta_key2']],
        'output' => [
            1 => ['meta_key1' => 'cached_value1_1', 'meta_key2' => 'cached_value1_2'],
            2 => ['meta_key1' => 'value2_1', 'meta_key2' => 'value2_2'],
            3 => ['meta_key1' => 'value3_1', 'meta_key2' => 'value3_2']
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => [
                        'meta_key1' => ['cached_value1_1'],
                        'meta_key2' => ['cached_value1_2']
                    ]
                ],
                [
                    'input' => [2, 'post_meta'],
                    'output' => false
                ],
                [
                    'input' => [3, 'post_meta'],
                    'output' => false
                ]
            ],
            '$wpdb->prepare' => [
                [
                    'input' => ['%s', 'meta_key1'],
                    'output' => "'meta_key1'"
                ],
                [
                    'input' => ['%s', 'meta_key2'],
                    'output' => "'meta_key2'"
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => ["SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 2,3 ) AND meta_key IN ( 'meta_key1','meta_key2' )", 'ARRAY_A'],
                    'output' => [
                        ['post_id' => 2, 'meta_key' => 'meta_key1', 'meta_value' => 'value2_1'],
                        ['post_id' => 2, 'meta_key' => 'meta_key2', 'meta_value' => 'value2_2'],
                        ['post_id' => 3, 'meta_key' => 'meta_key1', 'meta_value' => 'value3_1'],
                        ['post_id' => 3, 'meta_key' => 'meta_key2', 'meta_value' => 'value3_2']
                    ]
                ]
            ],
            'fr_cache' => [
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 2,3 ) AND meta_key IN ( 'meta_key1','meta_key2' )", false, true]],
                    'output' => false
                ]
            ],
            '$wpdb->$$prefix' => 'wp_'
        ]
    ],
]);







Fr_Tests::do_unit_tests('fr_query_get_meta', [
    'test with duplicate and non-integer IDs' => [
        'input' => ['postmeta', 'post_id', [1, '1', 2, '2', 3.5, 3.5], ['meta_key1']],
        'output' => [
            1 => ['meta_key1' => 'value1'],
            2 => ['meta_key1' => 'value2'],
            3 => ['meta_key1' => 'value3']
        ],
        'dependencies' => [
            'wp_cache_get' => [
                [
                    'input' => [1, 'post_meta'],
                    'output' => false
                ],
                [
                    'input' => [2, 'post_meta'],
                    'output' => false
                ],
                [
                    'input' => [3, 'post_meta'],
                    'output' => false
                ]
            ],
            '$wpdb->prepare' => [
                [
                    'input' => ['%s', 'meta_key1'],
                    'output' => "'meta_key1'"
                ]
            ],
            '$wpdb->get_results' => [
                [
                    'input' => ["SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2,3 ) AND meta_key IN ( 'meta_key1' )", 'ARRAY_A'],
                    'output' => [
                        ['post_id' => 1, 'meta_key' => 'meta_key1', 'meta_value' => 'value1'],
                        ['post_id' => 2, 'meta_key' => 'meta_key1', 'meta_value' => 'value2'],
                        ['post_id' => 3, 'meta_key' => 'meta_key1', 'meta_value' => 'value3']
                    ]
                ]
            ],
            'fr_cache' => [
                [
                    'input' => [['fr_query_get_postmeta_meta', "SELECT post_id, meta_key, meta_value FROM wp_postmeta WHERE post_id IN ( 1,2,3 ) AND meta_key IN ( 'meta_key1' )", false, true]],
                    'output' => false
                ]
            ],
            '$wpdb->$$prefix' => 'wp_'
        ]
    ],
]);






