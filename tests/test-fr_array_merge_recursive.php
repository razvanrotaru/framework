<?php
fr_unit_test( 'fr_array_merge_recursive', [
	'args' => [
		[
			'string1',
		],
		[
			'string2'
		]
	],

	'tests' => [
		[ 
			[
				'string1',
				'string2'
			], 
			'=' 
		]
	]
] );

fr_unit_test( 'fr_array_merge_recursive', [
	'args' => [
		[
			'string1',
			'array1' => [
			]
		],
		[
			'string2'
		]
	],

	'tests' => [
		[ 
			[
				'string1',
				'array1' => [
				],
				'string2'
			], 
			'=' 
		]
	]
] );

fr_unit_test( 'fr_array_merge_recursive', [
	'args' => [
		[
			'string1',
			'array1' => [
			]
		],
		[
			'string2',
			'array2' => [
			]
		]
	],

	'tests' => [
		[ 
			[
				'string1',
				'array1' => [
				],
				'array2' => [
				],
				'string2',
			], 
			'=' 
		]
	]
] );



fr_unit_test( 'fr_array_merge_recursive', [
	'args' => [
		[
			'string1',
			'array1' => [
				'item1',
				'array1' => [
				]
			]
		],
		[
			'string2',
			'array2' => [
			],
			'array1' => [
				'item2',
				'array1' => [
					'value'
				]
			]
		]
	],

	'tests' => [
		[ 
			[
				'string1',
				'array1' => [
					'item1',
					'array1' => [
						'value'
					],
					'item2'
				],
				'array2' => [
				],
				'string2',
			], 
			'=' 
		]
	]
] );