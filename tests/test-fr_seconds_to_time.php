<?php
fr_test('test hours',function(){
	$response = fr_seconds_to_time( 91820 );
	return array( '25:30:20', '=', $response );
});

fr_test('test minutes',function(){
	$response = fr_seconds_to_time( 1820 );
	return array( '30:20', '=', $response );
});

fr_test('test seconds',function(){
	$response = fr_seconds_to_time( 20 );
	return array( '00:20', '=', $response );
});