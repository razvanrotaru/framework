<?php

fr_test('dont increment',function(){
	$response = fr_unique_name('name','',function(){
		return false;
	});
	return array( 'name', '=', $response );
});

fr_test('increment',function(){
	$response = fr_unique_name('name', '',function( $string ){
		if( $string == 'name' ){
			return true;
		}
	});
	return array( 'name1', '=', $response );
});

fr_test('increment2',function(){
	$response = fr_unique_name('name1','',function( $string ){
		if( $string == 'name1' ){
			return true;
		}
	});
	return array( 'name2', '=', $response );
});

fr_test('increment10',function(){
	$response = fr_unique_name('name9','',function( $string ){
		if( $string == 'name9' ){
			return true;
		}
	});
	return array( 'name10', '=', $response );
});

fr_test('increment-',function(){
	$response = fr_unique_name('name', '-',function( $string ){
		if( $string == 'name' ){
			return true;
		}
	});
	return array( 'name-1', '=', $response );
});

fr_test('increment2-',function(){
	$response = fr_unique_name('name1','-',function( $string ){
		if( $string == 'name1' ){
			return true;
		}
	});
	return array( 'name-2', '=', $response );
});

fr_test('increment3-',function(){
	$response = fr_unique_name('name-1','-',function( $string ){
		if( $string == 'name-1' ){
			return true;
		}
	});
	return array( 'name-2', '=', $response );
});
