<?php

fr_unit_test( 'fr_do_conditional_shortcodes_array', [
	'args' => [
		'[if ok]1[/if ok]', [
			'ok' => 1
		]
	],

	'tests' => [
		[ 
			'1',
			'=' 
		]
	]
] );


fr_unit_test( 'fr_do_conditional_shortcodes_array', [
	'args' => [
		'text[if ok]1[/if ok]text', [
			'ok' => 1
		]
	],

	'tests' => [
		[ 
			'text1text',
			'=' 
		]
	]
] );


fr_unit_test( 'fr_do_conditional_shortcodes_array', [
	'args' => [
		'text[if ok]1[/if ok]text', [
			'ok' => 0
		]
	],

	'tests' => [
		[ 
			'texttext',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_do_conditional_shortcodes_array', [
	'args' => [
		'text[if ok]1[/if ok]text[if ok2]1[/if ok2]text', [
			'ok' => 1,
			'ok2' => 1
		]
	],

	'tests' => [
		[ 
			'text1text1text',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_do_conditional_shortcodes_array', [
	'args' => [
		'text[if ok]1[/if ok][if ok2]1[/if ok2]text', [
			'ok' => 1,
			'ok2' => 0
		]
	],

	'tests' => [
		[ 
			'text1text',
			'=' 
		]
	]
] );