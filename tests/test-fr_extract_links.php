<?php 

Fr_Tests::do_unit_tests( 'fr_extract_links', [
    [
        'input' => [ 'text' => 'Check out this website: https://example.com' ],
        'output' => ['https://example.com'],
    ],
    [
        'input' => [ 'text' => 'Visit https://example.com and https://example.org for more info.' ],
        'output' => ['https://example.com', 'https://example.org'],
    ],
    [
        'input' => [ 'text' => 'This is a link: https://example.com.' ],
        'output' => ['https://example.com'],
    ],
    [
        'input' => [ 'text' => 'Links: https://example.com, https://example.org.' ],
        'output' => ['https://example.com', 'https://example.org'],
    ],
    [
        'input' => [ 'text' => 'Find more at https://example.com!' ],
        'output' => ['https://example.com'],
    ],
    [
        'input' => [ 'text' => 'I found something interesting at https://example.com today.' ],
        'output' => ['https://example.com'],
    ],
    [
        'input' => [ 'text' => 'There are no links here.' ],
        'output' => [],
    ],
    [
        'input' => [ 'text' => 'Visit https://example.com?param=value&another=param' ],
        'output' => ['https://example.com?param=value&another=param'],
    ],
    [
        'input' => [ 'text' => 'Subdomains are cool: https://sub.example.com' ],
        'output' => ['https://sub.example.com'],
    ],
    [
        'input' => [ 'text' => 'Different protocols: http://example.com and https://secure.example.com' ],
        'output' => ['http://example.com', 'https://secure.example.com'],
    ],
    [
        'input' => [ 'text' => 'Unusual TLD: https://example.xyz' ],
        'output' => ['https://example.xyz'],
    ],
    [
        'input' => [ 'text' => 'URL with query: https://example.com?query=string' ],
        'output' => ['https://example.com?query=string'],
    ],
    [
        'input' => [ 'text' => 'URL with anchor: https://example.com#anchor' ],
        'output' => ['https://example.com#anchor'],
    ],
    [
        'input' => [ 'text' => 'Duplicate URL https://example.com https://example.com' ],
        'output' => ['https://example.com'],
    ],
    [
        'input' => [ 'text' => 'Long URL: https://example.com/very/long/path/with/many/segments' ],
        'output' => ['https://example.com/very/long/path/with/many/segments'],
    ],
    [
        'input' => [ 'text' => 'Separated URLs: https://example.com https://example.org' ],
        'output' => ['https://example.com', 'https://example.org'],
    ],
    [
        'input' => [ 'text' => 'URL ending in slash: https://example.com/' ],
        'output' => ['https://example.com/'],
    ],
    // Test with URLs embedded in HTML or Markdown syntax
    [
        'input' => [ 'text' => 'Check this <a href="https://example.com">link</a>' ],
        'output' => ['https://example.com'],
    ],
    // Test with a URL with multiple query parameters and anchors
    [
        'input' => [ 'text' => 'https://example.com?param1=value1&param2=value2#anchor' ],
        'output' => ['https://example.com?param1=value1&param2=value2#anchor'],
    ],
    // Test with URLs in parentheses or brackets
    [
        'input' => [ 'text' => 'Visit (https://example.com)' ],
        'output' => ['https://example.com'],
    ],
    // Test with a URL having encoded characters
    [
        'input' => [ 'text' => 'https://example.com/path%20with%20encoded%20characters' ],
        'output' => ['https://example.com/path%20with%20encoded%20characters'],
    ],
    // Test with URLs followed by various punctuation marks
    [
        'input' => [ 'text' => 'Visit https://example.com, and https://example.org;' ],
        'output' => ['https://example.com', 'https://example.org'],
    ],
    // Test with text containing JavaScript code snippets that include URLs
    [
        'input' => [ 'text' => 'var url = "https://example.com";' ],
        'output' => ['https://example.com'],
    ],
    // Test with multiple different URLs in a single sentence
    [
        'input' => [ 'text' => 'Visit https://example.com and https://example.org today.' ],
        'output' => ['https://example.com', 'https://example.org'],
    ],
    // Test with URLs that have trailing spaces
    [
        'input' => [ 'text' => 'https://example.com ' ],
        'output' => ['https://example.com'],
    ],
    // Test with a URL with uppercase letters in the domain or path
    [
        'input' => [ 'text' => 'Visit https://EXAMPLE.com/Path' ],
        'output' => ['https://EXAMPLE.com/Path'],
    ],
    // Test with a URL inside single or double quotes
    [
        'input' => [ 'text' => '"https://example.com"' ],
        'output' => ['https://example.com'],
    ],
    // Test with a text containing email addresses
    [
        'input' => [ 'text' => 'Email me at email@example.com or visit https://example.com' ],
        'output' => ['https://example.com'],
    ],
    // Test with a URL that includes a fragment identifier and query string together
    [
        'input' => [ 'text' => 'https://example.com?query=string#fragment' ],
        'output' => ['https://example.com?query=string#fragment'],
    ],
    // Test with URLs containing a variety of uncommon TLDs
    [
        'input' => [ 'text' => 'Visit https://example.site and https://example.tech' ],
        'output' => ['https://example.site', 'https://example.tech'],
    ],
    // Test with a URL in a string that also contains escape characters
    [
        'input' => [ 'text' => "Check this \nhttps://example.com" ],
        'output' => ['https://example.com'],
    ],
        // Test with URLs where the domain and subdomains have numeric parts
    [
        'input' => [ 'text' => 'Visit https://123.example.com and https://subdomain2.example.com' ],
        'output' => ['https://123.example.com', 'https://subdomain2.example.com'],
    ],
    // Test with URLs that have mixed http and https protocols in the same string
    [
        'input' => [ 'text' => 'Mixed protocols: http://example.com and https://secure.example.com' ],
        'output' => ['http://example.com', 'https://secure.example.com'],
    ],
    // Test with a URL that includes a dash or underscore in the domain name
    [
        'input' => [ 'text' => 'Visit https://example-domain.com and https://example_domain.com' ],
        'output' => ['https://example-domain.com', 'https://example_domain.com'],
    ],
    // Test with a URL with a long query string that includes array parameters
    // [
    //     'input' => [ 'text' => 'https://example.com/path?array[]=value1&array[]=value2' ],
    //     'output' => ['https://example.com/path?array[]=value1&array[]=value2'],
    // ],
    // Test with URLs within multi-line text or paragraphs
    [
        'input' => [ 'text' => "Here's a link:\nhttps://example.com\nin a paragraph." ],
        'output' => ['https://example.com'],
    ],
    // Test with URLs that have a mix of encoded and non-encoded characters
    [
        'input' => [ 'text' => 'https://example.com/path%20with%20spaces and non-encoded characters' ],
        'output' => ['https://example.com/path%20with%20spaces'],
    ],
    // Test with a URL with various subdomain levels
    [
        'input' => [ 'text' => 'Visit https://sub.subdomain.example.com' ],
        'output' => ['https://sub.subdomain.example.com'],
    ],
] );