<?php

fr_unit_test( 'fr_html_wp_attr_to_array', 
	[
		'test_name' => 'Various attributes',
		'args' => [
			'a="1" b=\'1"\' c="שמי אביתר" d ="1\'" e= "1" f = "1" g="" h=1	i 	j"1"' . "\r\n" . 'h',
		],
		'tests' => [
			[ 
				[ 
					'a' => '1',
					'b' => '1"',
					'c' => 'שמי אביתר',
					'd' => '1\'',
					'e' => '1',
					'f' => '1',
					'g' => '',
					'h' => '1',
					'i' => '',
					'j' => '1',
					'h' => ''
				], 
				'=' 
			]
		]
	]
);