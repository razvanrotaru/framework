<?php
fr_unit_test( 'fr_strip_html_tags_and_content', [
	'args' => [
		'a'
	],

	'tests' => [
		[ 
			'a', 
			'=' 
		]
	]
] );


fr_unit_test( 'fr_strip_html_tags_and_content', [
	'args' => [
		'<p>a</p>b'
	],

	'tests' => [
		[ 
			'b', 
			'=' 
		]
	]
] );


fr_unit_test( 'fr_strip_html_tags_and_content', [
	'args' => [
		'<p>a</p><a>b</a>c',
		[ 'a' ]
	],

	'tests' => [
		[ 
			'<p>a</p>c', 
			'=' 
		]
	]
] );


fr_unit_test( 'fr_strip_html_tags_and_content', [
	'args' => [
		'<p>a</p><b>b</a>c',
		[ 'b' ]
	],

	'tests' => [
		[ 
			'<p>a</p>', 
			'=' 
		]
	]
] );


fr_unit_test( 'fr_strip_html_tags_and_content', [
	'args' => [
		'<p>a</p><img />b</a>c',
		[ 'img' ]
	],

	'tests' => [
		[
			'<p>a</p>b</a>c', 
			'=' 
		]
	]
] );


fr_unit_test( 'fr_strip_html_tags_and_content', [
	'args' => [
		'<p>a</p><img />b</a>c',
		[ 'a' ]
	],

	'tests' => [
		[
			'<p>a</p><img />bc', 
			'=' 
		]
	]
] );


fr_unit_test( 'fr_strip_html_tags_and_content', [
	'args' => [
		'<ul class="sub-menu">
			<li id="menu-item-9244" class="icon-child-before phone-icon-child-before menu-item menu-item-type-custom menu-item-object-custom menu-item-9244"><a href="test">test</a></li>
			<li id="menu-item-9243" class="icon-child-before envelope-icon-child-before menu-item menu-item-type-custom menu-item-object-custom menu-item-9243"><a href="test">test</a></li>
		</ul>',
		[ 'a' ]
	],

	'tests' => [
		[
		'<ul class="sub-menu">
			<li id="menu-item-9244" class="icon-child-before phone-icon-child-before menu-item menu-item-type-custom menu-item-object-custom menu-item-9244"></li>
			<li id="menu-item-9243" class="icon-child-before envelope-icon-child-before menu-item menu-item-type-custom menu-item-object-custom menu-item-9243"></li>
		</ul>',
			'=' 
		]
	]
] );