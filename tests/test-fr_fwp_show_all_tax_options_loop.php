<?php
$res = fr_unit_test( 'fr_fwp_show_all_tax_options_loop', [
	'args' => [ '', [
		'facet' => [
			'name' => 'Facet name',
			'source' => 'tax/genre',
		],
		'values' => [
			[
				'term_id' => 1,
				'counter' => 10
			],
			[
				'term_id' => 2,
				'counter' => 20
			]
		]
	] ],

	'tests' => [
		[ 'Gentle', 'strstr' ],
		[ 'data-parent="gentle"', 'strstr' ],
		[ 'facetwp-counter">(20)', 'strstr' ]
	],

	'dependencies' => [
		'fr_fwp_search_parameters' => [
			'genre' => [
				'gentle',
				'lively'
			]
		],
		'fr_fwp_get_facet_settings' => [
			'soft_limit' => true
		],
		'get_terms' => function(){ 
			return fr_array_to_object( [
				[
					'term_id' => 1,
					'name' => 'Gentle',
					'slug' => 'gentle',
					'parent' => 0,
				],
				[
					'term_id' => 2,
					'name' => 'Lively',
					'slug' => 'lively',
					'parent' => 1
				]
			] );
		},
		'get_term_link' => 'http://site.com'
	]
] );