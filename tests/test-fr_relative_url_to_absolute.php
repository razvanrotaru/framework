<?php
fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/page2/index.html?param=1', 
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'#test',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/page2/index.html?param=1#test',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'?args#hash2',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/page2/index.html?args#hash2',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'/',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'/page3/',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page3/', 
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'page4',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/page2/page4',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'.',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/page2/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'.',
		'https://www.site.com/page/page2/?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/page2/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'.',
		'https://www.site.com/page/page2?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'./',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/page2/',
			'=' 
		]
	]
] );


fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'././',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/page2/',
			'=' 
		]
	]
] );


fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'./../',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/',
			'=' 
		]
	]
] );


fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'.././',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'..',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'..',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'../',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'../../',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/',
			'=' 
		]
	]
] );


fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'../../page3/',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page3/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'./page4/',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/page2/page4/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'../page4/',
		'https://www.site.com/page/page2/?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page/page4/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'../../page4/',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page4/',
			'=' 
		]
	]
] );


fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'//www.site.com/',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'http://www.site.com/page4/',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'http://www.site.com/page4/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'https://www.site.com/page4/',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://www.site.com/page4/',
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'https://google.com',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://google.com', 
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'//google.com',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://google.com', 
			'=' 
		]
	]
] );

fr_unit_test( 'fr_relative_url_to_absolute', [
	'args' => [
		'//google.com?param=1#hash',
		'https://www.site.com/page/page2/index.html?param=1#hash'
	],

	'tests' => [
		[ 
			'https://google.com?param=1#hash', 
			'=' 
		]
	]
] );