<?php
fr_unit_test( 'fr_parse_default_html_attr', [
	'args' => [
		'class="new"',
		'class="default" data'
	],

	'tests' => [
		[ 
			[
				'class' => [
					'default',
					'new'
				],
				'data' => ''
			], 
			'=' 
		]
	]
] );



fr_unit_test( 'fr_parse_default_html_attr', [
	'args' => [
		[ 'class' => 'new', 'width' => 100 ],
		'class="default" data'
	],

	'tests' => [
		[ 
			[
				'class' => [
					'default',
					'new'
				],
				'data' => '',
				'width' => 100
			], 
			'=' 
		]
	]
] );